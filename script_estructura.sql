USE [master]
GO
/****** Object:  Database [PRU_GdOrfeo]    Script Date: 5/09/2019 2:12:30 p. m. ******/
CREATE DATABASE [PRU_GdOrfeo]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'GdOrfeo', FILENAME = N'E:\MSSQLSERVER\PRU_GdOrfeo.mdf' , SIZE = 1472512KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'GdOrfeo_log', FILENAME = N'H:\MSSQLSERVER\PRU_GdOrfeo_log.ldf' , SIZE = 5605504KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [PRU_GdOrfeo] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PRU_GdOrfeo].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PRU_GdOrfeo] SET ANSI_NULL_DEFAULT ON 
GO
ALTER DATABASE [PRU_GdOrfeo] SET ANSI_NULLS ON 
GO
ALTER DATABASE [PRU_GdOrfeo] SET ANSI_PADDING ON 
GO
ALTER DATABASE [PRU_GdOrfeo] SET ANSI_WARNINGS ON 
GO
ALTER DATABASE [PRU_GdOrfeo] SET ARITHABORT ON 
GO
ALTER DATABASE [PRU_GdOrfeo] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PRU_GdOrfeo] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PRU_GdOrfeo] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PRU_GdOrfeo] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PRU_GdOrfeo] SET CURSOR_DEFAULT  LOCAL 
GO
ALTER DATABASE [PRU_GdOrfeo] SET CONCAT_NULL_YIELDS_NULL ON 
GO
ALTER DATABASE [PRU_GdOrfeo] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PRU_GdOrfeo] SET QUOTED_IDENTIFIER ON 
GO
ALTER DATABASE [PRU_GdOrfeo] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PRU_GdOrfeo] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PRU_GdOrfeo] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PRU_GdOrfeo] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PRU_GdOrfeo] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PRU_GdOrfeo] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PRU_GdOrfeo] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PRU_GdOrfeo] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PRU_GdOrfeo] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PRU_GdOrfeo] SET RECOVERY FULL 
GO
ALTER DATABASE [PRU_GdOrfeo] SET  MULTI_USER 
GO
ALTER DATABASE [PRU_GdOrfeo] SET PAGE_VERIFY NONE  
GO
ALTER DATABASE [PRU_GdOrfeo] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PRU_GdOrfeo] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PRU_GdOrfeo] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [PRU_GdOrfeo] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'PRU_GdOrfeo', N'ON'
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  User [usrbdorfeo]    Script Date: 5/09/2019 2:12:30 p. m. ******/
CREATE USER [usrbdorfeo] FOR LOGIN [usrbdorfeo] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [Des_jrueda]    Script Date: 5/09/2019 2:12:30 p. m. ******/
CREATE USER [Des_jrueda] FOR LOGIN [Des_jrueda] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [Des_flosada]    Script Date: 5/09/2019 2:12:30 p. m. ******/
CREATE USER [Des_flosada] FOR LOGIN [Des_flosada] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [Des_cgonzalez]    Script Date: 5/09/2019 2:12:30 p. m. ******/
CREATE USER [Des_cgonzalez] FOR LOGIN [Des_cgonzalez] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [Des_cbarrero]    Script Date: 5/09/2019 2:12:30 p. m. ******/
CREATE USER [Des_cbarrero] FOR LOGIN [Des_cbarrero] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [ANTCO\user_backup]    Script Date: 5/09/2019 2:12:30 p. m. ******/
CREATE USER [ANTCO\user_backup] FOR LOGIN [ANTCO\user_backup] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [ANTCO\jose.rueda]    Script Date: 5/09/2019 2:12:30 p. m. ******/
CREATE USER [ANTCO\jose.rueda] FOR LOGIN [ANTCO\jose.rueda] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [ANTCO\fabian.losada]    Script Date: 5/09/2019 2:12:30 p. m. ******/
CREATE USER [ANTCO\fabian.losada] FOR LOGIN [ANTCO\fabian.losada] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [usrbdorfeo]
GO
ALTER ROLE [db_owner] ADD MEMBER [Des_jrueda]
GO
ALTER ROLE [db_owner] ADD MEMBER [Des_flosada]
GO
ALTER ROLE [db_owner] ADD MEMBER [Des_cgonzalez]
GO
ALTER ROLE [db_owner] ADD MEMBER [Des_cbarrero]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [ANTCO\user_backup]
GO
ALTER ROLE [db_owner] ADD MEMBER [ANTCO\jose.rueda]
GO
ALTER ROLE [db_owner] ADD MEMBER [ANTCO\fabian.losada]
GO
/****** Object:  FullTextCatalog [idx_rad]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE FULLTEXT CATALOG [idx_rad] WITH ACCENT_SENSITIVITY = ON
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[dbo.sec_hist_eventos]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[dbo.sec_hist_eventos] 
 AS [smallint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -32768
 MAXVALUE 32767
 CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[id_user_security]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[id_user_security] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 NO CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[PLA_CODIGO_SEC]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[PLA_CODIGO_SEC] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[rad_datatemporal_rad_data_id_seq]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[rad_datatemporal_rad_data_id_seq] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 NO CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[scan_conf_owncloud_own_id_seq]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[scan_conf_owncloud_own_id_seq] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[sec_auditoria]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[sec_auditoria] 
 AS [bigint]
 START WITH 2
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 NO CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[SEC_CIU_CIUDADANO]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[SEC_CIU_CIUDADANO] 
 AS [bigint]
 START WITH 2
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[SEC_DIR_DIRECCIONES]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[SEC_DIR_DIRECCIONES] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[sec_informado]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[sec_informado] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[sec_oem_oempresas]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[sec_oem_oempresas] 
 AS [bigint]
 START WITH 2
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[sec_usua]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[sec_usua] 
 AS [bigint]
 START WITH 2
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 NO CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[SECR_TP1_1]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[SECR_TP1_1] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[SECR_TP1_100]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[SECR_TP1_100] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[SECR_TP2_1]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[SECR_TP2_1] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[SECR_TP2_100]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[SECR_TP2_100] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[SECR_TP3_1]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[SECR_TP3_1] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[SECR_TP3_100]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[SECR_TP3_100] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[SECR_TP4_1]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[SECR_TP4_1] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[seq_activo]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[seq_activo] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 NO CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[seq_anex_cert_id]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[seq_anex_cert_id] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 NO CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[seq_cexp_campdataexp]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[seq_cexp_campdataexp] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 NO CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[seq_iexp_metainfoexpediente]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[seq_iexp_metainfoexpediente] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 NO CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[seq_meta_metaexp]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[seq_meta_metaexp] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 NO CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[seq_seguridad]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[seq_seguridad] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 NO CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[sgd_anu_id]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[sgd_anu_id] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[sgd_certimail_id_seq]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[sgd_certimail_id_seq] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 NO CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[sgd_mot_motivos_sgd_mot_id_seq]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[sgd_mot_motivos_sgd_mot_id_seq] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 NO CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[sgd_pmrd_procedi_sgd_pmrd_codigo_seq]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[sgd_pmrd_procedi_sgd_pmrd_codigo_seq] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[sgd_renv_regenvio_id_seq]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[sgd_renv_regenvio_id_seq] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 NO CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[sgd_vmrd_matrivrd_sgd_vmrd_codigo_seq]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[sgd_vmrd_matrivrd_sgd_vmrd_codigo_seq] 
 AS [bigint]
 START WITH 6574
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 NO CACHE 
GO
USE [PRU_GdOrfeo]
GO
/****** Object:  Sequence [dbo].[sgd_vtrd_verstrd_sgd_vtrd_codigo_seq]    Script Date: 5/09/2019 2:12:31 p. m. ******/
CREATE SEQUENCE [dbo].[sgd_vtrd_verstrd_sgd_vtrd_codigo_seq] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 NO CACHE 
GO
/****** Object:  Table [dbo].[anexos]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[anexos](
	[anex_radi_nume] [numeric](38, 0) NOT NULL,
	[anex_codigo] [varchar](50) NOT NULL,
	[anex_tipo] [numeric](4, 0) NOT NULL,
	[anex_tamano] [numeric](18, 0) NULL,
	[anex_solo_lect] [varchar](1) NOT NULL,
	[anex_creador] [varchar](50) NOT NULL,
	[anex_desc] [varchar](512) NULL,
	[anex_numero] [numeric](5, 0) NOT NULL,
	[anex_nomb_archivo] [varchar](50) NOT NULL,
	[anex_borrado] [varchar](1) NOT NULL,
	[anex_origen] [numeric](1, 0) NULL,
	[anex_ubic] [varchar](15) NULL,
	[anex_salida] [numeric](1, 0) NULL,
	[radi_nume_salida] [numeric](18, 0) NULL,
	[anex_radi_fech] [datetime] NULL,
	[anex_estado] [numeric](1, 0) NULL,
	[usua_doc] [varchar](14) NULL,
	[sgd_rem_destino] [numeric](1, 0) NULL,
	[anex_fech_envio] [datetime] NULL,
	[sgd_dir_tipo] [numeric](4, 0) NULL,
	[anex_fech_impres] [date] NULL,
	[anex_depe_creador] [numeric](6, 0) NULL,
	[sgd_doc_secuencia] [numeric](15, 0) NULL,
	[sgd_doc_padre] [varchar](20) NULL,
	[sgd_arg_codigo] [numeric](2, 0) NULL,
	[sgd_tpr_codigo] [numeric](8, 0) NULL,
	[sgd_deve_codigo] [numeric](2, 0) NULL,
	[sgd_deve_fech] [datetime] NULL,
	[sgd_fech_impres] [datetime] NULL,
	[anex_fech_anex] [datetime] NULL,
	[anex_depe_codi] [varchar](6) NULL,
	[sgd_pnufe_codi] [numeric](4, 0) NULL,
	[sgd_dnufe_codi] [numeric](4, 0) NULL,
	[anex_usudoc_creador] [varchar](15) NULL,
	[sgd_fech_doc] [datetime] NULL,
	[sgd_apli_codi] [numeric](4, 0) NULL,
	[sgd_trad_codigo] [numeric](2, 0) NULL,
	[sgd_dir_direccion] [varchar](150) NULL,
	[muni_codi] [numeric](4, 0) NULL,
	[dpto_codi] [numeric](4, 0) NULL,
	[sgd_exp_numero] [varchar](18) NULL,
	[sgd_dir_cpcodi] [tinyint] NULL,
	[anex_folios_dig] [tinyint] NULL,
	[anex_recib] [tinyint] NULL,
	[anex_enlinea] [tinyint] NULL,
	[iddocumento] [varchar](100) NULL,
	[numeroradicado] [varchar](100) NULL,
	[anex_datafw] [numeric](1, 0) NULL,
	[rutadoc_webdav] [varchar](50) NULL,
	[anex_estado_email] [numeric](2, 0) NULL,
	[sgd_apli_codigo] [int] NULL,
	[radi_resp] [smallint] NULL,
 CONSTRAINT [anex_pk_anex_codigo] PRIMARY KEY CLUSTERED 
(
	[anex_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[anexos_tipo]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[anexos_tipo](
	[anex_tipo_codi] [numeric](4, 0) NOT NULL,
	[anex_tipo_ext] [varchar](10) NOT NULL,
	[anex_tipo_desc] [varchar](50) NULL,
	[anex_tipo_mime] [varchar](300) NULL,
	[anex_tipo_pqr] [smallint] NULL,
	[anex_perm_tipif_anexo] [smallint] NULL,
 CONSTRAINT [anex_pk_anex_tipo_codi] PRIMARY KEY CLUSTERED 
(
	[anex_tipo_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[arch_carp_carpeta]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[arch_carp_carpeta](
	[sgd_num_expediente] [nvarchar](24) NOT NULL,
	[radi_nume_radi] [numeric](18, 0) NOT NULL,
	[anex_nume] [nvarchar](50) NOT NULL,
	[anex_exp] [nvarchar](50) NOT NULL,
	[num_carpeta] [char](10) NULL,
 CONSTRAINT [PK_arch_carp_carpeta] PRIMARY KEY CLUSTERED 
(
	[sgd_num_expediente] ASC,
	[radi_nume_radi] ASC,
	[anex_nume] ASC,
	[anex_exp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[arch_edi_edificio]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[arch_edi_edificio](
	[arch_edi_codigo] [numeric](18, 0) NOT NULL,
	[arch_edi_nombre] [varchar](40) NULL,
	[arch_edi_sigla] [varchar](4) NULL,
	[codi_cont] [numeric](4, 0) NULL,
	[codi_pais] [numeric](4, 0) NULL,
	[codi_dpto] [numeric](4, 0) NULL,
	[codi_muni] [numeric](9, 0) NULL,
 CONSTRAINT [PK_arch_edi_edificio] PRIMARY KEY CLUSTERED 
(
	[arch_edi_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[arch_eit_items]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[arch_eit_items](
	[arch_eit_codigo] [numeric](18, 0) NOT NULL,
	[arch_eit_cod_padre] [numeric](18, 0) NULL,
	[arch_eit_nombre] [varchar](40) NULL,
	[arch_eit_sigla] [varchar](4) NULL,
	[arch_eit_codiedi] [numeric](18, 0) NULL,
 CONSTRAINT [PK_arch_eit_items] PRIMARY KEY CLUSTERED 
(
	[arch_eit_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[arch_evd_edivsdep]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[arch_evd_edivsdep](
	[arch_evd_id] [int] NOT NULL,
	[arch_evd_edificio] [int] NOT NULL,
	[arch_evd_depe] [int] NOT NULL,
	[arch_evd_item] [int] NOT NULL,
	[arch_evd_area] [int] NULL,
	[arch_evd_modulo] [int] NULL,
	[arch_evd_piso] [int] NULL,
 CONSTRAINT [PK_arch_evd_edivsdep] PRIMARY KEY CLUSTERED 
(
	[arch_evd_depe] ASC,
	[arch_evd_edificio] ASC,
	[arch_evd_item] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[arch_mda_motdesarch]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[arch_mda_motdesarch](
	[arch_mda_id] [int] NOT NULL,
	[arch_mda_nombre] [varchar](50) NOT NULL,
 CONSTRAINT [PK_arch_mda_motdesarch] PRIMARY KEY CLUSTERED 
(
	[arch_mda_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[arch_tpa_tpalmacenaje]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[arch_tpa_tpalmacenaje](
	[arch_tpa_id] [int] NOT NULL,
	[arch_tpa_name] [varchar](40) NULL,
	[arch_tpa_desc] [varchar](80) NULL,
	[arch_tpa_tamano] [int] NULL,
	[arch_tpa_tipo] [int] NULL,
 CONSTRAINT [PK_arch_tpa_tpalmacenaje] PRIMARY KEY CLUSTERED 
(
	[arch_tpa_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[arch_uexp_ubicacionexp]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[arch_uexp_ubicacionexp](
	[arch_uexp_codi] [int] NOT NULL,
	[sgd_exp_numero] [varchar](18) NOT NULL,
	[arch_uexp_ubica] [int] NULL,
	[arch_uexp_tpcaja] [int] NULL,
	[arch_uexp_tpcarpeta] [int] NULL,
	[arch_uexp_edificio] [int] NULL,
	[arch_uexp_psio] [int] NULL,
	[arch_uexp_area] [int] NULL,
	[arch_uexp_modulo] [int] NULL,
	[arch_uexp_estante] [int] NULL,
	[arch_uexp_entrepano] [int] NULL,
	[arch_uexp_codcaja] [int] NULL,
	[arch_uexp_nref] [varchar](7) NULL,
 CONSTRAINT [PK_arch_uexp_ubicacionexp] PRIMARY KEY CLUSTERED 
(
	[arch_uexp_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[auth_session]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[auth_session](
	[auth_id_usuario] [numeric](18, 0) NOT NULL,
	[auth_fech_session] [timestamp] NOT NULL,
	[auth_token_token] [varchar](max) NOT NULL,
	[auth_obj_permisos] [nchar](10) NOT NULL,
	[auth_ses_die] [numeric](18, 0) NOT NULL,
	[auth_ip] [varchar](50) NOT NULL,
 CONSTRAINT [PK_auth_session] PRIMARY KEY CLUSTERED 
(
	[auth_id_usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bodega_empresas]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bodega_empresas](
	[nombre_de_la_empresa] [varchar](160) NULL,
	[nuir] [varchar](13) NULL,
	[nit_de_la_empresa] [varchar](80) NULL,
	[sigla_de_la_empresa] [varchar](80) NULL,
	[direccion] [varchar](80) NULL,
	[codigo_del_departamento] [numeric](9, 0) NULL,
	[codigo_del_municipio] [numeric](9, 0) NULL,
	[telefono_1] [varchar](15) NULL,
	[telefono_2] [varchar](15) NULL,
	[email] [varchar](100) NULL,
	[nombre_rep_legal] [varchar](75) NULL,
	[cargo_rep_legal] [varchar](5) NULL,
	[identificador_empresa] [numeric](5, 0) NOT NULL,
	[are_esp_secue] [numeric](8, 0) NOT NULL,
	[id_cont] [numeric](2, 0) NULL,
	[id_pais] [numeric](4, 0) NULL,
	[activa] [numeric](1, 0) NULL,
	[flag_rups] [varchar](10) NULL,
	[sgd_dir_localidad] [varchar](50) NULL,
 CONSTRAINT [PK_bodega_empresas] PRIMARY KEY CLUSTERED 
(
	[identificador_empresa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[carpeta]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[carpeta](
	[carp_codi] [numeric](2, 0) NOT NULL,
	[carp_desc] [varchar](80) NOT NULL,
	[carp_estado] [numeric](1, 0) NULL,
	[rol_id] [int] NULL,
	[recepcion] [bit] NULL,
 CONSTRAINT [carpetas_pk] PRIMARY KEY CLUSTERED 
(
	[carp_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[carpeta_per]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[carpeta_per](
	[usua_codi] [numeric](10, 0) NOT NULL,
	[depe_codi] [numeric](5, 0) NOT NULL,
	[nomb_carp] [varchar](10) NULL,
	[desc_carp] [varchar](30) NULL,
	[codi_carp] [numeric](3, 0) NULL,
	[id_rol] [int] NULL,
	[id_carp] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[centro_poblado]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[centro_poblado](
	[cpob_codi] [numeric](4, 0) NOT NULL,
	[muni_codi] [numeric](9, 0) NOT NULL,
	[dpto_codi] [numeric](4, 0) NOT NULL,
	[cpob_nomb] [varchar](100) NOT NULL,
	[cpob_nomb_anterior] [varchar](100) NULL,
 CONSTRAINT [centro_poblado_pk] PRIMARY KEY CLUSTERED 
(
	[cpob_codi] ASC,
	[muni_codi] ASC,
	[dpto_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[departamento]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[departamento](
	[dpto_codi] [numeric](4, 0) NOT NULL,
	[dpto_nomb] [varchar](70) NOT NULL,
	[id_cont] [numeric](2, 0) NULL,
	[id_pais] [numeric](4, 0) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[depe_grupo]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[depe_grupo](
	[id_grupo] [tinyint] IDENTITY(1,1) NOT NULL,
	[depe_codi] [int] NOT NULL,
	[depe_grupo] [varchar](100) NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [pk_depe_grupo] PRIMARY KEY CLUSTERED 
(
	[id_grupo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dependencia]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dependencia](
	[depe_codi] [numeric](6, 0) NOT NULL,
	[depe_nomb] [varchar](100) NOT NULL,
	[dpto_codi] [numeric](2, 0) NULL,
	[depe_codi_padre] [numeric](6, 0) NULL,
	[muni_codi] [numeric](9, 0) NULL,
	[depe_codi_territorial] [numeric](6, 0) NULL,
	[dep_sigla] [varchar](100) NULL,
	[dep_central] [numeric](1, 0) NULL,
	[dep_direccion] [varchar](100) NULL,
	[depe_num_interna] [numeric](6, 0) NULL,
	[depe_num_resolucion] [numeric](6, 0) NULL,
	[depe_rad_tp1] [numeric](6, 0) NULL,
	[depe_rad_tp2] [numeric](6, 0) NULL,
	[depe_rad_tp3] [numeric](6, 0) NULL,
	[id_cont] [numeric](2, 0) NULL,
	[id_pais] [numeric](4, 0) NULL,
	[depe_estado] [numeric](1, 0) NULL,
	[depe_rad_tp8] [smallint] NULL,
	[depe_rad_tp4] [numeric](6, 0) NULL,
	[depe_rad_tp5] [numeric](6, 0) NULL,
	[depe_rad_tp6] [numeric](6, 0) NULL,
	[depe_rad_tp7] [numeric](6, 0) NULL,
	[depe_rad_tp9] [numeric](6, 0) NULL,
	[depe_grupo] [numeric](6, 0) NULL,
	[dep_telefono] [varchar](50) NULL,
	[depe_tipo] [int] NULL,
 CONSTRAINT [pk_depe] PRIMARY KEY CLUSTERED 
(
	[depe_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dependencia_visibilidad]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dependencia_visibilidad](
	[codigo_visibilidad] [numeric](18, 0) NOT NULL,
	[dependencia_visible] [numeric](5, 0) NOT NULL,
	[dependencia_observa] [numeric](5, 0) NOT NULL,
 CONSTRAINT [PK_dependencia_visibilidad] PRIMARY KEY CLUSTERED 
(
	[codigo_visibilidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[estado]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[estado](
	[esta_codi] [numeric](2, 0) NOT NULL,
	[esta_desc] [varchar](100) NOT NULL,
 CONSTRAINT [estados_pk] PRIMARY KEY CLUSTERED 
(
	[esta_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[firma_radicados]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[firma_radicados](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[fecha_solicitud] [datetime] NOT NULL,
	[usua_doc_solicita] [varchar](14) NOT NULL,
	[radi_nume_radi] [numeric](15, 0) NOT NULL,
	[usua_doc_firma] [varchar](14) NOT NULL,
	[fecha_firm_rech] [datetime] NULL,
	[estado_firma] [numeric](1, 0) NOT NULL,
	[orden_firma] [numeric](1, 0) NOT NULL,
	[observacion] [varchar](200) NULL,
	[depe_codi] [numeric](6, 0) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[firma_sellos]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[firma_sellos](
	[id_sello] [smallint] NOT NULL,
	[nombre_sello] [varchar](60) NOT NULL,
	[path_sello] [varchar](80) NOT NULL,
	[estado_sello] [numeric](1, 0) NOT NULL,
 CONSTRAINT [PK_FIRMA_SELLOS] PRIMARY KEY CLUSTERED 
(
	[nombre_sello] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[grupos]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[grupos](
	[grupo_codi] [numeric](10, 0) NOT NULL,
	[grupo_nombre] [varchar](255) NULL,
	[grupo_depe_codi] [numeric](6, 0) NOT NULL,
	[grupo_usua_codi] [int] NULL,
	[grupo_usua_ant_rol] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[grupo_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[grupos_usuarios]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[grupos_usuarios](
	[grupo_codi] [numeric](10, 0) NOT NULL,
	[usua_codi] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[hist_eventos]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[hist_eventos](
	[depe_codi] [numeric](6, 0) NOT NULL,
	[hist_fech] [datetime] NOT NULL,
	[usua_codi] [numeric](10, 0) NOT NULL,
	[radi_nume_radi] [numeric](18, 0) NOT NULL,
	[hist_obse] [varchar](900) NOT NULL,
	[usua_codi_dest] [numeric](10, 0) NULL,
	[usua_doc] [varchar](14) NULL,
	[usua_doc_old] [varchar](15) NULL,
	[sgd_ttr_codigo] [numeric](3, 0) NULL,
	[hist_usua_autor] [varchar](14) NULL,
	[hist_doc_dest] [varchar](14) NULL,
	[depe_codi_dest] [numeric](6, 0) NULL,
	[id_rol] [tinyint] NULL,
	[id_rol_dest] [tinyint] NULL,
	[id_grupo] [tinyint] NULL,
	[id] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[informados]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[informados](
	[radi_nume_radi] [numeric](38, 0) NOT NULL,
	[usua_codi] [numeric](10, 0) NOT NULL,
	[depe_codi] [numeric](6, 0) NOT NULL,
	[info_desc] [varchar](600) NULL,
	[info_fech] [datetime] NOT NULL,
	[info_leido] [numeric](1, 0) NULL,
	[usua_codi_info] [numeric](9, 0) NULL,
	[info_codi] [varchar](14) NULL,
	[usua_doc] [varchar](15) NULL,
	[id_rol] [int] NULL,
	[id] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mas_csv_base]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mas_csv_base](
	[mas_csv_id] [int] NOT NULL,
	[mas_csv_nombre] [varchar](250) NOT NULL,
	[mas_csv_estado] [int] NOT NULL,
	[mas_csv_depe] [int] NOT NULL,
	[mas_csv_extra] [varchar](1000) NULL,
 CONSTRAINT [mas_csv_primary_key] PRIMARY KEY CLUSTERED 
(
	[mas_csv_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mas_dat_csvdata]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mas_dat_csvdata](
	[mas_dat_id] [int] NOT NULL,
	[mas_dat_idcsv] [int] NOT NULL,
	[mas_dat_tprem] [int] NOT NULL,
	[mas_dat_codrem] [int] NOT NULL,
	[mas_dat_extras] [varchar](2000) NULL,
	[mas_dat_expediente] [varchar](18) NULL,
	[depe_codi] [numeric](6, 0) NULL,
	[list_campo1] [varchar](50) NULL,
	[list_campo2] [varchar](50) NULL,
	[list_campo3] [varchar](50) NULL,
	[list_campo4] [varchar](50) NULL,
	[list_campo5] [varchar](50) NULL,
	[list_campo6] [varchar](50) NULL,
	[list_campo7] [varchar](50) NULL,
 CONSTRAINT [mas_dat_csvdata_pkey] PRIMARY KEY CLUSTERED 
(
	[mas_dat_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[medio_recepcion]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[medio_recepcion](
	[mrec_codi] [numeric](2, 0) NOT NULL,
	[mrec_desc] [varchar](100) NOT NULL,
	[mrec_descAnterior] [varchar](100) NULL,
	[sgd_tpr_tp2] [numeric](1, 0) NULL,
	[sgd_tpr_tp1] [numeric](1, 0) NULL,
	[sgd_tpr_tp3] [numeric](1, 0) NULL,
 CONSTRAINT [pk_medio_recepcion] PRIMARY KEY CLUSTERED 
(
	[mrec_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[menu_estadisticas]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[menu_estadisticas](
	[menu_id] [int] NOT NULL,
	[menu_titulo] [char](250) NULL,
	[menu_descripcion] [char](250) NULL,
	[menu_url] [char](500) NULL,
	[menu_estado] [int] NULL,
	[depe_codi] [int] NULL,
	[indi_restri] [int] NULL,
	[id_padre] [int] NULL,
 CONSTRAINT [menu_estadisticas_pkey] PRIMARY KEY CLUSTERED 
(
	[menu_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[metodos_ws]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[metodos_ws](
	[cod_metodo] [int] NOT NULL,
	[nombre] [nvarchar](50) NOT NULL,
	[estado] [smallint] NOT NULL,
	[descripcion] [nvarchar](300) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[municipio]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[municipio](
	[muni_codi] [numeric](9, 0) NOT NULL,
	[dpto_codi] [numeric](4, 0) NOT NULL,
	[muni_nomb] [varchar](100) NOT NULL,
	[id_cont] [numeric](2, 0) NULL,
	[id_pais] [numeric](4, 0) NOT NULL,
	[homologa_muni] [numeric](8, 0) NULL,
	[homologa_idmuni] [numeric](4, 0) NULL,
	[activa] [numeric](1, 0) NULL,
	[cod_municipio] [varchar](15) NULL,
	[tipo] [numeric](12, 0) NULL,
	[codigo_centropoblado] [char](15) NULL,
	[id_centropoblado] [numeric](18, 0) NULL,
 CONSTRAINT [pk_municipio] PRIMARY KEY CLUSTERED 
(
	[muni_codi] ASC,
	[dpto_codi] ASC,
	[id_pais] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[not_noticias]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[not_noticias](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[not_desc] [varchar](max) NULL,
	[not_fech] [date] NULL,
	[not_estado] [numeric](18, 0) NULL,
 CONSTRAINT [PK__not_noti__3213E83FA2206F46] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pdf_plantilla]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pdf_plantilla](
	[pdf_cod] [int] NOT NULL,
	[pdf_nombre] [varchar](50) NULL,
	[pdf_head] [varchar](200) NULL,
	[pdf_footer] [varchar](200) NULL,
	[pdf_remitente] [varchar](400) NULL,
	[pdf_sticker] [varchar](400) NULL,
	[pdf_destinatario] [varchar](500) NULL,
	[pdf_texto] [varchar](1000) NULL,
	[pdf_estado] [int] NULL,
	[pdf_regional] [numeric](6, 0) NULL,
	[pdf_desc] [varchar](50) NULL,
	[pdf_trad_codigo] [numeric](2, 0) NULL,
	[pdf_depe_codi] [numeric](6, 0) NULL,
	[pdf_logo] [varchar](200) NULL,
	[pdf_alcance] [int] NULL,
 CONSTRAINT [pdf_plantilla_pkey] PRIMARY KEY CLUSTERED 
(
	[pdf_cod] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[permisos]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[permisos](
	[rol] [int] NULL,
	[mod] [int] NULL,
	[val] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pl_plantillas]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pl_plantillas](
	[pl_id] [int] NOT NULL,
	[pl_nombre] [char](250) NULL,
	[pl_url] [char](500) NULL,
	[pl_estado] [int] NULL,
	[depe_codi] [int] NULL,
	[categoria] [char](25) NULL,
 CONSTRAINT [pl_plantillas_pkey] PRIMARY KEY CLUSTERED 
(
	[pl_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pl_tipo_plt]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pl_tipo_plt](
	[plt_codi] [numeric](4, 0) NOT NULL,
	[plt_desc] [varchar](35) NULL,
 CONSTRAINT [PK__pl_tipo___9803B2819032C2C0] PRIMARY KEY CLUSTERED 
(
	[plt_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pla_entrega]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pla_entrega](
	[pla_codigo] [numeric](14, 0) NOT NULL,
	[radi_nume_radi] [numeric](38, 0) NOT NULL,
	[depe_codi_genera] [numeric](6, 0) NOT NULL,
	[pla_entrega_fech_crea] [datetime] NOT NULL,
	[usua_codi_crea] [numeric](10, 0) NOT NULL,
	[id_rol] [int] NOT NULL,
	[pla_entrega_estado] [int] NOT NULL,
	[pla_entrega_desc_fecha] [datetime] NULL,
	[pla_entrega_ano] [int] NOT NULL,
	[pla_codigo_secuencia] [int] NULL,
 CONSTRAINT [PK_pla_entrega] PRIMARY KEY CLUSTERED 
(
	[pla_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pla_entrega_radi]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pla_entrega_radi](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[pla_codigo] [numeric](14, 0) NULL,
	[radi_nume_radi] [numeric](38, 0) NOT NULL,
	[depe_codi_entrega] [numeric](5, 0) NOT NULL,
	[pla_radi_estado] [int] NOT NULL,
	[usua_codi_ref] [numeric](10, 0) NULL,
	[pla_fech_sol] [datetime] NOT NULL,
	[pla_solicitado] [int] NOT NULL,
	[pla_observacion] [varchar](500) NULL,
	[sgd_dir_tipo] [numeric](18, 0) NULL,
 CONSTRAINT [PK__pla_entr__3213E83F5C9898F1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[planillas]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[planillas](
	[pla_codigo] [numeric](38, 0) NULL,
	[usu_cre_codigo] [numeric](38, 0) NULL,
	[dep_cre_codigo] [varchar](5) NULL,
	[pla_fec_creacion] [datetime] NULL,
	[est_codigo] [numeric](38, 0) NULL,
	[cod_dep_destino] [varchar](5) NULL,
	[tip_planilla] [numeric](38, 0) NULL,
	[dep_sel_codigo] [varchar](5) NULL,
	[pla_imagen] [varchar](40) NULL,
	[usu_not_codigo] [numeric](10, 0) NULL,
	[zona] [varchar](50) NULL,
	[pla_fec_recp] [datetime] NULL,
	[cod_dep_destino_dep] [varchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[plantilla_pl]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[plantilla_pl](
	[pl_codi] [numeric](4, 0) NOT NULL,
	[depe_codi] [numeric](5, 0) NULL,
	[pl_nomb] [varchar](35) NULL,
	[pl_archivo] [varchar](35) NULL,
	[pl_desc] [varchar](150) NULL,
	[pl_fech] [datetime] NULL,
	[usua_codi] [numeric](10, 0) NULL,
	[pl_uso] [numeric](1, 0) NULL,
 CONSTRAINT [PK_plantilla_pl] PRIMARY KEY CLUSTERED 
(
	[pl_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[prestamo]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[prestamo](
	[pres_id] [numeric](10, 0) NOT NULL,
	[radi_nume_radi] [numeric](15, 0) NOT NULL,
	[usua_login_actu] [varchar](50) NOT NULL,
	[depe_codi] [numeric](6, 0) NOT NULL,
	[usua_login_pres] [varchar](50) NULL,
	[pres_desc] [varchar](200) NULL,
	[pres_fech_pres] [datetime] NULL,
	[pres_fech_devo] [datetime] NULL,
	[pres_fech_pedi] [datetime] NOT NULL,
	[pres_estado] [numeric](2, 0) NULL,
	[pres_requerimiento] [numeric](2, 0) NULL,
	[pres_depe_arch] [numeric](6, 0) NULL,
	[pres_fech_venc] [datetime] NULL,
	[dev_desc] [varchar](500) NULL,
	[pres_fech_canc] [datetime] NULL,
	[usua_login_canc] [varchar](50) NULL,
	[usua_login_rx] [varchar](50) NULL,
	[usua_login_auth] [varchar](50) NULL,
	[pres_path_acta] [varchar](200) NULL,
	[pres_acta] [numeric](8, 0) NULL,
	[pres_anyo_acta] [numeric](4, 0) NULL,
 CONSTRAINT [PK_prestamo] PRIMARY KEY CLUSTERED 
(
	[pres_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rad_asociados]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rad_asociados](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[radi_nume_radi] [int] NULL,
	[radi_nume_asociado] [int] NULL,
	[fechasociado] [date] NULL,
	[usua_codi] [int] NULL,
 CONSTRAINT [PK__rad_asoc__3213E83F5C630F40] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rad_datatemporal]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rad_datatemporal](
	[rad_data_id] [int] NOT NULL,
	[conanexo] [varchar](24) NULL,
	[radi_nume_radi] [int] NULL,
	[datos] [varchar](max) NULL,
	[otros] [varchar](3000) NULL,
	[firma] [varchar](2500) NULL,
	[header] [char](100) NULL,
	[footer] [char](100) NULL,
 CONSTRAINT [rad_datatemporal_pkey] PRIMARY KEY CLUSTERED 
(
	[rad_data_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rad_planilla]    Script Date: 5/09/2019 2:12:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rad_planilla](
	[pla_codigo] [numeric](38, 0) NOT NULL,
	[radi_nume_radi] [numeric](38, 0) NOT NULL,
	[est_codigo] [numeric](38, 0) NULL,
	[rad_fec_anexo] [datetime] NULL,
	[blo_planilla] [numeric](1, 0) NULL,
	[rad_fec_registro] [datetime] NULL,
	[usua_codi] [numeric](3, 0) NULL,
	[depe_codi] [varchar](5) NULL,
	[cod_externo] [numeric](15, 0) NULL,
	[tip_externo] [numeric](1, 0) NULL,
	[pla_acuse] [numeric](1, 0) NULL,
	[tip_destinatario] [numeric](2, 0) NULL,
	[ide_informado] [varchar](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[radi_cam_metacampos]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[radi_cam_metacampos](
	[radi_cam_id] [int] IDENTITY(1,1) NOT NULL,
	[radi_data_id] [int] NULL,
	[radi_cam_nombre] [varchar](30) NULL,
	[radi_cam_tipo] [varchar](10) NULL,
	[radi_cam_oblig] [int] NULL,
	[radi_cam_busq] [int] NULL,
	[radi_cam_desc] [varchar](100) NULL,
	[radi_cam_estado] [int] NULL,
 CONSTRAINT [PK__radi_cam__CAF7011F1F181E31] PRIMARY KEY CLUSTERED 
(
	[radi_cam_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[radi_data_metainfo]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[radi_data_metainfo](
	[radi_data_id] [int] NOT NULL,
	[radi_data_nomb] [varchar](50) NULL,
	[radi_data_busq] [int] NULL,
	[radi_data_estado] [int] NULL,
	[sgd_trad_codigo] [numeric](18, 0) NULL,
	[depe_codi] [numeric](6, 0) NULL,
 CONSTRAINT [PK_radi_data_metainfo] PRIMARY KEY CLUSTERED 
(
	[radi_data_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[radicado]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[radicado](
	[radi_nume_radi] [numeric](38, 0) NOT NULL,
	[radi_fech_radi] [datetime] NOT NULL,
	[tdoc_codi] [numeric](8, 0) NOT NULL,
	[trte_codi] [numeric](2, 0) NULL,
	[mrec_codi] [numeric](2, 0) NULL,
	[eesp_codi] [numeric](10, 0) NULL,
	[eotra_codi] [numeric](10, 0) NULL,
	[radi_tipo_empr] [varchar](2) NULL,
	[radi_fech_ofic] [datetime] NULL,
	[tdid_codi] [numeric](2, 0) NULL,
	[radi_nume_iden] [varchar](15) NULL,
	[radi_nomb] [varchar](150) NULL,
	[radi_prim_apel] [varchar](50) NULL,
	[radi_segu_apel] [varchar](150) NULL,
	[radi_pais] [varchar](70) NULL,
	[muni_codi] [numeric](9, 0) NULL,
	[cpob_codi] [numeric](4, 0) NULL,
	[carp_codi] [numeric](3, 0) NULL,
	[esta_codi] [numeric](2, 0) NULL,
	[dpto_codi] [numeric](4, 0) NULL,
	[cen_muni_codi] [numeric](4, 0) NULL,
	[cen_dpto_codi] [numeric](2, 0) NULL,
	[radi_dire_corr] [varchar](100) NULL,
	[radi_tele_cont] [numeric](15, 0) NULL,
	[radi_nume_hoja] [numeric](8, 0) NULL,
	[radi_desc_anex] [varchar](150) NULL,
	[radi_nume_deri] [numeric](38, 0) NULL,
	[radi_path] [varchar](100) NULL,
	[radi_usua_actu] [numeric](10, 0) NULL,
	[radi_depe_actu] [numeric](6, 0) NULL,
	[radi_fech_asig] [datetime] NULL,
	[ra_asun] [varchar](2000) NULL,
	[radi_usu_ante] [varchar](45) NULL,
	[radi_depe_radi] [numeric](6, 0) NULL,
	[radi_rem] [varchar](60) NULL,
	[radi_usua_radi] [numeric](10, 0) NULL,
	[codi_nivel] [numeric](2, 0) NULL,
	[flag_nivel] [numeric](18, 0) NULL,
	[carp_per] [numeric](1, 0) NULL,
	[radi_leido] [numeric](1, 0) NULL,
	[radi_cuentai] [varchar](20) NULL,
	[radi_tipo_deri] [numeric](2, 0) NULL,
	[listo] [varchar](2) NULL,
	[sgd_tma_codigo] [numeric](4, 0) NULL,
	[sgd_mtd_codigo] [numeric](4, 0) NULL,
	[par_serv_secue] [numeric](8, 0) NULL,
	[sgd_fld_codigo] [numeric](3, 0) NULL,
	[radi_agend] [numeric](1, 0) NULL,
	[radi_fech_agend] [datetime] NULL,
	[radi_fech_doc] [datetime] NULL,
	[sgd_doc_secuencia] [numeric](15, 0) NULL,
	[sgd_pnufe_codi] [numeric](4, 0) NULL,
	[sgd_eanu_codigo] [numeric](1, 0) NULL,
	[sgd_not_codi] [numeric](3, 0) NULL,
	[radi_fech_notif] [datetime] NULL,
	[sgd_tdec_codigo] [numeric](4, 0) NULL,
	[sgd_apli_codi] [numeric](4, 0) NULL,
	[sgd_ttr_codigo] [numeric](18, 0) NULL,
	[usua_doc_ante] [varchar](14) NULL,
	[radi_fech_antetx] [datetime] NULL,
	[sgd_trad_codigo] [numeric](2, 0) NULL,
	[fech_vcmto] [datetime] NULL,
	[tdoc_vcmto] [numeric](4, 0) NULL,
	[sgd_termino_real] [numeric](4, 0) NULL,
	[id_cont] [numeric](2, 0) NULL,
	[sgd_spub_codigo] [numeric](2, 0) NULL,
	[id_pais] [numeric](3, 0) NULL,
	[radi_nrr] [numeric](2, 0) NULL,
	[medio_m] [numeric](4, 0) NULL,
	[radi_usua_doc_actu] [varchar](20) NULL,
	[id_rol] [int] NULL,
	[usua_depe_ante] [int] NULL,
	[usua_rol_ante] [int] NULL,
	[radi_rol_radi] [int] NULL,
	[depe_codi] [numeric](6, 0) NULL,
	[radi_num_numinterno] [varchar](50) NULL,
	[radi_nume_folios] [int] NULL,
	[radi_nume_guia] [varchar](50) NULL,
	[radi_nume_anexo] [varchar](50) NULL,
	[radi_nume_folio] [numeric](5, 0) NULL,
	[radi_codi_verifica] [varchar](20) NULL,
	[radi_campo1] [varchar](100) NULL,
	[radi_campo2] [varchar](100) NULL,
	[radi_campo3] [varchar](100) NULL,
	[radi_campo4] [varchar](100) NULL,
	[radi_campo5] [varchar](100) NULL,
	[radi_campo6] [varchar](100) NULL,
	[radi_campo7] [varchar](100) NULL,
	[cod_planilla] [numeric](38, 0) NULL,
	[radi_mdata_metadatos] [int] NULL,
	[radi_fech_recib] [datetime] NULL,
	[asunt_masiva] [varchar](2000) NULL,
	[usua_doc] [varchar](14) NULL,
	[radi_fecha_vence] [varchar](20) NULL,
	[radi_dias_vence] [numeric](6, 0) NULL,
	[sgd_apli_codigo] [int] NULL,
	[requiere_resp] [tinyint] NULL,
	[radi_fech_tramite] [date] NULL,
	[radi_respuesta] [numeric](38, 0) NULL,
	[id] [numeric](38, 0) IDENTITY(1,1) NOT NULL,
 CONSTRAINT [radicado_pk] PRIMARY KEY CLUSTERED 
(
	[radi_nume_radi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[scan_conf_owncloud]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[scan_conf_owncloud](
	[own_id] [int] IDENTITY(1,1) NOT NULL,
	[own_clouduser] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[scan_conf_owncloud2]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[scan_conf_owncloud2](
	[own_id] [int] NOT NULL,
	[own_clouduser] [varchar](1) NULL,
 CONSTRAINT [PK__scan_con__2C16ED7A006B2884] PRIMARY KEY CLUSTERED 
(
	[own_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_aexp_aclexp]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_aexp_aclexp](
	[num_expediente] [varchar](24) NOT NULL,
	[depe_codi] [int] NOT NULL,
	[usua_codi] [int] NULL,
	[aexp_acl] [int] NOT NULL,
	[id_aexp] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK__sgd_aexp__B5FE1247BD4332A7] PRIMARY KEY CLUSTERED 
(
	[id_aexp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_aexp_anexexpediente]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_aexp_anexexpediente](
	[num_expediente] [nvarchar](50) NOT NULL,
	[num_anexo] [numeric](18, 0) NOT NULL,
	[aexp_path] [varchar](max) NOT NULL,
	[tipo_doc] [nchar](30) NULL,
	[tipo_archivo] [numeric](18, 0) NULL,
	[exp_fech] [datetime] NOT NULL,
	[aexp_borrado] [nchar](1) NULL,
	[aexp_nombre] [nvarchar](100) NULL,
	[aexp_codi] [nvarchar](50) NULL,
	[aexp_tpdoc] [numeric](18, 0) NULL,
	[aexp_asunto] [nvarchar](max) NULL,
	[aexp_subexp] [nvarchar](max) NULL,
	[aexp_folios] [numeric](18, 0) NULL,
	[aexp_usua] [varchar](50) NULL,
	[aexp_id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_sgd_aexp_anexexpediente] PRIMARY KEY CLUSTERED 
(
	[num_expediente] ASC,
	[num_anexo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_agen_agendados]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_agen_agendados](
	[sgd_agen_fech] [datetime] NULL,
	[sgd_agen_observacion] [varchar](4000) NULL,
	[radi_nume_radi] [numeric](38, 0) NOT NULL,
	[usua_doc] [varchar](18) NOT NULL,
	[depe_codi] [numeric](6, 0) NULL,
	[sgd_agen_codigo] [numeric](18, 0) NULL,
	[sgd_agen_fechplazo] [datetime] NULL,
	[sgd_agen_activo] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_anar_anexarg]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_anar_anexarg](
	[sgd_anar_codi] [numeric](4, 0) NOT NULL,
	[anex_codigo] [varchar](50) NULL,
	[sgd_argd_codi] [numeric](4, 0) NULL,
	[sgd_anar_argcod] [numeric](4, 0) NULL,
 CONSTRAINT [PK__sgd_anar__78110166587717D0] PRIMARY KEY CLUSTERED 
(
	[sgd_anar_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_anu_anulados]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_anu_anulados](
	[sgd_anu_id] [int] NOT NULL,
	[sgd_anu_desc] [varchar](250) NULL,
	[radi_nume_radi] [numeric](38, 0) NULL,
	[sgd_eanu_codi] [numeric](18, 0) NULL,
	[sgd_anu_sol_fech] [datetime] NULL,
	[sgd_anu_fech] [datetime] NULL,
	[depe_codi] [numeric](6, 0) NULL,
	[usua_doc] [varchar](14) NULL,
	[usua_codi] [numeric](4, 0) NULL,
	[depe_codi_anu] [numeric](6, 0) NULL,
	[usua_doc_anu] [varchar](14) NULL,
	[usua_codi_anu] [numeric](4, 0) NULL,
	[usua_anu_acta] [numeric](38, 0) NULL,
	[sgd_anu_path_acta] [varchar](200) NULL,
	[sgd_trad_codigo] [numeric](2, 0) NULL,
 CONSTRAINT [PK__sgd_anu___E901B27E9BEC085C] PRIMARY KEY CLUSTERED 
(
	[sgd_anu_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_aplicaciones]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_aplicaciones](
	[sgd_apli_codigo] [int] NOT NULL,
	[sgd_apli_descrip] [varchar](30) NOT NULL,
	[sgd_apli_estado] [smallint] NOT NULL,
	[sgd_apli_depe] [smallint] NOT NULL,
	[ip_acceso] [nvarchar](30) NULL,
	[usua_login] [varchar](24) NOT NULL,
	[cliente_ws_urlwsdl] [nvarchar](300) NULL,
	[cliente_ws_usuario] [nvarchar](50) NULL,
	[cliente_ws_password] [nvarchar](50) NULL,
	[cliente_bd_server] [nvarchar](50) NULL,
	[cliente_bd_database] [nvarchar](50) NULL,
	[cliente_bd_driver] [nvarchar](50) NULL,
	[cliente_bd_usuario] [nvarchar](50) NULL,
	[cliente_bd_password] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[sgd_apli_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_argup_argudoctop]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_argup_argudoctop](
	[sgd_argup_codi] [numeric](4, 0) NOT NULL,
	[sgd_argup_desc] [varchar](50) NULL,
	[sgd_tpr_codigo] [numeric](4, 0) NULL,
 CONSTRAINT [PK__sgd_argu__295F3F66EA1A827A] PRIMARY KEY CLUSTERED 
(
	[sgd_argup_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_auditoria]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_auditoria](
	[usua_doc] [varchar](50) NULL,
	[tipo] [varchar](20) NULL,
	[tabla] [varchar](50) NULL,
	[isql] [varchar](1000) NULL,
	[fecha] [numeric](20, 0) NULL,
	[ip] [varchar](40) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_auditoria_ws]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_auditoria_ws](
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[fecha_evento] [datetime] NOT NULL,
	[aplicativo] [varchar](30) NULL,
	[xml_parametros] [xml] NOT NULL,
	[usua_login] [varchar](24) NULL,
	[funcionalidad] [varchar](50) NULL,
	[estado] [varchar](50) NULL,
	[mensaje] [text] NULL,
	[ano] [smallint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_camexp_campoexpediente]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_camexp_campoexpediente](
	[sgd_camexp_codigo] [numeric](4, 0) NOT NULL,
	[sgd_camexp_campo] [varchar](30) NOT NULL,
	[sgd_parexp_codigo] [numeric](4, 0) NOT NULL,
	[sgd_camexp_fk] [numeric](18, 0) NULL,
	[sgd_camexp_tablafk] [varchar](30) NULL,
	[sgd_camexp_campofk] [varchar](30) NULL,
	[sgd_camexp_campovalor] [varchar](30) NULL,
	[sgd_campexp_orden] [numeric](1, 0) NULL,
 CONSTRAINT [PK__sgd_came__6BDCD447274CB570] PRIMARY KEY CLUSTERED 
(
	[sgd_camexp_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_carp_descripcion]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_carp_descripcion](
	[sgd_carp_depecodi] [numeric](6, 0) NOT NULL,
	[sgd_carp_tiporad] [numeric](2, 0) NOT NULL,
	[sgd_carp_descr] [varchar](40) NULL,
 CONSTRAINT [PK__sgd_carp__1393E8BC729A8CC0] PRIMARY KEY CLUSTERED 
(
	[sgd_carp_depecodi] ASC,
	[sgd_carp_tiporad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_categorias]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_categorias](
	[sgd_cat_codigo] [int] NOT NULL,
	[sgd_cat_descripcion] [char](250) NULL,
	[sgd_cat_estado] [int] NULL,
 CONSTRAINT [sgd_categorias_pkey] PRIMARY KEY CLUSTERED 
(
	[sgd_cat_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_cau_causal]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_cau_causal](
	[sgd_cau_codigo] [numeric](4, 0) NOT NULL,
	[sgd_cau_descrip] [varchar](150) NULL,
	[sgd_cau_estado] [tinyint] NULL,
 CONSTRAINT [PK_SGD_CAU_CAUSAL] PRIMARY KEY CLUSTERED 
(
	[sgd_cau_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_cen_codenvio]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_cen_codenvio](
	[sgd_cen_pfech] [int] NOT NULL,
	[sgd_cen_con] [int] NULL,
	[depe_codi] [int] NOT NULL,
 CONSTRAINT [PK_sgd_cen_codenvio] PRIMARY KEY CLUSTERED 
(
	[sgd_cen_pfech] ASC,
	[depe_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_certificado]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_certificado](
	[sgd_cert_id] [int] NOT NULL,
	[radi_nume_radi] [numeric](38, 0) NULL,
	[anex_codigo] [varchar](50) NULL,
	[usua_login] [varchar](50) NULL,
	[depe_codi] [int] NULL,
	[sgd_cert_id_transaccion] [int] NULL,
	[sgd_cert_fech] [datetime] NOT NULL,
	[sgd_cert_tipo] [smallint] NULL,
 CONSTRAINT [PK_sgd_certificado] PRIMARY KEY CLUSTERED 
(
	[sgd_cert_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_certimail]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_certimail](
	[id] [int] NOT NULL,
	[radi_nume_radi] [numeric](38, 0) NOT NULL,
	[certi_fech] [datetime] NOT NULL,
	[certi_mail] [varchar](50) NULL,
	[certi_mail_dest] [varchar](50) NULL,
 CONSTRAINT [PK__sgd_cert__3213E83F57451500] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_cexp_campdataexp]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_cexp_campdataexp](
	[sgd_mexp_codigo] [numeric](18, 0) NULL,
	[sgd_cexp_codigo] [int] NOT NULL,
	[sgd_cexp_tipo] [varchar](50) NULL,
	[sgd_cexp_nombre] [varchar](50) NULL,
	[sgd_cexp_desc] [varchar](100) NULL,
	[sgd_cexp_estado] [numeric](18, 0) NULL,
	[sgd_cexp_busq] [numeric](18, 0) NULL,
	[sgd_cexp_oblig] [numeric](18, 0) NULL,
 CONSTRAINT [PK__sgd_cexp__A2C96BC6D2DBAD8D] PRIMARY KEY CLUSTERED 
(
	[sgd_cexp_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_ciu_ciudadano]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_ciu_ciudadano](
	[tdid_codi] [numeric](2, 0) NULL,
	[sgd_ciu_codigo] [numeric](8, 0) NOT NULL,
	[sgd_ciu_nombre] [varchar](150) NULL,
	[sgd_ciu_direccion] [varchar](150) NULL,
	[sgd_ciu_apell1] [varchar](50) NULL,
	[sgd_ciu_apell2] [varchar](50) NULL,
	[sgd_ciu_telefono] [varchar](50) NULL,
	[sgd_ciu_email] [varchar](100) NULL,
	[muni_codi] [numeric](9, 0) NULL,
	[dpto_codi] [numeric](4, 0) NULL,
	[sgd_ciu_cedula] [varchar](20) NULL,
	[id_cont] [numeric](2, 0) NULL,
	[id_pais] [numeric](4, 0) NULL,
	[sgd_ciu_estado] [int] NULL,
	[id_centropoblado] [varchar](10) NULL,
	[id_persona] [varchar](100) NULL,
	[id_direccion] [varchar](100) NULL,
 CONSTRAINT [PK__sgd_ciu___E402F7DD5B22168A] PRIMARY KEY CLUSTERED 
(
	[sgd_ciu_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_clta_clstarif]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_clta_clstarif](
	[sgd_fenv_codigo] [numeric](5, 0) NULL,
	[sgd_clta_codser] [numeric](5, 0) NULL,
	[sgd_tar_codigo] [numeric](5, 0) NULL,
	[sgd_clta_descrip] [varchar](100) NULL,
	[sgd_clta_pesdes] [numeric](15, 0) NULL,
	[sgd_clta_peshast] [numeric](15, 0) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_cob_campobliga]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_cob_campobliga](
	[sgd_cob_codi] [numeric](4, 0) NOT NULL,
	[sgd_cob_desc] [varchar](150) NULL,
	[sgd_cob_label] [varchar](50) NULL,
	[sgd_tidm_codi] [numeric](4, 0) NULL,
 CONSTRAINT [PK__sgd_cob___35FD5D778A9658B9] PRIMARY KEY CLUSTERED 
(
	[sgd_cob_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_config]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_config](
	[conf_nombre] [nvarchar](50) NOT NULL,
	[conf_valor] [nvarchar](100) NOT NULL,
	[conf_constante] [nvarchar](50) NULL,
	[conf_arreglo] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_dcau_causal]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_dcau_causal](
	[sgd_dcau_codigo] [numeric](4, 0) NOT NULL,
	[sgd_cau_codigo] [numeric](4, 0) NULL,
	[sgd_dcau_descrip] [varchar](150) NULL,
	[sgd_dcau_estado] [tinyint] NULL,
	[sgd_dcau_pqr] [smallint] NULL,
	[sgd_dcau_pqrdesc] [varchar](500) NULL,
 CONSTRAINT [PK_SGD_DCAU_CAUSAL] PRIMARY KEY CLUSTERED 
(
	[sgd_dcau_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_def_continentes]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_def_continentes](
	[id_cont] [numeric](2, 0) NOT NULL,
	[nombre_cont] [varchar](20) NOT NULL,
 CONSTRAINT [pk_sgd_def_continentes] PRIMARY KEY CLUSTERED 
(
	[id_cont] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_def_paises]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_def_paises](
	[id_pais] [numeric](4, 0) NOT NULL,
	[id_cont] [numeric](2, 0) NOT NULL,
	[nombre_pais] [varchar](30) NOT NULL,
 CONSTRAINT [pk_sgd_def_paises] PRIMARY KEY CLUSTERED 
(
	[id_pais] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_depe_activo]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_depe_activo](
	[sgd_dp_act_id] [int] NULL,
	[depe_codi] [numeric](6, 0) NULL,
	[rol_id] [int] NULL,
	[sgd_dp_act] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_depe_mrd]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_depe_mrd](
	[tipo] [int] NULL,
	[sgd_srd_codigo] [int] NULL,
	[sgd_sbrd_codigo] [int] NULL,
	[sgd_tpr_codigo] [int] NULL,
	[categoria] [int] NULL,
	[depe_codi] [nchar](10) NULL,
	[mrd_soporte] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_deve_dev_envio]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_deve_dev_envio](
	[sgd_deve_codigo] [numeric](2, 0) NOT NULL,
	[sgd_deve_desc] [varchar](150) NOT NULL,
	[sgd_deve_tipo] [smallint] NULL,
 CONSTRAINT [PK__sgd_deve__91ADDFB73B4CCD3C] PRIMARY KEY CLUSTERED 
(
	[sgd_deve_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_dir_drecciones]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_dir_drecciones](
	[sgd_dir_codigo] [numeric](10, 0) NOT NULL,
	[sgd_dir_tipo] [numeric](4, 0) NOT NULL,
	[sgd_oem_codigo] [numeric](8, 0) NULL,
	[sgd_ciu_codigo] [numeric](8, 0) NULL,
	[radi_nume_radi] [numeric](38, 0) NULL,
	[sgd_esp_codi] [numeric](5, 0) NULL,
	[muni_codi] [numeric](9, 0) NULL,
	[dpto_codi] [numeric](4, 0) NULL,
	[sgd_dir_direccion] [varchar](150) NULL,
	[sgd_dir_telefono] [varchar](50) NULL,
	[sgd_dir_mail] [varchar](100) NULL,
	[sgd_sec_codigo] [numeric](13, 0) NULL,
	[sgd_temporal_nombre] [varchar](150) NULL,
	[anex_codigo] [numeric](20, 0) NULL,
	[sgd_anex_codigo] [varchar](25) NULL,
	[sgd_dir_nombre] [varchar](150) NULL,
	[sgd_doc_fun] [varchar](14) NULL,
	[sgd_dir_nomremdes] [varchar](1000) NULL,
	[sgd_trd_codigo] [numeric](1, 0) NULL,
	[sgd_dir_tdoc] [numeric](1, 0) NULL,
	[sgd_dir_doc] [varchar](14) NULL,
	[id_pais] [numeric](4, 0) NULL,
	[id_cont] [numeric](2, 0) NULL,
	[sgd_dir_cpcodi] [int] NULL,
	[radi_nume_copia] [numeric](18, 0) NULL,
 CONSTRAINT [pk_sgd_dir] PRIMARY KEY CLUSTERED 
(
	[sgd_dir_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_dpr_dep_per_rol]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_dpr_dep_per_rol](
	[depe_codi] [numeric](6, 0) NULL,
	[rol_codi] [int] NULL,
	[depe_codi_per] [numeric](6, 0) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_drm_dep_mod_rol]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_drm_dep_mod_rol](
	[sgd_drm_depecodi] [numeric](6, 0) NOT NULL,
	[sgd_drm_rolcodi] [int] NOT NULL,
	[sgd_drm_modecodi] [int] NOT NULL,
	[sgd_drm_valor] [int] NULL,
	[sgd_drm_nivel] [int] NULL,
 CONSTRAINT [PK_sgd_drm_dep_mod_rol] PRIMARY KEY CLUSTERED 
(
	[sgd_drm_depecodi] ASC,
	[sgd_drm_rolcodi] ASC,
	[sgd_drm_modecodi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_eanu_estanulacion]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_eanu_estanulacion](
	[sgd_eanu_desc] [varchar](150) NULL,
	[sgd_eanu_codi] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK__sgd_eanu__2323725E713A4A25] PRIMARY KEY CLUSTERED 
(
	[sgd_eanu_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_einv_inventario]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_einv_inventario](
	[sgd_einv_codigo] [numeric](18, 0) NOT NULL,
	[sgd_depe_nomb] [varchar](400) NULL,
	[sgd_depe_codi] [numeric](18, 0) NULL,
	[sgd_einv_expnum] [varchar](18) NULL,
	[sgd_einv_titulo] [varchar](400) NULL,
	[sgd_einv_unidad] [numeric](18, 0) NULL,
	[sgd_einv_fech] [date] NULL,
	[sgd_einv_fechfin] [date] NULL,
	[sgd_einv_radicados] [varchar](40) NULL,
	[sgd_einv_folios] [numeric](18, 0) NULL,
	[sgd_einv_nundocu] [numeric](18, 0) NULL,
	[sgd_einv_nundocubodega] [numeric](18, 0) NULL,
	[sgd_einv_caja] [numeric](18, 0) NULL,
	[sgd_einv_cajabodega] [numeric](18, 0) NULL,
	[sgd_einv_srd] [numeric](18, 0) NULL,
	[sgd_einv_nomsrd] [varchar](400) NULL,
	[sgd_einv_sbrd] [numeric](18, 0) NULL,
	[sgd_einv_nomsbrd] [varchar](400) NULL,
	[sgd_einv_retencion] [varchar](400) NULL,
	[sgd_einv_disfinal] [varchar](400) NULL,
	[sgd_einv_ubicacion] [varchar](400) NULL,
	[sgd_einv_observacion] [varchar](400) NULL,
 CONSTRAINT [PK__sgd_einv__D8F4556222E764DD] PRIMARY KEY CLUSTERED 
(
	[sgd_einv_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_eit_items]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_eit_items](
	[sgd_eit_codigo] [numeric](18, 0) NOT NULL,
	[sgd_eit_cod_padre] [varchar](4) NULL,
	[sgd_eit_nombre] [varchar](40) NULL,
	[sgd_eit_sigla] [varchar](4) NULL,
	[codi_dpto] [numeric](4, 0) NULL,
	[codi_muni] [numeric](5, 0) NULL,
 CONSTRAINT [PK__sgd_eit___B84752EA3EC79B9B] PRIMARY KEY CLUSTERED 
(
	[sgd_eit_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_enve_envioespecial]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_enve_envioespecial](
	[sgd_fenv_codigo] [numeric](4, 0) NULL,
	[sgd_enve_valorl] [varchar](30) NULL,
	[sgd_enve_valorn] [varchar](30) NULL,
	[sgd_enve_desc] [varchar](30) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_exp_expediente]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_exp_expediente](
	[sgd_exp_numero] [varchar](30) NOT NULL,
	[radi_nume_radi] [numeric](38, 0) NOT NULL,
	[sgd_exp_fech] [datetime] NULL,
	[sgd_exp_fech_mod] [datetime] NULL,
	[depe_codi] [numeric](6, 0) NULL,
	[usua_codi] [numeric](4, 0) NULL,
	[usua_doc] [varchar](15) NULL,
	[sgd_exp_estado] [numeric](1, 0) NULL,
	[sgd_exp_titulo] [varchar](50) NULL,
	[sgd_exp_asunto] [varchar](300) NULL,
	[sgd_exp_carpeta] [varchar](30) NULL,
	[sgd_exp_ufisica] [varchar](20) NULL,
	[sgd_exp_isla] [varchar](10) NULL,
	[sgd_exp_estante] [varchar](10) NULL,
	[sgd_exp_caja] [varchar](10) NULL,
	[sgd_exp_fech_arch] [date] NULL,
	[sgd_srd_codigo] [numeric](3, 0) NULL,
	[sgd_sbrd_codigo] [numeric](3, 0) NULL,
	[sgd_fexp_codigo] [numeric](3, 0) NULL,
	[sgd_exp_archivo] [numeric](1, 0) NULL,
	[sgd_exp_unicon] [numeric](1, 0) NULL,
	[sgd_exp_fechfin] [datetime] NULL,
	[sgd_exp_folios] [varchar](4) NULL,
	[sgd_exp_rete] [numeric](2, 0) NULL,
	[sgd_exp_entrepa] [numeric](2, 0) NULL,
	[radi_usua_arch] [varchar](40) NULL,
	[sgd_exp_edificio] [numeric](5, 0) NULL,
	[sgd_exp_caja_bodega] [varchar](40) NULL,
	[sgd_exp_carro] [varchar](40) NULL,
	[sgd_exp_carpeta_bodega] [varchar](40) NULL,
	[sgd_exp_privado] [numeric](1, 0) NULL,
	[sgd_exp_cd] [varchar](10) NULL,
	[sgd_exp_nref] [varchar](7) NULL,
	[sgd_exp_fechafin] [datetime] NULL,
	[sgd_exp_subexpediente] [varchar](50) NULL,
 CONSTRAINT [PK__sgd_exp___6D2CA041F8971427] PRIMARY KEY CLUSTERED 
(
	[sgd_exp_numero] ASC,
	[radi_nume_radi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_fenv_frmenvio]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_fenv_frmenvio](
	[sgd_fenv_codigo] [numeric](5, 0) NOT NULL,
	[sgd_fenv_descrip] [varchar](100) NULL,
	[sgd_fenv_estado] [numeric](1, 0) NOT NULL,
	[sgd_fenv_planilla] [numeric](1, 0) NOT NULL,
	[sgd_fenv_sipost] [numeric](3, 0) NULL,
	[depe_codi] [numeric](8, 0) NULL,
 CONSTRAINT [PK__sgd_fenv__22A5BAEEB79F8C46] PRIMARY KEY CLUSTERED 
(
	[sgd_fenv_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_fexp_flujoexpedientes]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_fexp_flujoexpedientes](
	[sgd_fexp_codigo] [numeric](6, 0) NOT NULL,
	[sgd_pexp_codigo] [numeric](6, 0) NULL,
	[sgd_fexp_orden] [numeric](4, 0) NULL,
	[sgd_fexp_terminos] [numeric](4, 0) NULL,
	[sgd_fexp_imagen] [varchar](50) NULL,
	[sgd_fexp_descrip] [varchar](50) NULL,
 CONSTRAINT [PK__sgd_fexp__556008DFA7091B60] PRIMARY KEY CLUSTERED 
(
	[sgd_fexp_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_fmenv_eventos]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_fmenv_eventos](
	[sgd_fmenv_codigo] [int] NOT NULL,
	[sgd_menv_codigo] [int] NULL,
	[sgd_fenv_codigo] [numeric](5, 0) NULL,
	[alias] [varchar](35) NULL,
	[estado] [bit] NULL,
	[valor] [varchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_hfld_histflujodoc]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_hfld_histflujodoc](
	[sgd_hfld_codigo] [numeric](6, 0) NULL,
	[sgd_fexp_codigo] [numeric](3, 0) NOT NULL,
	[sgd_exp_fechflujoant] [datetime] NULL,
	[sgd_hfld_fech] [datetime] NULL,
	[sgd_exp_numero] [varchar](30) NULL,
	[radi_nume_radi] [numeric](38, 0) NULL,
	[usua_doc] [varchar](15) NULL,
	[usua_codi] [numeric](10, 0) NULL,
	[depe_codi] [numeric](6, 0) NULL,
	[sgd_ttr_codigo] [numeric](3, 0) NULL,
	[sgd_fexp_observa] [varchar](500) NULL,
	[sgd_hfld_observa] [varchar](500) NULL,
	[sgd_fars_codigo] [numeric](18, 0) NULL,
	[sgd_hfld_automatico] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_hist_img_anex_exp]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_hist_img_anex_exp](
	[id_hiae] [int] IDENTITY(1,1) NOT NULL,
	[anexos_exp_id] [int] NOT NULL,
	[ruta] [varchar](150) NOT NULL,
	[usua_doc] [varchar](14) NOT NULL,
	[usua_login] [varchar](24) NOT NULL,
	[fecha] [datetime] NOT NULL,
	[id_ttr_hian] [numeric](3, 0) NOT NULL,
	[estado] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_hiae] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_hmtd_hismatdoc]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_hmtd_hismatdoc](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SGD_HMTD_FECHA] [datetime] NOT NULL,
	[RADI_NUME_RADI] [numeric](15, 0) NOT NULL,
	[USUA_CODI] [numeric](10, 0) NOT NULL,
	[SGD_HMTD_OBSE] [varchar](600) NOT NULL,
	[USUA_DOC] [numeric](13, 0) NULL,
	[DEPE_CODI] [int] NULL,
	[SGD_TTR_CODIGO] [numeric](3, 0) NOT NULL,
	[SGD_MRD_CODIGO] [smallint] NOT NULL,
	[SGD_TPR_CODIGO] [smallint] NULL,
 CONSTRAINT [PK_SGD_HMTD_HISMATDOC] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_iexp_metainfoexpediente]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_iexp_metainfoexpediente](
	[sgd_iexp_codigo] [numeric](18, 0) NOT NULL,
	[sgd_exp_numero] [varchar](18) NULL,
	[sgd_cexp_codigo] [numeric](18, 0) NULL,
	[sgd_iexp_valor] [varchar](100) NULL,
 CONSTRAINT [PK_sgd_iexp_metainfoexpediente] PRIMARY KEY CLUSTERED 
(
	[sgd_iexp_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_inf_infpob]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_inf_infpob](
	[id_infpob] [int] NOT NULL,
	[sgd_infpob_desc] [varchar](120) NOT NULL,
	[sgd_infpob_activo] [tinyint] NOT NULL,
 CONSTRAINT [PK_INFPOB] PRIMARY KEY CLUSTERED 
(
	[id_infpob] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_masiva_excel]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_masiva_excel](
	[sgd_masiva_dependencia] [numeric](3, 0) NULL,
	[sgd_masiva_usuario] [numeric](10, 0) NULL,
	[sgd_masiva_tiporadicacion] [numeric](1, 0) NULL,
	[sgd_masiva_codigo] [numeric](15, 1) NOT NULL,
	[sgd_masiva_radicada] [numeric](1, 0) NULL,
	[sgd_masiva_intervalo] [numeric](3, 0) NULL,
	[sgd_masiva_rangoini] [varchar](15) NULL,
	[sgd_masiva_rangofin] [varchar](15) NULL,
 CONSTRAINT [PK__sgd_masi__3AEACEAF4A5105DB] PRIMARY KEY CLUSTERED 
(
	[sgd_masiva_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_men_menu]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_men_menu](
	[sgd_men_codigo] [int] NOT NULL,
	[sgd_men_nombre] [varchar](50) NULL,
	[sgd_men_perm] [varchar](100) NULL,
	[sgd_men_permvalor] [int] NULL,
	[sgd_men_categoria] [int] NULL,
	[sgd_men_url] [varchar](500) NULL,
 CONSTRAINT [PK__sgd_men___38990844C68EAF34] PRIMARY KEY CLUSTERED 
(
	[sgd_men_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_menv_metodos]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_menv_metodos](
	[sgd_menv_codigo] [int] NOT NULL,
	[tipo_metodo] [varchar](30) NULL,
	[query_campo] [varchar](35) NULL,
 CONSTRAINT [PK__sgd_menv__1075E8AA3A82D6DA] PRIMARY KEY CLUSTERED 
(
	[sgd_menv_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_mexp_metadataexp]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_mexp_metadataexp](
	[sgd_mexp_codigo] [numeric](18, 0) NOT NULL,
	[sgd_srd_codigo] [numeric](18, 0) NULL,
	[sgd_sbrd_codigo] [numeric](18, 0) NULL,
	[depe_codi] [numeric](6, 0) NULL,
	[sgd_mexp_nombre] [varchar](50) NULL,
	[sgd_mexp_estado] [numeric](18, 0) NULL,
	[sgd_mexp_busq] [numeric](18, 0) NULL,
	[sgd_mexp_desc] [varchar](150) NULL,
 CONSTRAINT [PK_sgd_mexp_metadataexp] PRIMARY KEY CLUSTERED 
(
	[sgd_mexp_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_mod_modules]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_mod_modules](
	[sgd_mod_id] [int] NOT NULL,
	[sgd_mod_modulo] [varchar](50) NOT NULL,
	[sgd_mod_valmax] [int] NOT NULL,
	[sgd_mod_detalles] [varchar](100) NULL,
	[sgd_mod_path] [varchar](100) NULL,
	[sgd_mod_titulo] [varchar](100) NULL,
	[sgd_mod_menu] [varchar](150) NULL,
	[sgd_mod_estado] [int] NULL,
	[sgd_mod_nivel] [int] NULL,
 CONSTRAINT [PK__sgd_mod___0C1CDA24FA920C0D] PRIMARY KEY CLUSTERED 
(
	[sgd_mod_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_mod_values]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_mod_values](
	[id_mod_value] [int] IDENTITY(1,1) NOT NULL,
	[sgd_mod_id] [int] NULL,
	[sgd_mod_value] [numeric](18, 0) NULL,
	[sgd_mod_descrip] [varchar](100) NULL,
 CONSTRAINT [PK__sgd_mod___DC49B5B10CDAC2A6] PRIMARY KEY CLUSTERED 
(
	[id_mod_value] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_mot_motivos]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_mot_motivos](
	[sgd_mot_id] [int] NOT NULL,
	[sgd_mot_descrp] [varchar](150) NULL,
	[sgd_mot_tpaplica] [int] NULL,
	[sgd_mot_arch] [smallint] NULL,
 CONSTRAINT [PK__sgd_mot___A751B765A706E478] PRIMARY KEY CLUSTERED 
(
	[sgd_mot_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_mpes_mddpeso]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_mpes_mddpeso](
	[sgd_mpes_codigo] [numeric](5, 0) NOT NULL,
	[sgd_mpes_descrip] [varchar](10) NULL,
 CONSTRAINT [PK__sgd_mpes__5FA8CBDA4B959F44] PRIMARY KEY CLUSTERED 
(
	[sgd_mpes_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_mrd_matrird]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_mrd_matrird](
	[sgd_mrd_codigo] [numeric](5, 0) NOT NULL,
	[depe_codi] [numeric](6, 0) NOT NULL,
	[sgd_srd_codigo] [numeric](4, 0) NOT NULL,
	[sgd_sbrd_codigo] [numeric](4, 0) NOT NULL,
	[sgd_tpr_codigo] [numeric](4, 0) NOT NULL,
	[soporte] [varchar](10) NULL,
	[sgd_mrd_fechini] [datetime] NULL,
	[sgd_mrd_fechfin] [datetime] NULL,
	[sgd_mrd_esta] [varchar](10) NULL,
	[sgd_mrd_esta_exp] [int] NULL,
	[sgd_mrd_esta_radi] [int] NULL,
	[sgd_srd_version] [int] NULL,
	[sgd_sbrd_version] [int] NULL,
	[sgd_vmrd_codigo] [int] NULL,
	[sgd_categoria] [int] NULL,
	[sgd_sbrd_procedi] [varchar](1500) NULL,
	[depe_origen] [numeric](6, 0) NULL,
 CONSTRAINT [PK__sgd_mrd___ABF7C4702ABE52A9] PRIMARY KEY CLUSTERED 
(
	[sgd_mrd_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_sgd_mrd_matrird] UNIQUE NONCLUSTERED 
(
	[depe_codi] ASC,
	[sgd_sbrd_codigo] ASC,
	[sgd_srd_codigo] ASC,
	[sgd_tpr_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_sgd_mrd_matrird] UNIQUE NONCLUSTERED 
(
	[sgd_sbrd_codigo] ASC,
	[sgd_srd_codigo] ASC,
	[sgd_tpr_codigo] ASC,
	[depe_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_msdep_msgdep]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_msdep_msgdep](
	[sgd_msdep_codi] [numeric](15, 0) NOT NULL,
	[depe_codi] [numeric](6, 0) NOT NULL,
	[sgd_msg_codi] [numeric](15, 0) NOT NULL,
 CONSTRAINT [PK_sgd_msdep_msgdep] PRIMARY KEY CLUSTERED 
(
	[sgd_msdep_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_mvr_modvsrol]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_mvr_modvsrol](
	[sgd_rol_id] [int] NOT NULL,
	[sgd_mod_id] [int] NOT NULL,
 CONSTRAINT [PK_sgd_mvr_modvsrol] PRIMARY KEY CLUSTERED 
(
	[sgd_rol_id] ASC,
	[sgd_mod_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_noh_nohabiles]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_noh_nohabiles](
	[noh_fecha] [datetime] NOT NULL,
 CONSTRAINT [PK__sgd_noh___73E9029BC4C8601B] PRIMARY KEY CLUSTERED 
(
	[noh_fecha] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_not_notificacion]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_not_notificacion](
	[sgd_not_codi] [numeric](3, 0) NOT NULL,
	[sgd_not_descrip] [varchar](100) NOT NULL,
 CONSTRAINT [PK__sgd_not___C0D90E509C43D2ED] PRIMARY KEY CLUSTERED 
(
	[sgd_not_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_oem_oempresas]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_oem_oempresas](
	[sgd_oem_codigo] [numeric](8, 0) NOT NULL,
	[tdid_codi] [numeric](2, 0) NULL,
	[sgd_oem_oempresa] [varchar](250) NULL,
	[sgd_oem_rep_legal] [varchar](250) NULL,
	[sgd_oem_nit] [varchar](28) NULL,
	[sgd_oem_sigla] [varchar](50) NULL,
	[muni_codi] [numeric](9, 0) NULL,
	[dpto_codi] [numeric](4, 0) NULL,
	[sgd_oem_direccion] [varchar](250) NULL,
	[sgd_oem_telefono] [varchar](150) NULL,
	[id_cont] [numeric](2, 0) NULL,
	[id_pais] [numeric](4, 0) NULL,
	[sgd_oem_email] [varchar](250) NULL,
	[sgd_oem_estado] [int] NULL,
	[sgd_tipo_camp] [int] NULL,
	[id_centropoblado] [varchar](10) NULL,
	[id_persona] [varchar](100) NULL,
	[id_direccion] [varchar](100) NULL,
 CONSTRAINT [PK__sgd_oem___148681335ECAFA87] PRIMARY KEY CLUSTERED 
(
	[sgd_oem_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_panu_peranulados]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_panu_peranulados](
	[sgd_panu_codi] [numeric](4, 0) NOT NULL,
	[sgd_panu_desc] [varchar](200) NULL,
 CONSTRAINT [PK__sgd_panu__1DFB2EC25D5D0063] PRIMARY KEY CLUSTERED 
(
	[sgd_panu_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_parametro]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_parametro](
	[param_nomb] [varchar](25) NOT NULL,
	[param_codi] [numeric](5, 0) NOT NULL,
	[param_valor] [varchar](25) NOT NULL,
 CONSTRAINT [PK_sgd_parametro] PRIMARY KEY CLUSTERED 
(
	[param_nomb] ASC,
	[param_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_parexp_paramexpediente]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_parexp_paramexpediente](
	[sgd_parexp_codigo] [numeric](4, 0) NOT NULL,
	[depe_codi] [numeric](6, 0) NOT NULL,
	[sgd_parexp_tabla] [varchar](30) NOT NULL,
	[sgd_parexp_etiqueta] [varchar](15) NOT NULL,
	[sgd_parexp_orden] [numeric](1, 0) NULL,
 CONSTRAINT [PK__sgd_pare__F351384B2FEC1770] PRIMARY KEY CLUSTERED 
(
	[sgd_parexp_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_pexp_procexpedientes]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_pexp_procexpedientes](
	[sgd_pexp_codigo] [numeric](18, 0) NOT NULL,
	[sgd_pexp_descrip] [varchar](100) NULL,
	[sgd_pexp_terminos] [numeric](18, 0) NULL,
	[sgd_srd_codigo] [numeric](3, 0) NULL,
	[sgd_sbrd_codigo] [numeric](3, 0) NULL,
	[sgd_pexp_automatico] [numeric](1, 0) NULL,
	[sgd_pexp_tieneflujo] [numeric](18, 0) NULL,
 CONSTRAINT [PK__sgd_pexp__2BCAD6517ACC1F5A] PRIMARY KEY CLUSTERED 
(
	[sgd_pexp_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_pmrd_procedi]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_pmrd_procedi](
	[sgd_pmrd_codigo] [int] NOT NULL,
	[sgd_sbrd_version] [int] NOT NULL,
	[depe_codi] [numeric](6, 0) NOT NULL,
	[sgd_sbrd_procedi] [varchar](1500) NULL,
	[sgd_sbrd_tiemag] [numeric](4, 0) NULL,
	[sgd_sbrd_tiemac] [numeric](4, 0) NULL,
	[sgd_sbrd_dispfin] [varchar](5) NULL,
	[sgd_categoria] [int] NULL,
 CONSTRAINT [PK__sgd_pmrd__25EF21467230F713] PRIMARY KEY CLUSTERED 
(
	[sgd_pmrd_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_pqr_metadata]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_pqr_metadata](
	[radi_nume_radi] [numeric](18, 0) NOT NULL,
	[sgd_fenv_modalidad] [varchar](1) NOT NULL,
	[id_infpob] [int] NULL,
	[pqrverbal] [tinyint] NULL,
PRIMARY KEY CLUSTERED 
(
	[radi_nume_radi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_pqr_temausu]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_pqr_temausu](
	[sgd_pqr_codigo] [int] IDENTITY(1,1) NOT NULL,
	[sgd_dcau_codigo] [numeric](4, 0) NOT NULL,
	[depe_codi] [int] NOT NULL,
	[usua_codi] [bigint] NOT NULL,
 CONSTRAINT [PK_PQR_CODIGO] PRIMARY KEY CLUSTERED 
(
	[sgd_pqr_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_rda_retdoca]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_rda_retdoca](
	[anex_radi_nume] [numeric](15, 0) NOT NULL,
	[anex_codigo] [varchar](20) NOT NULL,
	[radi_nume_salida] [numeric](15, 0) NULL,
	[anex_borrado] [varchar](1) NULL,
	[sgd_mrd_codigo] [numeric](4, 0) NOT NULL,
	[depe_codi] [numeric](5, 0) NOT NULL,
	[usua_codi] [numeric](10, 0) NOT NULL,
	[usua_doc] [varchar](14) NOT NULL,
	[sgd_rda_fech] [datetime] NULL,
	[sgd_deve_codigo] [numeric](2, 0) NULL,
	[anex_solo_lect] [varchar](1) NULL,
	[anex_radi_fech] [datetime] NULL,
	[anex_estado] [numeric](1, 0) NULL,
	[anex_nomb_archivo] [varchar](50) NULL,
	[anex_tipo] [numeric](4, 0) NULL,
	[sgd_dir_tipo] [numeric](4, 0) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_rdf_retdocf]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_rdf_retdocf](
	[sgd_mrd_codigo] [numeric](5, 0) NOT NULL,
	[radi_nume_radi] [numeric](38, 0) NOT NULL,
	[depe_codi] [numeric](6, 0) NOT NULL,
	[usua_codi] [numeric](10, 0) NOT NULL,
	[usua_doc] [varchar](14) NOT NULL,
	[sgd_rdf_fech] [datetime] NOT NULL,
 CONSTRAINT [IX_sgd_rdf_retdocf] UNIQUE NONCLUSTERED 
(
	[sgd_mrd_codigo] ASC,
	[depe_codi] ASC,
	[radi_nume_radi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_renv_regenvio]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_renv_regenvio](
	[sgd_renv_codigo] [numeric](6, 0) NOT NULL,
	[sgd_fenv_codigo] [numeric](5, 0) NULL,
	[sgd_renv_fech] [datetime] NULL,
	[radi_nume_sal] [numeric](38, 0) NULL,
	[sgd_renv_destino] [varchar](150) NULL,
	[sgd_renv_telefono] [varchar](50) NULL,
	[sgd_renv_mail] [varchar](150) NULL,
	[sgd_renv_peso] [varchar](10) NULL,
	[sgd_renv_valor] [varchar](10) NULL,
	[sgd_renv_certificado] [numeric](1, 0) NULL,
	[sgd_renv_estado] [numeric](1, 0) NULL,
	[usua_doc] [numeric](15, 0) NULL,
	[sgd_renv_nombre] [varchar](100) NULL,
	[sgd_rem_destino] [numeric](1, 0) NULL,
	[sgd_dir_codigo] [numeric](10, 0) NULL,
	[sgd_renv_planilla] [varchar](38) NULL,
	[sgd_renv_fech_sal] [datetime] NULL,
	[depe_codi] [numeric](6, 0) NULL,
	[sgd_dir_tipo] [numeric](4, 0) NULL,
	[radi_nume_grupo] [numeric](38, 0) NULL,
	[sgd_renv_dir] [varchar](100) NULL,
	[sgd_renv_depto] [varchar](40) NULL,
	[sgd_renv_mpio] [varchar](80) NULL,
	[sgd_renv_tel] [varchar](20) NULL,
	[sgd_renv_cantidad] [numeric](4, 0) NULL,
	[sgd_renv_tipo] [numeric](1, 0) NULL,
	[sgd_renv_observa] [varchar](200) NULL,
	[sgd_renv_grupo] [numeric](14, 0) NULL,
	[sgd_deve_codigo] [numeric](2, 0) NULL,
	[sgd_deve_fech] [datetime] NULL,
	[sgd_renv_valortotal] [varchar](14) NULL,
	[sgd_renv_valistamiento] [varchar](10) NULL,
	[sgd_renv_vdescuento] [varchar](10) NULL,
	[sgd_renv_vadicional] [varchar](14) NULL,
	[sgd_depe_genera] [numeric](5, 0) NULL,
	[sgd_renv_pais] [varchar](30) NULL,
	[sgd_renv_sipostplan] [varchar](20) NULL,
	[sgd_renv_guia] [varchar](20) NULL,
	[id] [int] NULL,
	[sgd_renv_precinto] [varchar](20) NULL,
	[sgd_renv_tula] [int] NULL,
	[sgd_renv_pesot] [int] NULL,
	[sgd_renv_depe_dest] [numeric](6, 0) NULL,
	[sgd_renv_numguia] [varchar](38) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_rg_multiple]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_rg_multiple](
	[radi_nume_radi] [numeric](18, 0) NOT NULL,
	[usuario] [decimal](10, 0) NOT NULL,
	[area] [decimal](10, 0) NOT NULL,
	[estatus] [varchar](50) NULL,
	[fechafinalizado] [datetime] NULL,
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[fechainicio] [datetime] NULL,
	[radi_leido] [bit] NULL,
	[usua_doc] [varchar](14) NULL,
 CONSTRAINT [PK_PENDIENTES_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_rmr_radmasivre]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_rmr_radmasivre](
	[sgd_rmr_grupo] [numeric](15, 0) NOT NULL,
	[sgd_rmr_radi] [numeric](15, 0) NOT NULL,
 CONSTRAINT [PK_sgd_rmr_radmasivre] PRIMARY KEY CLUSTERED 
(
	[sgd_rmr_grupo] ASC,
	[sgd_rmr_radi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_rol_roles]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_rol_roles](
	[sgd_rol_id] [int] NOT NULL,
	[sgd_rol_nombre] [varchar](100) NOT NULL,
	[sgd_rol_estado] [int] NULL,
 CONSTRAINT [id_rol] PRIMARY KEY CLUSTERED 
(
	[sgd_rol_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_sbrd_subserierd]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_sbrd_subserierd](
	[sgd_srd_codigo] [numeric](4, 0) NOT NULL,
	[sgd_sbrd_codigo] [numeric](4, 0) NOT NULL,
	[sgd_sbrd_descrip] [varchar](400) NOT NULL,
	[sgd_sbrd_fechini] [datetime] NOT NULL,
	[sgd_sbrd_fechfin] [datetime] NOT NULL,
	[sgd_sbrd_tiemag] [numeric](4, 0) NULL,
	[sgd_sbrd_tiemac] [numeric](4, 0) NULL,
	[sgd_sbrd_dispfin] [varchar](50) NULL,
	[sgd_sbrd_soporte] [varchar](50) NULL,
	[sgd_sbrd_procedi] [varchar](3000) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[sgd_srd_version] [int] NOT NULL,
	[sgd_sbrd_version] [int] NOT NULL,
 CONSTRAINT [PK__sgd_sbrd__3213E83F49DAFB50] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_sgd_sbrd_subserierd] UNIQUE NONCLUSTERED 
(
	[sgd_srd_codigo] ASC,
	[sgd_sbrd_codigo] ASC,
	[sgd_sbrd_version] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_sgd_sbrd_subserierd_1] UNIQUE NONCLUSTERED 
(
	[sgd_sbrd_version] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_seg_seguridad]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_seg_seguridad](
	[id] [bigint] NOT NULL,
	[usua_codi] [int] NULL,
	[unic_id] [varchar](50) NULL,
	[fecha] [datetime] NULL,
	[radi_nume_radi] [numeric](38, 0) NULL,
 CONSTRAINT [PK_sgd_seg_seguridad] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_sexp_secexpedientes]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_sexp_secexpedientes](
	[sgd_exp_numero] [varchar](30) NOT NULL,
	[sgd_srd_codigo] [numeric](4, 0) NULL,
	[sgd_sbrd_codigo] [numeric](4, 0) NULL,
	[sgd_sexp_secuencia] [int] NULL,
	[depe_codi] [numeric](18, 0) NULL,
	[usua_doc] [varchar](15) NULL,
	[sgd_sexp_fech] [datetime] NULL,
	[sgd_fexp_codigo] [numeric](18, 0) NULL,
	[sgd_sexp_ano] [numeric](18, 0) NULL,
	[usua_doc_responsable] [varchar](18) NULL,
	[sgd_sexp_parexp1] [varchar](550) NULL,
	[sgd_sexp_parexp2] [varchar](160) NULL,
	[sgd_sexp_parexp3] [varchar](160) NULL,
	[sgd_sexp_parexp4] [varchar](160) NULL,
	[sgd_sexp_parexp5] [varchar](160) NULL,
	[sgd_pexp_codigo] [numeric](3, 0) NOT NULL,
	[sgd_exp_fech_arch] [datetime] NULL,
	[sgd_fld_codigo] [numeric](3, 0) NULL,
	[sgd_exp_fechflujoant] [datetime] NULL,
	[sgd_mrd_codigo] [numeric](4, 0) NULL,
	[sgd_exp_subexpediente] [numeric](18, 0) NULL,
	[sgd_exp_privado] [numeric](1, 0) NULL,
	[sgd_sexp_fechafin] [datetime] NULL,
	[sgd_sexp_metadato] [numeric](18, 0) NULL,
	[sgd_sexp_estado] [int] NULL,
	[sgd_srd_version] [int] NULL,
	[sgd_sbrd_version] [int] NULL,
	[sgd_sexp_id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_sgd_sexp_secexpedientes_1] PRIMARY KEY CLUSTERED 
(
	[sgd_exp_numero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_sgd_sexp_secexpedientes] UNIQUE NONCLUSTERED 
(
	[sgd_sexp_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_srd_seriesrd]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_srd_seriesrd](
	[sgd_srd_codigo] [numeric](4, 0) NOT NULL,
	[sgd_srd_descrip] [varchar](130) NOT NULL,
	[sgd_srd_fechini] [datetime] NOT NULL,
	[sgd_srd_fechfin] [datetime] NOT NULL,
	[sgd_srd_version] [int] NOT NULL,
 CONSTRAINT [IX_sgd_srd_seriesrd] UNIQUE NONCLUSTERED 
(
	[sgd_srd_version] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_tar_tarifas]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_tar_tarifas](
	[sgd_fenv_codigo] [numeric](5, 0) NULL,
	[sgd_tar_codser] [numeric](5, 0) NULL,
	[sgd_tar_codigo] [numeric](5, 0) NULL,
	[sgd_tar_valenv1] [numeric](15, 0) NULL,
	[sgd_tar_valenv2] [numeric](15, 0) NULL,
	[sgd_tar_valenv1g1] [numeric](15, 0) NULL,
	[sgd_clta_codser] [numeric](5, 0) NULL,
	[sgd_tar_valenv2g2] [numeric](15, 0) NULL,
	[sgd_clta_descrip] [varchar](100) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_temas_tiposdoc]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_temas_tiposdoc](
	[sgd_temtdoc_codigo] [int] IDENTITY(1,1) NOT NULL,
	[sgd_dcau_codigo] [numeric](4, 0) NOT NULL,
	[sgd_tpr_codigo] [smallint] NOT NULL,
	[sgd_temtdoc_estado] [smallint] NOT NULL,
 CONSTRAINT [PK_TEMTDOC] PRIMARY KEY CLUSTERED 
(
	[sgd_temtdoc_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_tidm_tidocmasiva]    Script Date: 5/09/2019 2:12:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_tidm_tidocmasiva](
	[sgd_tidm_codi] [numeric](4, 0) NOT NULL,
	[sgd_tidm_desc] [varchar](150) NULL,
 CONSTRAINT [PK__sgd_tidm__E172AE164A344B02] PRIMARY KEY CLUSTERED 
(
	[sgd_tidm_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_tip3_tipotercero]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_tip3_tipotercero](
	[sgd_tip3_codigo] [numeric](2, 0) NOT NULL,
	[sgd_dir_tipo] [numeric](4, 0) NULL,
	[sgd_tip3_nombre] [varchar](15) NULL,
	[sgd_tip3_desc] [varchar](30) NULL,
	[sgd_tip3_imgpestana] [varchar](20) NULL,
	[sgd_tpr_tp1] [numeric](1, 0) NULL,
	[sgd_tpr_tp2] [numeric](1, 0) NULL,
	[sgd_tpr_tp3] [numeric](1, 0) NULL,
	[sgd_tpr_tp8] [smallint] NULL,
	[sgd_tpr_tp4] [numeric](1, 0) NULL,
	[sgd_tpr_tp5] [numeric](1, 0) NULL,
	[sgd_tpr_tp6] [numeric](1, 0) NULL,
	[sgd_tpr_tp9] [numeric](1, 0) NULL,
	[sgd_tpr_tp7] [numeric](1, 0) NULL,
 CONSTRAINT [PK__sgd_tip3__5CB44BC156473C0C] PRIMARY KEY CLUSTERED 
(
	[sgd_tip3_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_tmp_depe]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_tmp_depe](
	[depe_codi] [numeric](6, 0) NULL,
	[usua_codi] [numeric](6, 0) NOT NULL,
 CONSTRAINT [IX_sgd_tmp_depe] UNIQUE NONCLUSTERED 
(
	[depe_codi] ASC,
	[usua_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_tpr_tpdcumento]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_tpr_tpdcumento](
	[sgd_tpr_codigo] [numeric](4, 0) NOT NULL,
	[sgd_tpr_descrip] [varchar](500) NULL,
	[sgd_tpr_termino] [numeric](4, 0) NULL,
	[sgd_tpr_tpuso] [numeric](1, 0) NULL,
	[sgd_tpr_numera] [char](1) NULL,
	[sgd_tpr_radica] [char](1) NULL,
	[sgd_tpr_tp3] [numeric](1, 0) NULL,
	[sgd_tpr_tp1] [numeric](1, 0) NULL,
	[sgd_tpr_tp2] [numeric](1, 0) NULL,
	[sgd_tpr_estado] [numeric](1, 0) NULL,
	[sgd_termino_real] [numeric](4, 0) NULL,
	[sgd_tpr_tp6] [numeric](1, 0) NULL,
	[sgd_tpr_web] [numeric](1, 0) NULL,
	[sgd_tpr_tp8] [smallint] NULL,
	[sgd_tpr_tp4] [numeric](1, 0) NULL,
	[sgd_tpr_tp5] [numeric](1, 0) NULL,
	[sgd_tpr_tp7] [numeric](1, 0) NULL,
	[sgd_tpr_tp9] [numeric](1, 0) NULL,
	[sgd_pqr_label] [varchar](50) NULL,
	[sgd_pqr_descrip] [varchar](500) NULL,
	[sgd_tpr_alerta] [numeric](18, 0) NULL,
	[sgd_tpr_reqresp] [tinyint] NULL,
 CONSTRAINT [PK__sgd_tpr___E46631BA85963016] PRIMARY KEY CLUSTERED 
(
	[sgd_tpr_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_trad_tiporad]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_trad_tiporad](
	[sgd_trad_codigo] [numeric](2, 0) NOT NULL,
	[sgd_trad_descr] [varchar](30) NULL,
	[sgd_trad_icono] [varchar](30) NULL,
	[sgd_trad_genradsal] [numeric](1, 0) NULL,
	[sgd_trad_gencodverif] [numeric](1, 0) NULL,
 CONSTRAINT [PK__sgd_trad__C9865B4973508CE2] PRIMARY KEY CLUSTERED 
(
	[sgd_trad_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_ttr_transaccion]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_ttr_transaccion](
	[sgd_ttr_codigo] [numeric](3, 0) NOT NULL,
	[sgd_ttr_descrip] [varchar](100) NOT NULL,
 CONSTRAINT [pk_sgd_ttr_transaccion] PRIMARY KEY CLUSTERED 
(
	[sgd_ttr_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_urd_usuaroldep]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_urd_usuaroldep](
	[usua_codi] [numeric](6, 0) NOT NULL,
	[depe_codi] [int] NOT NULL,
	[rol_codi] [int] NOT NULL,
 CONSTRAINT [primary  key] PRIMARY KEY CLUSTERED 
(
	[usua_codi] ASC,
	[depe_codi] ASC,
	[rol_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_ush_usuhistorico]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_ush_usuhistorico](
	[sgd_ush_admcod] [numeric](10, 0) NOT NULL,
	[sgd_ush_admdep] [numeric](5, 0) NOT NULL,
	[sgd_ush_admdoc] [varchar](14) NOT NULL,
	[sgd_ush_usucod] [numeric](10, 0) NOT NULL,
	[sgd_ush_usudep] [numeric](5, 0) NOT NULL,
	[sgd_ush_usudoc] [varchar](14) NOT NULL,
	[sgd_ush_modcod] [numeric](5, 0) NOT NULL,
	[sgd_ush_fechevento] [datetime] NOT NULL,
	[sgd_ush_usulogin] [varchar](20) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_usm_usumodifica]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_usm_usumodifica](
	[sgd_usm_modcod] [numeric](5, 0) NOT NULL,
	[sgd_usm_moddescr] [varchar](60) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_vmrd_matrivrd]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_vmrd_matrivrd](
	[sgd_vmrd_codigo] [int] NOT NULL,
	[sgd_sbrd_version] [int] NOT NULL,
	[depe_codi] [numeric](6, 0) NOT NULL,
	[sgd_vmrd_origen] [int] NOT NULL,
	[sgd_vmrd_crea] [datetime] NOT NULL,
 CONSTRAINT [PK__sgd_vmrd__25EF21467230F713] PRIMARY KEY CLUSTERED 
(
	[sgd_vmrd_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sgd_vtrd_verstrd]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sgd_vtrd_verstrd](
	[sgd_vtrd_codigo] [int] NOT NULL,
	[sgd_srd_codigo] [numeric](4, 0) NULL,
	[sgd_sbrd_codigo] [numeric](4, 0) NULL,
	[sgd_vtrd_crea] [datetime] NULL,
	[sgd_vtrd_modifica] [datetime] NULL,
 CONSTRAINT [PK__sgd_vtrd__6239FCAF017C82E2] PRIMARY KEY CLUSTERED 
(
	[sgd_vtrd_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tipo_doc_identificacion]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tipo_doc_identificacion](
	[tdid_codi] [numeric](2, 0) NOT NULL,
	[tdid_desc] [varchar](100) NOT NULL,
 CONSTRAINT [PK__tipo_doc__D5030026EB46DFC9] PRIMARY KEY CLUSTERED 
(
	[tdid_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tipo_remitente]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tipo_remitente](
	[trte_codi] [numeric](2, 0) NOT NULL,
	[trte_desc] [varchar](100) NOT NULL,
 CONSTRAINT [PK__tipo_rem__DA8F5E27FD81D3D8] PRIMARY KEY CLUSTERED 
(
	[trte_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_security]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_security](
	[id] [numeric](8, 0) NOT NULL,
	[usua_codi] [numeric](10, 0) NULL,
	[fecha_cambio] [datetime] NULL,
	[usua_passw] [varchar](30) NULL,
	[ip_server] [varchar](32) NULL,
	[ip_client] [varchar](32) NULL,
	[sistema_operativo] [varchar](200) NULL,
 CONSTRAINT [PK__user_sec__3213E83FAD1C0143] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[usua_migracion]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usua_migracion](
	[usua_codi] [int] NULL,
	[usua_old] [int] NOT NULL,
	[depe_codi] [numeric](6, 0) NOT NULL,
	[usua_fech_crea] [datetime] NULL,
	[usua_pasw] [varchar](30) NULL,
	[usua_esta] [varchar](10) NULL,
	[usua_nomb] [varchar](50) NULL,
	[usua_nuevo] [varchar](1) NULL,
	[usua_doc] [varchar](14) NULL,
	[usua_sesion] [varchar](100) NULL,
	[usua_email] [varchar](100) NULL,
	[usua_auth_ldap] [int] NULL,
	[id_grupo] [int] NULL,
	[jefe_grupo] [int] NULL,
	[usua_politica] [int] NULL,
	[usua_login] [varchar](50) NULL,
 CONSTRAINT [PK_usua_migracion] PRIMARY KEY CLUSTERED 
(
	[usua_old] ASC,
	[depe_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[usuario]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usuario](
	[usua_codi] [int] NOT NULL,
	[usua_login] [varchar](50) NOT NULL,
	[usua_fech_crea] [datetime] NOT NULL,
	[usua_pasw] [varchar](30) NOT NULL,
	[usua_esta] [varchar](10) NOT NULL,
	[usua_nomb] [varchar](45) NULL,
	[usua_nuevo] [varchar](1) NULL,
	[usua_doc] [varchar](14) NULL,
	[usua_sesion] [varchar](100) NULL,
	[usua_fech_sesion] [datetime2](7) NULL,
	[usua_ext] [numeric](4, 0) NULL,
	[usua_nacim] [date] NULL,
	[usua_email] [varchar](100) NULL,
	[usua_auth_ldap] [int] NULL,
	[usua_pages] [int] NULL,
	[usua_login_externo] [varchar](1) NULL,
	[usua_at] [varchar](50) NULL,
	[usua_debug] [smallint] NULL,
	[usua_fech_fin] [datetime] NULL,
	[usua_vistagraphip] [int] NULL,
	[usua_pregunta] [int] NULL,
	[usua_respuesta] [varchar](100) NULL,
	[usua_segunda_clave] [varchar](60) NULL,
	[usua_politica] [int] NULL,
	[usua_categoria] [int] NULL,
	[usua_emailcorreos] [varchar](100) NULL,
	[usua_passwordemail] [varchar](500) NULL,
	[id_grupo] [int] NULL,
 CONSTRAINT [usuario_PK] PRIMARY KEY CLUSTERED 
(
	[usua_login] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ__usuario__514A558312787252] UNIQUE NONCLUSTERED 
(
	[usua_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_arch_carp_carpeta]    Script Date: 5/09/2019 2:12:33 p. m. ******/
CREATE NONCLUSTERED INDEX [IX_arch_carp_carpeta] ON [dbo].[arch_carp_carpeta]
(
	[sgd_num_expediente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_departamento]    Script Date: 5/09/2019 2:12:33 p. m. ******/
CREATE NONCLUSTERED INDEX [IX_departamento] ON [dbo].[departamento]
(
	[dpto_codi] ASC,
	[id_pais] ASC,
	[id_cont] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_informados]    Script Date: 5/09/2019 2:12:33 p. m. ******/
CREATE NONCLUSTERED INDEX [IX_informados] ON [dbo].[informados]
(
	[radi_nume_radi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [in_muni_depto]    Script Date: 5/09/2019 2:12:33 p. m. ******/
CREATE UNIQUE NONCLUSTERED INDEX [in_muni_depto] ON [dbo].[municipio]
(
	[muni_codi] ASC,
	[dpto_codi] ASC,
	[id_cont] ASC,
	[id_pais] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_rad_planilla]    Script Date: 5/09/2019 2:12:33 p. m. ******/
CREATE NONCLUSTERED INDEX [IX_rad_planilla] ON [dbo].[rad_planilla]
(
	[radi_nume_radi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_rad_planilla_1]    Script Date: 5/09/2019 2:12:33 p. m. ******/
CREATE NONCLUSTERED INDEX [IX_rad_planilla_1] ON [dbo].[rad_planilla]
(
	[pla_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_rad_planilla_2]    Script Date: 5/09/2019 2:12:33 p. m. ******/
CREATE NONCLUSTERED INDEX [IX_rad_planilla_2] ON [dbo].[rad_planilla]
(
	[pla_acuse] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_rad_planilla_3]    Script Date: 5/09/2019 2:12:33 p. m. ******/
CREATE NONCLUSTERED INDEX [IX_rad_planilla_3] ON [dbo].[rad_planilla]
(
	[tip_destinatario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_radicado_listo]    Script Date: 5/09/2019 2:12:33 p. m. ******/
CREATE NONCLUSTERED INDEX [IX_radicado_listo] ON [dbo].[radicado]
(
	[listo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_radicado_numederi]    Script Date: 5/09/2019 2:12:33 p. m. ******/
CREATE NONCLUSTERED INDEX [IX_radicado_numederi] ON [dbo].[radicado]
(
	[radi_nume_deri] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [radicado_radi_fech_radi_idx]    Script Date: 5/09/2019 2:12:33 p. m. ******/
CREATE NONCLUSTERED INDEX [radicado_radi_fech_radi_idx] ON [dbo].[radicado]
(
	[radi_fech_radi] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [continente_pais]    Script Date: 5/09/2019 2:12:33 p. m. ******/
CREATE UNIQUE NONCLUSTERED INDEX [continente_pais] ON [dbo].[sgd_def_paises]
(
	[id_pais] ASC,
	[id_cont] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_sgd_tpr_tpdcumento]    Script Date: 5/09/2019 2:12:33 p. m. ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_sgd_tpr_tpdcumento] ON [dbo].[sgd_tpr_tpdcumento]
(
	[sgd_tpr_codigo] ASC,
	[sgd_tpr_termino] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [idx_vtrd_verstrd]    Script Date: 5/09/2019 2:12:33 p. m. ******/
CREATE UNIQUE NONCLUSTERED INDEX [idx_vtrd_verstrd] ON [dbo].[sgd_vtrd_verstrd]
(
	[sgd_srd_codigo] ASC,
	[sgd_sbrd_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TestTable_TestCol1]    Script Date: 5/09/2019 2:12:33 p. m. ******/
CREATE NONCLUSTERED INDEX [IX_TestTable_TestCol1] ON [dbo].[usuario]
(
	[usua_codi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[anexos] ADD  CONSTRAINT [DF__anexos__anex_ori__20C1E124]  DEFAULT ((0)) FOR [anex_origen]
GO
ALTER TABLE [dbo].[anexos] ADD  CONSTRAINT [DF__anexos__anex_sal__21B6055D]  DEFAULT ((0)) FOR [anex_salida]
GO
ALTER TABLE [dbo].[anexos] ADD  CONSTRAINT [DF__anexos__anex_est__22AA2996]  DEFAULT ((0)) FOR [anex_estado]
GO
ALTER TABLE [dbo].[anexos] ADD  CONSTRAINT [DF__anexos__sgd_rem___239E4DCF]  DEFAULT ((0)) FOR [sgd_rem_destino]
GO
ALTER TABLE [dbo].[anexos] ADD  CONSTRAINT [DF_anexos_anex_datafw]  DEFAULT ((0)) FOR [anex_datafw]
GO
ALTER TABLE [dbo].[arch_eit_items] ADD  CONSTRAINT [DF__arch_eit___arch___05D8E0BE]  DEFAULT ((0)) FOR [arch_eit_cod_padre]
GO
ALTER TABLE [dbo].[arch_tpa_tpalmacenaje] ADD  CONSTRAINT [DF__arch_tpa___arch___151B244E]  DEFAULT ((0)) FOR [arch_tpa_tamano]
GO
ALTER TABLE [dbo].[arch_tpa_tpalmacenaje] ADD  CONSTRAINT [DF__arch_tpa___arch___160F4887]  DEFAULT ((0)) FOR [arch_tpa_tipo]
GO
ALTER TABLE [dbo].[arch_uexp_ubicacionexp] ADD  CONSTRAINT [DF__arch_uexp__arch___123EB7A3]  DEFAULT ((0)) FOR [arch_uexp_ubica]
GO
ALTER TABLE [dbo].[bodega_empresas] ADD  CONSTRAINT [DF__bodega_em__id_co__0D7A0286]  DEFAULT ((1)) FOR [id_cont]
GO
ALTER TABLE [dbo].[bodega_empresas] ADD  CONSTRAINT [DF__bodega_em__id_pa__0E6E26BF]  DEFAULT ((170)) FOR [id_pais]
GO
ALTER TABLE [dbo].[bodega_empresas] ADD  CONSTRAINT [DF__bodega_em__activ__0F624AF8]  DEFAULT ((1)) FOR [activa]
GO
ALTER TABLE [dbo].[carpeta] ADD  DEFAULT ((0)) FOR [recepcion]
GO
ALTER TABLE [dbo].[departamento] ADD  CONSTRAINT [DF__departame__id_co__531856C7]  DEFAULT ((1)) FOR [id_cont]
GO
ALTER TABLE [dbo].[departamento] ADD  CONSTRAINT [DF__departame__id_pa__540C7B00]  DEFAULT ((170)) FOR [id_pais]
GO
ALTER TABLE [dbo].[depe_grupo] ADD  CONSTRAINT [DF_depe_grupo_estado]  DEFAULT ((1)) FOR [estado]
GO
ALTER TABLE [dbo].[dependencia] ADD  CONSTRAINT [DF__dependenc__id_co__164452B1]  DEFAULT ((1)) FOR [id_cont]
GO
ALTER TABLE [dbo].[dependencia] ADD  CONSTRAINT [DF__dependenc__id_pa__173876EA]  DEFAULT ((170)) FOR [id_pais]
GO
ALTER TABLE [dbo].[dependencia] ADD  CONSTRAINT [DF__dependenc__depe___182C9B23]  DEFAULT ((1)) FOR [depe_estado]
GO
ALTER TABLE [dbo].[firma_radicados] ADD  CONSTRAINT [DF_FIRMA_RADICADOS_ESTADO_FIRMA]  DEFAULT ((1)) FOR [estado_firma]
GO
ALTER TABLE [dbo].[informados] ADD  CONSTRAINT [DF__informado__info___1CBC4616]  DEFAULT ((0)) FOR [info_leido]
GO
ALTER TABLE [dbo].[informados] ADD  CONSTRAINT [DF_informados_id]  DEFAULT (NEXT VALUE FOR [dbo].[sec_informado]) FOR [id]
GO
ALTER TABLE [dbo].[menu_estadisticas] ADD  CONSTRAINT [DF__menu_estadistica__es__5DEAEAF5]  DEFAULT ((0)) FOR [menu_estado]
GO
ALTER TABLE [dbo].[menu_estadisticas] ADD  CONSTRAINT [DF__menu_estadistica__dp__5DEAEAF5]  DEFAULT ((0)) FOR [depe_codi]
GO
ALTER TABLE [dbo].[menu_estadisticas] ADD  CONSTRAINT [DF__menu_estadistica__ir__5DEAEAF5]  DEFAULT ((0)) FOR [indi_restri]
GO
ALTER TABLE [dbo].[municipio] ADD  CONSTRAINT [DF__municipio__id_co__48CFD27E]  DEFAULT ((1)) FOR [id_cont]
GO
ALTER TABLE [dbo].[municipio] ADD  CONSTRAINT [DF__municipio__id_pa__49C3F6B7]  DEFAULT ((170)) FOR [id_pais]
GO
ALTER TABLE [dbo].[municipio] ADD  CONSTRAINT [DF__municipio__activ__4AB81AF0]  DEFAULT ((1)) FOR [activa]
GO
ALTER TABLE [dbo].[municipio] ADD  CONSTRAINT [DF__municipio__tipo__4BAC3F29]  DEFAULT ((0)) FOR [tipo]
GO
ALTER TABLE [dbo].[municipio] ADD  CONSTRAINT [DF__municipio__id_ce__4CA06362]  DEFAULT ((0)) FOR [id_centropoblado]
GO
ALTER TABLE [dbo].[pdf_plantilla] ADD  CONSTRAINT [DF__pdf_plant__pdf_e__71BCD978]  DEFAULT ((0)) FOR [pdf_estado]
GO
ALTER TABLE [dbo].[pdf_plantilla] ADD  CONSTRAINT [DF__pdf_plant__pdf_d__72B0FDB1]  DEFAULT ((0)) FOR [pdf_depe_codi]
GO
ALTER TABLE [dbo].[pl_plantillas] ADD  CONSTRAINT [DF__pl_planti__pl_es__5DEAEAF5]  DEFAULT ((0)) FOR [pl_estado]
GO
ALTER TABLE [dbo].[pl_plantillas] ADD  CONSTRAINT [DF__pl_planti__depe___5EDF0F2E]  DEFAULT ((0)) FOR [depe_codi]
GO
ALTER TABLE [dbo].[pla_entrega] ADD  CONSTRAINT [DF__pla_entre__pla_e__45F365D3]  DEFAULT ((0)) FOR [pla_entrega_estado]
GO
ALTER TABLE [dbo].[pla_entrega_radi] ADD  CONSTRAINT [DF__pla_entre__pla_r__2645B050]  DEFAULT ((0)) FOR [pla_radi_estado]
GO
ALTER TABLE [dbo].[pla_entrega_radi] ADD  CONSTRAINT [DF__pla_entre__pla_f__2739D489]  DEFAULT (getdate()) FOR [pla_fech_sol]
GO
ALTER TABLE [dbo].[pla_entrega_radi] ADD  CONSTRAINT [DF__pla_entre__pla_s__282DF8C2]  DEFAULT ((0)) FOR [pla_solicitado]
GO
ALTER TABLE [dbo].[plantilla_pl] ADD  CONSTRAINT [DF__plantilla__pl_us__2A164134]  DEFAULT ((0)) FOR [pl_uso]
GO
ALTER TABLE [dbo].[rad_datatemporal] ADD  CONSTRAINT [DF_rad_datatemporal_rad_data_id]  DEFAULT (NEXT VALUE FOR [rad_datatemporal_rad_data_id_seq]) FOR [rad_data_id]
GO
ALTER TABLE [dbo].[rad_planilla] ADD  CONSTRAINT [DF__rad_plani__est_c__5FB337D6]  DEFAULT ((1)) FOR [est_codigo]
GO
ALTER TABLE [dbo].[rad_planilla] ADD  CONSTRAINT [DF__rad_plani__blo_p__60A75C0F]  DEFAULT ((1)) FOR [blo_planilla]
GO
ALTER TABLE [dbo].[rad_planilla] ADD  CONSTRAINT [DF__rad_plani__cod_e__619B8048]  DEFAULT ((0)) FOR [cod_externo]
GO
ALTER TABLE [dbo].[rad_planilla] ADD  CONSTRAINT [DF__rad_plani__tip_e__628FA481]  DEFAULT ((1)) FOR [tip_externo]
GO
ALTER TABLE [dbo].[rad_planilla] ADD  CONSTRAINT [DF__rad_plani__tip_d__6383C8BA]  DEFAULT ((1)) FOR [tip_destinatario]
GO
ALTER TABLE [dbo].[rad_planilla] ADD  CONSTRAINT [DF__rad_plani__ide_i__6477ECF3]  DEFAULT ((0)) FOR [ide_informado]
GO
ALTER TABLE [dbo].[radicado] ADD  CONSTRAINT [DF_radicado_radi_nume_hoja]  DEFAULT ((0)) FOR [radi_nume_hoja]
GO
ALTER TABLE [dbo].[radicado] ADD  CONSTRAINT [DF__radicado__codi_n__33D4B598]  DEFAULT ((1)) FOR [codi_nivel]
GO
ALTER TABLE [dbo].[radicado] ADD  CONSTRAINT [DF__radicado__carp_p__34C8D9D1]  DEFAULT ((0)) FOR [carp_per]
GO
ALTER TABLE [dbo].[radicado] ADD  CONSTRAINT [DF__radicado__radi_l__35BCFE0A]  DEFAULT ((0)) FOR [radi_leido]
GO
ALTER TABLE [dbo].[radicado] ADD  CONSTRAINT [DF__radicado__radi_t__36B12243]  DEFAULT ((0)) FOR [radi_tipo_deri]
GO
ALTER TABLE [dbo].[radicado] ADD  CONSTRAINT [DF__radicado__sgd_fl__37A5467C]  DEFAULT ((0)) FOR [sgd_fld_codigo]
GO
ALTER TABLE [dbo].[radicado] ADD  CONSTRAINT [DF__radicado__id_con__38996AB5]  DEFAULT ((1)) FOR [id_cont]
GO
ALTER TABLE [dbo].[radicado] ADD  CONSTRAINT [DF__radicado__sgd_sp__398D8EEE]  DEFAULT ((0)) FOR [sgd_spub_codigo]
GO
ALTER TABLE [dbo].[radicado] ADD  CONSTRAINT [DF__radicado__radi_n__3A81B327]  DEFAULT ((0)) FOR [radi_nrr]
GO
ALTER TABLE [dbo].[radicado] ADD  CONSTRAINT [DF_radicado_radi_nume_folios]  DEFAULT ((0)) FOR [radi_nume_folios]
GO
ALTER TABLE [dbo].[radicado] ADD  CONSTRAINT [DF_radicado_radi_nume_folio]  DEFAULT ((0)) FOR [radi_nume_folio]
GO
ALTER TABLE [dbo].[radicado] ADD  CONSTRAINT [DF__radicado__cod_pl__3B75D760]  DEFAULT ((0)) FOR [cod_planilla]
GO
ALTER TABLE [dbo].[radicado] ADD  CONSTRAINT [DF_radicado_radi_fech_recib]  DEFAULT (getdate()) FOR [radi_fech_recib]
GO
ALTER TABLE [dbo].[sgd_aexp_anexexpediente] ADD  CONSTRAINT [DF_sgd_aexp_anexexpediente_exp_fech]  DEFAULT (getdate()) FOR [exp_fech]
GO
ALTER TABLE [dbo].[sgd_aexp_anexexpediente] ADD  CONSTRAINT [DF_sgd_aexp_anexexpediente_aexp_borrado]  DEFAULT ('N') FOR [aexp_borrado]
GO
ALTER TABLE [dbo].[sgd_anu_anulados] ADD  CONSTRAINT [DF_sgd_anu_anulados_sgd_anu_id]  DEFAULT (NEXT VALUE FOR [dbo].[sgd_anu_id]) FOR [sgd_anu_id]
GO
ALTER TABLE [dbo].[sgd_camexp_campoexpediente] ADD  CONSTRAINT [DF__sgd_camex__sgd_c__498EEC8D]  DEFAULT ((0)) FOR [sgd_camexp_fk]
GO
ALTER TABLE [dbo].[sgd_categorias] ADD  CONSTRAINT [DF__sgd_categ__sgd_c__6ADAD1BF]  DEFAULT ((0)) FOR [sgd_cat_estado]
GO
ALTER TABLE [dbo].[sgd_certificado] ADD  CONSTRAINT [DF_sgd_anex_cert_sgd_anex_id]  DEFAULT (NEXT VALUE FOR [seq_anex_cert_id]) FOR [sgd_cert_id]
GO
ALTER TABLE [dbo].[sgd_certificado] ADD  CONSTRAINT [DF_sgd_anex_cert_sgd_anex_fech]  DEFAULT (getdate()) FOR [sgd_cert_fech]
GO
ALTER TABLE [dbo].[sgd_certimail] ADD  CONSTRAINT [DF_sgd_certimail_id]  DEFAULT (NEXT VALUE FOR [sgd_certimail_id_seq]) FOR [id]
GO
ALTER TABLE [dbo].[sgd_cexp_campdataexp] ADD  CONSTRAINT [DF_sgd_cexp_campdataexp_sgd_cexp_codigo]  DEFAULT (NEXT VALUE FOR [seq_cexp_campdataexp]) FOR [sgd_cexp_codigo]
GO
ALTER TABLE [dbo].[sgd_ciu_ciudadano] ADD  CONSTRAINT [DF__sgd_ciu_c__id_co__4E53A1AA]  DEFAULT ((1)) FOR [id_cont]
GO
ALTER TABLE [dbo].[sgd_ciu_ciudadano] ADD  CONSTRAINT [DF__sgd_ciu_c__id_pa__4F47C5E3]  DEFAULT ((170)) FOR [id_pais]
GO
ALTER TABLE [dbo].[sgd_dcau_causal] ADD  DEFAULT ((0)) FOR [sgd_dcau_pqr]
GO
ALTER TABLE [dbo].[sgd_def_paises] ADD  CONSTRAINT [DF__sgd_def_p__id_co__5CD6CB2B]  DEFAULT ((1)) FOR [id_cont]
GO
ALTER TABLE [dbo].[sgd_depe_activo] ADD  CONSTRAINT [DF_sgd_depe_activo_sgd_dp_act_id]  DEFAULT (NEXT VALUE FOR [seq_activo]) FOR [sgd_dp_act_id]
GO
ALTER TABLE [dbo].[sgd_dir_drecciones] ADD  CONSTRAINT [DF__sgd_dir_d__id_pa__2C3393D0]  DEFAULT ((170)) FOR [id_pais]
GO
ALTER TABLE [dbo].[sgd_dir_drecciones] ADD  CONSTRAINT [DF__sgd_dir_d__id_co__2D27B809]  DEFAULT ((1)) FOR [id_cont]
GO
ALTER TABLE [dbo].[sgd_eit_items] ADD  CONSTRAINT [DF__sgd_eit_i__sgd_e__0697FACD]  DEFAULT ('0') FOR [sgd_eit_cod_padre]
GO
ALTER TABLE [dbo].[sgd_exp_expediente] ADD  CONSTRAINT [DF__sgd_exp_e__sgd_e__0A688BB1]  DEFAULT ((0)) FOR [sgd_exp_estado]
GO
ALTER TABLE [dbo].[sgd_exp_expediente] ADD  CONSTRAINT [DF__sgd_exp_e__sgd_f__0B5CAFEA]  DEFAULT ((0)) FOR [sgd_fexp_codigo]
GO
ALTER TABLE [dbo].[sgd_fenv_frmenvio] ADD  CONSTRAINT [DF__sgd_fenv___sgd_f__13F1F5EB]  DEFAULT ((1)) FOR [sgd_fenv_estado]
GO
ALTER TABLE [dbo].[sgd_fenv_frmenvio] ADD  CONSTRAINT [DF__sgd_fenv___sgd_f__14E61A24]  DEFAULT ((0)) FOR [sgd_fenv_planilla]
GO
ALTER TABLE [dbo].[sgd_fmenv_eventos] ADD  CONSTRAINT [DF__sgd_fmenv__estad__18B6AB08]  DEFAULT ((1)) FOR [estado]
GO
ALTER TABLE [dbo].[sgd_hist_img_anex_exp] ADD  DEFAULT (getdate()) FOR [fecha]
GO
ALTER TABLE [dbo].[sgd_hmtd_hismatdoc] ADD  DEFAULT ((0)) FOR [SGD_TTR_CODIGO]
GO
ALTER TABLE [dbo].[sgd_hmtd_hismatdoc] ADD  DEFAULT ((0)) FOR [SGD_MRD_CODIGO]
GO
ALTER TABLE [dbo].[sgd_iexp_metainfoexpediente] ADD  CONSTRAINT [DF_sgd_iexp_metainfoexpediente_sgd_iexp_codigo]  DEFAULT (NEXT VALUE FOR [dbo].[seq_iexp_metainfoexpediente]) FOR [sgd_iexp_codigo]
GO
ALTER TABLE [dbo].[sgd_mexp_metadataexp] ADD  CONSTRAINT [DF_sgd_mexp_metadataexp_sgd_mexp_codigo]  DEFAULT (NEXT VALUE FOR [seq_meta_metaexp]) FOR [sgd_mexp_codigo]
GO
ALTER TABLE [dbo].[sgd_mod_modules] ADD  CONSTRAINT [DF__sgd_mod_m__sgd_m__625A9A57]  DEFAULT ((0)) FOR [sgd_mod_valmax]
GO
ALTER TABLE [dbo].[sgd_mod_modules] ADD  CONSTRAINT [DF__sgd_mod_m__sgd_m__634EBE90]  DEFAULT ((0)) FOR [sgd_mod_estado]
GO
ALTER TABLE [dbo].[sgd_mot_motivos] ADD  CONSTRAINT [DF_sgd_mot_motivos_sgd_mot_id]  DEFAULT (NEXT VALUE FOR [sgd_mot_motivos_sgd_mot_id_seq]) FOR [sgd_mot_id]
GO
ALTER TABLE [dbo].[sgd_mrd_matrird] ADD  CONSTRAINT [DF__sgd_mrd_m__sgd_m__67DE6983]  DEFAULT ((0)) FOR [sgd_mrd_esta_exp]
GO
ALTER TABLE [dbo].[sgd_mrd_matrird] ADD  CONSTRAINT [DF__sgd_mrd_m__sgd_c__6BCEF5F8]  DEFAULT ((0)) FOR [sgd_categoria]
GO
ALTER TABLE [dbo].[sgd_oem_oempresas] ADD  CONSTRAINT [DF__sgd_oem_o__id_co__7CD98669]  DEFAULT ((1)) FOR [id_cont]
GO
ALTER TABLE [dbo].[sgd_oem_oempresas] ADD  CONSTRAINT [DF__sgd_oem_o__id_pa__7DCDAAA2]  DEFAULT ((170)) FOR [id_pais]
GO
ALTER TABLE [dbo].[sgd_pexp_procexpedientes] ADD  CONSTRAINT [DF__sgd_pexp___sgd_p__0D0FEE32]  DEFAULT ((0)) FOR [sgd_pexp_terminos]
GO
ALTER TABLE [dbo].[sgd_pexp_procexpedientes] ADD  CONSTRAINT [DF__sgd_pexp___sgd_p__0E04126B]  DEFAULT ((1)) FOR [sgd_pexp_automatico]
GO
ALTER TABLE [dbo].[sgd_pmrd_procedi] ADD  CONSTRAINT [DF_sgd_pmrd_procedi_sgd_pmrd_codigo]  DEFAULT (NEXT VALUE FOR [sgd_pmrd_procedi_sgd_pmrd_codigo_seq]) FOR [sgd_pmrd_codigo]
GO
ALTER TABLE [dbo].[sgd_renv_regenvio] ADD  CONSTRAINT [DF__sgd_renv___sgd_r__1699586C]  DEFAULT ((0)) FOR [sgd_rem_destino]
GO
ALTER TABLE [dbo].[sgd_renv_regenvio] ADD  CONSTRAINT [DF__sgd_renv___sgd_d__178D7CA5]  DEFAULT ((0)) FOR [sgd_dir_tipo]
GO
ALTER TABLE [dbo].[sgd_renv_regenvio] ADD  CONSTRAINT [DF__sgd_renv___sgd_r__1881A0DE]  DEFAULT ((0)) FOR [sgd_renv_cantidad]
GO
ALTER TABLE [dbo].[sgd_renv_regenvio] ADD  CONSTRAINT [DF__sgd_renv___sgd_r__1975C517]  DEFAULT ((0)) FOR [sgd_renv_tipo]
GO
ALTER TABLE [dbo].[sgd_renv_regenvio] ADD  CONSTRAINT [DF__sgd_renv___sgd_r__1A69E950]  DEFAULT ('0') FOR [sgd_renv_valortotal]
GO
ALTER TABLE [dbo].[sgd_renv_regenvio] ADD  CONSTRAINT [DF__sgd_renv___sgd_r__1B5E0D89]  DEFAULT ('0') FOR [sgd_renv_valistamiento]
GO
ALTER TABLE [dbo].[sgd_renv_regenvio] ADD  CONSTRAINT [DF__sgd_renv___sgd_r__1C5231C2]  DEFAULT ('0') FOR [sgd_renv_vdescuento]
GO
ALTER TABLE [dbo].[sgd_renv_regenvio] ADD  CONSTRAINT [DF__sgd_renv___sgd_r__1D4655FB]  DEFAULT ('0') FOR [sgd_renv_vadicional]
GO
ALTER TABLE [dbo].[sgd_renv_regenvio] ADD  CONSTRAINT [DF__sgd_renv___sgd_r__1E3A7A34]  DEFAULT ('colombia') FOR [sgd_renv_pais]
GO
ALTER TABLE [dbo].[sgd_renv_regenvio] ADD  CONSTRAINT [DF_sgd_renv_regenvio_id]  DEFAULT (NEXT VALUE FOR [sgd_renv_regenvio_id_seq]) FOR [id]
GO
ALTER TABLE [dbo].[sgd_rol_roles] ADD  CONSTRAINT [DF__sgd_rol_r__sgd_r__1B0907CE]  DEFAULT ((0)) FOR [sgd_rol_estado]
GO
ALTER TABLE [dbo].[sgd_seg_seguridad] ADD  CONSTRAINT [DF_sgd_seg_seguridad_id]  DEFAULT (NEXT VALUE FOR [seq_seguridad]) FOR [id]
GO
ALTER TABLE [dbo].[sgd_sexp_secexpedientes] ADD  CONSTRAINT [DF__sgd_sexp___sgd_s__2AA05119]  DEFAULT ((0)) FOR [sgd_sexp_metadato]
GO
ALTER TABLE [dbo].[sgd_sexp_secexpedientes] ADD  CONSTRAINT [DF_sgd_sexp_secexpedientes_sgd_sexp_estado]  DEFAULT ((0)) FOR [sgd_sexp_estado]
GO
ALTER TABLE [dbo].[sgd_temas_tiposdoc] ADD  DEFAULT ((0)) FOR [sgd_temtdoc_estado]
GO
ALTER TABLE [dbo].[sgd_tip3_tipotercero] ADD  CONSTRAINT [DF__sgd_tip3___sgd_t__3429BB53]  DEFAULT ((0)) FOR [sgd_tpr_tp1]
GO
ALTER TABLE [dbo].[sgd_tip3_tipotercero] ADD  CONSTRAINT [DF__sgd_tip3___sgd_t__351DDF8C]  DEFAULT ((0)) FOR [sgd_tpr_tp2]
GO
ALTER TABLE [dbo].[sgd_tip3_tipotercero] ADD  CONSTRAINT [DF__sgd_tip3___sgd_t__361203C5]  DEFAULT ((0)) FOR [sgd_tpr_tp3]
GO
ALTER TABLE [dbo].[sgd_tip3_tipotercero] ADD  CONSTRAINT [DF__sgd_tip3___sgd_t__370627FE]  DEFAULT ((1)) FOR [sgd_tpr_tp8]
GO
ALTER TABLE [dbo].[sgd_tpr_tpdcumento] ADD  CONSTRAINT [DF__sgd_tpr_t__sgd_t__39E294A9]  DEFAULT ((0)) FOR [sgd_tpr_tp3]
GO
ALTER TABLE [dbo].[sgd_tpr_tpdcumento] ADD  CONSTRAINT [DF__sgd_tpr_t__sgd_t__3AD6B8E2]  DEFAULT ((0)) FOR [sgd_tpr_tp1]
GO
ALTER TABLE [dbo].[sgd_tpr_tpdcumento] ADD  CONSTRAINT [DF__sgd_tpr_t__sgd_t__3BCADD1B]  DEFAULT ((0)) FOR [sgd_tpr_tp2]
GO
ALTER TABLE [dbo].[sgd_tpr_tpdcumento] ADD  CONSTRAINT [DF__sgd_tpr_t__sgd_t__3CBF0154]  DEFAULT ((0)) FOR [sgd_tpr_tp6]
GO
ALTER TABLE [dbo].[sgd_tpr_tpdcumento] ADD  CONSTRAINT [DF__sgd_tpr_t__sgd_t__3DB3258D]  DEFAULT ((0)) FOR [sgd_tpr_web]
GO
ALTER TABLE [dbo].[sgd_vmrd_matrivrd] ADD  CONSTRAINT [DF_sgd_vmrd_matrivrd_sgd_vmrd_codigo]  DEFAULT (NEXT VALUE FOR [sgd_vmrd_matrivrd_sgd_vmrd_codigo_seq]) FOR [sgd_vmrd_codigo]
GO
ALTER TABLE [dbo].[sgd_vtrd_verstrd] ADD  CONSTRAINT [DF_sgd_vtrd_verstrd_sgd_vtrd_codigo]  DEFAULT (NEXT VALUE FOR [sgd_vtrd_verstrd_sgd_vtrd_codigo_seq]) FOR [sgd_vtrd_codigo]
GO
ALTER TABLE [dbo].[user_security] ADD  CONSTRAINT [DF_user_security_id]  DEFAULT (NEXT VALUE FOR [id_user_security]) FOR [id]
GO
ALTER TABLE [dbo].[usua_migracion] ADD  CONSTRAINT [DF_usua_migracion_usua_codi]  DEFAULT (NEXT VALUE FOR [dbo].[sec_usua]) FOR [usua_codi]
GO
ALTER TABLE [dbo].[usuario] ADD  CONSTRAINT [DF_usuario_usua_vistagraphip]  DEFAULT ((0)) FOR [usua_vistagraphip]
GO
ALTER TABLE [dbo].[anexos]  WITH CHECK ADD  CONSTRAINT [fk_anex_radi] FOREIGN KEY([anex_codigo])
REFERENCES [dbo].[anexos] ([anex_codigo])
GO
ALTER TABLE [dbo].[anexos] CHECK CONSTRAINT [fk_anex_radi]
GO
ALTER TABLE [dbo].[anexos]  WITH CHECK ADD  CONSTRAINT [fk_anex_usuario] FOREIGN KEY([anex_creador])
REFERENCES [dbo].[usuario] ([usua_login])
GO
ALTER TABLE [dbo].[anexos] CHECK CONSTRAINT [fk_anex_usuario]
GO
ALTER TABLE [dbo].[anexos]  WITH CHECK ADD  CONSTRAINT [FK_anexos_anexos] FOREIGN KEY([anex_codigo])
REFERENCES [dbo].[anexos] ([anex_codigo])
GO
ALTER TABLE [dbo].[anexos] CHECK CONSTRAINT [FK_anexos_anexos]
GO
ALTER TABLE [dbo].[anexos]  WITH CHECK ADD  CONSTRAINT [FK_anexos_anexos_tipo] FOREIGN KEY([anex_tipo])
REFERENCES [dbo].[anexos_tipo] ([anex_tipo_codi])
GO
ALTER TABLE [dbo].[anexos] CHECK CONSTRAINT [FK_anexos_anexos_tipo]
GO
ALTER TABLE [dbo].[anexos]  WITH CHECK ADD  CONSTRAINT [FK_anexos_anexos1] FOREIGN KEY([anex_codigo])
REFERENCES [dbo].[anexos] ([anex_codigo])
GO
ALTER TABLE [dbo].[anexos] CHECK CONSTRAINT [FK_anexos_anexos1]
GO
ALTER TABLE [dbo].[anexos]  WITH CHECK ADD  CONSTRAINT [FK_anexos_radicado] FOREIGN KEY([anex_radi_nume])
REFERENCES [dbo].[radicado] ([radi_nume_radi])
GO
ALTER TABLE [dbo].[anexos] CHECK CONSTRAINT [FK_anexos_radicado]
GO
ALTER TABLE [dbo].[arch_edi_edificio]  WITH CHECK ADD  CONSTRAINT [FK_arch_edi_edificio_arch_edi_edificio] FOREIGN KEY([arch_edi_codigo])
REFERENCES [dbo].[arch_edi_edificio] ([arch_edi_codigo])
GO
ALTER TABLE [dbo].[arch_edi_edificio] CHECK CONSTRAINT [FK_arch_edi_edificio_arch_edi_edificio]
GO
ALTER TABLE [dbo].[arch_edi_edificio]  WITH CHECK ADD  CONSTRAINT [fk_edi_pa] FOREIGN KEY([codi_pais])
REFERENCES [dbo].[sgd_def_paises] ([id_pais])
GO
ALTER TABLE [dbo].[arch_edi_edificio] CHECK CONSTRAINT [fk_edi_pa]
GO
ALTER TABLE [dbo].[departamento]  WITH CHECK ADD  CONSTRAINT [fk_pais] FOREIGN KEY([id_pais])
REFERENCES [dbo].[sgd_def_paises] ([id_pais])
GO
ALTER TABLE [dbo].[departamento] CHECK CONSTRAINT [fk_pais]
GO
ALTER TABLE [dbo].[grupos]  WITH CHECK ADD  CONSTRAINT [fk_grupos_depe] FOREIGN KEY([grupo_depe_codi])
REFERENCES [dbo].[dependencia] ([depe_codi])
GO
ALTER TABLE [dbo].[grupos] CHECK CONSTRAINT [fk_grupos_depe]
GO
ALTER TABLE [dbo].[grupos]  WITH CHECK ADD  CONSTRAINT [fk_grupos_usua] FOREIGN KEY([grupo_usua_codi])
REFERENCES [dbo].[usuario] ([usua_codi])
GO
ALTER TABLE [dbo].[grupos] CHECK CONSTRAINT [fk_grupos_usua]
GO
ALTER TABLE [dbo].[grupos_usuarios]  WITH CHECK ADD  CONSTRAINT [fk_grupos_usuarios_grupo] FOREIGN KEY([grupo_codi])
REFERENCES [dbo].[grupos] ([grupo_codi])
GO
ALTER TABLE [dbo].[grupos_usuarios] CHECK CONSTRAINT [fk_grupos_usuarios_grupo]
GO
ALTER TABLE [dbo].[grupos_usuarios]  WITH CHECK ADD  CONSTRAINT [fk_grupos_usuarios_usua] FOREIGN KEY([usua_codi])
REFERENCES [dbo].[usuario] ([usua_codi])
GO
ALTER TABLE [dbo].[grupos_usuarios] CHECK CONSTRAINT [fk_grupos_usuarios_usua]
GO
ALTER TABLE [dbo].[hist_eventos]  WITH CHECK ADD  CONSTRAINT [fk_hist_depe] FOREIGN KEY([depe_codi])
REFERENCES [dbo].[dependencia] ([depe_codi])
GO
ALTER TABLE [dbo].[hist_eventos] CHECK CONSTRAINT [fk_hist_depe]
GO
ALTER TABLE [dbo].[hist_eventos]  WITH CHECK ADD  CONSTRAINT [FK_hist_eventos_sgd_ttr_transaccion] FOREIGN KEY([sgd_ttr_codigo])
REFERENCES [dbo].[sgd_ttr_transaccion] ([sgd_ttr_codigo])
GO
ALTER TABLE [dbo].[hist_eventos] CHECK CONSTRAINT [FK_hist_eventos_sgd_ttr_transaccion]
GO
ALTER TABLE [dbo].[informados]  WITH CHECK ADD  CONSTRAINT [FK_informados_dependencia] FOREIGN KEY([depe_codi])
REFERENCES [dbo].[dependencia] ([depe_codi])
GO
ALTER TABLE [dbo].[informados] CHECK CONSTRAINT [FK_informados_dependencia]
GO
ALTER TABLE [dbo].[mas_dat_csvdata]  WITH CHECK ADD  CONSTRAINT [mas_dat_csvdata_mas_dat_idcsv_fkey] FOREIGN KEY([mas_dat_idcsv])
REFERENCES [dbo].[mas_csv_base] ([mas_csv_id])
GO
ALTER TABLE [dbo].[mas_dat_csvdata] CHECK CONSTRAINT [mas_dat_csvdata_mas_dat_idcsv_fkey]
GO
ALTER TABLE [dbo].[pla_entrega]  WITH CHECK ADD  CONSTRAINT [FK_pla_entrega_dependencia] FOREIGN KEY([depe_codi_genera])
REFERENCES [dbo].[dependencia] ([depe_codi])
GO
ALTER TABLE [dbo].[pla_entrega] CHECK CONSTRAINT [FK_pla_entrega_dependencia]
GO
ALTER TABLE [dbo].[pla_entrega]  WITH CHECK ADD  CONSTRAINT [FK_pla_entrega_radicado] FOREIGN KEY([radi_nume_radi])
REFERENCES [dbo].[radicado] ([radi_nume_radi])
GO
ALTER TABLE [dbo].[pla_entrega] CHECK CONSTRAINT [FK_pla_entrega_radicado]
GO
ALTER TABLE [dbo].[pla_entrega]  WITH CHECK ADD  CONSTRAINT [FK_pla_entrega_sgd_rol_roles] FOREIGN KEY([id_rol])
REFERENCES [dbo].[sgd_rol_roles] ([sgd_rol_id])
GO
ALTER TABLE [dbo].[pla_entrega] CHECK CONSTRAINT [FK_pla_entrega_sgd_rol_roles]
GO
ALTER TABLE [dbo].[pla_entrega_radi]  WITH CHECK ADD  CONSTRAINT [FK_pla_entrega_radi_pla_entrega_radi] FOREIGN KEY([id])
REFERENCES [dbo].[pla_entrega_radi] ([id])
GO
ALTER TABLE [dbo].[pla_entrega_radi] CHECK CONSTRAINT [FK_pla_entrega_radi_pla_entrega_radi]
GO
ALTER TABLE [dbo].[pla_entrega_radi]  WITH CHECK ADD  CONSTRAINT [pla_entrega_radi_pla_codigo_fkey] FOREIGN KEY([pla_codigo])
REFERENCES [dbo].[pla_entrega] ([pla_codigo])
GO
ALTER TABLE [dbo].[pla_entrega_radi] CHECK CONSTRAINT [pla_entrega_radi_pla_codigo_fkey]
GO
ALTER TABLE [dbo].[pla_entrega_radi]  WITH CHECK ADD  CONSTRAINT [pla_entrega_radi_radi_nume_radi_fkey] FOREIGN KEY([radi_nume_radi])
REFERENCES [dbo].[radicado] ([radi_nume_radi])
GO
ALTER TABLE [dbo].[pla_entrega_radi] CHECK CONSTRAINT [pla_entrega_radi_radi_nume_radi_fkey]
GO
ALTER TABLE [dbo].[prestamo]  WITH CHECK ADD  CONSTRAINT [fk_prestamo_depe_arch] FOREIGN KEY([pres_depe_arch])
REFERENCES [dbo].[dependencia] ([depe_codi])
GO
ALTER TABLE [dbo].[prestamo] CHECK CONSTRAINT [fk_prestamo_depe_arch]
GO
ALTER TABLE [dbo].[prestamo]  WITH CHECK ADD  CONSTRAINT [FK_prestamo_dependencia] FOREIGN KEY([depe_codi])
REFERENCES [dbo].[dependencia] ([depe_codi])
GO
ALTER TABLE [dbo].[prestamo] CHECK CONSTRAINT [FK_prestamo_dependencia]
GO
ALTER TABLE [dbo].[prestamo]  WITH CHECK ADD  CONSTRAINT [FK_prestamo_prestamo] FOREIGN KEY([pres_id])
REFERENCES [dbo].[prestamo] ([pres_id])
GO
ALTER TABLE [dbo].[prestamo] CHECK CONSTRAINT [FK_prestamo_prestamo]
GO
ALTER TABLE [dbo].[rad_planilla]  WITH CHECK ADD  CONSTRAINT [FK_rad_planilla_radicado] FOREIGN KEY([radi_nume_radi])
REFERENCES [dbo].[radicado] ([radi_nume_radi])
GO
ALTER TABLE [dbo].[rad_planilla] CHECK CONSTRAINT [FK_rad_planilla_radicado]
GO
ALTER TABLE [dbo].[radi_cam_metacampos]  WITH CHECK ADD  CONSTRAINT [fk_meta_data] FOREIGN KEY([radi_data_id])
REFERENCES [dbo].[radi_data_metainfo] ([radi_data_id])
GO
ALTER TABLE [dbo].[radi_cam_metacampos] CHECK CONSTRAINT [fk_meta_data]
GO
ALTER TABLE [dbo].[radi_data_metainfo]  WITH CHECK ADD  CONSTRAINT [fk_meta_depe] FOREIGN KEY([depe_codi])
REFERENCES [dbo].[dependencia] ([depe_codi])
GO
ALTER TABLE [dbo].[radi_data_metainfo] CHECK CONSTRAINT [fk_meta_depe]
GO
ALTER TABLE [dbo].[radicado]  WITH CHECK ADD  CONSTRAINT [fk_rad_trad] FOREIGN KEY([sgd_trad_codigo])
REFERENCES [dbo].[sgd_trad_tiporad] ([sgd_trad_codigo])
GO
ALTER TABLE [dbo].[radicado] CHECK CONSTRAINT [fk_rad_trad]
GO
ALTER TABLE [dbo].[radicado]  WITH CHECK ADD  CONSTRAINT [fk_radi_cpob] FOREIGN KEY([cpob_codi], [muni_codi], [dpto_codi])
REFERENCES [dbo].[centro_poblado] ([cpob_codi], [muni_codi], [dpto_codi])
GO
ALTER TABLE [dbo].[radicado] CHECK CONSTRAINT [fk_radi_cpob]
GO
ALTER TABLE [dbo].[radicado]  WITH CHECK ADD  CONSTRAINT [fk_radi_depe] FOREIGN KEY([radi_depe_radi])
REFERENCES [dbo].[dependencia] ([depe_codi])
GO
ALTER TABLE [dbo].[radicado] CHECK CONSTRAINT [fk_radi_depe]
GO
ALTER TABLE [dbo].[radicado]  WITH CHECK ADD  CONSTRAINT [fk_radi_depe_actu] FOREIGN KEY([radi_depe_actu])
REFERENCES [dbo].[dependencia] ([depe_codi])
GO
ALTER TABLE [dbo].[radicado] CHECK CONSTRAINT [fk_radi_depe_actu]
GO
ALTER TABLE [dbo].[radicado]  WITH CHECK ADD  CONSTRAINT [fk_radi_esta] FOREIGN KEY([esta_codi])
REFERENCES [dbo].[estado] ([esta_codi])
GO
ALTER TABLE [dbo].[radicado] CHECK CONSTRAINT [fk_radi_esta]
GO
ALTER TABLE [dbo].[radicado]  WITH CHECK ADD  CONSTRAINT [fk_radi_meta] FOREIGN KEY([radi_mdata_metadatos])
REFERENCES [dbo].[radi_data_metainfo] ([radi_data_id])
GO
ALTER TABLE [dbo].[radicado] CHECK CONSTRAINT [fk_radi_meta]
GO
ALTER TABLE [dbo].[radicado]  WITH CHECK ADD  CONSTRAINT [fk_radi_mrec] FOREIGN KEY([mrec_codi])
REFERENCES [dbo].[medio_recepcion] ([mrec_codi])
GO
ALTER TABLE [dbo].[radicado] CHECK CONSTRAINT [fk_radi_mrec]
GO
ALTER TABLE [dbo].[radicado]  WITH CHECK ADD  CONSTRAINT [fk_radi_usua_radi] FOREIGN KEY([radi_nume_radi])
REFERENCES [dbo].[radicado] ([radi_nume_radi])
GO
ALTER TABLE [dbo].[radicado] CHECK CONSTRAINT [fk_radi_usua_radi]
GO
ALTER TABLE [dbo].[radicado]  WITH CHECK ADD  CONSTRAINT [fk_radicado_par_serv] FOREIGN KEY([radi_nume_radi])
REFERENCES [dbo].[radicado] ([radi_nume_radi])
GO
ALTER TABLE [dbo].[radicado] CHECK CONSTRAINT [fk_radicado_par_serv]
GO
ALTER TABLE [dbo].[sgd_agen_agendados]  WITH CHECK ADD  CONSTRAINT [FK_sgd_agen_agendados_dependencia] FOREIGN KEY([depe_codi])
REFERENCES [dbo].[dependencia] ([depe_codi])
GO
ALTER TABLE [dbo].[sgd_agen_agendados] CHECK CONSTRAINT [FK_sgd_agen_agendados_dependencia]
GO
ALTER TABLE [dbo].[sgd_agen_agendados]  WITH CHECK ADD  CONSTRAINT [FK_sgd_agen_agendados_radicado] FOREIGN KEY([radi_nume_radi])
REFERENCES [dbo].[radicado] ([radi_nume_radi])
GO
ALTER TABLE [dbo].[sgd_agen_agendados] CHECK CONSTRAINT [FK_sgd_agen_agendados_radicado]
GO
ALTER TABLE [dbo].[sgd_anar_anexarg]  WITH CHECK ADD  CONSTRAINT [FK_sgd_anar_anexarg_anexos] FOREIGN KEY([anex_codigo])
REFERENCES [dbo].[anexos] ([anex_codigo])
GO
ALTER TABLE [dbo].[sgd_anar_anexarg] CHECK CONSTRAINT [FK_sgd_anar_anexarg_anexos]
GO
ALTER TABLE [dbo].[sgd_anu_anulados]  WITH CHECK ADD  CONSTRAINT [fk_esta_anulacion] FOREIGN KEY([sgd_eanu_codi])
REFERENCES [dbo].[sgd_eanu_estanulacion] ([sgd_eanu_codi])
GO
ALTER TABLE [dbo].[sgd_anu_anulados] CHECK CONSTRAINT [fk_esta_anulacion]
GO
ALTER TABLE [dbo].[sgd_anu_anulados]  WITH CHECK ADD  CONSTRAINT [FK_sgd_anu_anulados_radicado] FOREIGN KEY([radi_nume_radi])
REFERENCES [dbo].[radicado] ([radi_nume_radi])
GO
ALTER TABLE [dbo].[sgd_anu_anulados] CHECK CONSTRAINT [FK_sgd_anu_anulados_radicado]
GO
ALTER TABLE [dbo].[sgd_anu_anulados]  WITH CHECK ADD  CONSTRAINT [FK_sgd_anu_anulados_sgd_anu_anulados] FOREIGN KEY([sgd_anu_id])
REFERENCES [dbo].[sgd_anu_anulados] ([sgd_anu_id])
GO
ALTER TABLE [dbo].[sgd_anu_anulados] CHECK CONSTRAINT [FK_sgd_anu_anulados_sgd_anu_anulados]
GO
ALTER TABLE [dbo].[sgd_carp_descripcion]  WITH CHECK ADD  CONSTRAINT [FK_sgd_carp_descripcion_sgd_carp_descripcion] FOREIGN KEY([sgd_carp_depecodi])
REFERENCES [dbo].[dependencia] ([depe_codi])
GO
ALTER TABLE [dbo].[sgd_carp_descripcion] CHECK CONSTRAINT [FK_sgd_carp_descripcion_sgd_carp_descripcion]
GO
ALTER TABLE [dbo].[sgd_certificado]  WITH CHECK ADD  CONSTRAINT [FK_sgd_anex_cert_radicado] FOREIGN KEY([radi_nume_radi])
REFERENCES [dbo].[radicado] ([radi_nume_radi])
GO
ALTER TABLE [dbo].[sgd_certificado] CHECK CONSTRAINT [FK_sgd_anex_cert_radicado]
GO
ALTER TABLE [dbo].[sgd_certificado]  WITH CHECK ADD  CONSTRAINT [FK_sgd_anex_cert_usuario] FOREIGN KEY([usua_login])
REFERENCES [dbo].[usuario] ([usua_login])
GO
ALTER TABLE [dbo].[sgd_certificado] CHECK CONSTRAINT [FK_sgd_anex_cert_usuario]
GO
ALTER TABLE [dbo].[sgd_certimail]  WITH CHECK ADD  CONSTRAINT [FK_sgd_certimail_sgd_certimail] FOREIGN KEY([radi_nume_radi])
REFERENCES [dbo].[radicado] ([radi_nume_radi])
GO
ALTER TABLE [dbo].[sgd_certimail] CHECK CONSTRAINT [FK_sgd_certimail_sgd_certimail]
GO
ALTER TABLE [dbo].[sgd_cexp_campdataexp]  WITH CHECK ADD  CONSTRAINT [fk_cexp_mexp] FOREIGN KEY([sgd_cexp_codigo])
REFERENCES [dbo].[sgd_cexp_campdataexp] ([sgd_cexp_codigo])
GO
ALTER TABLE [dbo].[sgd_cexp_campdataexp] CHECK CONSTRAINT [fk_cexp_mexp]
GO
ALTER TABLE [dbo].[sgd_def_paises]  WITH CHECK ADD  CONSTRAINT [FK_sgd_def_paises_sgd_def_continentes] FOREIGN KEY([id_cont])
REFERENCES [dbo].[sgd_def_continentes] ([id_cont])
GO
ALTER TABLE [dbo].[sgd_def_paises] CHECK CONSTRAINT [FK_sgd_def_paises_sgd_def_continentes]
GO
ALTER TABLE [dbo].[sgd_depe_activo]  WITH CHECK ADD  CONSTRAINT [FK_sgd_depe_activo_dependencia] FOREIGN KEY([depe_codi])
REFERENCES [dbo].[dependencia] ([depe_codi])
GO
ALTER TABLE [dbo].[sgd_depe_activo] CHECK CONSTRAINT [FK_sgd_depe_activo_dependencia]
GO
ALTER TABLE [dbo].[sgd_depe_activo]  WITH CHECK ADD  CONSTRAINT [FK_sgd_depe_activo_sgd_rol_roles] FOREIGN KEY([rol_id])
REFERENCES [dbo].[sgd_rol_roles] ([sgd_rol_id])
GO
ALTER TABLE [dbo].[sgd_depe_activo] CHECK CONSTRAINT [FK_sgd_depe_activo_sgd_rol_roles]
GO
ALTER TABLE [dbo].[sgd_dir_drecciones]  WITH CHECK ADD  CONSTRAINT [fk_bodega] FOREIGN KEY([sgd_esp_codi])
REFERENCES [dbo].[bodega_empresas] ([identificador_empresa])
GO
ALTER TABLE [dbo].[sgd_dir_drecciones] CHECK CONSTRAINT [fk_bodega]
GO
ALTER TABLE [dbo].[sgd_dir_drecciones]  WITH CHECK ADD  CONSTRAINT [fk_oem_dir] FOREIGN KEY([sgd_oem_codigo])
REFERENCES [dbo].[sgd_oem_oempresas] ([sgd_oem_codigo])
GO
ALTER TABLE [dbo].[sgd_dir_drecciones] CHECK CONSTRAINT [fk_oem_dir]
GO
ALTER TABLE [dbo].[sgd_dir_drecciones]  WITH CHECK ADD  CONSTRAINT [fk_sgd_dir_radicado] FOREIGN KEY([radi_nume_radi])
REFERENCES [dbo].[radicado] ([radi_nume_radi])
GO
ALTER TABLE [dbo].[sgd_dir_drecciones] CHECK CONSTRAINT [fk_sgd_dir_radicado]
GO
ALTER TABLE [dbo].[sgd_dir_drecciones]  WITH CHECK ADD  CONSTRAINT [fk_sgd_dir_sgd_ciu] FOREIGN KEY([sgd_ciu_codigo])
REFERENCES [dbo].[sgd_ciu_ciudadano] ([sgd_ciu_codigo])
GO
ALTER TABLE [dbo].[sgd_dir_drecciones] CHECK CONSTRAINT [fk_sgd_dir_sgd_ciu]
GO
ALTER TABLE [dbo].[sgd_dpr_dep_per_rol]  WITH CHECK ADD  CONSTRAINT [fk_rol] FOREIGN KEY([rol_codi])
REFERENCES [dbo].[sgd_rol_roles] ([sgd_rol_id])
GO
ALTER TABLE [dbo].[sgd_dpr_dep_per_rol] CHECK CONSTRAINT [fk_rol]
GO
ALTER TABLE [dbo].[sgd_dpr_dep_per_rol]  WITH CHECK ADD  CONSTRAINT [fk_rol_depe] FOREIGN KEY([depe_codi])
REFERENCES [dbo].[dependencia] ([depe_codi])
GO
ALTER TABLE [dbo].[sgd_dpr_dep_per_rol] CHECK CONSTRAINT [fk_rol_depe]
GO
ALTER TABLE [dbo].[sgd_drm_dep_mod_rol]  WITH CHECK ADD  CONSTRAINT [FK_sgd_drm_dep_mod_rol] FOREIGN KEY([sgd_drm_depecodi])
REFERENCES [dbo].[dependencia] ([depe_codi])
GO
ALTER TABLE [dbo].[sgd_drm_dep_mod_rol] CHECK CONSTRAINT [FK_sgd_drm_dep_mod_rol]
GO
ALTER TABLE [dbo].[sgd_drm_dep_mod_rol]  WITH CHECK ADD  CONSTRAINT [FK_sgd_drm_dep_mod_rol_sgd_mod_modules] FOREIGN KEY([sgd_drm_modecodi])
REFERENCES [dbo].[sgd_mod_modules] ([sgd_mod_id])
GO
ALTER TABLE [dbo].[sgd_drm_dep_mod_rol] CHECK CONSTRAINT [FK_sgd_drm_dep_mod_rol_sgd_mod_modules]
GO
ALTER TABLE [dbo].[sgd_drm_dep_mod_rol]  WITH CHECK ADD  CONSTRAINT [FK_sgd_drm_dep_mod_rol_sgd_rol_roles] FOREIGN KEY([sgd_drm_rolcodi])
REFERENCES [dbo].[sgd_rol_roles] ([sgd_rol_id])
GO
ALTER TABLE [dbo].[sgd_drm_dep_mod_rol] CHECK CONSTRAINT [FK_sgd_drm_dep_mod_rol_sgd_rol_roles]
GO
ALTER TABLE [dbo].[sgd_exp_expediente]  WITH CHECK ADD  CONSTRAINT [FK_sgd_exp_expediente_dependencia] FOREIGN KEY([depe_codi])
REFERENCES [dbo].[dependencia] ([depe_codi])
GO
ALTER TABLE [dbo].[sgd_exp_expediente] CHECK CONSTRAINT [FK_sgd_exp_expediente_dependencia]
GO
ALTER TABLE [dbo].[sgd_exp_expediente]  WITH CHECK ADD  CONSTRAINT [FK_sgd_exp_expediente_radicado] FOREIGN KEY([radi_nume_radi])
REFERENCES [dbo].[radicado] ([radi_nume_radi])
GO
ALTER TABLE [dbo].[sgd_exp_expediente] CHECK CONSTRAINT [FK_sgd_exp_expediente_radicado]
GO
ALTER TABLE [dbo].[sgd_fenv_frmenvio]  WITH CHECK ADD  CONSTRAINT [FK_sgd_fenv_frmenvio_sgd_fenv_frmenvio] FOREIGN KEY([sgd_fenv_codigo])
REFERENCES [dbo].[sgd_fenv_frmenvio] ([sgd_fenv_codigo])
GO
ALTER TABLE [dbo].[sgd_fenv_frmenvio] CHECK CONSTRAINT [FK_sgd_fenv_frmenvio_sgd_fenv_frmenvio]
GO
ALTER TABLE [dbo].[sgd_fmenv_eventos]  WITH CHECK ADD  CONSTRAINT [FK_sgd_fmenv_eventos_sgd_fenv_frmenvio] FOREIGN KEY([sgd_fenv_codigo])
REFERENCES [dbo].[sgd_fenv_frmenvio] ([sgd_fenv_codigo])
GO
ALTER TABLE [dbo].[sgd_fmenv_eventos] CHECK CONSTRAINT [FK_sgd_fmenv_eventos_sgd_fenv_frmenvio]
GO
ALTER TABLE [dbo].[sgd_fmenv_eventos]  WITH CHECK ADD  CONSTRAINT [FK_sgd_fmenv_eventos_sgd_menv_metodos] FOREIGN KEY([sgd_menv_codigo])
REFERENCES [dbo].[sgd_menv_metodos] ([sgd_menv_codigo])
GO
ALTER TABLE [dbo].[sgd_fmenv_eventos] CHECK CONSTRAINT [FK_sgd_fmenv_eventos_sgd_menv_metodos]
GO
ALTER TABLE [dbo].[sgd_hfld_histflujodoc]  WITH CHECK ADD  CONSTRAINT [FK_sgd_hfld_histflujodoc_dependencia] FOREIGN KEY([depe_codi])
REFERENCES [dbo].[dependencia] ([depe_codi])
GO
ALTER TABLE [dbo].[sgd_hfld_histflujodoc] CHECK CONSTRAINT [FK_sgd_hfld_histflujodoc_dependencia]
GO
ALTER TABLE [dbo].[sgd_hfld_histflujodoc]  WITH NOCHECK ADD  CONSTRAINT [FK_sgd_hfld_histflujodoc_radicado] FOREIGN KEY([radi_nume_radi])
REFERENCES [dbo].[radicado] ([radi_nume_radi])
GO
ALTER TABLE [dbo].[sgd_hfld_histflujodoc] NOCHECK CONSTRAINT [FK_sgd_hfld_histflujodoc_radicado]
GO
ALTER TABLE [dbo].[sgd_hfld_histflujodoc]  WITH CHECK ADD  CONSTRAINT [FK_sgd_hfld_histflujodoc_sgd_sexp_secexpedientes] FOREIGN KEY([sgd_exp_numero])
REFERENCES [dbo].[sgd_sexp_secexpedientes] ([sgd_exp_numero])
GO
ALTER TABLE [dbo].[sgd_hfld_histflujodoc] CHECK CONSTRAINT [FK_sgd_hfld_histflujodoc_sgd_sexp_secexpedientes]
GO
ALTER TABLE [dbo].[sgd_hist_img_anex_exp]  WITH CHECK ADD  CONSTRAINT [FK_sgd_hist_img_anex_exp_sgd_sexp_secexpedientes] FOREIGN KEY([anexos_exp_id])
REFERENCES [dbo].[sgd_sexp_secexpedientes] ([sgd_sexp_id])
GO
ALTER TABLE [dbo].[sgd_hist_img_anex_exp] CHECK CONSTRAINT [FK_sgd_hist_img_anex_exp_sgd_sexp_secexpedientes]
GO
ALTER TABLE [dbo].[sgd_hist_img_anex_exp]  WITH CHECK ADD  CONSTRAINT [fk_sgd_hist_img_anex_exp_ttr_transaccion] FOREIGN KEY([id_ttr_hian])
REFERENCES [dbo].[sgd_ttr_transaccion] ([sgd_ttr_codigo])
GO
ALTER TABLE [dbo].[sgd_hist_img_anex_exp] CHECK CONSTRAINT [fk_sgd_hist_img_anex_exp_ttr_transaccion]
GO
ALTER TABLE [dbo].[sgd_hmtd_hismatdoc]  WITH CHECK ADD  CONSTRAINT [FK_SGD_HMTD_TTR] FOREIGN KEY([SGD_TTR_CODIGO])
REFERENCES [dbo].[sgd_ttr_transaccion] ([sgd_ttr_codigo])
GO
ALTER TABLE [dbo].[sgd_hmtd_hismatdoc] CHECK CONSTRAINT [FK_SGD_HMTD_TTR]
GO
ALTER TABLE [dbo].[sgd_iexp_metainfoexpediente]  WITH CHECK ADD  CONSTRAINT [fk_expediente] FOREIGN KEY([sgd_iexp_codigo])
REFERENCES [dbo].[sgd_iexp_metainfoexpediente] ([sgd_iexp_codigo])
GO
ALTER TABLE [dbo].[sgd_iexp_metainfoexpediente] CHECK CONSTRAINT [fk_expediente]
GO
ALTER TABLE [dbo].[sgd_mexp_metadataexp]  WITH CHECK ADD  CONSTRAINT [fk_mexp_depe] FOREIGN KEY([depe_codi])
REFERENCES [dbo].[dependencia] ([depe_codi])
GO
ALTER TABLE [dbo].[sgd_mexp_metadataexp] CHECK CONSTRAINT [fk_mexp_depe]
GO
ALTER TABLE [dbo].[sgd_mod_values]  WITH CHECK ADD  CONSTRAINT [fk_modulos] FOREIGN KEY([sgd_mod_id])
REFERENCES [dbo].[sgd_mod_modules] ([sgd_mod_id])
GO
ALTER TABLE [dbo].[sgd_mod_values] CHECK CONSTRAINT [fk_modulos]
GO
ALTER TABLE [dbo].[sgd_mrd_matrird]  WITH CHECK ADD  CONSTRAINT [fk_mrd_tpr] FOREIGN KEY([sgd_tpr_codigo])
REFERENCES [dbo].[sgd_tpr_tpdcumento] ([sgd_tpr_codigo])
GO
ALTER TABLE [dbo].[sgd_mrd_matrird] CHECK CONSTRAINT [fk_mrd_tpr]
GO
ALTER TABLE [dbo].[sgd_mrd_matrird]  WITH CHECK ADD  CONSTRAINT [FK_sgd_mrd_matrird_sgd_mrd_matrird] FOREIGN KEY([depe_codi])
REFERENCES [dbo].[dependencia] ([depe_codi])
GO
ALTER TABLE [dbo].[sgd_mrd_matrird] CHECK CONSTRAINT [FK_sgd_mrd_matrird_sgd_mrd_matrird]
GO
ALTER TABLE [dbo].[sgd_msdep_msgdep]  WITH CHECK ADD  CONSTRAINT [FK_sgd_msdep_msgdep_sgd_msdep_msgdep] FOREIGN KEY([depe_codi])
REFERENCES [dbo].[dependencia] ([depe_codi])
GO
ALTER TABLE [dbo].[sgd_msdep_msgdep] CHECK CONSTRAINT [FK_sgd_msdep_msgdep_sgd_msdep_msgdep]
GO
ALTER TABLE [dbo].[sgd_mvr_modvsrol]  WITH CHECK ADD  CONSTRAINT [FK_sgd_mvr_modvsrol_sgd_mod_modules] FOREIGN KEY([sgd_mod_id])
REFERENCES [dbo].[sgd_mod_modules] ([sgd_mod_id])
GO
ALTER TABLE [dbo].[sgd_mvr_modvsrol] CHECK CONSTRAINT [FK_sgd_mvr_modvsrol_sgd_mod_modules]
GO
ALTER TABLE [dbo].[sgd_mvr_modvsrol]  WITH CHECK ADD  CONSTRAINT [FK_sgd_mvr_modvsrol_sgd_rol_roles] FOREIGN KEY([sgd_rol_id])
REFERENCES [dbo].[sgd_rol_roles] ([sgd_rol_id])
GO
ALTER TABLE [dbo].[sgd_mvr_modvsrol] CHECK CONSTRAINT [FK_sgd_mvr_modvsrol_sgd_rol_roles]
GO
ALTER TABLE [dbo].[sgd_oem_oempresas]  WITH CHECK ADD  CONSTRAINT [FK_sgd_oem_oempresas_sgd_oem_oempresas] FOREIGN KEY([sgd_oem_codigo])
REFERENCES [dbo].[sgd_oem_oempresas] ([sgd_oem_codigo])
GO
ALTER TABLE [dbo].[sgd_oem_oempresas] CHECK CONSTRAINT [FK_sgd_oem_oempresas_sgd_oem_oempresas]
GO
ALTER TABLE [dbo].[sgd_parexp_paramexpediente]  WITH CHECK ADD  CONSTRAINT [FK_sgd_parexp_paramexpediente_dependencia] FOREIGN KEY([depe_codi])
REFERENCES [dbo].[dependencia] ([depe_codi])
GO
ALTER TABLE [dbo].[sgd_parexp_paramexpediente] CHECK CONSTRAINT [FK_sgd_parexp_paramexpediente_dependencia]
GO
ALTER TABLE [dbo].[sgd_rdf_retdocf]  WITH CHECK ADD  CONSTRAINT [fk_rdf_radi] FOREIGN KEY([radi_nume_radi])
REFERENCES [dbo].[radicado] ([radi_nume_radi])
GO
ALTER TABLE [dbo].[sgd_rdf_retdocf] CHECK CONSTRAINT [fk_rdf_radi]
GO
ALTER TABLE [dbo].[sgd_rdf_retdocf]  WITH CHECK ADD  CONSTRAINT [FK_sgd_rdf_retdocf_dependencia] FOREIGN KEY([depe_codi])
REFERENCES [dbo].[dependencia] ([depe_codi])
GO
ALTER TABLE [dbo].[sgd_rdf_retdocf] CHECK CONSTRAINT [FK_sgd_rdf_retdocf_dependencia]
GO
ALTER TABLE [dbo].[sgd_rdf_retdocf]  WITH CHECK ADD  CONSTRAINT [FK_sgd_rdf_retdocf_sgd_mrd_matrird] FOREIGN KEY([sgd_mrd_codigo])
REFERENCES [dbo].[sgd_mrd_matrird] ([sgd_mrd_codigo])
GO
ALTER TABLE [dbo].[sgd_rdf_retdocf] CHECK CONSTRAINT [FK_sgd_rdf_retdocf_sgd_mrd_matrird]
GO
ALTER TABLE [dbo].[sgd_rdf_retdocf]  WITH CHECK ADD  CONSTRAINT [FK_sgd_rdf_retdocf_sgd_rdf_retdocf] FOREIGN KEY([sgd_mrd_codigo], [depe_codi], [radi_nume_radi])
REFERENCES [dbo].[sgd_rdf_retdocf] ([sgd_mrd_codigo], [depe_codi], [radi_nume_radi])
GO
ALTER TABLE [dbo].[sgd_rdf_retdocf] CHECK CONSTRAINT [FK_sgd_rdf_retdocf_sgd_rdf_retdocf]
GO
ALTER TABLE [dbo].[sgd_rdf_retdocf]  WITH CHECK ADD  CONSTRAINT [fk_sgd_rdf_trd] FOREIGN KEY([sgd_mrd_codigo])
REFERENCES [dbo].[sgd_mrd_matrird] ([sgd_mrd_codigo])
GO
ALTER TABLE [dbo].[sgd_rdf_retdocf] CHECK CONSTRAINT [fk_sgd_rdf_trd]
GO
ALTER TABLE [dbo].[sgd_renv_regenvio]  WITH CHECK ADD  CONSTRAINT [fk_fenv] FOREIGN KEY([sgd_fenv_codigo])
REFERENCES [dbo].[sgd_fenv_frmenvio] ([sgd_fenv_codigo])
GO
ALTER TABLE [dbo].[sgd_renv_regenvio] CHECK CONSTRAINT [fk_fenv]
GO
ALTER TABLE [dbo].[sgd_renv_regenvio]  WITH CHECK ADD  CONSTRAINT [fk_renv_radicado] FOREIGN KEY([radi_nume_sal])
REFERENCES [dbo].[radicado] ([radi_nume_radi])
GO
ALTER TABLE [dbo].[sgd_renv_regenvio] CHECK CONSTRAINT [fk_renv_radicado]
GO
ALTER TABLE [dbo].[sgd_renv_regenvio]  WITH CHECK ADD  CONSTRAINT [FK_sgd_renv_regenvio_dependencia] FOREIGN KEY([depe_codi])
REFERENCES [dbo].[dependencia] ([depe_codi])
GO
ALTER TABLE [dbo].[sgd_renv_regenvio] CHECK CONSTRAINT [FK_sgd_renv_regenvio_dependencia]
GO
ALTER TABLE [dbo].[sgd_renv_regenvio]  WITH CHECK ADD  CONSTRAINT [FK_sgd_renv_regenvio_sgd_deve_dev_envio] FOREIGN KEY([sgd_deve_codigo])
REFERENCES [dbo].[sgd_deve_dev_envio] ([sgd_deve_codigo])
GO
ALTER TABLE [dbo].[sgd_renv_regenvio] CHECK CONSTRAINT [FK_sgd_renv_regenvio_sgd_deve_dev_envio]
GO
ALTER TABLE [dbo].[sgd_seg_seguridad]  WITH CHECK ADD  CONSTRAINT [FK_sgd_seg_seguridad_radicado] FOREIGN KEY([radi_nume_radi])
REFERENCES [dbo].[radicado] ([radi_nume_radi])
GO
ALTER TABLE [dbo].[sgd_seg_seguridad] CHECK CONSTRAINT [FK_sgd_seg_seguridad_radicado]
GO
ALTER TABLE [dbo].[sgd_sexp_secexpedientes]  WITH NOCHECK ADD  CONSTRAINT [fk_sexp_mexp] FOREIGN KEY([sgd_sexp_metadato])
REFERENCES [dbo].[sgd_mexp_metadataexp] ([sgd_mexp_codigo])
GO
ALTER TABLE [dbo].[sgd_sexp_secexpedientes] NOCHECK CONSTRAINT [fk_sexp_mexp]
GO
ALTER TABLE [dbo].[sgd_temas_tiposdoc]  WITH CHECK ADD  CONSTRAINT [FK_TEMTDOC_TEMAS] FOREIGN KEY([sgd_dcau_codigo])
REFERENCES [dbo].[sgd_dcau_causal] ([sgd_dcau_codigo])
GO
ALTER TABLE [dbo].[sgd_temas_tiposdoc] CHECK CONSTRAINT [FK_TEMTDOC_TEMAS]
GO
ALTER TABLE [dbo].[sgd_vtrd_verstrd]  WITH CHECK ADD  CONSTRAINT [FK_sgd_vtrd_verstrd_sgd_vtrd_verstrd] FOREIGN KEY([sgd_vtrd_codigo])
REFERENCES [dbo].[sgd_vtrd_verstrd] ([sgd_vtrd_codigo])
GO
ALTER TABLE [dbo].[sgd_vtrd_verstrd] CHECK CONSTRAINT [FK_sgd_vtrd_verstrd_sgd_vtrd_verstrd]
GO
/****** Object:  StoredProcedure [dbo].[ANEXOEXPEDIENTEConsultarSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ANEXOEXPEDIENTEConsultarSIT]
(
	@ANEXOS_EXP_ID int
)
AS
/********************************************************************************* 
 Nombre:		[ANEXOEXPEDIENTEConsultarSIT]
 Propósito:		Obtener información del anexo
 Autor:			Fabian Losada		
 Fecha:			31/07/2019
 **********************************************************************************/
BEGIN
	SET NOCOUNT ON;
Select 
	aexp_codi  as RADI_NUME_RADI
	,exp_fech  as RADI_FECH_RADI
	,aexp_tpdoc as TDOC_CODI  
	,null as TRTE_CODI  
	,null as MREC_CODI  
	,null as EESP_CODI  
	,null as EOTRA_CODI  
	,null as RADI_TIPO_EMPR  
	,null as RADI_FECH_OFIC  
	,null as TDID_CODI  
	,null as RADI_NUME_IDEN  
	,null as RADI_NOMB  
	,null as RADI_PRIM_APEL  
	,null as RADI_SEGU_APEL  
	,null as RADI_PAIS  
	,null as MUNI_CODI  
	,null as CPOB_CODI  
	,null as CARP_CODI  
	,null as ESTA_CODI  
	,null as DPTO_CODI  
	,null as CEN_MUNI_CODI  
	,null as CEN_DPTO_CODI  
	,null as ID_CONT  
	,null as ID_PAIS  
	,null as RADI_DIRE_CORR  
	,null as RADI_TELE_CONT  
	,null as RADI_NUME_HOJA  
	,null as RADI_DESC_ANEX  
	,null as RADI_NUME_DERI  
  ,aexp_path AS RADI_PATH  
	,null as RADI_USUA_ACTU  
	,null as RADI_DEPE_ACTU  
	,null as RADI_FECH_ASIG  
	,null as RADI_ARCH1  
	,null as RADI_ARCH2  
	,null as RADI_ARCH3  
	,null as RADI_ARCH4  
	,null as RA_ASUN  
	,null as RADI_USU_ANTE  
	,null as RADI_DEPE_RADI  
	,null as RADI_REM  
	,null as RADI_USUA_RADI  
	,null as CODI_NIVEL  
	,null as FLAG_NIVEL  
	,null as CARP_PER  
	,null as RADI_LEIDO  
	,null as RADI_CUENTAI  
	,null as RADI_TIPO_DERI  
	,null as LISTO  
	,null as SGD_TMA_CODIGO  
	,null as SGD_MTD_CODIGO  
	,null as PAR_SERV_SECUE  
	,null as SGD_FLD_CODIGO  
	,null as RADI_AGEND  
	,null as RADI_FECH_AGEND  
	,null as RADI_FECH_DOC  
	,null as SGD_DOC_SECUENCIA  
	,null as SGD_PNUFE_CODI  
	,null as SGD_EANU_CODIGO  
	,null as SGD_NOT_CODI  
	,null as RADI_FECH_NOTIFf  
	,null as SGD_APLI_CODI  
	,null as FECH_VCMTO  
	,null as SGD_SPUB_CODIGO  
	,null as SGD_DIR_DIRECCION  
	,aexp_usua as USUA_DOC

	,null as ID  
	,null as RADI_NOTIFICADO  
	,null as RADI_FECHA_VENCE  
	,null as RADI_DIAS_VENCE  
	,null as RADI_TEMA_ID  
	,null as RADI_DESPLA  
	,null as radi_nume_solicitud  
  ,num_expediente as exp_soli_id  
	,null as SGD_ID_TRAM  
	,null as RADI_NOTIF_FIJACION  
	,null as RADI_NOTIF_DESFIJACION  
	,null as SGD_APLI_CODIGO  
	,null as RADI_USUA_PRIVADO  
	,null as RADI_RESPUESTA  
	,null as REQUIERE_RESP  
	,null as TIPO_RAD  
	,null as ANO_RAD  
	,null as SEC_RAD  
 from sgd_aexp_anexexpediente WITH (NOLOCK)
 where num_anexo = @ANEXOS_EXP_ID

END;
GO
/****** Object:  StoredProcedure [dbo].[ANEXORADICADOConsultarSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ANEXORADICADOConsultarSIT]
(
	@ANEX_CODIGO VARCHAR(20)
)
AS
/********************************************************************************* 
 Nombre:		[ANEXORADICADOConsultarSIT]
 Propósito:		Obtener información del anexo expediente.
 Autor:			Fabian Losada			
 Fecha:			31/07/2019
 **********************************************************************************/
BEGIN
	SET NOCOUNT ON;

	Select 
	ANEX_RADI_NUME  as RADI_NUME_RADI
	,ANEX_RADI_FECH  as RADI_FECH_RADI
	,ANEX_RADI_NUME as TDOC_CODI  
	,null as TRTE_CODI  
	,null as MREC_CODI  
	,null as EESP_CODI  
	,null as EOTRA_CODI  
	,null as RADI_TIPO_EMPR  
	,null as RADI_FECH_OFIC  
	,null as TDID_CODI  
	,null as RADI_NUME_IDEN  
	,null as RADI_NOMB  
	,null as RADI_PRIM_APEL  
	,null as RADI_SEGU_APEL  
	,null as RADI_PAIS  
	,null as MUNI_CODI  
	,null as CPOB_CODI  
	,null as CARP_CODI  
	,null as ESTA_CODI  
	,null as DPTO_CODI  
	,null as CEN_MUNI_CODI  
	,null as CEN_DPTO_CODI  
	,null as ID_CONT  
	,null as ID_PAIS  
	,null as RADI_DIRE_CORR  
	,null as RADI_TELE_CONT  
	,null as RADI_NUME_HOJA  
	,null as RADI_DESC_ANEX  
	,null as RADI_NUME_DERI  
  ,ANEX_NOMB_ARCHIVO AS RADI_PATH  
	,null as RADI_USUA_ACTU  
	,null as RADI_DEPE_ACTU  
	,null as RADI_FECH_ASIG  
	,null as RADI_ARCH1  
	,null as RADI_ARCH2  
	,null as RADI_ARCH3  
	,null as RADI_ARCH4  
	,null as RA_ASUN  
	,null as RADI_USU_ANTE  
	,null as RADI_DEPE_RADI  
	,null as RADI_REM  
	,null as RADI_USUA_RADI  
	,null as CODI_NIVEL  
	,null as FLAG_NIVEL  
	,null as CARP_PER  
	,null as RADI_LEIDO  
	,null as RADI_CUENTAI  
	,null as RADI_TIPO_DERI  
	,null as LISTO  
	,null as SGD_TMA_CODIGO  
	,null as SGD_MTD_CODIGO  
	,null as PAR_SERV_SECUE  
	,null as SGD_FLD_CODIGO  
	,null as RADI_AGEND  
	,null as RADI_FECH_AGEND  
	,null as RADI_FECH_DOC  
	,null as SGD_DOC_SECUENCIA  
	,null as SGD_PNUFE_CODI  
	,null as SGD_EANU_CODIGO  
	,null as SGD_NOT_CODI  
	,null as RADI_FECH_NOTIFf  
	,null as SGD_APLI_CODI  
	,null as FECH_VCMTO  
	,null as SGD_SPUB_CODIGO  
	,null as SGD_DIR_DIRECCION  
	,ANEX_CREADOR as USUA_DOC

	,null as ID  
	,null as RADI_NOTIFICADO  
	,null as RADI_FECHA_VENCE  
	,null as RADI_DIAS_VENCE  
	,null as RADI_TEMA_ID  
	,null as RADI_DESPLA  
	,null as radi_nume_solicitud  
	,null as exp_soli_id  
	,null as SGD_ID_TRAM  
	,null as RADI_NOTIF_FIJACION  
	,null as RADI_NOTIF_DESFIJACION  
	,null as SGD_APLI_CODIGO  
	,null as RADI_USUA_PRIVADO  
	,null as RADI_RESPUESTA  
	,null as REQUIERE_RESP  
	,null as TIPO_RAD  
	,null as ANO_RAD  
	,null as SEC_RAD  
 from dbo.ANEXOS  WITH (NOLOCK)
 WHERE ANEX_CODIGO = @ANEX_CODIGO


END;
GO
/****** Object:  StoredProcedure [dbo].[ANEXOS_TIPOConsultaConFiltroSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ANEXOS_TIPOConsultaConFiltroSIT]
	@ANEX_TIPO_EXT varchar(10)
AS
/********************************************************************************* 
 Nombre:		ANEXOS_TIPOConsultaConFiltro
 Propósito:		Obtiene datos de Tipos de Anexos 
 Tablas:		ANEXOS_TIPO
 Autor:			Fabian Losada
 Fecha:			01/08/2019
 Modifica:  
 Resultado(s):	Datos de Tipos de Anexos
 **********************************************************************************/
 
DECLARE @SQL VARCHAR(MAX);
DECLARE @CON INT;

BEGIN
	SET NOCOUNT ON;

	DECLARE @Resultado TABLE 
    (
		ANEX_TIPO_CODI NUMERIC,
		ANEX_TIPO_EXT VARCHAR(MAX),
		ANEX_TIPO_DESC VARCHAR(MAX),
		ANEX_TIPO_MIME NVARCHAR(MAX),
		ANEX_TIPO_PQR SMALLINT,
		ANEX_PERM_TIPIF_ANEXO SMALLINT
	)

	SET @CON=0;
	SET @SQL = 'SELECT  ANEX_TIPO_CODI
						,ANEX_TIPO_EXT
						,ANEX_TIPO_DESC
						,ANEX_TIPO_MIME
						,ANEX_TIPO_PQR
						,ANEX_PERM_TIPIF_ANEXO
				FROM ANEXOS_TIPO WITH (NOLOCK)
				where 	1=1';
	
	if( LEN(@ANEX_TIPO_EXT) > 0 )
	begin
		set @SQL=@SQL+' AND ANEX_TIPO_EXT like ''%'+CAST(@ANEX_TIPO_EXT as NVARCHAR)+'%''';
		set @CON=1;
	end
	if( @CON = 0 )
	begin
		set @SQL=@SQL+' AND 1<>1';
	end

	INSERT INTO @Resultado	
	EXEC(@SQL);

	SELECT	ANEX_TIPO_CODI
			,ANEX_TIPO_EXT
			,ANEX_TIPO_DESC
			,ANEX_TIPO_MIME
			,ANEX_TIPO_PQR
			,ANEX_PERM_TIPIF_ANEXO
	FROM	@Resultado

END;
GO
/****** Object:  StoredProcedure [dbo].[ANEXOSActualizarSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ANEXOSActualizarSIT]
(
	@ANEX_RADI_NUME numeric(15,0)
    ,@ANEX_CODIGO varchar(20)
    ,@ANEX_TIPO numeric(4,0)
    ,@ANEX_TAMANO numeric(18,0)
    ,@ANEX_SOLO_LECT varchar(1)
    ,@ANEX_CREADOR varchar(24)
    ,@ANEX_DESC varchar(512)
    ,@ANEX_NUMERO numeric(5,0)
    ,@ANEX_NOMB_ARCHIVO varchar(50)
    ,@ANEX_BORRADO varchar(1)
    ,@ANEX_SALIDA numeric(1,0)
    ,@RADI_NUME_SALIDA numeric(15,0)
    ,@ANEX_RADI_FECH datetime
    ,@ANEX_ESTADO numeric(1,0)
    ,@SGD_REM_DESTINO numeric(1,0)
    ,@SGD_DIR_TIPO numeric(4,0)
    ,@ANEX_DEPE_CREADOR numeric(4,0)
    ,@ANEX_FECH_ANEX datetime
    ,@SGD_TRAD_CODIGO tinyint
    ,@SGD_APLI_CODIGO int
	,@ANEX_FECH_ENVIO datetime
	
	,@Original_ANEX_RADI_NUME numeric(15,0)
    ,@Original_ANEX_CODIGO varchar(20)
    ,@Original_ANEX_TIPO numeric(4,0)
    ,@Original_ANEX_TAMANO numeric(18,0)
    ,@Original_ANEX_SOLO_LECT varchar(1)
    ,@Original_ANEX_CREADOR varchar(24)
    ,@Original_ANEX_DESC varchar(512)
    ,@Original_ANEX_NUMERO numeric(5,0)
    ,@Original_ANEX_NOMB_ARCHIVO varchar(50)
    ,@Original_ANEX_BORRADO varchar(1)
    ,@Original_ANEX_SALIDA numeric(1,0)
    ,@Original_RADI_NUME_SALIDA numeric(15,0)
    ,@Original_ANEX_RADI_FECH datetime
    ,@Original_ANEX_ESTADO numeric(1,0)
    ,@Original_SGD_REM_DESTINO numeric(1,0)
    ,@Original_SGD_DIR_TIPO numeric(4,0)
    ,@Original_ANEX_DEPE_CREADOR numeric(4,0)
    ,@Original_ANEX_FECH_ANEX datetime
    ,@Original_SGD_TRAD_CODIGO tinyint
    ,@Original_SGD_APLI_CODIGO int
	,@Original_ANEX_FECH_ENVIO datetime
)
AS SET NOCOUNT OFF;
BEGIN

/********************************************************************************* 
 Nombre	: ANEXOSActualizar
																					
 Proposito: actualiza los datos en la tabla principal de Anexos  
																					
 Tablas	:	dbo.ANEXOS
																					
 Autor	 :  Fabian Losada			Fecha: 2019/08/01
 Modifica:  

 NOTAS:	
 
 Parametros:	
  	@ANEX_RADI_NUME numeric(15,0)
    ,@ANEX_CODIGO varchar(20)
    ,@ANEX_TIPO numeric(4,0)
    ,@ANEX_TAMANO numeric(18,0)
    ,@ANEX_SOLO_LECT varchar(1)
    ,@ANEX_CREADOR varchar(24)
    ,@ANEX_DESC varchar(512)
    ,@ANEX_NUMERO numeric(5,0)
    ,@ANEX_NOMB_ARCHIVO varchar(50)
    ,@ANEX_BORRADO varchar(1)
    ,@ANEX_SALIDA numeric(1,0)
    ,@RADI_NUME_SALIDA numeric(15,0)
    ,@ANEX_RADI_FECH datetime
    ,@ANEX_ESTADO numeric(1,0)
    ,@SGD_REM_DESTINO numeric(1,0)
    ,@SGD_DIR_TIPO numeric(4,0)
    ,@ANEX_DEPE_CREADOR numeric(4,0)
    ,@ANEX_FECH_ANEX datetime
    ,@SGD_TRAD_CODIGO tinyint
    ,@SGD_APLI_CODIGO int
	,@ANEX_FECH_ENVIO datetime
	
	,@Original_ANEX_RADI_NUME numeric(15,0)
    ,@Original_ANEX_CODIGO varchar(20)
    ,@Original_ANEX_TIPO numeric(4,0)
    ,@Original_ANEX_TAMANO numeric(18,0)
    ,@Original_ANEX_SOLO_LECT varchar(1)
    ,@Original_ANEX_CREADOR varchar(24)
    ,@Original_ANEX_DESC varchar(512)
    ,@Original_ANEX_NUMERO numeric(5,0)
    ,@Original_ANEX_NOMB_ARCHIVO varchar(50)
    ,@Original_ANEX_BORRADO varchar(1)
    ,@Original_ANEX_SALIDA numeric(1,0)
    ,@Original_RADI_NUME_SALIDA numeric(15,0)
    ,@Original_ANEX_RADI_FECH datetime
    ,@Original_ANEX_ESTADO numeric(1,0)
    ,@Original_SGD_REM_DESTINO numeric(1,0)
    ,@Original_SGD_DIR_TIPO numeric(4,0)
    ,@Original_ANEX_DEPE_CREADOR numeric(4,0)
    ,@Original_ANEX_FECH_ANEX datetime
    ,@Original_SGD_TRAD_CODIGO tinyint
    ,@Original_SGD_APLI_CODIGO int
	,@Original_ANEX_FECH_ENVIO datetime
	
 Resultado(s):	 Datos de ANEXOS Actualizados
**********************************************************************************/
UPDATE [dbo].[ANEXOS]
SET 
	ANEX_TIPO=@ANEX_TIPO,
	ANEX_TAMANO =@ANEX_TAMANO ,
	ANEX_SOLO_LECT =@ANEX_SOLO_LECT ,
	ANEX_CREADOR=@ANEX_CREADOR,
	ANEX_DESC =@ANEX_DESC ,
	ANEX_NUMERO=@ANEX_NUMERO,
	ANEX_NOMB_ARCHIVO =@ANEX_NOMB_ARCHIVO ,
	ANEX_BORRADO=@ANEX_BORRADO,
	ANEX_SALIDA=@ANEX_SALIDA,
	RADI_NUME_SALIDA =@RADI_NUME_SALIDA ,
	ANEX_RADI_FECH=@ANEX_RADI_FECH,
	ANEX_ESTADO=@ANEX_ESTADO,
	SGD_REM_DESTINO=@SGD_REM_DESTINO,
	SGD_DIR_TIPO=@SGD_DIR_TIPO,
	ANEX_DEPE_CREADOR=@ANEX_DEPE_CREADOR,
	ANEX_FECH_ANEX=@ANEX_FECH_ANEX,
	SGD_TRAD_CODIGO=@SGD_TRAD_CODIGO,
	SGD_APLI_CODIGO=@SGD_APLI_CODIGO,
	ANEX_FECH_ENVIO = @ANEX_FECH_ENVIO 
	
WHERE
(
	ANEX_CODIGO =@Original_ANEX_CODIGO 

);
SELECT [ANEX_RADI_NUME]
	  ,[ANEX_CODIGO]
      ,[ANEX_TIPO]
      ,[ANEX_TAMANO]
      ,[ANEX_SOLO_LECT]
      ,[ANEX_CREADOR]
      ,[ANEX_DESC]
      ,[ANEX_NUMERO]
      ,[ANEX_NOMB_ARCHIVO]
      ,[ANEX_BORRADO]
      ,[ANEX_ORIGEN]
      ,[ANEX_UBIC]
      ,[ANEX_SALIDA]
      ,[RADI_NUME_SALIDA]
      ,[ANEX_RADI_FECH]
      ,[ANEX_ESTADO]
      ,[USUA_DOC]
      ,[SGD_REM_DESTINO]
      ,[ANEX_FECH_ENVIO]
      ,[SGD_DIR_TIPO]
      ,[ANEX_FECH_IMPRES]
      ,[ANEX_DEPE_CREADOR]
      ,[SGD_DOC_SECUENCIA]
      ,[SGD_DOC_PADRE]
      ,[SGD_ARG_CODIGO]
      ,[SGD_TPR_CODIGO]
      ,[SGD_DEVE_CODIGO]
      ,[SGD_DEVE_FECH]
      ,[SGD_FECH_IMPRES]
      ,[ANEX_FECH_ANEX]
      ,[ANEX_DEPE_CODI]
      ,[SGD_PNUFE_CODI]
      ,[SGD_DNUFE_CODI]
      ,[ANEX_USUDOC_CREADOR]
      ,[SGD_FECH_DOC]
      ,[SGD_APLI_CODI]
      ,[SGD_TRAD_CODIGO]
      ,[SGD_EXP_NUMERO]
      ,[MUNI_CODI]
      ,[DPTO_CODI]
      ,[SGD_DIR_DIRECCION]
      ,[ANEX_ESTADO_EMAIL]
      ,NULL AS [ANEX_ORDEN]
      ,[SGD_APLI_CODIGO]
      ,[RUTADOC_WEBDAV]
	FROM [dbo].[ANEXOS] WITH (NOLOCK)
	WHERE ANEX_RADI_NUME  = @ANEX_RADI_NUME and ANEX_CODIGO =@ANEX_CODIGO 
	
END
GO
/****** Object:  StoredProcedure [dbo].[ANEXOSConsultaConFiltroSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ANEXOSConsultaConFiltroSIT]
	
	
	@ANEX_RADI_NUME numeric(15,0)
    ,@ANEX_CODIGO varchar(20)

AS
 
DECLARE @SQL VARCHAR(MAX);
DECLARE @CON INT;

BEGIN
	SET NOCOUNT ON;

	 DECLARE @Resultado TABLE 
    (
		[ANEX_RADI_NUME] [numeric](15, 0) NOT NULL,
		[ANEX_CODIGO] [varchar](20) NOT NULL,
		[ANEX_TIPO] [numeric](4, 0) NOT NULL,
		[ANEX_TAMANO] [numeric](18, 0) NULL,
		[ANEX_SOLO_LECT] [varchar](1) NOT NULL,
		[ANEX_CREADOR] [varchar](24) NOT NULL,
		[ANEX_DESC] [varchar](512) NULL,
		[ANEX_NUMERO] [numeric](5, 0) NOT NULL,
		[ANEX_NOMB_ARCHIVO] [varchar](50) NOT NULL,
		[ANEX_BORRADO] [varchar](1) NOT NULL,
		[ANEX_ORIGEN] [numeric](1, 0) NULL,
		[ANEX_UBIC] [varchar](15) NULL,
		[ANEX_SALIDA] [numeric](1, 0) NULL,
		[RADI_NUME_SALIDA] [numeric](15, 0) NULL,
		[ANEX_RADI_FECH] [datetime] NULL,
		[ANEX_ESTADO] [numeric](1, 0) NULL,
		[USUA_DOC] [varchar](14) NULL,
		[SGD_REM_DESTINO] [numeric](1, 0) NULL,
		[ANEX_FECH_ENVIO] [datetime] NULL,
		[SGD_DIR_TIPO] [numeric](4, 0) NULL,
		[ANEX_FECH_IMPRES] [datetime] NULL,
		[ANEX_DEPE_CREADOR] [numeric](4, 0) NULL,
		[SGD_DOC_SECUENCIA] [numeric](15, 0) NULL,
		[SGD_DOC_PADRE] [varchar](20) NULL,
		[SGD_ARG_CODIGO] [numeric](2, 0) NULL,
		[SGD_TPR_CODIGO] [numeric](4, 0) NULL,
		[SGD_DEVE_CODIGO] [numeric](2, 0) NULL,
		[SGD_DEVE_FECH] [datetime] NULL,
		[SGD_FECH_IMPRES] [datetime] NULL,
		[ANEX_FECH_ANEX] [datetime] NULL,
		[ANEX_DEPE_CODI] [varchar](3) NULL,
		[SGD_PNUFE_CODI] [numeric](4, 0) NULL,
		[SGD_DNUFE_CODI] [numeric](4, 0) NULL,
		[ANEX_USUDOC_CREADOR] [varchar](15) NULL,
		[SGD_FECH_DOC] [datetime] NULL,
		[SGD_APLI_CODI] [smallint] NULL,
		[SGD_TRAD_CODIGO] [tinyint] NULL,
		[SGD_EXP_NUMERO] [varchar](25) NULL,
		[MUNI_CODI] [int] NULL,
		[DPTO_CODI] [int] NULL,
		[SGD_DIR_DIRECCION] [varchar](100) NULL,
		[ANEX_ESTADO_EMAIL] [numeric](1, 0) NULL,
		[ANEX_ORDEN] [numeric](4, 0) NULL,
		[SGD_APLI_CODIGO] [int] NULL,
		[RUTADOC_WEBDAV] [varchar](50) NULL
	)

	SET @CON=0;
	SET @SQL = 'SELECT [ANEX_RADI_NUME]
	  ,[ANEX_CODIGO]
      ,[ANEX_TIPO]
      ,[ANEX_TAMANO]
      ,[ANEX_SOLO_LECT]
      ,[ANEX_CREADOR]
      ,[ANEX_DESC]
      ,[ANEX_NUMERO]
      ,[ANEX_NOMB_ARCHIVO]
      ,[ANEX_BORRADO]
      ,[ANEX_ORIGEN]
      ,[ANEX_UBIC]
      ,[ANEX_SALIDA]
      ,[RADI_NUME_SALIDA]
      ,[ANEX_RADI_FECH]
      ,[ANEX_ESTADO]
      ,[USUA_DOC]
      ,[SGD_REM_DESTINO]
      ,[ANEX_FECH_ENVIO]
      ,[SGD_DIR_TIPO]
      ,[ANEX_FECH_IMPRES]
      ,[ANEX_DEPE_CREADOR]
      ,[SGD_DOC_SECUENCIA]
      ,[SGD_DOC_PADRE]
      ,[SGD_ARG_CODIGO]
      ,[SGD_TPR_CODIGO]
      ,[SGD_DEVE_CODIGO]
      ,[SGD_DEVE_FECH]
      ,[SGD_FECH_IMPRES]
      ,[ANEX_FECH_ANEX]
      ,[ANEX_DEPE_CODI]
      ,[SGD_PNUFE_CODI]
      ,[SGD_DNUFE_CODI]
      ,[ANEX_USUDOC_CREADOR]
      ,[SGD_FECH_DOC]
      ,[SGD_APLI_CODI]
      ,[SGD_TRAD_CODIGO]
      ,[SGD_EXP_NUMERO]
      ,[MUNI_CODI]
      ,[DPTO_CODI]
      ,[SGD_DIR_DIRECCION]
      ,[ANEX_ESTADO_EMAIL]
      ,NULL AS [ANEX_ORDEN]
      ,[SGD_APLI_CODIGO]
      ,[RUTADOC_WEBDAV]
			FROM [dbo].[ANEXOS] WITH (NOLOCK)
				where 	1=1';
	
	if( @ANEX_RADI_NUME > 0 )
	begin
		set @SQL=@SQL+' AND ANEX_RADI_NUME='+CAST(@ANEX_RADI_NUME as NVARCHAR)+'';
		set @CON=1;
	end
	if( LEN(@ANEX_CODIGO) > 0 )
	begin
		set @SQL=@SQL+' AND ANEX_CODIGO like ''%'+CAST(@ANEX_CODIGO as NVARCHAR)+'%''';
		set @CON=1;
	end
	if( @CON = 0 )
	begin
		set @SQL=@SQL+' AND 1<>1';
	end
	


	INSERT INTO @Resultado	
	EXEC(@Sql)

	SELECT *
	FROM @Resultado

END;
GO
/****** Object:  StoredProcedure [dbo].[ANEXOSInsertarSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ANEXOSInsertarSIT]
(
	@ANEX_RADI_NUME numeric(15,0)
    ,@ANEX_CODIGO varchar(20)
    ,@ANEX_TIPO numeric(4,0)
    ,@ANEX_TAMANO numeric(18,0)
    ,@ANEX_SOLO_LECT varchar(1)
    ,@ANEX_CREADOR varchar(24)
    ,@ANEX_DESC varchar(512)
    ,@ANEX_NUMERO numeric(5,0)
    ,@ANEX_NOMB_ARCHIVO varchar(50)
    ,@ANEX_BORRADO varchar(1)
    ,@ANEX_SALIDA numeric(1,0)
    ,@RADI_NUME_SALIDA numeric(15,0)
    ,@ANEX_RADI_FECH datetime
    ,@ANEX_ESTADO numeric(1,0)
    ,@SGD_REM_DESTINO numeric(1,0)
    ,@SGD_DIR_TIPO numeric(4,0)
    ,@ANEX_DEPE_CREADOR numeric(4,0)
    ,@ANEX_FECH_ANEX datetime
    ,@SGD_TRAD_CODIGO tinyint
    ,@SGD_APLI_CODIGO int
	,@SGD_TPR_CODIGO numeric(15,0)
)
AS
BEGIN

/********************************************************************************* 
 Nombre	: ANEXOSInsertar
																					
 Proposito: inserta los datos en la tabla principal de Anexos  
																					
 Tablas	:	dbo.ANEXOS
																					
 Autor	 :  Fabian Losada			Fecha: 2019/08/01
 Modifica:  

 NOTAS:	
 
 Parametros:	
  	@ANEX_RADI_NUME 
    ,@ANEX_CODIGO 
    ,@ANEX_TIPO
    ,@ANEX_TAMANO 
    ,@ANEX_SOLO_LECT 
    ,@ANEX_CREADOR
    ,@ANEX_DESC 
    ,@ANEX_NUMERO
    ,@ANEX_NOMB_ARCHIVO 
    ,@ANEX_BORRADO
    ,@ANEX_SALIDA
    ,@RADI_NUME_SALIDA 
    ,@ANEX_RADI_FECH
    ,@ANEX_ESTADO
    ,@SGD_REM_DESTINO
    ,@SGD_DIR_TIPO
    ,@ANEX_DEPE_CREADOR
    ,@ANEX_FECH_ANEX
    ,@SGD_TRAD_CODIGO
    ,@SGD_APLI_CODIGO 
	
 Resultado(s):	 Datos de ANEXOS Insertado
**********************************************************************************/

	INSERT INTO [dbo].[ANEXOS]
           (ANEX_RADI_NUME 
			,ANEX_CODIGO 
			,ANEX_TIPO
			,ANEX_TAMANO 
			,ANEX_SOLO_LECT 
			,ANEX_CREADOR
			,ANEX_DESC 
			,ANEX_NUMERO
			,ANEX_NOMB_ARCHIVO 
			,ANEX_BORRADO
			,ANEX_SALIDA
			,RADI_NUME_SALIDA 
			,ANEX_RADI_FECH
			,ANEX_ESTADO
			,SGD_REM_DESTINO
			,SGD_DIR_TIPO
			,ANEX_DEPE_CREADOR
			,ANEX_FECH_ANEX
			,SGD_TRAD_CODIGO
			,SGD_APLI_CODIGO
			,SGD_TPR_CODIGO
		   )
		 VALUES
		(	
			@ANEX_RADI_NUME 
			,@ANEX_CODIGO 
			,@ANEX_TIPO
			,@ANEX_TAMANO 
			,@ANEX_SOLO_LECT 
			,@ANEX_CREADOR
			,@ANEX_DESC 
			,@ANEX_NUMERO
			,@ANEX_NOMB_ARCHIVO 
			,@ANEX_BORRADO
			,@ANEX_SALIDA
			,@RADI_NUME_SALIDA 
			,@ANEX_RADI_FECH
			,@ANEX_ESTADO
			,@SGD_REM_DESTINO
			,@SGD_DIR_TIPO
			,@ANEX_DEPE_CREADOR
			,@ANEX_FECH_ANEX
			,@SGD_TRAD_CODIGO
			,@SGD_APLI_CODIGO
			,@SGD_TPR_CODIGO
		   )

	SELECT ANEX_RADI_NUME
		,ANEX_CODIGO
		,ANEX_TIPO
		,ANEX_TAMANO
		,ANEX_SOLO_LECT
		,ANEX_CREADOR
		,ANEX_DESC
		,ANEX_NUMERO
		,ANEX_NOMB_ARCHIVO
		,ANEX_BORRADO
		,ANEX_ORIGEN
		,ANEX_UBIC
		,ANEX_SALIDA
		,RADI_NUME_SALIDA
		,ANEX_RADI_FECH
		,ANEX_ESTADO
		,USUA_DOC
		,SGD_REM_DESTINO
		,ANEX_FECH_ENVIO
		,SGD_DIR_TIPO
		,ANEX_FECH_IMPRES
		,ANEX_DEPE_CREADOR
		,SGD_DOC_SECUENCIA
		,SGD_DOC_PADRE
		,SGD_ARG_CODIGO
		,SGD_TPR_CODIGO
		,SGD_DEVE_CODIGO
		,SGD_DEVE_FECH
		,SGD_FECH_IMPRES
		,ANEX_FECH_ANEX
		,ANEX_DEPE_CODI
		,SGD_PNUFE_CODI
		,SGD_DNUFE_CODI
		,ANEX_USUDOC_CREADOR
		,SGD_FECH_DOC
		,SGD_APLI_CODI
		,SGD_TRAD_CODIGO
		,SGD_EXP_NUMERO
		,MUNI_CODI
		,DPTO_CODI
		,SGD_DIR_DIRECCION
		,ANEX_ESTADO_EMAIL
		,NULL AS ANEX_ORDEN
		,SGD_APLI_CODIGO
		,RUTADOC_WEBDAV
	FROM [dbo].[ANEXOS] with (nolock)
	WHERE ANEX_RADI_NUME  = @ANEX_RADI_NUME and ANEX_CODIGO =@ANEX_CODIGO 
	
END


GO
/****** Object:  StoredProcedure [dbo].[anuladosM]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[anuladosM]
AS
BEGIN
	-- Declare the return variable here
	DECLARE @USUA_CODI	int,	 --Código del usuario 
			@DEPE_CODI	int,	-- Código de la Dependencia actual
			@CODI	int,	--secuencia informados
			@USUA_CODI_NEW	int, -- codigo nuevo usuario
			@USUA_CODI_NEW_ANU	int, --codigo nuevo anulacion 
			@USUA_CODI_ANU int, --usuario anulacion
			@DEPE_CODI_ANU int --dependencia anulacion
    -- migracion datos tabla sgd_anu_anulados
	insert into sgd_anu_anulados ([sgd_anu_desc],[radi_nume_radi],[sgd_eanu_codi],[sgd_anu_sol_fech],[sgd_anu_fech],[depe_codi],[usua_doc],[usua_codi]
      ,[depe_codi_anu],[usua_doc_anu],[usua_codi_anu],[usua_anu_acta],[sgd_anu_path_acta],[sgd_trad_codigo])
SELECT [sgd_anu_desc],[radi_nume_radi],[sgd_eanu_codi],[sgd_anu_sol_fech],[sgd_anu_fech],[depe_codi],[usua_doc],[usua_codi]
      ,[depe_codi_anu],[usua_doc_anu],[usua_codi_anu],[usua_anu_acta],[sgd_anu_path_acta],[sgd_trad_codigo]
  FROM bdorfeo.[dbo].[sgd_anu_anulados] where RADI_NUME_RADI in (select radi_nume_radi from radicado);
  --cursor usuarios tabla sgd_anu_anulados
	DECLARE anu CURSOR FOR select USUA_CODI,DEPE_CODI,sgd_anu_id,usua_codi_anu,depe_codi_anu
 from sgd_anu_anulados;
  OPEN anu;
  WHILE (1=1)
  begin;
  FETCH NEXT
			   FROM anu
			   INTO @USUA_CODI, @DEPE_CODI, @CODI,@USUA_CODI_ANU,@DEPE_CODI_ANU;
	  IF @@FETCH_STATUS < 0 BREAK;
	select @USUA_CODI_NEW=usua_codi from usua_migracion where depe_codi=@DEPE_CODI and usua_old=@USUA_CODI;
	select @USUA_CODI_NEW_ANU=usua_codi from usua_migracion where depe_codi=@DEPE_CODI_ANU and usua_old=@USUA_CODI_ANU;
	update sgd_anu_anulados set usua_codi=@USUA_CODI_NEW, usua_codi_anu=@USUA_CODI_NEW_ANU where sgd_anu_id=@CODI;
  end;
  close anu;
END
GO
/****** Object:  StoredProcedure [dbo].[BODEGA_EMPRESASConsultaConFiltroSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BODEGA_EMPRESASConsultaConFiltroSIT]
	
	
	@IDENTIFICADOR_EMPRESA int,
	@NOMBRE_DE_LA_EMPRESA varchar(160),
	@NIT_DE_LA_EMPRESA varchar(80),
	@SIGLA_DE_LA_EMPRESA varchar(80)

AS
/********************************************************************************* 
 Nombre:		BODEGA_EMPRESASConsultaConFiltro
 Propósito:		Obtiene datos de contacto Entidades
 Tablas:		BODEGA_EMPRESAS
 Autor:			Fabian Losada	
 Fecha:			01/08/2019
 Modifica:  
 Resultado(s):	Datos de contacto de tipo Entidades
 **********************************************************************************/
 
DECLARE @SQL VARCHAR(MAX);
DECLARE @CON INT;

BEGIN
	SET NOCOUNT ON;
	SET @CON=0;
	SET @SQL = 'Select	NUIR,
                        NOMBRE_DE_LA_EMPRESA,
                        NIT_DE_LA_EMPRESA,
                        SIGLA_DE_LA_EMPRESA,
                        DIRECCION,
                        CODIGO_DEL_DEPARTAMENTO,
                        CODIGO_DEL_MUNICIPIO,
                        TELEFONO_1,
                        EMAIL,
                        NOMBRE_REP_LEGAL,
                        IDENTIFICADOR_EMPRESA,
                        ID_PAIS,
                        ID_CONT,
                        ACTIVA,
						COD_POSTAL
				from 	dbo.BODEGA_EMPRESAS WITH (NOLOCK)
				where 	1=1';
	
	if( @IDENTIFICADOR_EMPRESA > 0 )
	begin
		set @SQL=@SQL+' AND IDENTIFICADOR_EMPRESA='''+CAST(@IDENTIFICADOR_EMPRESA as NVARCHAR)+'''';
		set @CON=1;
	end
	if( LEN(@NOMBRE_DE_LA_EMPRESA) > 0 )
	begin
		set @SQL=@SQL+' AND NOMBRE_DE_LA_EMPRESA like ''%'+CAST(@NOMBRE_DE_LA_EMPRESA as NVARCHAR)+'%''';
		set @CON=1;
	end
	if( LEN(@NIT_DE_LA_EMPRESA) > 0 )
	begin
		set @SQL=@SQL+' AND NIT_DE_LA_EMPRESA like ''%'+CAST(@NIT_DE_LA_EMPRESA as NVARCHAR)+'%''';
		set @CON=1;
	end
	if( LEN(@SIGLA_DE_LA_EMPRESA) > 0 )
	begin
		set @SQL=@SQL+' AND SIGLA_DE_LA_EMPRESA like ''%'+CAST(@SIGLA_DE_LA_EMPRESA as NVARCHAR)+'%''';
		set @CON=1;
	end
	if( @CON = 0 )
	begin
		set @SQL=@SQL+' AND 1<>1';
	end

	DECLARE @Resultado TABLE
	(
		NUIR				 varchar(13),
		NOMBRE_DE_LA_EMPRESA varchar(160),
		NIT_DE_LA_EMPRESA	 varchar(80),
		SIGLA_DE_LA_EMPRESA varchar(80),
		DIRECCION			varchar(80),
		CODIGO_DEL_DEPARTAMENTO numeric(2, 0),
		CODIGO_DEL_MUNICIPIO numeric(3, 0),
		TELEFONO_1				varchar(15),
		EMAIL				varchar(100),
		NOMBRE_REP_LEGAL varchar(75),
		IDENTIFICADOR_EMPRESA int,
		ID_PAIS smallint,
		ID_CONT tinyint,
		ACTIVA smallint,
		COD_POSTAL varchar(8)
	)

	INSERT INTO @Resultado 
	exec(@SQL)

	select
		NUIR,
		NOMBRE_DE_LA_EMPRESA,
		NIT_DE_LA_EMPRESA,
		SIGLA_DE_LA_EMPRESA,
		DIRECCION,
		CODIGO_DEL_DEPARTAMENTO,
		CODIGO_DEL_MUNICIPIO,
		TELEFONO_1,
		EMAIL,
		NOMBRE_REP_LEGAL,
		IDENTIFICADOR_EMPRESA,
		ID_PAIS,
		ID_CONT,
		ACTIVA,
		COD_POSTAL
	FROM @Resultado

END;
GO
/****** Object:  StoredProcedure [dbo].[BODEGA_EMPRESASObtenerEmpresasPaginadoCantidadSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BODEGA_EMPRESASObtenerEmpresasPaginadoCantidadSIT] 
	-- Add the parameters for the stored procedure here
	@IdentificadorEmpresa	NVARCHAR(100),
	@NombreEmpresa			NVARCHAR(100),
	@NitEmpresa				NVARCHAR(100),
	@OrdenarPor				NVARCHAR(100),
	@RegistroInicial		INT,
	@CantidadRegistros		INT,
	@Registros				INT	OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	DECLARE @Sql NVARCHAR(MAX)
	DECLARE @SqlCount NVARCHAR(MAX)
    DECLARE @Where NVARCHAR(MAX)
    DECLARE @Resultado TABLE 
    (
		[NUIR] NVARCHAR(13),
		[NOMBRE_DE_LA_EMPRESA] NVARCHAR(160),
		[NIT_DE_LA_EMPRESA] NVARCHAR(80),
		[SIGLA_DE_LA_EMPRESA] NVARCHAR(80),
		[DIRECCION] NVARCHAR(80),
		[CODIGO_DEL_DEPARTAMENTO] NUMERIC(2,0),
		[CODIGO_DEL_MUNICIPIO] NUMERIC(3,0),
		[TELEFONO_1] NVARCHAR(15),
		[TELEFONO_2] NVARCHAR(15),
		[EMAIL] NVARCHAR(100),
		[NOMBRE_REP_LEGAL] NVARCHAR(75),
		[CARGO_REP_LEGAL] NUMERIC(2,0),
		[IDENTIFICADOR_EMPRESA] INT,
		[ARE_ESP_SECUE] NUMERIC(18,0),
		[ID_CONT] TINYINT,
		[ID_PAIS] SMALLINT,
		[ACTIVA] SMALLINT,
		[COD_POSTAL] NVARCHAR(8)
	)
	
	SET @Where = ''
	
	IF @RegistroInicial IS NULL OR @RegistroInicial = 0
    BEGIN
    	SET @RegistroInicial = 1
    END
    IF @CantidadRegistros IS NULL
    BEGIN
    	SET @CantidadRegistros = 25
    END
    IF @IdentificadorEmpresa IS NOT NULL AND @IdentificadorEmpresa <> ''
	BEGIN
		SET @Where = @Where + ' AND [IDENTIFICADOR_EMPRESA] = ' + @IdentificadorEmpresa 
	END
	IF @NombreEmpresa IS NOT NULL
	BEGIN
		SET @Where = @Where + ' AND (ISNULL([NOMBRE_DE_LA_EMPRESA], '''')) LIKE ''%' + @NombreEmpresa + '%'''
	END
	IF @NitEmpresa IS NOT NULL AND @NitEmpresa <> ''
	BEGIN
		SET @Where = @Where + ' AND (ISNULL([NIT_DE_LA_EMPRESA], '''')) LIKE ''%' + @NitEmpresa + '%'''
	END
	
	SELECT @Registros= COUNT(*) FROM [dbo].[BODEGA_EMPRESAS] WHERE ACTIVA = 1 
	AND [IDENTIFICADOR_EMPRESA] = ISNULL(NULLIF(@IdentificadorEmpresa,''),[IDENTIFICADOR_EMPRESA])
	AND (ISNULL([NOMBRE_DE_LA_EMPRESA], '')) LIKE '%' + ISNULL(NULLIF(@NombreEmpresa,''),(ISNULL([NOMBRE_DE_LA_EMPRESA], ''))) + '%'
	AND (ISNULL([NIT_DE_LA_EMPRESA], '')) LIKE '%' + ISNULL(NULLIF(@NitEmpresa,''),(ISNULL([NIT_DE_LA_EMPRESA], ''))) + '%'

	SET @Sql = '
	WITH TABLA AS 
	(
		SELECT	ROW_NUMBER () OVER (ORDER BY CONSULTA.'+@OrdenarPor+') FILA, CONSULTA.* FROM(
			SELECT	[NUIR]
				  ,[NOMBRE_DE_LA_EMPRESA]
				  ,[NIT_DE_LA_EMPRESA]
				  ,[SIGLA_DE_LA_EMPRESA]
				  ,[DIRECCION]
				  ,[CODIGO_DEL_DEPARTAMENTO]
				  ,[CODIGO_DEL_MUNICIPIO]
				  ,[TELEFONO_1]
				  ,[TELEFONO_2]
				  ,[EMAIL]
				  ,[NOMBRE_REP_LEGAL]
				  ,[CARGO_REP_LEGAL]
				  ,[IDENTIFICADOR_EMPRESA]
				  ,[ARE_ESP_SECUE]
				  ,[ID_CONT]
				  ,[ID_PAIS]
				  ,[ACTIVA]
				  ,[COD_POSTAL]
			FROM	[dbo].[BODEGA_EMPRESAS]
			WHERE	ACTIVA = 1
					'+ @Where +'
		) CONSULTA
	)
	SELECT	[NUIR]
			,[NOMBRE_DE_LA_EMPRESA]
			,[NIT_DE_LA_EMPRESA]
			,[SIGLA_DE_LA_EMPRESA]
			,[DIRECCION]
			,[CODIGO_DEL_DEPARTAMENTO]
			,[CODIGO_DEL_MUNICIPIO]
			,[TELEFONO_1]
			,[TELEFONO_2]
			,[EMAIL]
			,[NOMBRE_REP_LEGAL]
			,[CARGO_REP_LEGAL]
			,[IDENTIFICADOR_EMPRESA]
			,[ARE_ESP_SECUE]
			,[ID_CONT]
			,[ID_PAIS]
			,[ACTIVA]
			,[COD_POSTAL]
	FROM	TABLA t	
	WHERE	FILA BETWEEN '+CAST(@RegistroInicial AS NVARCHAR)+' AND ('+CAST(@RegistroInicial AS NVARCHAR)+' + '+ CAST(@CantidadRegistros -1 AS NVARCHAR)+')'

	print @Sql

	INSERT INTO @Resultado	
	EXEC(@Sql)
	
	SELECT	[NUIR]
			,[NOMBRE_DE_LA_EMPRESA]
			,[NIT_DE_LA_EMPRESA]
			,[SIGLA_DE_LA_EMPRESA]
			,[DIRECCION]
			,[CODIGO_DEL_DEPARTAMENTO]
			,[CODIGO_DEL_MUNICIPIO]
			,[TELEFONO_1]
			,[TELEFONO_2]
			,[EMAIL]
			,[NOMBRE_REP_LEGAL]
			,[CARGO_REP_LEGAL]
			,[IDENTIFICADOR_EMPRESA]
			,[ARE_ESP_SECUE]
			,[ID_CONT]
			,[ID_PAIS]
			,[ACTIVA]
			,[COD_POSTAL]
	FROM	@Resultado
	RETURN
END
GO
/****** Object:  StoredProcedure [dbo].[BODEGA_EMPRESASObtenerEmpresasPaginadoSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BODEGA_EMPRESASObtenerEmpresasPaginadoSIT] 
	-- Add the parameters for the stored procedure here
	@IdentificadorEmpresa	NVARCHAR(100),
	@NombreEmpresa			NVARCHAR(100),
	@NitEmpresa				NVARCHAR(100),
	@OrdenarPor				NVARCHAR(100),
	@RegistroInicial		INT,
	@CantidadRegistros		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	DECLARE @Sql NVARCHAR(MAX)
    DECLARE @Where NVARCHAR(MAX)
    DECLARE @Resultado TABLE 
    (
		[NUIR] NVARCHAR(13),
		[NOMBRE_DE_LA_EMPRESA] NVARCHAR(160),
		[NIT_DE_LA_EMPRESA] NVARCHAR(80),
		[SIGLA_DE_LA_EMPRESA] NVARCHAR(80),
		[DIRECCION] NVARCHAR(80),
		[CODIGO_DEL_DEPARTAMENTO] NUMERIC(2,0),
		[CODIGO_DEL_MUNICIPIO] NUMERIC(3,0),
		[TELEFONO_1] NVARCHAR(15),
		[TELEFONO_2] NVARCHAR(15),
		[EMAIL] NVARCHAR(100),
		[NOMBRE_REP_LEGAL] NVARCHAR(75),
		[CARGO_REP_LEGAL] NUMERIC(2,0),
		[IDENTIFICADOR_EMPRESA] INT,
		[ARE_ESP_SECUE] NUMERIC(18,0),
		[ID_CONT] TINYINT,
		[ID_PAIS] SMALLINT,
		[ACTIVA] SMALLINT,
		[COD_POSTAL] NVARCHAR(8)
	)
	
	SET @Where = ''
	
	IF @RegistroInicial IS NULL OR @RegistroInicial = 0
    BEGIN
    	SET @RegistroInicial = 1
    END
    IF @CantidadRegistros IS NULL
    BEGIN
    	SET @CantidadRegistros = 25
    END
    IF @IdentificadorEmpresa IS NOT NULL AND @IdentificadorEmpresa <> ''
	BEGIN
		SET @Where = @Where + ' AND [IDENTIFICADOR_EMPRESA] = ' + @IdentificadorEmpresa 
	END
	IF @NombreEmpresa IS NOT NULL
	BEGIN
		SET @Where = @Where + ' AND (ISNULL([NOMBRE_DE_LA_EMPRESA], '''')) LIKE ''%' + @NombreEmpresa + '%'''
	END
	IF @NitEmpresa IS NOT NULL AND @NitEmpresa <> ''
	BEGIN
		SET @Where = @Where + ' AND (ISNULL([NIT_DE_LA_EMPRESA], '''')) LIKE ''%' + @NitEmpresa + '%'''
	END
	
		
	SET @Sql = '
	WITH TABLA AS 
	(
		SELECT	ROW_NUMBER () OVER (ORDER BY CONSULTA.'+@OrdenarPor+') FILA, CONSULTA.* FROM(
			SELECT	[NUIR]
				  ,[NOMBRE_DE_LA_EMPRESA]
				  ,[NIT_DE_LA_EMPRESA]
				  ,[SIGLA_DE_LA_EMPRESA]
				  ,[DIRECCION]
				  ,[CODIGO_DEL_DEPARTAMENTO]
				  ,[CODIGO_DEL_MUNICIPIO]
				  ,[TELEFONO_1]
				  ,[TELEFONO_2]
				  ,[EMAIL]
				  ,[NOMBRE_REP_LEGAL]
				  ,[CARGO_REP_LEGAL]
				  ,[IDENTIFICADOR_EMPRESA]
				  ,[ARE_ESP_SECUE]
				  ,[ID_CONT]
				  ,[ID_PAIS]
				  ,[ACTIVA]
				  ,[COD_POSTAL]
			FROM	[dbo].[BODEGA_EMPRESAS]
			WHERE	ACTIVA = 1
					'+ @Where +'
		) CONSULTA
	)
	SELECT	[NUIR]
			,[NOMBRE_DE_LA_EMPRESA]
			,[NIT_DE_LA_EMPRESA]
			,[SIGLA_DE_LA_EMPRESA]
			,[DIRECCION]
			,[CODIGO_DEL_DEPARTAMENTO]
			,[CODIGO_DEL_MUNICIPIO]
			,[TELEFONO_1]
			,[TELEFONO_2]
			,[EMAIL]
			,[NOMBRE_REP_LEGAL]
			,[CARGO_REP_LEGAL]
			,[IDENTIFICADOR_EMPRESA]
			,[ARE_ESP_SECUE]
			,[ID_CONT]
			,[ID_PAIS]
			,[ACTIVA]
			,[COD_POSTAL]
	FROM	TABLA t	
	WHERE	FILA BETWEEN '+CAST(@RegistroInicial AS NVARCHAR)+' AND ('+CAST(@RegistroInicial AS NVARCHAR)+' + '+ CAST(@CantidadRegistros -1 AS NVARCHAR)+')'

	print @Sql

	INSERT INTO @Resultado	
	EXEC(@Sql)
	
	SELECT	[NUIR]
			,[NOMBRE_DE_LA_EMPRESA]
			,[NIT_DE_LA_EMPRESA]
			,[SIGLA_DE_LA_EMPRESA]
			,[DIRECCION]
			,[CODIGO_DEL_DEPARTAMENTO]
			,[CODIGO_DEL_MUNICIPIO]
			,[TELEFONO_1]
			,[TELEFONO_2]
			,[EMAIL]
			,[NOMBRE_REP_LEGAL]
			,[CARGO_REP_LEGAL]
			,[IDENTIFICADOR_EMPRESA]
			,[ARE_ESP_SECUE]
			,[ID_CONT]
			,[ID_PAIS]
			,[ACTIVA]
			,[COD_POSTAL]
	FROM	@Resultado
	
END
GO
/****** Object:  StoredProcedure [dbo].[EXPEDIENTE_CONSULTA_CONSECUTIVO_SIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[EXPEDIENTE_CONSULTA_CONSECUTIVO_SIT]
(
	@sgd_exp_numero varchar(25)
)
AS
BEGIN
	SELECT 
	sgd_exp_numero as CONSECUTIVO
	FROM [dbo].[SGD_SEXP_SECEXPEDIENTES]
	WHERE sgd_exp_numero = @sgd_exp_numero
END
GO
/****** Object:  StoredProcedure [dbo].[expedienteM]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[expedienteM]
AS
BEGIN
	-- Declare the return variable here
	DECLARE @USUA_CODI	int,	 --Código del usuario 
			@DEPE_CODI	int,	-- Código de la Dependencia actual
			@RADI_NUME_RADI	numeric(18),	--numero de radicado
			@EXPEDIENTE	varchar(38),	--numero de expediente
			@USUA_CODI_NEW	int  --codigo nuevo usuario
    -- Insert statements for procedure here
	DECLARE expe CURSOR FOR select USUA_CODI,DEPE_CODI,radi_nume_radi,SGD_EXP_NUMERO
 from sgd_exp_expediente;
  OPEN expe;
  WHILE (1=1)
  begin;
  FETCH NEXT
			   FROM expe
			   INTO @USUA_CODI, @DEPE_CODI, @RADI_NUME_RADI,@EXPEDIENTE;
	  IF @@FETCH_STATUS < 0 BREAK;
	select @USUA_CODI_NEW=usua_codi from usua_migracion where depe_codi=@DEPE_CODI and usua_old=@USUA_CODI
	update sgd_exp_expediente set usua_codi=@USUA_CODI_NEW where radi_nume_radi=@RADI_NUME_RADI and sgd_exp_numero=@EXPEDIENTE;
  end;
  close expe;
END
GO
/****** Object:  StoredProcedure [dbo].[HIST_EVENTOS_ANEXOSInsertarSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[HIST_EVENTOS_ANEXOSInsertarSIT]
(
	@ANEX_RADI_NUME numeric(15, 0),
	@ANEX_CODIGO varchar(20),
	@SGD_TTR_CODIGO numeric(18, 0),
	@HIST_FECH datetime,
	@USUA_DOC varchar(14)
)
AS
BEGIN

/********************************************************************************* 
 Nombre	: HIST_EVENTOS_ANEXOSInsertar
																					
 Proposito: inserta los datos en la tabla de historico dbo.HIST_EVENTOS_ANEXOS  
																					
 Tablas	:	dbo.HIST_EVENTOS_ANEXOS
																					
 Autor	 :  Fabian Losada		Fecha: 2019/08/01

 Modifica:  

 NOTAS:	
 
 Parametros:	
  	
	@ANEX_RADI_NUME numeric(15, 0),
	@SGD_TTR_CODIGO numeric(18, 0),
	@HIST_FECH datetime,
	@USUA_DOC varchar(14)
	
 Resultado(s):	 Datos de HIST_EVENTOS_ANEXOS Insertado
**********************************************************************************/
DECLARE 
	@DEPE_CODI numeric(6),
	@ROL_ID numeric(4),
	@USUA_CODI numeric(8)
	SELECT @DEPE_CODI=r.depe_codi,@ROL_ID=rol_codi,@USUA_CODI=r.usua_codi from sgd_urd_usuaroldep r, usuario u where u.usua_codi=r.usua_codi
		and u.usua_doc=@USUA_DOC;
	INSERT INTO [dbo].[hist_eventos]
           ([depe_codi]
           ,[hist_fech]
           ,[usua_codi]
           ,[radi_nume_radi]
           ,[hist_obse]
           ,[usua_codi_dest]
           ,[usua_doc]
           ,[usua_doc_old]
           ,[sgd_ttr_codigo]
           ,[hist_doc_dest]
           ,[depe_codi_dest]
           ,[id_rol]
           ,[id_rol_dest])
     VALUES
           (@DEPE_CODI
           ,@HIST_FECH
           ,@USUA_CODI
           ,@ANEX_RADI_NUME
           ,CONCAT ('Anexo codigo ', @ANEX_CODIGO)
           ,@USUA_CODI
           ,@USUA_DOC
           ,@USUA_DOC
           ,@SGD_TTR_CODIGO
           ,@USUA_DOC
           ,@DEPE_CODI
           ,@ROL_ID
           ,@ROL_ID
		   )

	SELECT id,
			radi_nume_radi,
			@ANEX_CODIGO,
			SGD_TTR_CODIGO,
			HIST_FECH,
			USUA_DOC
	FROM [dbo].[HIST_EVENTOS]  WITH (NOLOCK)
	WHERE id =SCOPE_IDENTITY()
	
END
GO
/****** Object:  StoredProcedure [dbo].[HIST_EVENTOSInsertarSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[HIST_EVENTOSInsertarSIT]
(
           @DEPE_CODI int,
           @HIST_FECH datetime,
           @USUA_CODI numeric(10,0),
           @RADI_NUME_RADI numeric(15,0),
           @HIST_OBSE varchar(600),
           @USUA_CODI_DEST numeric(10,0),
           @USUA_DOC varchar(14),
           @HIST_DOC_DEST varchar(14),
           @DEPE_CODI_DEST numeric(3,0),
		   @SGD_TTR_CODIGO numeric(3,0)
)
AS
BEGIN

/********************************************************************************* 
 Nombre	: HIST_EVENTOSInsertar
																					
 Proposito: inserta los datos en la tabla de historico dbo.HIST_EVENTOS  
																					
 Tablas	:	dbo.HIST_EVENTOS
																					
 Autor	 :  Fabian Losada		Fecha: 2019/08/01

 Modifica:  

 NOTAS:	
 
 Parametros:	
  	
	@DEPE_CODI,
    @HIST_FECH,
    @USUA_CODI,
    @RADI_NUME_RADI,
    @HIST_OBSE,
    @USUA_CODI_DEST,
    @USUA_DOC,
    @HIST_DOC_DEST,
    @DEPE_CODI_DEST,
	@SGD_TTR_CODIGO
	
 Resultado(s):	 Datos de HIST_EVENTOS Insertado
**********************************************************************************/
DECLARE 
	@ROL_ID numeric(4),
	@ROL_ID_DES numeric(4)
	SELECT @ROL_ID=rol_codi from sgd_urd_usuaroldep where usua_codi=@USUA_CODI AND depe_codi=@DEPE_CODI;
	SELECT @ROL_ID_DES=rol_codi from sgd_urd_usuaroldep where usua_codi=@USUA_CODI_DEST AND depe_codi=@DEPE_CODI_DEST;
	INSERT INTO [dbo].[hist_eventos]
           ([depe_codi]
           ,[hist_fech]
           ,[usua_codi]
           ,[radi_nume_radi]
           ,[hist_obse]
           ,[usua_codi_dest]
           ,[usua_doc]
           ,[usua_doc_old]
           ,[sgd_ttr_codigo]
           ,[hist_doc_dest]
           ,[depe_codi_dest]
           ,[id_rol]
           ,[id_rol_dest])
     VALUES
		( @DEPE_CODI,
			@HIST_FECH,
			@USUA_CODI,
			@RADI_NUME_RADI,
			@HIST_OBSE,
			@USUA_CODI_DEST,
			@USUA_DOC,
			@USUA_DOC,
			@SGD_TTR_CODIGO,
			@HIST_DOC_DEST,
			@DEPE_CODI_DEST,
			@ROL_ID,
			@ROL_ID_DES
		   )

	SELECT DEPE_CODI,
			HIST_FECH,
			USUA_CODI,
			RADI_NUME_RADI,
			HIST_OBSE,
			USUA_CODI_DEST,
			USUA_DOC,
			HIST_DOC_DEST,
			DEPE_CODI_DEST,
			SGD_TTR_CODIGO
	FROM [dbo].[HIST_EVENTOS]
	WHERE DEPE_CODI = @DEPE_CODI AND
			HIST_FECH = @HIST_FECH AND
			USUA_CODI = @USUA_CODI AND
			RADI_NUME_RADI = @RADI_NUME_RADI AND 
			HIST_OBSE = @HIST_OBSE AND
			USUA_CODI_DEST = @USUA_CODI_DEST AND
			USUA_DOC = @USUA_DOC AND 
			HIST_DOC_DEST = @HIST_DOC_DEST AND
			DEPE_CODI_DEST = @DEPE_CODI_DEST AND
			SGD_TTR_CODIGO = @SGD_TTR_CODIGO
	
END
GO
/****** Object:  StoredProcedure [dbo].[hist_eventosM]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[hist_eventosM]  
as
insert into PRU_GdOrfeo.dbo.hist_eventos(depe_codi, hist_fech, usua_codi, 
radi_nume_radi, hist_obse, usua_codi_dest, usua_doc, usua_doc_old, 
sgd_ttr_codigo, hist_usua_autor, hist_doc_dest, depe_codi_dest, id_grupo)
select DEPE_CODI, HIST_FECH, USUA_CODI, RADI_NUME_RADI, HIST_OBSE, USUA_CODI_DEST, USUA_DOC, USUA_DOC_OLD, SGD_TTR_CODIGO, HIST_USUA_AUTOR, HIST_DOC_DEST, DEPE_CODI_DEST,
	  ID_GRUPO
from bdorfeo.dbo.HIST_EVENTOS h;

update hist_eventos  
set id_rol = r.rol_codi, id_rol_dest = r1.rol_codi
from hist_eventos h 
left join sgd_urd_usuaroldep r on h.usua_codi = r.usua_codi and h.depe_codi = r.depe_codi 
left join sgd_urd_usuaroldep r1 on h.usua_codi_dest = r.usua_codi and h.depe_codi_dest = r.depe_codi
where r.depe_codi = h.depe_codi 
and r.usua_codi = h.usua_codi;
GO
/****** Object:  StoredProcedure [dbo].[informadosM]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[informadosM]
AS
BEGIN
	-- Declare the return variable here
	DECLARE @USUA_CODI	int,	 --Código del usuario 
			@DEPE_CODI	int,	-- Código de la Dependencia actual
			@INFO_CODI	int,	--secuencia informados
			@USUA_CODI_NEW	int,
			@ROL_ID int
    -- Insert statements for procedure here
	DECLARE info CURSOR FOR select USUA_CODI,DEPE_CODI,ID
 from INFORMADOS;
  OPEN info;
  WHILE (1=1)
  begin;
  FETCH NEXT
			   FROM info
			   INTO @USUA_CODI, @DEPE_CODI, @INFO_CODI;
	  IF @@FETCH_STATUS < 0 BREAK;
	select @USUA_CODI_NEW=usua_codi from usua_migracion where depe_codi=@DEPE_CODI and usua_old=@USUA_CODI
	select @ROL_ID=rol_codi from sgd_urd_usuaroldep where depe_codi=@DEPE_CODI and usua_codi=@USUA_CODI_NEW;
	update informados set id_rol=@ROL_ID,usua_codi=@USUA_CODI_NEW where id=@INFO_CODI;
  end;
  close info;
END
GO
/****** Object:  StoredProcedure [dbo].[mig_radicado]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[mig_radicado]  
AS
BEGIN
	insert into radicado(
radi_nume_radi,
radi_fech_radi,
tdoc_codi,
trte_codi,
mrec_codi,
eesp_codi,
eotra_codi,
radi_tipo_empr,
radi_fech_ofic,
tdid_codi,
radi_nume_iden,
radi_nomb,
radi_prim_apel,
radi_segu_apel,
radi_pais,
muni_codi,
cpob_codi,
carp_codi,
esta_codi,
dpto_codi,
cen_muni_codi,
cen_dpto_codi,
radi_dire_corr,
radi_tele_cont,
radi_nume_hoja,
radi_desc_anex,
radi_nume_deri,
radi_path,
radi_usua_actu,
radi_depe_actu,
radi_fech_asig,
ra_asun,
radi_usu_ante,
radi_depe_radi,
radi_rem,
radi_usua_radi,
codi_nivel,
flag_nivel,
carp_per,
radi_leido,
radi_cuentai,
radi_tipo_deri,
listo,
sgd_tma_codigo,
sgd_mtd_codigo,
par_serv_secue,
sgd_fld_codigo,
radi_agend,
radi_fech_agend,
radi_fech_doc,
sgd_doc_secuencia,
sgd_pnufe_codi,
sgd_eanu_codigo,
sgd_not_codi,
sgd_apli_codi,
fech_vcmto,
id_cont,
sgd_spub_codigo,
id_pais,
usua_doc,
radi_fecha_vence,
radi_dias_vence,
sgd_apli_codigo,
requiere_resp,
radi_fech_tramite,
sgd_trad_codigo,
radi_nume_folios	
)
	SELECT  
radi_nume_radi,
radi_fech_radi,
tdoc_codi,
trte_codi,
mrec_codi,
eesp_codi,
eotra_codi,
radi_tipo_empr,
radi_fech_ofic,
tdid_codi,
radi_nume_iden,
radi_nomb,
radi_prim_apel,
radi_segu_apel,
radi_pais,
NULL,
cpob_codi,
carp_codi,
esta_codi,
NULL,
cen_muni_codi,
cen_dpto_codi,
radi_dire_corr,
radi_tele_cont,
radi_nume_hoja,
radi_desc_anex,
radi_nume_deri,
radi_path,
radi_usua_actu,
radi_depe_actu,
radi_fech_asig,
ra_asun,
radi_usu_ante,
radi_depe_radi,
radi_rem,
radi_usua_radi,
codi_nivel,
flag_nivel,
carp_per,
radi_leido,
radi_cuentai,
radi_tipo_deri,
listo,
sgd_tma_codigo,
sgd_mtd_codigo,
par_serv_secue,
sgd_fld_codigo,
radi_agend,
radi_fech_agend,
radi_fech_doc,
sgd_doc_secuencia,
sgd_pnufe_codi,
sgd_eanu_codigo,
sgd_not_codi,
NULL,
fech_vcmto,
id_cont,
sgd_spub_codigo,
id_pais,
usua_doc,
radi_fecha_vence,
radi_dias_vence,
sgd_apli_codigo,
requiere_resp,
radi_fech_tramite,
TIPO_RAD,
RADI_NUME_FOLIO_RADI	
	from bdorfeo.dbo.RADICADO 

/*Actualiza usuario actual y radicador con el nuevo codigo*/

DECLARE @usua_codi AS int
DECLARE @depe_codi AS numeric(6,0)
DECLARE @usua_old AS int

DECLARE usuariosN CURSOR FAST_FORWARD  FOR SELECT usua_codi,depe_codi,usua_old FROM usua_migracion
OPEN usuariosN
FETCH NEXT FROM usuariosN INTO @usua_codi,@depe_codi,@usua_old
WHILE @@fetch_status = 0
BEGIN
 
 /*usuario actual*/   
	update radicado set radi_usua_actu=@usua_codi where radi_usua_actu=@usua_old and radi_depe_actu=@depe_codi

/*usuario radicador*/

	update radicado set radi_usua_radi=@usua_codi where radi_usua_radi=@usua_old and radi_depe_radi=@depe_codi


   FETCH NEXT FROM usuariosN INTO @usua_codi,@depe_codi,@usua_old

END

CLOSE usuariosN

DEALLOCATE usuariosN

/*Termina Actualiza usuarios con el nuevo codigo*/

/*Procedimiento para actualizar codigos de municipios*/
DECLARE @muni_codi AS int
DECLARE @dpto_codi AS int
DECLARE @radicado AS numeric(15,0)


DECLARE municipiosV CURSOR FAST_FORWARD  FOR SELECT muni_codi,DPTO_CODI,radi_nume_radi  FROM bdorfeo.dbo.RADICADO where MUNI_CODI is not null
OPEN municipiosV
FETCH NEXT FROM municipiosV INTO @muni_codi,@dpto_codi,@radicado
WHILE @@fetch_status = 0

BEGIN

	update radicado set muni_codi=CONCAT(@dpto_codi,RIGHT(CONCAT('00',@muni_codi),3),'000') 
	where radi_nume_radi=@radicado

	FETCH NEXT FROM municipiosV INTO @muni_codi,@dpto_codi,@radicado
END
CLOSE municipiosV
DEALLOCATE municipiosV
/*Fin procedimiento para actualizar codigos de municipios*/


END
GO
/****** Object:  StoredProcedure [dbo].[mrdM]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[mrdM]
AS
BEGIN
	-- Declare the return variable here
	DECLARE @mrd	int,	 --Código del usuario en la dependencia nueva
			@con	int	 --Código del usuario en la dependencia nueva
			
    -- Insert statements for procedure here
	DECLARE tpr CURSOR FOR SELECT MIN(SGD_MRD_CODIGO),count(*) FROM [bdorfeo].dbo.[sgd_mrd_matrird] 
	  group by [depe_codi],[sgd_srd_codigo],[sgd_sbrd_codigo],[sgd_tpr_codigo];
  OPEN tpr;
  WHILE (1=1)
  begin;
  FETCH NEXT
			   FROM tpr
			   INTO @mrd,@con;
	  IF @@FETCH_STATUS < 0 BREAK;
	  insert into sgd_mrd_matrird SELECT [sgd_mrd_codigo],[depe_codi],[sgd_srd_codigo],[sgd_sbrd_codigo],[sgd_tpr_codigo],[soporte],[sgd_mrd_fechini]
      ,[sgd_mrd_fechfin],[sgd_mrd_esta],1,0,0,0,NULL,0,NULL,NULL FROM [bdorfeo].dbo.[sgd_mrd_matrird] where SGD_MRD_CODIGO=@mrd;
  end;
  close tpr;
END
GO
/****** Object:  StoredProcedure [dbo].[pa_reporte_seguimiento]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[pa_reporte_seguimiento]
@fechaini datetime,
@fechafin datetime
As
Begin

	/**********************************************************************************/
	/* Se limian los datos de la tabla #TEMPORAL_REPORTE_SEGUIMIENTO   */
	/**********************************************************************************/
	/*
	SELECT DISTINCT R.RADI_NUME_RADI AS RADICADO, 
			R.RADI_FECH_RADI AS FECHA_RADICADO, 
			MR.MREC_DESC AS MEDIO_RECEPCION, 
			Replace(Replace(Replace(Replace(Replace(Replace(DIR.SGD_DIR_NOMREMDES, char(9), ' '), char(10), ' '), char(13), ' '), ', ', ' '), '&', 'Y'), ';', '') AS REMITENTE, 
			DP.DPTO_NOMB AS DEPARTAMENTO, 
			M.MUNI_NOMB AS MUNICIPIO, 
			Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(R.RA_ASUN, char(9), ' '), char(10), ' '), char(13), ' '), ', ', ' '), '<', ' '), '>', ' '), '&', 'Y'), ';', ', ') AS ASUNTO, 
			R.RADI_FECHA_VENCE AS FECHA_VENCE, 
			R.RADI_DIAS_VENCE AS DIAS_POR_VENCER, 
			/****************************************************************************/
			Convert(varchar(150), ' ') as CATEGORIZACION,
			/****************************************************************************/
			Replace(T.SGD_TPR_DESCRIP, ', ', '') AS TIPODOC_ACTUAL,
			CASE WHEN R.REQUIERE_RESP = 0 THEN 'NO' ELSE 'SI' END AS REQUIERE_RESP,
			/****************************************************************************/
			Convert(varchar(45), ' ') as USUARIO_ACTUAL,
			/****************************************************************************/
			Convert(varchar(10), ' ') as USUARIO_ESTADO,
			/****************************************************************************/
			Convert(varchar(90), ' ') as DEPENDENCIA_ACTUAL,
			/****************************************************************************/
			Convert(varchar(50), ' ') as GRUPO_ACTUAL,
			/****************************************************************************/
			CASE WHEN R.RADI_PATH LIKE '%.PDF' THEN 'SI' ELSE 'NO' END AS IMAGEN_ENTRADA, 
			R.RADI_RESPUESTA AS RESPUESTA, 
			CASE WHEN RESP.RADI_FECH_TRAMITE IS NOT NULL THEN RESP.RADI_FECH_TRAMITE 
					ELSE RESP.RADI_FECH_RADI 
					END AS FECHA_RESPUESTA, 
			Replace(T2.SGD_TPR_DESCRIP, ', ', '') AS TIPO_DOC_RESPUESTA, 
			CASE WHEN R.RADI_RESPUESTA IS NOT NULL AND A.ANEX_ESTADO = 4 THEN 'ENVIADO' 
					WHEN R.REQUIERE_RESP = 0 THEN 'TRAMITADO' 
					WHEN R.RADI_RESPUESTA IS NOT NULL AND A.ANEX_ESTADO IN (2, 3) AND RV.SGD_DEVE_FECH IS NOT NULL THEN 'DEVUELTO' 
					WHEN R.RADI_RESPUESTA IS NOT NULL AND RESP.RADI_PATH LIKE '%.PDF' AND RV.SGD_DEVE_FECH IS NULL THEN 'TRAMITADO' 
					WHEN R.RADI_RESPUESTA IS NULL THEN 'SIN TRAMITAR' 
					ELSE 'EN TRAMITE' END AS ESTADO,
			/****************************************************************************/
			Convert(Datetime, ' ') as FECHA_ENVIO,
			/****************************************************************************/  
			Convert(varchar(15), ' ') as No_GUIA,
			/****************************************************************************/   
			Convert(varchar(40), ' ') as FORMA_ENVIO,
			/****************************************************************************/   
			Convert(Int, 0) as CANT_REASIGNACIONES,
			/****************************************************************************/   
			Convert(Int, 0) as CANT_DERIVACIONES,
			/****************************************************************************/
			ISNULL(D3.DEPE_NOMB, 'SUBDIRECCION ADMINISTRATIVA Y FINANCIERA') AS DEPEENDENCIA_RADICA, 
			ISNULL(U3.USUA_NOMB, 'RADICACION MAIL') AS USUARIO_RADICA, 
			/****************************************************************************/
			Convert(varchar(45), ' ') as USUARIO_REASIGNA
			/****************************************************************************/ 
	Into	#TEMPORAL_REPORTE_SEGUIMIENTO
	FROM	RADICADO AS R
			LEFT JOIN MEDIO_RECEPCION MR ON MR.MREC_CODI = R.MREC_CODI 
			JOIN SGD_DIR_DRECCIONES AS DIR ON DIR.RADI_NUME_RADI = R.RADI_NUME_RADI AND DIR.SGD_DIR_TIPO = 1 
			LEFT JOIN DEPARTAMENTO AS DP ON DP.DPTO_CODI = DIR.DPTO_CODI AND DP.ID_PAIS = DIR.ID_PAIS AND DP.ID_CONT = DIR.ID_CONT 
			LEFT JOIN MUNICIPIO AS M ON M.ID_CONT = DIR.ID_CONT AND M.ID_PAIS = DIR.ID_PAIS AND M.DPTO_CODI = DIR.DPTO_CODI AND M.MUNI_CODI = DIR.MUNI_CODI 
			LEFT JOIN SGD_TPR_TPDCUMENTO T ON R.TDOC_CODI = T.SGD_TPR_CODIGO 
			JOIN DEPENDENCIA DEP ON DEP.DEPE_CODI = R.RADI_DEPE_ACTU 
			JOIN USUARIO U ON U.DEPE_CODI = R.RADI_DEPE_ACTU AND U.USUA_CODI = R.RADI_USUA_ACTU 
			LEFT JOIN DEPE_GRUPO G ON G.ID_GRUPO = U.ID_GRUPO 
			LEFT JOIN RADICADO AS RESP ON RESP.RADI_NUME_RADI = R.RADI_RESPUESTA 
			LEFT JOIN SGD_TPR_TPDCUMENTO T2 ON T2.SGD_TPR_CODIGO = RESP.TDOC_CODI 
			LEFT JOIN ANEXOS A ON A.RADI_NUME_SALIDA = R.RADI_RESPUESTA AND A.ANEX_SALIDA = 1 
			LEFT JOIN SGD_RENV_REGENVIO RV ON RV.RADI_NUME_SAL = RESP.RADI_NUME_RADI 
			LEFT JOIN HIST_EVENTOS H3 ON H3.RADI_NUME_RADI = R.RADI_NUME_RADI AND H3.SGD_TTR_CODIGO in (2, 64) 
			LEFT JOIN USUARIO U3 ON U3.USUA_DOC = H3.USUA_DOC 
			LEFT JOIN DEPENDENCIA D3 ON D3.DEPE_CODI = U3.DEPE_CODI 
	WHERE	R.RADI_FECH_RADI Between @fechaini And @fechafin
    AND     R.RADI_NUME_RADI LIKE '%2'
	AND		R.RADI_DEPE_RADI <> 998
	ORDER BY 2 
	*/

SELECT DISTINCT R.RADI_NUME_RADI AS RADICADO, 
			R.RADI_FECH_RADI AS FECHA_RADICADO, 
			MR.MREC_DESC AS MEDIO_RECEPCION, 
			Replace(Replace(Replace(Replace(Replace(Replace(DIR.SGD_DIR_NOMREMDES, char(9), ' '), char(10), ' '), char(13), ' '), ', ', ' '), '&', 'Y'), ';', '') AS REMITENTE, 
			DP.DPTO_NOMB AS DEPARTAMENTO, 
			M.MUNI_NOMB AS MUNICIPIO, 
			Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(R.RA_ASUN, char(9), ' '), char(10), ' '), char(13), ' '), ', ', ' '), '<', ' '), '>', ' '), '&', 'Y'), ';', ', ') AS ASUNTO, 
			R.RADI_FECHA_VENCE AS FECHA_VENCE, 
			R.RADI_DIAS_VENCE AS DIAS_POR_VENCER, 
			/****************************************************************************/
			Convert(varchar(150), ' ') as CATEGORIZACION,
			/****************************************************************************/
			Replace(T.SGD_TPR_DESCRIP, ', ', '') AS TIPODOC_ACTUAL,
			CASE WHEN R.REQUIERE_RESP = 0 THEN 'NO' ELSE 'SI' END AS REQUIERE_RESP,
			/****************************************************************************/
			Convert(varchar(45), ' ') as USUARIO_ACTUAL,
			/****************************************************************************/
			Convert(varchar(10), ' ') as USUARIO_ESTADO,
			/****************************************************************************/
			Convert(varchar(90), ' ') as DEPENDENCIA_ACTUAL,
			/****************************************************************************/
			Convert(varchar(50), ' ') as GRUPO_ACTUAL,
			/****************************************************************************/
			CASE WHEN R.RADI_PATH LIKE '%.PDF' THEN 'SI' ELSE 'NO' END AS IMAGEN_ENTRADA, 
			R.RADI_RESPUESTA AS RESPUESTA, 
			CASE WHEN RESP.RADI_FECH_TRAMITE IS NOT NULL THEN RESP.RADI_FECH_TRAMITE 
					ELSE RESP.RADI_FECH_RADI 
					END AS FECHA_RESPUESTA, 
			Replace(T2.SGD_TPR_DESCRIP, ', ', '') AS TIPO_DOC_RESPUESTA, 
			CASE WHEN R.RADI_RESPUESTA IS NOT NULL AND A.ANEX_ESTADO = 4 THEN 'ENVIADO' 
					WHEN R.REQUIERE_RESP = 0 THEN 'TRAMITADO' 
					WHEN R.RADI_RESPUESTA IS NOT NULL AND A.ANEX_ESTADO IN (2, 3) AND RV.SGD_DEVE_FECH IS NOT NULL THEN 'DEVUELTO' 
					WHEN R.RADI_RESPUESTA IS NOT NULL AND RESP.RADI_PATH LIKE '%.PDF' AND RV.SGD_DEVE_FECH IS NULL THEN 'TRAMITADO' 
					WHEN R.RADI_RESPUESTA IS NULL THEN 'SIN TRAMITAR' 
					ELSE 'EN TRAMITE' END AS ESTADO,
			/****************************************************************************/
			Convert(Datetime, ' ') as FECHA_ENVIO,
			/****************************************************************************/  
			Convert(varchar(15), ' ') as No_GUIA,
			/****************************************************************************/   
			Convert(varchar(40), ' ') as FORMA_ENVIO,
			/****************************************************************************/   
			Convert(Int, 0) as CANT_REASIGNACIONES,
			/****************************************************************************/   
			Convert(Int, 0) as CANT_DERIVACIONES,
			/****************************************************************************/
			ISNULL(D3.DEPE_NOMB, 'SUBDIRECCION ADMINISTRATIVA Y FINANCIERA') AS DEPEENDENCIA_RADICA, 
			ISNULL(U4.USUA_NOMB, 'RADICACION MAIL') AS USUARIO_RADICA, 
			/****************************************************************************/
			Convert(varchar(45), ' ') as USUARIO_REASIGNA
			/****************************************************************************/ 
	Into	#TEMPORAL_REPORTE_SEGUIMIENTO
	FROM	RADICADO AS R
			LEFT JOIN MEDIO_RECEPCION MR ON MR.MREC_CODI = R.MREC_CODI 
			JOIN SGD_DIR_DRECCIONES AS DIR ON DIR.RADI_NUME_RADI = R.RADI_NUME_RADI AND DIR.SGD_DIR_TIPO = 1 
			LEFT JOIN DEPARTAMENTO AS DP ON DP.DPTO_CODI = DIR.DPTO_CODI AND DP.ID_PAIS = DIR.ID_PAIS AND DP.ID_CONT = DIR.ID_CONT 
			LEFT JOIN MUNICIPIO AS M ON M.ID_CONT = DIR.ID_CONT AND M.ID_PAIS = DIR.ID_PAIS AND M.DPTO_CODI = DIR.DPTO_CODI AND M.MUNI_CODI = DIR.MUNI_CODI 
			LEFT JOIN SGD_TPR_TPDCUMENTO T ON R.TDOC_CODI = T.SGD_TPR_CODIGO 
			JOIN DEPENDENCIA DEP ON DEP.DEPE_CODI = R.RADI_DEPE_ACTU 
--			JOIN USUARIO U ON U.DEPE_CODI = R.RADI_DEPE_ACTU AND U.USUA_CODI = R.RADI_USUA_ACTU 
			JOIN sgd_urd_usuaroldep U ON U.DEPE_CODI = R.RADI_DEPE_ACTU AND U.USUA_CODI = R.RADI_USUA_ACTU 
			LEFT JOIN RADICADO AS RESP ON RESP.RADI_NUME_RADI = R.RADI_RESPUESTA 
			LEFT JOIN SGD_TPR_TPDCUMENTO T2 ON T2.SGD_TPR_CODIGO = RESP.TDOC_CODI 
			LEFT JOIN ANEXOS A ON A.RADI_NUME_SALIDA = R.RADI_RESPUESTA AND A.ANEX_SALIDA = 1 
			LEFT JOIN SGD_RENV_REGENVIO RV ON RV.RADI_NUME_SAL = RESP.RADI_NUME_RADI 
			LEFT JOIN HIST_EVENTOS H3 ON H3.RADI_NUME_RADI = R.RADI_NUME_RADI AND H3.SGD_TTR_CODIGO in (2, 64) 
			LEFT JOIN sgd_urd_usuaroldep U3 ON U3.USUA_CODI = H3.usua_codi and u3.depe_codi=h3.depe_codi
			LEFT JOIN usuario u4 on u4.usua_doc=h3.usua_doc
			LEFT JOIN DEPE_GRUPO G ON G.ID_GRUPO = U4.ID_GRUPO 
			LEFT JOIN DEPENDENCIA D3 ON D3.DEPE_CODI = U3.DEPE_CODI 
WHERE	R.RADI_FECH_RADI Between @fechaini And @fechafin
    AND     R.RADI_NUME_RADI LIKE '%2'
	AND		R.RADI_DEPE_RADI <> 998
	ORDER BY 2




	/*************************************************************************************/
	/* Se crea un indice para esta tabla con datos temporales para agilizar la consulta  */
	 Create Nonclustered index IND_RADICADO ON #TEMPORAL_REPORTE_SEGUIMIENTO (RADICADO) 
	/*************************************************************************************/

	/****************************************************************************/
	----- Columna CATEGORIZACION
	/****************************************************************************/
	Update #TEMPORAL_REPORTE_SEGUIMIENTO
	set CATEGORIZACION = CATEGORIZACION.SGD_TPR_DESCRIP
	From (	SELECT tmp.RADICADO, MAx(TP.SGD_TPR_DESCRIP) as SGD_TPR_DESCRIP
			FROM	SGD_HMTD_HISMATDOC AS HIST  JOIN SGD_TPR_TPDCUMENTO TP ON TP.SGD_TPR_CODIGO = HIST.SGD_TPR_CODIGO ,
					#TEMPORAL_REPORTE_SEGUIMIENTO tmp
			WHERE tmp.RADICADO = HIST.RADI_NUME_RADI 
	--		20181030011282 = HIST.RADI_NUME_RADI 
			AND HIST.SGD_TTR_CODIGO = 35
			Group by tmp.RADICADO
			--ORDER BY HIST.SGD_HMTD_FECHA
		) AS CATEGORIZACION
	Where	#TEMPORAL_REPORTE_SEGUIMIENTO.RADICADO = CATEGORIZACION.RADICADO

	/****************************************************************************/
	----- Columna USUARIO_ACTUAL
	/****************************************************************************/
	UPdate	#TEMPORAL_REPORTE_SEGUIMIENTO
	Set		USUARIO_ACTUAL = USUARIO_ACTUAL.USUARIO
	From	(	SELECT	Distinct tmp.RADICADO,
						CASE WHEN RAD.RADI_DEPE_ACTU = 999 THEN 
						USU2.USUA_NOMB 
						ELSE usu3.usua_nomb
						END AS USUARIO 
				FROM	RADICADO RAD JOIN sgd_urd_usuaroldep USU ON USU.DEPE_CODI = RAD.RADI_DEPE_ACTU AND USU.USUA_CODI = RAD.RADI_USUA_ACTU ,
						#TEMPORAL_REPORTE_SEGUIMIENTO tmp
						LEFT JOIN HIST_EVENTOS HIST ON HIST.RADI_NUME_RADI = tmp.RADICADO AND HIST.SGD_TTR_CODIGO = 13 
						LEFT JOIN USUARIO USU2 ON USU2.USUA_DOC = HIST.USUA_DOC
						left join usua_migracion usu3 on usu3.usua_codi=tmp.USUARIO_ACTUAL
				WHERE	RAD.RADI_NUME_RADI = tmp.RADICADO 
			) AS USUARIO_ACTUAL
	Where	#TEMPORAL_REPORTE_SEGUIMIENTO.RADICADO = USUARIO_ACTUAL.RADICADO

	/****************************************************************************/
	----- Columna USUARIO_ESTADO
	/****************************************************************************/
	UPdate	#TEMPORAL_REPORTE_SEGUIMIENTO
	Set		USUARIO_ESTADO = USUARIO_ESTADO.USUARIO
	From	(	SELECT	Distinct tmp.RADICADO,
						CASE WHEN RAD.RADI_DEPE_ACTU = 999 THEN 
						USU2.USUA_ESTA 
						ELSE USU3.USUA_ESTA 
						END AS USUARIO 
				FROM	RADICADO RAD JOIN sgd_urd_usuaroldep USU ON USU.DEPE_CODI = RAD.RADI_DEPE_ACTU AND USU.USUA_CODI = RAD.RADI_USUA_ACTU,
						#TEMPORAL_REPORTE_SEGUIMIENTO tmp
						LEFT JOIN HIST_EVENTOS HIST ON HIST.RADI_NUME_RADI = tmp.RADICADO AND HIST.SGD_TTR_CODIGO = 13 
						LEFT JOIN USUARIO USU2 ON USU2.USUA_DOC = HIST.USUA_DOC
						left join usua_migracion usu3 on usu3.usua_codi=tmp.USUARIO_ACTUAL
				WHERE	RAD.RADI_NUME_RADI = tmp.RADICADO
				/*ORDER BY HIST.HIST_FECH DESC */) AS USUARIO_ESTADO
	Where	#TEMPORAL_REPORTE_SEGUIMIENTO.RADICADO = USUARIO_ESTADO.RADICADO

	/****************************************************************************/
	----- Columna DEPENDENCIA_ACTUAL
	/****************************************************************************/
	UPdate	#TEMPORAL_REPORTE_SEGUIMIENTO
	Set		DEPENDENCIA_ACTUAL = DEPENDENCIA_ACTUAL.DEPENDENCIA
	From	(	SELECT Distinct tmp.RADICADO,
							CASE WHEN RAD.RADI_DEPE_ACTU = 999 AND DEP2.DEPE_CODI = 950 THEN '600 - SECRETARIA GENERAL' 
							WHEN RAD.RADI_DEPE_ACTU = 999 AND DEP2.DEPE_CODI = 994 THEN '620 - SUBDIRECCION ADMINISTRATIVA Y FINANCIERA' 
							WHEN RAD.RADI_DEPE_ACTU = 999 THEN CONCAT(DEP2.DEPE_CODI, ' - ', DEP2.DEPE_NOMB)
						ELSE CONCAT(DEP.DEPE_CODI, ' - ', DEP.DEPE_NOMB)
						END AS DEPENDENCIA
					FROM RADICADO RAD JOIN DEPENDENCIA DEP ON DEP.DEPE_CODI = RAD.RADI_DEPE_ACTU,
						#TEMPORAL_REPORTE_SEGUIMIENTO tmp
						LEFT JOIN HIST_EVENTOS HIST ON HIST.RADI_NUME_RADI = tmp.RADICADO AND HIST.SGD_TTR_CODIGO = 13 
						LEFT JOIN DEPENDENCIA DEP2 ON DEP2.DEPE_CODI = HIST.DEPE_CODI 
					WHERE RAD.RADI_NUME_RADI = tmp.RADICADO
					/*ORDER BY HIST.HIST_FECH DESC */) AS DEPENDENCIA_ACTUAL
	where	#TEMPORAL_REPORTE_SEGUIMIENTO.RADICADO = DEPENDENCIA_ACTUAL.RADICADO

	/****************************************************************************/
	----- Columna GRUPO_ACTUAL
	/****************************************************************************/

	UPdate	#TEMPORAL_REPORTE_SEGUIMIENTO
	Set		GRUPO_ACTUAL = GRUPO_ACTUAL.DEPENDENCIA
	From	(	SELECT	Distinct tmp.RADICADO,
						CASE WHEN RAD.RADI_DEPE_ACTU = 999 AND DEP2.DEPE_CODI = 950 THEN 'SERVICIO AL CIUDADANO' 
							 WHEN RAD.RADI_DEPE_ACTU = 999 AND DEP2.DEPE_CODI = 600 AND HIST.USUA_DOC IN ('1094919699', '1128265299') THEN 'SERVICIO AL CIUDADANO' 
							 WHEN RAD.RADI_DEPE_ACTU = 999 AND DEP2.DEPE_CODI = 994 THEN 'CORRESPONDENCIA Y ADMINISTRACION DE ARCHIVO' 
							 WHEN RAD.RADI_DEPE_ACTU = 999 THEN GRUPO2.DEPE_GRUPO 
						ELSE GRUPO.DEPE_GRUPO END AS DEPENDENCIA 
					FROM	RADICADO RAD 
							JOIN sgd_urd_usuaroldep USU ON USU.DEPE_CODI = RAD.RADI_DEPE_ACTU AND USU.USUA_CODI = RAD.RADI_USUA_ACTU,
							#TEMPORAL_REPORTE_SEGUIMIENTO tmp
							LEFT JOIN HIST_EVENTOS HIST ON HIST.RADI_NUME_RADI = tmp.RADICADO AND HIST.SGD_TTR_CODIGO = 13 
							LEFT JOIN DEPENDENCIA DEP2 ON DEP2.DEPE_CODI = HIST.DEPE_CODI 
							LEFT JOIN USUARIO USU2 ON USU2.USUA_DOC = HIST.USUA_DOC
							LEFT JOIN DEPE_GRUPO GRUPO ON GRUPO.ID_GRUPO = USU2.ID_GRUPO 
							LEFT JOIN DEPE_GRUPO GRUPO2 ON GRUPO2.ID_GRUPO = USU2.ID_GRUPO 
					WHERE RAD.RADI_NUME_RADI = tmp.RADICADO
					/*ORDER BY HIST.HIST_FECH DESC*/) AS GRUPO_ACTUAL
	Where #TEMPORAL_REPORTE_SEGUIMIENTO.RADICADO = GRUPO_ACTUAL.RADICADO


	/****************************************************************************/
	----- Columna FECHA_ENVIO
	----- Columna No_GUIA
	/****************************************************************************/

	UPdate	#TEMPORAL_REPORTE_SEGUIMIENTO
	Set		FECHA_ENVIO = SUB.SGD_RENV_FECH
	,		No_GUIA		= SUB.SGD_RENV_NUMGUIA
	From	(	SELECT	tmp.RADICADO, 
						tmp.RESPUESTA,
						V.SGD_RENV_FECH,
						V.SGD_RENV_NUMGUIA
				FROM	SGD_RENV_REGENVIO V,
						#TEMPORAL_REPORTE_SEGUIMIENTO tmp
				WHERE V.RADI_NUME_SAL = tmp.RESPUESTA
				AND V.SGD_DIR_TIPO = 1 
				AND V.SGD_RENV_ESTADO = 1 
				AND SGD_RENV_OBSERVA NOT LIKE 'Masiva grupo%' 
				/*ORDER BY V.SGD_RENV_FECH DESC*/) AS SUB
	Where	#TEMPORAL_REPORTE_SEGUIMIENTO.RADICADO = SUB.RADICADO
	And		#TEMPORAL_REPORTE_SEGUIMIENTO.RESPUESTA = SUB.RESPUESTA


	/****************************************************************************/
	----- Columna FORMA_ENVIO
	/****************************************************************************/

	UPdate	#TEMPORAL_REPORTE_SEGUIMIENTO
	Set		FORMA_ENVIO = FORMA_ENVIO.SGD_FENV_DESCRIP
	From	(	SELECT	tmp.RADICADO, 
						tmp.RESPUESTA,
						FE.SGD_FENV_DESCRIP 
				FROM	SGD_RENV_REGENVIO V JOIN SGD_FENV_FRMENVIO FE ON FE.SGD_FENV_CODIGO = V.SGD_FENV_CODIGO,
						#TEMPORAL_REPORTE_SEGUIMIENTO tmp
				WHERE	V.RADI_NUME_SAL = tmp.RESPUESTA
				AND		V.SGD_DIR_TIPO = 1 
				AND		V.SGD_RENV_ESTADO = 1 
				AND		SGD_RENV_OBSERVA NOT LIKE 'Masiva grupo%' 
				/*ORDER BY V.SGD_RENV_FECH DESC*/) AS FORMA_ENVIO
	Where	#TEMPORAL_REPORTE_SEGUIMIENTO.RADICADO = FORMA_ENVIO.RADICADO
	And		#TEMPORAL_REPORTE_SEGUIMIENTO.RESPUESTA = FORMA_ENVIO.RESPUESTA

	/****************************************************************************/
	----- Columna CANT_REASIGNACIONES
	/****************************************************************************/
	UPdate	#TEMPORAL_REPORTE_SEGUIMIENTO
	Set		CANT_REASIGNACIONES = CANT_REASIGNACIONES.conteo
	From	(	SELECT	sub.RADICADO, COUNT(HE.RADI_NUME_RADI) as conteo
				FROM	HIST_EVENTOS HE,
						(Select distinct RADICADO 
						from #TEMPORAL_REPORTE_SEGUIMIENTO) as sub
				WHERE	HE.RADI_NUME_RADI = sub.RADICADO
				AND		HE.SGD_TTR_CODIGO = 9
				Group by sub.RADICADO) AS CANT_REASIGNACIONES
	Where	#TEMPORAL_REPORTE_SEGUIMIENTO.RADICADO = CANT_REASIGNACIONES.RADICADO

				

	/****************************************************************************/
	----- Columna CANT_DERIVACIONES
	/****************************************************************************/
	UPdate	#TEMPORAL_REPORTE_SEGUIMIENTO
	Set		CANT_DERIVACIONES = CANT_DERIVACIONES.conteo
	From	(SELECT sub.RADICADO, COUNT(HE.RADI_NUME_RADI) as conteo
			 FROM	HIST_EVENTOS HE,
					(Select	distinct RADICADO 
					 from	#TEMPORAL_REPORTE_SEGUIMIENTO) as sub
			 WHERE	HE.RADI_NUME_RADI = sub.RADICADO
			 AND HE.SGD_TTR_CODIGO = 14
			 Group by sub.RADICADO) AS CANT_DERIVACIONES
	Where	#TEMPORAL_REPORTE_SEGUIMIENTO.RADICADO = CANT_DERIVACIONES.RADICADO


	/****************************************************************************/
	----- Columna USUARIO_REASIGNA
	/****************************************************************************/
	UPdate	#TEMPORAL_REPORTE_SEGUIMIENTO
	Set		USUARIO_REASIGNA = USUARIO_REASIGNA.USUA_NOMB
	From	(	SELECT	minimo.RADICADO, US.USUA_NOMB
				FROM	HIST_EVENTOS HI JOIN USUARIO US ON US.USUA_DOC = HI.USUA_DOC,
						(Select	tmp.RADICADO, Min(HI.HIST_FECH) as HIST_FECH
						FROM	HIST_EVENTOS HI JOIN USUARIO US ON US.USUA_DOC = HI.USUA_DOC,
								#TEMPORAL_REPORTE_SEGUIMIENTO tmp
						WHERE	HI.RADI_NUME_RADI = tmp.RADICADO
						AND		HI.SGD_TTR_CODIGO IN (9, 99)
						Group by tmp.RADICADO ) as minimo
				WHERE	HI.RADI_NUME_RADI = minimo.RADICADO
				AND		HI.SGD_TTR_CODIGO IN (9, 99) 
				And		HI.HIST_FECH = minimo.HIST_FECH
				/*ORDER BY HI.HIST_FECH */) AS USUARIO_REASIGNA
	Where	#TEMPORAL_REPORTE_SEGUIMIENTO.RADICADO = USUARIO_REASIGNA.RADICADO


	Select	RADICADO, 			FECHA_RADICADO, 			MEDIO_RECEPCION, 			REMITENTE, 				DEPARTAMENTO, 
			MUNICIPIO, 			ASUNTO,						FECHA_VENCE,				DIAS_POR_VENCER, 		CATEGORIZACION,
			TIPODOC_ACTUAL,		REQUIERE_RESP,				USUARIO_ACTUAL,				USUARIO_ESTADO,			DEPENDENCIA_ACTUAL,
			GRUPO_ACTUAL,		IMAGEN_ENTRADA, 			RESPUESTA,					FECHA_RESPUESTA, 		TIPO_DOC_RESPUESTA, 
			ESTADO,				FECHA_ENVIO,				No_GUIA,					FORMA_ENVIO,			CANT_REASIGNACIONES,
			CANT_DERIVACIONES,	DEPEENDENCIA_RADICA, 		USUARIO_RADICA, 			USUARIO_REASIGNA
	from	#TEMPORAL_REPORTE_SEGUIMIENTO
END
GO
/****** Object:  StoredProcedure [dbo].[RADICADO_SGD_DIR_DRECCIONESConsultarConFiltroSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RADICADO_SGD_DIR_DRECCIONESConsultarConFiltroSIT]
	
	
	@NoRadicado varchar(MAX),
	@nombreContacto varchar(14),
	@tipoRadicado int,
	@fechaInicio datetime,
	@fechaFin datetime,
	@codigoTipoDocumental  numeric(4,0),
	@codigoDepenendenciaActual numeric(5,0),
	@NoPaginaResultado int,
	@campoOrden varchar(30),
	@tipoOrden varchar(4),
	@NoRegistrosPorPagina int,
	@usuarioActual varchar(30),
	@codigoCarpeta numeric(3,0),
	@carpetaPersonal numeric(1,0)

AS
/********************************************************************************* 
 Nombre:		RADICADO_SGD_DIR_DRECCIONESConsultarConFiltroSIT
 Propósito:		Obtiene datos generales de radicados
 Tablas:		Radicado - Usuario - SGD_TPR_TPDOCUMENTO - SGD_PQR_METADATA - SGD_DIR_DRECCIONES
 Autor:			Fabian Losada
 Fecha:			01/08/2019
 Modifica:  
 Resultado(s):	Datos basicos de Usuario destino de radicacion
 **********************************************************************************/
 
DECLARE @SQL VARCHAR(MAX);
DECLARE @Where VARCHAR(MAX);
DECLARE @Order VARCHAR(MAX);
DECLARE @RowNum VARCHAR(MAX);
DECLARE @WRowNum VARCHAR(MAX);
DECLARE @Wfecha VARCHAR(MAX);
DECLARE @joinDirUnique VARCHAR(MAX);
DECLARE @bloqueRG_MULTIPLE VARCHAR(MAX);
DECLARE @bloquePPAL VARCHAR(MAX);
DECLARE @camposSelect VARCHAR(MAX);
DECLARE @unionRG_MULTIPLE VARCHAR(MAX);
DECLARE @whereRG_MULTIPLE VARCHAR(MAX);
DECLARE @WhereRadicado VARCHAR(MAX);

DECLARE @CON INT;

	
BEGIN
	SET NOCOUNT ON;
	SET @CON=0;
	SET @Where='';
	SET @Wfecha='';
	SET @WRowNum='';
	SET @joinDirUnique='';
	SET @bloqueRG_MULTIPLE ='';
    SET @bloquePPAL='';
    SET @camposSelect='';
	SET @unionRG_MULTIPLE='';
	SET @whereRG_MULTIPLE='';
	SET @WhereRadicado='';

	if( LEN(@nombreContacto) > 0 )
	begin
		set @Where=@Where+' AND SGD_DIR_NOMREMDES like ''%'+CAST(@nombreContacto as NVARCHAR)+'%''';
		set @CON=1;
	end
	if( @tipoRadicado > 0 )
	begin
		set @Where=@Where+' AND R.RADI_NUME_RADI like ''%'+CAST(@tipoRadicado as NVARCHAR)+'''';
		set @CON=1;
	end
	if( @codigoTipoDocumental > 0 )
	begin
		set @Where=@Where+' AND R.TDOC_CODI ='+CAST(@codigoTipoDocumental as NVARCHAR);
		set @CON=1;
	end
	if( LEN(@usuarioActual) > 0 )
	begin
		set @Where=@Where+' AND U.USUA_LOGIN = '''+CAST(@usuarioActual as NVARCHAR)+'''';
		set @CON=1;
	end
	if( @codigoDepenendenciaActual > 0 )
	begin
		set @Where=@Where+' AND R.RADI_DEPE_ACTU ='+CAST(@codigoDepenendenciaActual as NVARCHAR);
		set @CON=1;
	end
	
	if( @codigoCarpeta >= 0 )
	begin
		set @joinDirUnique='LEFT JOIN (SELECT MAX(SGD_DIR_CODIGO) AS SGD_DIR_CODIGO,RADI_NUME_RADI FROM SGD_DIR_DRECCIONES GROUP BY RADI_NUME_RADI) D1 ON D1.RADI_NUME_RADI=R.RADI_NUME_RADI
							LEFT JOIN SGD_DIR_DRECCIONES D ON D.RADI_NUME_RADI=D1.RADI_NUME_RADI AND D.SGD_DIR_CODIGO=D1.SGD_DIR_CODIGO'
		set @Where=@Where+' AND R.CARP_CODI ='+CAST(@codigoCarpeta as NVARCHAR);
		set @CON=1;
	end
	else
	begin
		set @joinDirUnique='JOIN SGD_DIR_DRECCIONES D ON D.RADI_NUME_RADI=R.RADI_NUME_RADI'
	end 
	
	if( @carpetaPersonal >= 0 )
	begin
		set @Where=@Where+' AND R.CARP_PER ='+CAST(@carpetaPersonal as NVARCHAR);
		set @CON=1;
	end
	
	if( LEN(@NoRadicado) > 0 )
	begin
		set @WhereRadicado=@WhereRadicado+' AND (';
	---En este Bloque se construyen las sentencias para buscar un radicado especifico o parte de el.	
	 DECLARE @strlist NVARCHAR(max), @pos INT,@j INT, @delim CHAR, @lstr NVARCHAR(max), @strOR NVARCHAR(4)
		 SET @strlist = @NoRadicado
		 SET @delim = ','
		 SET @j=0;
		 SET @lstr = ' '
		 SET @strOR = ' ' 
		 SET @pos = charindex(@delim, @strlist)
		 WHILE ((len(@strlist) > 0))
		 BEGIN
			SET @pos = charindex(@delim, @strlist)
			IF(@j=1)
			BEGIN
				SET @strOR = ' OR ';
			END
			
			IF @pos > 0
			   BEGIN
				  SET @lstr = @lstr + @strOR + 'R.RADI_NUME_RADI LIKE ''%'+ substring(@strlist, 1, @pos-1)+''''
				  SET @strlist = ltrim(substring(@strlist,charindex(@delim, @strlist)+1, len(@strlist)- charindex(@delim, @strlist)+1 ))
			   END
			ELSE
			   BEGIN
				  SET @lstr = @lstr + @strOR + 'CONVERT(NVARCHAR,R.RADI_NUME_RADI) LIKE ''%'+ @strlist+''''
				  SET @strlist = ''
			   END
			SET @j=1;
		 END
		 
		SET @WhereRadicado= @WhereRadicado + @lstr+ ' )';
		set @CON=1;
		SET @Where = @Where + @WhereRadicado
	end
	else
	begin
		set @Wfecha= ' AND R.RADI_FECH_RADI between '''+CONVERT(VARCHAR, @fechaInicio, 109)+''' and '''+CONVERT(VARCHAR, @fechaFin, 109)+''' '
		set @CON=1;
	end
	
	if(LEN(@campoOrden)<=0)
	begin
		set @campoOrden='B.RADI_NUME_RADI';
	end
	
	if(LEN(@tipoOrden)<=0)
	begin
		set @tipoOrden=' ASC ';
	end
	
	if(@NoRegistrosPorPagina>0)
	begin
		set @WRowNum = ' RowNum BETWEEN (('+CAST(@NoPaginaResultado as NVARCHAR)+'-1)*'+CAST(@NoRegistrosPorPagina as NVARCHAR)+')+1 AND (('+CAST(@NoPaginaResultado as NVARCHAR)+'-1)*'+CAST(@NoRegistrosPorPagina as NVARCHAR)+')+'+CAST(@NoRegistrosPorPagina as NVARCHAR);
	end
	else
	begin
		set @WRowNum = ' RowNum BETWEEN 1 and totalRows';
	end
	
	if(  @CON > 0 )
	begin
		set @Where= ' 1=1 ' + @Where;
	end
	else
	begin
		set @Where= ' 1=2 ';
	end
	
	SET @bloquePPAL='
				,r.RADI_LEIDO
				,CAST(0 as smallint) DERIVADO
				FROM RADICADO r 
				'+ISNULL(@joinDirUnique,'')+'
				JOIN USUARIO U ON r.RADI_DEPE_ACTU=U.DEPE_CODI AND R.RADI_USUA_ACTU = U.USUA_CODI
				LEFT JOIN  USUARIO U2 ON U2.usua_doc=D.SGD_DOC_FUN 
				LEFT JOIN SGD_TPR_TPDCUMENTO c ON r.tdoc_codi = c.sgd_tpr_codigo
				LEFT JOIN SGD_PQR_METADATA X ON X.RADI_NUME_RADI=r.RADI_NUME_RADI 
				where  '+ISNULL(@Where,'')+ISNULL(@Wfecha,'') +' '

	SET @camposSelect=' SELECT
							R.RADI_NUME_RADI,
							R.RADI_FECH_RADI, 
							R.RA_ASUN, 
							R.TDOC_CODI, 
							R.RADI_NUME_DERI, 
							R.RADI_PATH, 
							D.SGD_DIR_NOMREMDES, 
							D.SGD_DIR_DIRECCION, 
							D.ID_CONT, 
							D.ID_PAIS, 
							D.DPTO_CODI, 
							D.MUNI_CODI,
							U.USUA_LOGIN,
							U.DEPE_CODI,
							NULL AS codigoPostal,
							D.SGD_CIU_CODIGO idCiudadano,
							D.SGD_OEM_CODIGO idEmpresa,
							D.SGD_ESP_CODI idEntidad,
							U2.USUA_LOGIN loginFuncionario,
							R.CARP_CODI,
							R.CARP_PER,
							D.SGD_DIR_MAIL,
							R.RADI_FECHA_VENCE,
							R.RADI_DIAS_VENCE,
							c.SGD_TPR_DESCRIP,
							c.SGD_TPR_TERMINO,
							D.SGD_DIR_TIPO,
							R.ID,
							R.RADI_USU_ANTE,
							R.SGD_EANU_CODIGO,
							X.SGD_FENV_MODALIDAD'
	

	if( (@codigoCarpeta = 0  AND  @carpetaPersonal = 0) or LEN(@NoRadicado) > 0)
	begin

		if( LEN(@NoRadicado) > 0 )
		begin
			set @whereRG_MULTIPLE= @WhereRadicado
			set @CON=1;
		end
		set @bloqueRG_MULTIPLE='
				,P.RADI_LEIDO
				,CAST(1 as smallint) DERIVADO
				FROM SGD_RG_MULTIPLE P
				JOIN RADICADO r ON r.RADI_NUME_RADI=P.RADI_NUME_RADI
				LEFT JOIN SGD_PQR_METADATA X ON X.RADI_NUME_RADI=r.RADI_NUME_RADI 
				'+ISNULL(@joinDirUnique,'')+' JOIN USUARIO U ON P.AREA=U.DEPE_CODI AND P.USUARIO = U.USUA_CODI
				LEFT JOIN  USUARIO U2 ON U2.usua_doc=D.SGD_DOC_FUN 
				LEFT JOIN SGD_TPR_TPDCUMENTO c ON r.tdoc_codi = c.sgd_tpr_codigo
				where  (p.estatus = ''ACTIVO'') AND U.USUA_LOGIN = '''+CAST(@usuarioActual as NVARCHAR)+''''+ISNULL(@whereRG_MULTIPLE,'');
			
		set @unionRG_MULTIPLE =' 
								UNION ALL '+
								ISNULL(@camposSelect,'')+' '+
								ISNULL(@bloqueRG_MULTIPLE,'')
	end

	SET @SQL='WITH RADICADODETALLE AS
			(
				SELECT *,
				ROW_NUMBER() OVER (ORDER BY '+CAST(@campoOrden as NVARCHAR)+' '+CAST(@tipoOrden as NVARCHAR)+') AS RowNum,
				(
					select COUNT(0) FROM
					(	
						'+ISNULL(@camposSelect,'')+' 
						'+ISNULL(@bloquePPAL,'')+'
						'+ISNULL(@unionRG_MULTIPLE,'')+'
					) X
				) totalRows
				FROM 
				(
					'+ISNULL(@camposSelect,'')+'
					'+ISNULL(@bloquePPAL,'')+'
					'+ISNULL(@unionRG_MULTIPLE,'')+'
				) B
			)
			SELECT * FROM RADICADODETALLE WHERE '+ISNULL(@WRowNum,'')

	DECLARE @Resultado TABLE 
	(	
		RADI_NUME_RADI		VARCHAR(MAX), 
		RADI_FECH_RADI 		DATETIME,
		RA_ASUN 			NVARCHAR(MAX),
		TDOC_CODI 			INT,
		RADI_NUME_DERI 		VARCHAR(MAX),
		RADI_PATH 			NVARCHAR(MAX),
		SGD_DIR_NOMREMDES 	NVARCHAR(MAX),
		SGD_DIR_DIRECCION 	NVARCHAR(MAX),
		ID_CONT 			INT,
		ID_PAIS 			INT,
		DPTO_CODI 			NVARCHAR(100),
		MUNI_CODI			INT,
		USUA_LOGIN			NVARCHAR(100), 
		DEPE_CODI			INT,
		codigoPostal VARCHAR(MAX), --codigoPostal		INT,	
		idCiudadano			INT,		
		idEmpresa			INT,
		idEntidad			INT,
		loginFuncionario    NVARCHAR(MAX),
		CARP_CODI			INT,
		CARP_PER			INT,
		SGD_DIR_MAIL        NVARCHAR(MAX),
		RADI_FECHA_VENCE    NVARCHAR(MAX),
		RADI_DIAS_VENCE		INT,
		SGD_TPR_DESCRIP     NVARCHAR(MAX),
		SGD_TPR_TERMINO		INT,
		SGD_DIR_TIPO		INT,
		ID					INT,
		RADI_USU_ANTE       NVARCHAR(MAX),
		SGD_EANU_CODIGO		INT,
		SGD_FENV_MODALIDAD  NVARCHAR(MAX),
		RADI_LEIDO          INT,
		DERIVADO            INT,
		RowNum              INT,
		totalRows           INT

	)
	
	print(@SQL) 
	INSERT INTO @Resultado
	EXEC(@SQL)
	
	    SELECT 

		CONVERT(DECIMAL, ISNULL(RADI_NUME_RADI,0)) AS RADI_NUME_RADI,		
		RADI_FECH_RADI, 		
		RA_ASUN, 		
		ISNULL(TDOC_CODI,0) AS TDOC_CODI, 			
		CONVERT(INT,RADI_NUME_DERI) AS RADI_NUME_DERI,
		RADI_PATH, 			
		SGD_DIR_NOMREMDES, 
		SGD_DIR_DIRECCION, 
		ISNULL(ID_CONT,0) AS ID_CONT, 			
		ISNULL(ID_PAIS,0) AS ID_PAIS, 			
		DPTO_CODI, 			
		ISNULL(MUNI_CODI,0) AS MUNI_CODI,				
		USUA_LOGIN,			
		ISNULL(DEPE_CODI,0) AS DEPE_CODI,			
		0 AS codigoPostal,		
		ISNULL(idCiudadano,0) AS idCiudadano,			
		ISNULL(idEmpresa,0) AS idEmpresa,
		ISNULL(idEntidad,0) AS idEntidad,			
		ISNULL(loginFuncionario,0) AS loginFuncionario,  
		CARP_CODI,			
		CARP_PER,			
		SGD_DIR_MAIL,      
		RADI_FECHA_VENCE,  
		ISNULL(RADI_DIAS_VENCE,0) AS RADI_DIAS_VENCE,		
		SGD_TPR_DESCRIP,   
		ISNULL(SGD_TPR_TERMINO,0) AS SGD_TPR_TERMINO,		
		ISNULL(SGD_DIR_TIPO,0) AS SGD_DIR_TIPO,		
		ID,					
		RADI_USU_ANTE,     
		ISNULL(SGD_EANU_CODIGO,0) AS SGD_EANU_CODIGO,				
		SGD_FENV_MODALIDAD,
		ISNULL(RADI_LEIDO,0) AS RADI_LEIDO,        
		ISNULL(DERIVADO,0) AS DERIVADO,         
		ISNULL(RowNum,0) AS RowNum,        
		ISNULL(totalRows,0) AS totalRows     
		
	FROM @Resultado
	

	    


END;
GO
/****** Object:  StoredProcedure [dbo].[RADICADOActualizar_SIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RADICADOActualizar_SIT]  
(  
 @RADI_NUME_RADI numeric(15,0),  
    @RADI_FECH_RADI datetime,  
    @TDOC_CODI numeric(4,0),  
    @MREC_CODI numeric(2,0),  
    @RADI_FECH_OFIC datetime,  
    @CARP_CODI numeric(3,0),  
    @RADI_DESC_ANEX varchar(100),  
    @RADI_NUME_DERI numeric(15,0),  
    @RADI_USUA_ACTU numeric(10,0),  
    @RADI_DEPE_ACTU numeric(5,0),  
    @RA_ASUN varchar(350),  
    @RADI_DEPE_RADI numeric(3,0),  
    @RADI_USUA_RADI numeric(10,0),  
    @CODI_NIVEL numeric(2,0),  
    @FLAG_NIVEL numeric(18,0),  
    @CARP_PER numeric(1,0),  
    @RADI_LEIDO numeric(1,0),  
    @RADI_TIPO_DERI numeric(2,0),  
    @USUA_DOC varchar(14),  
    @SGD_APLI_CODIGO int,  
 @RADI_PATH varchar(100)  
)  
AS SET NOCOUNT OFF;  
BEGIN  
  
/*********************************************************************************   
 Nombre : RADICADOActualizar  
                       
 Proposito: inserta los datos en la tabla principla de radicacion dbo.Radicado    
                       
 Tablas : dbo.Radicado  
                       
 Autor  :  Fabian Losada   Fecha: 2019/08/05  
 Modifica:    
  
 NOTAS:   
   
 Parametros:   
  @RADI_NUME_RADI numeric(15,0),  
    @RADI_FECH_RADI datetime,  
    @TDOC_CODI numeric(4,0),  
    @MREC_CODI numeric(2,0),  
    @RADI_FECH_OFIC datetime,  
    @CARP_CODI numeric(3,0),  
    @RADI_DESC_ANEX varchar(100),  
    @RADI_NUME_DERI numeric(15,0),  
    @RADI_USUA_ACTU numeric(10,0),  
    @RADI_DEPE_ACTU numeric(5,0),  
    @RA_ASUN varchar(350),  
    @RADI_DEPE_RADI numeric(3,0),  
    @RADI_USUA_RADI numeric(10,0),  
    @CODI_NIVEL numeric(2,0),  
    @FLAG_NIVEL numeric(18,0),  
    @CARP_PER numeric(1,0),  
    @RADI_LEIDO numeric(1,0),  
    @RADI_TIPO_DERI numeric(2,0),  
    @USUA_DOC varchar(14),  
    @SGD_APLI_CODIGO int,  
 @RADI_PATH nvarchar(100),  
   
 @Original_RADI_NUME_RADI numeric(15,0),  
    @Original_RADI_FECH_RADI datetime,  
    @Original_TDOC_CODI numeric(4,0),  
    @Original_MREC_CODI numeric(2,0),  
    @Original_RADI_FECH_OFIC datetime,  
    @Original_CARP_CODI numeric(3,0),  
    @Original_RADI_DESC_ANEX varchar(100),  
    @Original_RADI_NUME_DERI numeric(15,0),  
    @Original_RADI_USUA_ACTU numeric(10,0),  
    @Original_RADI_DEPE_ACTU numeric(5,0),  
    @Original_RA_ASUN varchar(350),  
    @Original_RADI_DEPE_RADI numeric(3,0),  
    @Original_RADI_USUA_RADI numeric(10,0),  
    @Original_CODI_NIVEL numeric(2,0),  
    @Original_FLAG_NIVEL numeric(18,0),  
    @Original_CARP_PER numeric(1,0),  
    @Original_RADI_LEIDO numeric(1,0),  
    @Original_RADI_TIPO_DERI numeric(2,0),  
    @Original_USUA_DOC varchar(14),  
    @Original_SGD_APLI_CODIGO int,  
 @Original_RADI_PATH varchar(100)  
   
 Resultado(s):  Datos de Radicado Modificados  
**********************************************************************************/  
  
UPDATE [dbo].[RADICADO]  
SET   
 RADI_FECH_RADI =@RADI_FECH_RADI ,  
 TDOC_CODI =@TDOC_CODI ,  
 MREC_CODI =@MREC_CODI ,  
 RADI_FECH_OFIC =@RADI_FECH_OFIC ,  
 CARP_CODI =@CARP_CODI ,  
 RADI_DESC_ANEX =@RADI_DESC_ANEX ,  
 RADI_NUME_DERI =@RADI_NUME_DERI ,  
 RADI_USUA_ACTU=@RADI_USUA_ACTU,  
 RADI_DEPE_ACTU=@RADI_DEPE_ACTU,  
 RA_ASUN =@RA_ASUN ,  
 RADI_DEPE_RADI=@RADI_DEPE_RADI,  
 RADI_USUA_RADI=@RADI_USUA_RADI,  
 CODI_NIVEL=@CODI_NIVEL,  
 FLAG_NIVEL =@FLAG_NIVEL ,  
 CARP_PER =@CARP_PER ,  
 RADI_LEIDO =@RADI_LEIDO ,  
 RADI_TIPO_DERI=@RADI_TIPO_DERI,  
 USUA_DOC =@USUA_DOC ,  
 SGD_APLI_CODIGO =@SGD_APLI_CODIGO ,  
 RADI_PATH=@RADI_PATH  
WHERE  
(  
 RADI_NUME_RADI=@RADI_NUME_RADI   
  
);  
SELECT [RADI_NUME_RADI]  
           ,[RADI_FECH_RADI]  
           ,[TDOC_CODI]  
           ,[MREC_CODI]  
     ,sgd_trad_codigo as TIPO_RAD
           ,[RADI_FECH_OFIC]  
           ,[CARP_CODI]  
           ,[RADI_DESC_ANEX]  
           ,[RADI_NUME_DERI]  
           ,[RADI_USUA_ACTU]  
           ,[RADI_DEPE_ACTU]  
           ,[RA_ASUN]  
           ,[RADI_DEPE_RADI]  
           ,[RADI_USUA_RADI]  
           ,[CODI_NIVEL]  
           ,[FLAG_NIVEL]  
           ,[CARP_PER]  
           ,[RADI_LEIDO]   
           ,[RADI_TIPO_DERI]  
           ,[USUA_DOC]  
           ,[SGD_APLI_CODIGO]  
     ,[RADI_PATH]  
	 ,REQUIERE_RESP
 FROM [dbo].[RADICADO]  
 WHERE RADI_NUME_RADI  = @RADI_NUME_RADI  
END  
GO
/****** Object:  StoredProcedure [dbo].[RADICADOConsultarConFiltroSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RADICADOConsultarConFiltroSIT]
(
	@RADI_NUME_RADI numeric(18,0)
)
AS
/********************************************************************************* 
 Nombre:		RADICADOConsultarConFiltro
 Propósito:		Obtiene los radicados
 Tablas:		Radicado
 Autor:			Oscar Malagón			
 Fecha:			05/04/2013
 Modifica:		Cristian Alejandro Neira Lopez
 Resultado(s):	Listado de Radicado
 **********************************************************************************/
BEGIN
	SET NOCOUNT ON;

	Select RADI_NUME_RADI
		,RADI_FECH_RADI
		,TDOC_CODI
		,TRTE_CODI
		,MREC_CODI
		,EESP_CODI
		,EOTRA_CODI
		,RADI_TIPO_EMPR
		,RADI_FECH_OFIC
		,TDID_CODI
		,RADI_NUME_IDEN
		,RADI_NOMB
		,RADI_PRIM_APEL
		,RADI_SEGU_APEL
		,RADI_PAIS
		,MUNI_CODI
		,CPOB_CODI
		,CARP_CODI
		,ESTA_CODI
		,DPTO_CODI
		,CEN_MUNI_CODI
		,CEN_DPTO_CODI
		,ID_CONT
		,ID_PAIS
		,RADI_DIRE_CORR
		,RADI_TELE_CONT
		,RADI_NUME_HOJA
		,RADI_DESC_ANEX
		,RADI_NUME_DERI
		,RADI_PATH
		,RADI_USUA_ACTU
		,RADI_DEPE_ACTU
		,RADI_FECH_ASIG
		,NULL AS RADI_ARCH1
		,NULL AS RADI_ARCH2
		,NULL AS RADI_ARCH3
		,NULL AS RADI_ARCH4
		,RA_ASUN
		,RADI_USU_ANTE
		,RADI_DEPE_RADI
		,RADI_REM
		,RADI_USUA_RADI
		,CODI_NIVEL
		,FLAG_NIVEL
		,CARP_PER
		,RADI_LEIDO
		,RADI_CUENTAI
		,RADI_TIPO_DERI
		,LISTO
		,SGD_TMA_CODIGO
		,SGD_MTD_CODIGO
		,PAR_SERV_SECUE
		,SGD_FLD_CODIGO
		,RADI_AGEND
		,RADI_FECH_AGEND
		,RADI_FECH_DOC
		,SGD_DOC_SECUENCIA
		,SGD_PNUFE_CODI
		,SGD_EANU_CODIGO
		,SGD_NOT_CODI
		,NULL AS RADI_FECH_NOTIFf
		,SGD_APLI_CODI
		,FECH_VCMTO
		,SGD_SPUB_CODIGO
		,NULL AS SGD_DIR_DIRECCION
		,USUA_DOC
		,id
		,NULL AS RADI_NOTIFICADO
		,RADI_FECHA_VENCE
		,RADI_DIAS_VENCE
		,NULL AS RADI_TEMA_ID
		,NULL AS RADI_DESPLA
		,NULL AS radi_nume_solicitud
		,NULL AS exp_soli_id
		,NULL AS SGD_ID_TRAM
		,NULL as RADI_NOTIF_FIJACION
		,NULL AS RADI_NOTIF_DESFIJACION
		,SGD_APLI_CODIGO
		,sgd_spub_codigo as RADI_USUA_PRIVADO
		,RADI_RESPUESTA
		,REQUIERE_RESP
		,isnull(sgd_trad_codigo, SUBSTRING(str(RADI_NUME_RADI,14,0),14,1)) as TIPO_RAD
		,SUBSTRING(CAST(radi_nume_radi AS varchar),1,4) AS ANO_RAD
		,SUBSTRING(CAST(radi_nume_radi AS varchar),8,6) AS SEC_RAD
	from dbo.RADICADO WITH (NOLOCK)
	WHERE RADI_NUME_RADI  = @RADI_NUME_RADI
	ORDER BY RADI_NUME_RADI desc

END;
GO
/****** Object:  StoredProcedure [dbo].[RADICADOInsertarSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RADICADOInsertarSIT]    
(    
 @RADI_NUME_RADI numeric(15,0),    
    @RADI_FECH_RADI datetime,    
    @TDOC_CODI numeric(4,0),    
    @MREC_CODI numeric(2,0),    
    @RADI_FECH_OFIC datetime,    
    @CARP_CODI numeric(3,0),    
    @RADI_DESC_ANEX varchar(100),    
    @RADI_NUME_DERI numeric(15,0),    
    @RADI_USUA_ACTU numeric(10,0),    
    @RADI_DEPE_ACTU numeric(5,0),    
    @RA_ASUN varchar(350),    
    @RADI_DEPE_RADI numeric(3,0),    
    @RADI_USUA_RADI numeric(10,0),    
    @CODI_NIVEL numeric(2,0),    
    @FLAG_NIVEL numeric(18,0),    
    @CARP_PER numeric(1,0),    
    @RADI_LEIDO numeric(1,0),    
    @RADI_TIPO_DERI numeric(2,0),    
    @USUA_DOC varchar(14),    
    @SGD_APLI_CODIGO int,    
 @RADI_PATH nvarchar(100)    ,
	@REQUIERE_RESP tinyint = 1
     
)    
AS    
BEGIN    
    
/*********************************************************************************     
 Nombre : RADICADOInsertar    
                         
 Proposito: inserta los datos en la tabla principla de radicacion dbo.Radicado      
                         
 Tablas : dbo.Radicado    
                         
 Autor  :  Fabian Losada   Fecha: 2019/08/05    
 Modifica:      
    
 NOTAS:     
     
 Parametros:     
   @RADI_NUME_RADI    
 @RADI_FECH_RADI    
 @TDOC_CODI    
 @MREC_CODI    
 @RADI_FECH_OFIC    
 @CARP_CODI    
 @RADI_DESC_ANEX    
 @RADI_NUME_DERI    
 @RADI_USUA_ACTU    
 @RADI_DEPE_ACTU    
 @RA_ASUN    
 @RADI_DEPE_RADI    
 @RADI_USUA_RADI    
 @CODI_NIVEL    
 @FLAG_NIVEL    
 @CARP_PER    
 @RADI_LEIDO    
 @RADI_TIPO_DERI    
 @USUA_DOC    
 @SGD_APLI_CODIGO    
 @RADI_PATH     
     
 Resultado(s):  Datos de Radicado Insertado    
**********************************************************************************/    
    
 INSERT INTO [dbo].[RADICADO]    
           ([RADI_NUME_RADI]    
           ,[RADI_FECH_RADI]    
           ,[TDOC_CODI]    
           ,[MREC_CODI]    
           ,[RADI_FECH_OFIC]    
           ,[CARP_CODI]    
           ,[RADI_DESC_ANEX]    
           ,[RADI_NUME_DERI]    
           ,[RADI_USUA_ACTU]    
           ,[RADI_DEPE_ACTU]    
           ,[RA_ASUN]    
           ,[RADI_DEPE_RADI]    
           ,[RADI_USUA_RADI]    
           ,[CODI_NIVEL]    
           ,[FLAG_NIVEL]    
           ,[CARP_PER]    
           ,[RADI_LEIDO]     
           ,[RADI_TIPO_DERI]    
           ,[USUA_DOC]    
           ,[SGD_APLI_CODIGO]    
     ,[RADI_PATH]    
		   ,[REQUIERE_RESP] 
     )    
   VALUES    
  (     
   @RADI_NUME_RADI,    
   @RADI_FECH_RADI ,    
   @TDOC_CODI ,    
   @MREC_CODI ,    
   @RADI_FECH_OFIC ,    
   @CARP_CODI ,    
   @RADI_DESC_ANEX ,    
   @RADI_NUME_DERI ,    
   @RADI_USUA_ACTU,    
   @RADI_DEPE_ACTU,    
   @RA_ASUN ,    
   @RADI_DEPE_RADI,    
   @RADI_USUA_RADI,    
   @CODI_NIVEL,    
   @FLAG_NIVEL ,    
   @CARP_PER ,    
   @RADI_LEIDO ,    
   @RADI_TIPO_DERI,    
   @USUA_DOC ,    
   @SGD_APLI_CODIGO ,    
   @RADI_PATH     
			,@REQUIERE_RESP
     )    
    
 SELECT RADI_NUME_RADI  
,RADI_FECH_RADI  
,TDOC_CODI  
,TRTE_CODI  
,MREC_CODI  
,EESP_CODI  
,EOTRA_CODI  
,RADI_TIPO_EMPR  
,RADI_FECH_OFIC  
,TDID_CODI  
,RADI_NUME_IDEN  
,RADI_NOMB  
,RADI_PRIM_APEL  
,RADI_SEGU_APEL  
,RADI_PAIS  
,MUNI_CODI  
,CPOB_CODI  
,CARP_CODI  
,ESTA_CODI  
,DPTO_CODI  
,CEN_MUNI_CODI  
,CEN_DPTO_CODI  
,ID_CONT  
,ID_PAIS  
,RADI_DIRE_CORR  
,RADI_TELE_CONT  
,RADI_NUME_HOJA  
,RADI_DESC_ANEX  
,RADI_NUME_DERI  
,RADI_PATH  
,RADI_USUA_ACTU  
,RADI_DEPE_ACTU  
,RADI_FECH_ASIG  
,NULL AS RADI_ARCH1  
,NULL AS RADI_ARCH2  
,NULL AS RADI_ARCH3  
,NULL AS RADI_ARCH4  
,RA_ASUN  
,RADI_USU_ANTE  
,RADI_DEPE_RADI  
,RADI_REM  
,RADI_USUA_RADI  
,CODI_NIVEL  
,FLAG_NIVEL  
,CARP_PER  
,RADI_LEIDO  
,RADI_CUENTAI  
,RADI_TIPO_DERI  
,LISTO  
,SGD_TMA_CODIGO  
,SGD_MTD_CODIGO  
,PAR_SERV_SECUE  
,SGD_FLD_CODIGO  
,RADI_AGEND  
,RADI_FECH_AGEND  
,RADI_FECH_DOC  
,SGD_DOC_SECUENCIA  
,SGD_PNUFE_CODI  
,SGD_EANU_CODIGO  
,SGD_NOT_CODI  
,NULL AS RADI_FECH_NOTIFf  
,SGD_APLI_CODI  
,FECH_VCMTO  
,SGD_SPUB_CODIGO  
,NULL AS SGD_DIR_DIRECCION  
,USUA_DOC  
,ID  
,NULL AS RADI_NOTIFICADO  
,RADI_FECHA_VENCE  
,RADI_DIAS_VENCE  
,NULL AS RADI_TEMA_ID  
,NULL AS RADI_DESPLA  
,NULL AS radi_nume_solicitud  
,NULL AS exp_soli_id  
,NULL AS SGD_ID_TRAM  
,NULL AS RADI_NOTIF_FIJACION  
,NULL AS RADI_NOTIF_DESFIJACION  
,SGD_APLI_CODIGO  
,sgd_spub_codigo AS RADI_USUA_PRIVADO  
,RADI_RESPUESTA  
,REQUIERE_RESP  
,sgd_trad_codigo AS TIPO_RAD  
,SUBSTRING(CAST(radi_nume_radi AS varchar),1,4) AS ANO_RAD
,SUBSTRING(CAST(radi_nume_radi AS varchar),8,6) AS SEC_RAD  
,RADI_FECH_TRAMITE  
 FROM [dbo].[RADICADO]    
 WHERE RADI_NUME_RADI  = @RADI_NUME_RADI    
     
END
GO
/****** Object:  StoredProcedure [dbo].[rdfM]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[rdfM]
AS
BEGIN

	-- Declare the return variable here
	DECLARE @USUA_CODI	int,	 --Código del usuario 
			@DEPE_CODI	int,	-- Código de la Dependencia actual
			@RADI_NUME_RADI	numeric(18),	--numero de radicado
			@fech	datetime,	--fecha tipificacion
			@USUA_CODI_NEW	int  --codigo nuevo usuario
    -- migracion data sgd_rdf_retdocf
	insert into sgd_rdf_retdocf
SELECT [sgd_mrd_codigo],[radi_nume_radi],[depe_codi],[usua_codi],[usua_doc],[sgd_rdf_fech] FROM [bdorfeo].dbo.[sgd_rdf_retdocf];
--declaracion cursor tipificacion
	DECLARE tpr CURSOR FOR select USUA_CODI,DEPE_CODI,radi_nume_radi,sgd_rdf_fech
 from sgd_rdf_retdocf;
  OPEN tpr;
  WHILE (1=1)
  begin;
  FETCH NEXT
			   FROM tpr
			   INTO @USUA_CODI, @DEPE_CODI, @RADI_NUME_RADI,@fech;
	  IF @@FETCH_STATUS < 0 BREAK;
	select @USUA_CODI_NEW=usua_codi from usua_migracion where depe_codi=@DEPE_CODI and usua_old=@USUA_CODI
	update sgd_rdf_retdocf set usua_codi=@USUA_CODI_NEW where radi_nume_radi=@RADI_NUME_RADI and sgd_rdf_fech=@fech;
  end;
  close tpr;
END
GO
/****** Object:  StoredProcedure [dbo].[RESPONSABLEObtenerpordependenciaSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--aUTHOR: Fabian Losada
--Description: Obtiene el documento del usuario jefe por la dependencia obtenida.

CREATE PROCEDURE [dbo].[RESPONSABLEObtenerpordependenciaSIT]
(
	@COD_DEP int
)

AS
BEGIN
	SELECT u.USUA_DOC FROM USUARIO u, sgd_urd_usuaroldep r WHERE u.usua_codi=r.usua_codi and r.rol_codi = 1 AND r.DEPE_CODI = @COD_DEP
END
GO
/****** Object:  StoredProcedure [dbo].[SecuenciaExpedienteConsultarSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SecuenciaExpedienteConsultarSIT]

	@sgd_srd_codigo smallint,
	@sgd_sbrd_codigo smallint,
	@depe_codi int,
	@sgd_sexp_ano smallint
AS
/********************************************************************************* 
 Nombre:		SecuenciaExpedienteConsultar
 Propósito:		Obtiene la ultimo secuencia del expediente
 Tablas:		SGD_SEXP_SECEXPEDIENTE
 Autor:			Fabian Losada	
 Fecha:			05/08/2019
 Modifica:  
 Resultado(s):	Secuencia de SGD_SEXP_SECEXPEDIENTE
 **********************************************************************************/
BEGIN
	SET NOCOUNT ON;

	select ISNULL(MAX(se.SGD_SEXP_SECUENCIA),0) as SECUENCIA
			from SGD_SEXP_SECEXPEDIENTES se
			WHERE SGD_SRD_CODIGO = @sgd_srd_codigo AND
                    SGD_SBRD_CODIGO = @sgd_sbrd_codigo AND
                    SGD_SEXP_ANO = @sgd_sexp_ano AND
                    DEPE_CODI = @depe_codi AND
                    SGD_SEXP_SECUENCIA > 0 AND
                    SGD_SEXP_SECUENCIA IS NOT NULL

END
GO
/****** Object:  StoredProcedure [dbo].[SGD_ANEXOS_EXPConsultaConFiltroSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[SGD_ANEXOS_EXPConsultaConFiltroSIT]    Script Date: 5/08/2019 10:52:40 a. m. ******/
CREATE PROCEDURE [dbo].[SGD_ANEXOS_EXPConsultaConFiltroSIT]
	@ANEXOS_EXP_ID	INT,
	@SGD_EXP_NUMERO NVARCHAR(20)
AS
BEGIN
	SELECT  aexp_id as	ANEXOS_EXP_ID,
			num_expediente as SGD_EXP_NUMERO,
			tipo_archivo as ANEX_TIPO_CODI,
			aexp_usua as USUA_LOGIN_CREA,
			exp_fech as ANEXOS_EXP_FECH_CREA,
			aexp_asunto as ANEXOS_EXP_DESC,
			aexp_path as ANEXOS_EXP_PATH,
			aexp_tpdoc as SGD_TPR_CODIGO,
			(select r.depe_codi from sgd_urd_usuaroldep r, usuario u where u.usua_codi=r.usua_codi and u.usua_login=aexp_usua) as DEPE_CODI,
			aexp_borrado as ANEXOS_EXP_BORRADO,
			NULL AS USUA_LOGIN_BORRA,
			null AS ANEXOS_EXP_FECH_BORRA,
			aexp_nombre AS ANEXOS_EXP_NOMBRE,
			1 AS ANEXOS_EXP_ESTADO,
			NULL AS SGD_APLI_CODIGO
	FROM	[dbo].sgd_aexp_anexexpediente
	WHERE	num_anexo = @ANEXOS_EXP_ID
			AND num_expediente = @SGD_EXP_NUMERO
END
GO
/****** Object:  StoredProcedure [dbo].[SGD_ANEXOS_EXPInsertarSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[SGD_ANEXOS_EXPInsertarSIT]    Script Date: 5/08/2019 11:32:24 a. m. ******/
CREATE PROCEDURE [dbo].[SGD_ANEXOS_EXPInsertarSIT]
(
	@SGD_EXP_NUMERO varchar(20),
	@ANEX_TIPO_CODI numeric(4, 0),
	@USUA_LOGIN_CREA varchar(24),
	@ANEXOS_EXP_FECH_CREA datetime,
	@ANEXOS_EXP_DESC varchar(350),
	@ANEXOS_EXP_PATH varchar(100),
	@SGD_TPR_CODIGO smallint,
	@DEPE_CODI int,
	@ANEXOS_EXP_BORRADO varchar(1),
	@USUA_LOGIN_BORRA varchar(24),
	@ANEXOS_EXP_FECH_BORRA datetime,
	@ANEXOS_EXP_NOMBRE varchar(100),
	@ANEXOS_EXP_ESTADO tinyint,
	@SGD_APLI_CODIGO int
)
AS
BEGIN

/********************************************************************************* 
 Nombre	: SGD_ANEXOS_EXPInsertar
																					
 Proposito: inserta los datos en la tabla principal de Anexos  a Expediente
																					
 Tablas	:	dbo.SGD_ANEXOS_EXP
																					
 Autor	 : Fabian Losada			Fecha: 2019/08/05
 Modifica:  

 NOTAS:	
 
 Parametros:	
  	--@ANEXOS_EXP_ID int IDENTITY(1,1),
	@SGD_EXP_NUMERO varchar(20),
	@ANEX_TIPO_CODI numeric(4, 0),
	@USUA_LOGIN_CREA varchar(24),
	@ANEXOS_EXP_FECH_CREA datetime,
	@ANEXOS_EXP_DESC varchar(350),
	@ANEXOS_EXP_PATH varchar(100),
	@SGD_TPR_CODIGO smallint,
	@DEPE_CODI int,
	@ANEXOS_EXP_BORRADO varchar(1),
	@USUA_LOGIN_BORRA varchar(24),
	@ANEXOS_EXP_FECH_BORRA datetime,
	@ANEXOS_EXP_NOMBRE varchar(100),
	@ANEXOS_EXP_ESTADO tinyint,
	@SGD_APLI_CODIGO int
	
 Resultado(s):	 Datos de SGD_ANEXOS_EXP Insertado
**********************************************************************************/

	INSERT INTO [dbo].sgd_aexp_anexexpediente
           (
		   
			NUM_EXPEDIENTE ,
			tipo_archivo,
			aexp_usua,
			exp_fech,
			aexp_asunto,
			aexp_path ,
			aexp_tpdoc,
			aexp_borrado,
			aexp_nombre
			)
		 VALUES
		(	
			@SGD_EXP_NUMERO ,
			@ANEX_TIPO_CODI ,
			@USUA_LOGIN_CREA,
			@ANEXOS_EXP_FECH_CREA ,
			@ANEXOS_EXP_DESC ,
			@ANEXOS_EXP_PATH ,
			@SGD_TPR_CODIGO,
			@ANEXOS_EXP_BORRADO ,
			@ANEXOS_EXP_NOMBRE
		   )

	SELECT  aexp_id as	ANEXOS_EXP_ID,
			num_expediente as SGD_EXP_NUMERO,
			tipo_archivo as ANEX_TIPO_CODI,
			aexp_usua as USUA_LOGIN_CREA,
			exp_fech as ANEXOS_EXP_FECH_CREA,
			aexp_asunto as ANEXOS_EXP_DESC,
			aexp_path as ANEXOS_EXP_PATH,
			aexp_tpdoc as SGD_TPR_CODIGO,
			(select r.depe_codi from sgd_urd_usuaroldep r, usuario u where u.usua_codi=r.usua_codi and u.usua_login=aexp_usua) as DEPE_CODI,
			aexp_borrado as ANEXOS_EXP_BORRADO,
			NULL AS USUA_LOGIN_BORRA,
			null AS ANEXOS_EXP_FECH_BORRA,
			aexp_nombre AS ANEXOS_EXP_NOMBRE,
			1 AS ANEXOS_EXP_ESTADO,
			NULL AS SGD_APLI_CODIGO
	FROM	[dbo].sgd_aexp_anexexpediente
	WHERE	aexp_id = SCOPE_IDENTITY()
	
END
GO
/****** Object:  StoredProcedure [dbo].[SGD_AUDITORIA_WSInsertar]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SGD_AUDITORIA_WSInsertar]
	
	@FECHA_EVENTO DATETIME ,
	@APLICATIVO VARCHAR(30),
	@XML_PARAMETROS XML ,
	@USUA_LOGIN VARCHAR(24),
	@FUNCIONALIDAD VARCHAR(50),
	@ESTADO VARCHAR(50),
	@MENSAJE NVARCHAR(MAX)

AS
/********************************************************************************* 
 Nombre:		SGD_AUDITORIA_WSInsertar
 Propósito:		Inserta Información de auditoria a nivel de Web Service
 Tablas:		SGD_AUDITORIA_WS
 Autor:			Fabian Losada	
 Fecha:			05/08/2019
 Modifica:  
 PARAMETROS:
   @FECHA_EVENTO DATETIME ,
	@APLICATIVO VARCHAR(30),
	@XML_PARAMETROS XML ,
	@USUA_LOGIN VARCHAR(24),
	@FUNCIONALIDAD VARCHAR(50),
	@ESTADO VARCHAR(50),
	@MENSAJE TEXT
 
 Resultado(s):	Datos de insertados
 **********************************************************************************/
BEGIN
DECLARE @SQL NVARCHAR(MAX);

SET @SQL='
INSERT INTO [dbo].[SGD_AUDITORIA_WS]
           (
            FECHA_EVENTO ,
			APLICATIVO,
			XML_PARAMETROS,
			USUA_LOGIN,
			FUNCIONALIDAD,
			ESTADO,
			MENSAJE,
			ANO
		   )
		 VALUES
		( '''+CONVERT(NVARCHAR(MAX),@FECHA_EVENTO)+''' ,
			'''+@APLICATIVO+''',
			'''+CONVERT(NVARCHAR(MAX),@XML_PARAMETROS)+''',
			'''+@USUA_LOGIN+''',
			'''+@FUNCIONALIDAD+''',
			'''+@ESTADO+''',
			'''+@MENSAJE+''',
			'+ CONVERT(VARCHAR(4),YEAR(GETDATE())) +'
		   )

	SELECT	ID,
			FECHA_EVENTO ,
			APLICATIVO,
			XML_PARAMETROS,
			USUA_LOGIN,
			FUNCIONALIDAD,
			ESTADO,
			MENSAJE
	FROM [dbo].[SGD_AUDITORIA_WS]
	WHERE ID=SCOPE_IDENTITY()'

EXEC(@SQL);
END;

GO
/****** Object:  StoredProcedure [dbo].[SGD_CIU_CIUDADANOConsultaConFiltroSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SGD_CIU_CIUDADANOConsultaConFiltroSIT]
	
	
	@SGD_CIU_CODIGO numeric(18,0),
	@SGD_CIU_NOMBRE varchar(80),
	@SGD_CIU_APELLIDO1 varchar(100),
	@SGD_CIU_APELLIDO2 varchar(100),
	@SGD_CIU_CEDULA varchar(13)

AS
/********************************************************************************* 
 Nombre:		SGD_CIU_CIUDADANOConsultaConFiltro
 Propósito:		Obtiene datos de contacto Ciudadano
 Tablas:		SGD_CIU_CIUDADANO
 Autor:			Fabian Losada	
 Fecha:			05/08/2019
 Modifica:  
 Resultado(s):	Datos de contacto de tipo Ciudadano
 **********************************************************************************/
 
DECLARE @SQL VARCHAR(MAX);
DECLARE @CON INT;

BEGIN
	SET NOCOUNT ON;
	SET @CON=0;
	SET @SQL = 'Select	TDID_CODI,
                        SGD_CIU_CODIGO,
                        SGD_CIU_NOMBRE,
                        SGD_CIU_APELL1,
                        SGD_CIU_DIRECCION,
                        SGD_CIU_TELEFONO,
                        SGD_CIU_APELL2,
                        SGD_CIU_EMAIL,
                        MUNI_CODI,
                        DPTO_CODI,
                        ID_PAIS,
                        ID_CONT,
                        SGD_CIU_CEDULA
				from 	dbo.SGD_CIU_CIUDADANO 
				where 	1=1';
	
	if( @SGD_CIU_CODIGO > 0 )
	begin
		set @SQL=@SQL+' AND SGD_CIU_CODIGO='''+CAST(@SGD_CIU_CODIGO as NVARCHAR)+'''';
		set @CON=1;
	end
	if( LEN(@SGD_CIU_NOMBRE) > 0 )
	begin
		set @SQL=@SQL+' AND SGD_CIU_NOMBRE like ''%'+CAST(@SGD_CIU_NOMBRE as NVARCHAR)+'%''';
		set @CON=1;
	end
	if( LEN(@SGD_CIU_APELLIDO1) > 0 )
	begin
		set @SQL=@SQL+' AND SGD_CIU_APELL1 like ''%'+CAST(@SGD_CIU_APELLIDO1 as NVARCHAR)+'%''';
		set @CON=1;
	end
	if( LEN(@SGD_CIU_APELLIDO2) > 0 )
	begin
		set @SQL=@SQL+' AND SGD_CIU_APELL2 like ''%'+CAST(@SGD_CIU_APELLIDO2 as NVARCHAR)+'%''';
		set @CON=1;
	end
	if( LEN(@SGD_CIU_CEDULA) > 0 )
	begin
		set @SQL=@SQL+' AND SGD_CIU_CEDULA like ''%'+CAST(@SGD_CIU_CEDULA as NVARCHAR)+'%''';
		set @CON=1;
	end
	if( @CON = 0 )
	begin
		set @SQL=@SQL+' AND 1<>1';
	end

	DECLARE @Resultado TABLE 
	(
			[TDID_CODI] numeric(2, 0)
           ,[SGD_CIU_CODIGO] numeric(18, 0)
           ,[SGD_CIU_NOMBRE] varchar(80)
           ,[SGD_CIU_DIRECCION] varchar(100)
           ,[SGD_CIU_APELL1] varchar(50)
           ,[SGD_CIU_APELL2] varchar(50)
           ,[SGD_CIU_TELEFONO] varchar(50)
           ,[SGD_CIU_EMAIL] varchar(100)
           ,[MUNI_CODI] int
           ,[DPTO_CODI] int
           ,[ID_PAIS] smallint
           ,[ID_CONT] tinyint
           ,[SGD_CIU_CEDULA] varchar(13)
	)
	INSERT INTO @Resultado
	exec(@SQL);


	SELECT [TDID_CODI]
           ,[SGD_CIU_CODIGO]
           ,[SGD_CIU_NOMBRE]
           ,[SGD_CIU_DIRECCION]
           ,[SGD_CIU_APELL1]
           ,[SGD_CIU_APELL2]
           ,[SGD_CIU_TELEFONO]
           ,[SGD_CIU_EMAIL]
           ,[MUNI_CODI]
           ,[DPTO_CODI]
           ,[ID_PAIS]
           ,[ID_CONT]
           ,[SGD_CIU_CEDULA]
		   ,null as [SGD_CIU_CODPOSTAL]
	FROM @Resultado

END;
GO
/****** Object:  StoredProcedure [dbo].[SGD_CIU_CIUDADANOInsertarSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SGD_CIU_CIUDADANOInsertarSIT]
(
	@TDID_CODI numeric(2,0),
    @SGD_CIU_NOMBRE varchar(80),
    @SGD_CIU_DIRECCION varchar(100),
    @SGD_CIU_APELL1 varchar(50),
    @SGD_CIU_APELL2 varchar(50),
    @SGD_CIU_TELEFONO varchar(50),
    @SGD_CIU_EMAIL varchar(100),
    @MUNI_CODI int,
    @DPTO_CODI int,
    @ID_PAIS smallint,
    @ID_CONT tinyint,
    @SGD_CIU_CEDULA varchar(13),
	@SGD_CIU_CODPOSTAL VARCHAR(8) 
)
AS
BEGIN

/********************************************************************************* 
 Nombre	: SGD_CIU_CIUDADANOInsertar
																					
 Proposito: inserta los datos en la tabla de Ciudadano dbo.SGD_CIU_CIUDADANO  
																					
 Tablas	:	dbo.SGD_CIU_CIUDADANO
																					
 Autor	 :  Fabian Losada	Fecha: 2019/08/05

 Modifica:  

 NOTAS:	
 
 Parametros:	
  	
    @TDID_CODI
           ,@SGD_CIU_CODIGO
           ,@SGD_CIU_NOMBRE
           ,@SGD_CIU_DIRECCION
           ,@SGD_CIU_APELL1
           ,@SGD_CIU_APELL2
           ,@SGD_CIU_TELEFONO
           ,@SGD_CIU_EMAIL
           ,@MUNI_CODI
           ,@DPTO_CODI
           ,@ID_PAIS
           ,@ID_CONT
           ,@SGD_CIU_CEDULA
		   ,@SGD_CIU_CODPOSTAL
	
 Resultado(s):	 Datos de SGD_CIU_CIUDADANO Insertado
**********************************************************************************/


DECLARE @CSC float;
DECLARE @SGD_CIU_CODIGO numeric(8, 0);

select top(1) @SGD_CIU_CODIGO = SGD_CIU_CODIGO 
from [dbo].[SGD_CIU_CIUDADANO] where SGD_CIU_NOMBRE = @SGD_CIU_NOMBRE and SGD_CIU_CEDULA = @SGD_CIU_CEDULA and SGD_CIU_APELL1 = @SGD_CIU_APELL1 and SGD_CIU_APELL2 = @SGD_CIU_APELL2
order by SGD_CIU_CODIGO desc

if @SGD_CIU_CODIGO is null 
begin
Set @SGD_CIU_CODIGO = NEXT VALUE FOR [dbo].[SEC_CIU_CIUDADANO];

	INSERT INTO [dbo].[SGD_CIU_CIUDADANO]
           ([TDID_CODI]
           ,[SGD_CIU_CODIGO]
           ,[SGD_CIU_NOMBRE]
           ,[SGD_CIU_DIRECCION]
           ,[SGD_CIU_APELL1]
           ,[SGD_CIU_APELL2]
           ,[SGD_CIU_TELEFONO]
           ,[SGD_CIU_EMAIL]
           ,[MUNI_CODI]
           ,[DPTO_CODI]
           ,[ID_PAIS]
           ,[ID_CONT]
           ,[SGD_CIU_CEDULA]
		   )
		 VALUES
		(	
			@TDID_CODI
           ,@SGD_CIU_CODIGO
           ,@SGD_CIU_NOMBRE
           ,@SGD_CIU_DIRECCION
           ,@SGD_CIU_APELL1
           ,@SGD_CIU_APELL2
           ,@SGD_CIU_TELEFONO
           ,@SGD_CIU_EMAIL
           ,@MUNI_CODI
           ,@DPTO_CODI
           ,@ID_PAIS
           ,@ID_CONT
           ,@SGD_CIU_CEDULA
		   )
end

	SELECT [TDID_CODI]
           ,[SGD_CIU_CODIGO]
           ,[SGD_CIU_NOMBRE]
           ,[SGD_CIU_DIRECCION]
           ,[SGD_CIU_APELL1]
           ,[SGD_CIU_APELL2]
           ,[SGD_CIU_TELEFONO]
           ,[SGD_CIU_EMAIL]
           ,[MUNI_CODI]
           ,[DPTO_CODI]
           ,[ID_PAIS]
           ,[ID_CONT]
           ,[SGD_CIU_CEDULA]
		   ,NULL AS [SGD_CIU_CODPOSTAL]
	FROM [dbo].[SGD_CIU_CIUDADANO]
	WHERE SGD_CIU_CODIGO  = @SGD_CIU_CODIGO
	
END
GO
/****** Object:  StoredProcedure [dbo].[SGD_DIR_DRECCIONESInsertarSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SGD_DIR_DRECCIONESInsertarSIT]
(
		
           @SGD_DIR_TIPO numeric(4,0),
           @SGD_OEM_CODIGO numeric(8,0),
           @SGD_CIU_CODIGO numeric(18,0),
           @RADI_NUME_RADI numeric(15,0),
           @SGD_ESP_CODI numeric(5,0),
           @MUNI_CODI int,
           @DPTO_CODI int,
           @ID_PAIS smallint,
           @ID_CONT tinyint,
           @SGD_DIR_DIRECCION varchar(100),
           @SGD_DIR_TELEFONO varchar(50),
           @SGD_DIR_MAIL varchar(100),
           @SGD_DIR_NOMBRE varchar(60),
           @SGD_DOC_FUN varchar(14),
           @SGD_DIR_NOMREMDES varchar(250),
           @mrec_codi numeric(2,0),
		   @SGD_DIR_CODPOSTAL VARCHAR(8) 
         
)
AS
BEGIN

/********************************************************************************* 
 Nombre	: SGD_DIR_DRECCIONESInsertar
																					
 Proposito: inserta los datos en la tabla de relacion contacto VS radicado dbo.SGD_DIR_DRECCIONES  
																					
 Tablas	:	dbo.SGD_DIR_DRECCIONES
																					
 Autor	 :  Fabian Losada		Fecha: 2019/08/06

 Modifica:  

 NOTAS:	
 
 Parametros:	
  	
   @SGD_DIR_TIPO,
   @SGD_OEM_CODIGO,
   @SGD_CIU_CODIGO,
   @RADI_NUME_RADI,
   @SGD_ESP_CODI,
   @MUNI_CODI,
   @DPTO_CODI,
   @ID_PAIS,
   @ID_CONT,
   @SGD_DIR_DIRECCION,
   @SGD_DIR_TELEFONO,
   @SGD_DIR_MAIL,
   @SGD_DIR_NOMBRE,
   @SGD_DOC_FUN,
   @SGD_DIR_NOMREMDES,
   @mrec_codi,
   @SGD_DIR_CODPOSTAL
	
 Resultado(s):	 Datos de SGD_DIR_DRECCIONES Insertado
**********************************************************************************/
DECLARE @SGD_DIR_CODIGO numeric(10, 0);

Set @SGD_DIR_CODIGO=NEXT VALUE FOR [dbo].SEC_DIR_DIRECCIONES;


	INSERT INTO [dbo].[SGD_DIR_DRECCIONES]
           (SGD_DIR_CODIGO,
		   SGD_DIR_TIPO,
		   SGD_OEM_CODIGO,
		   SGD_CIU_CODIGO,
		   RADI_NUME_RADI,
		   SGD_ESP_CODI,
		   MUNI_CODI,
		   DPTO_CODI,
		   ID_PAIS,
		   ID_CONT,
		   SGD_DIR_DIRECCION,
		   SGD_DIR_TELEFONO,
		   SGD_DIR_MAIL,
		   SGD_DIR_NOMBRE,
		   SGD_DOC_FUN,
		   SGD_DIR_NOMREMDES
		   )
		 VALUES
		( @SGD_DIR_CODIGO,
		   @SGD_DIR_TIPO,
		   @SGD_OEM_CODIGO,
		   @SGD_CIU_CODIGO,
		   @RADI_NUME_RADI,
		   @SGD_ESP_CODI,
		   @MUNI_CODI,
		   @DPTO_CODI,
		   @ID_PAIS,
		   @ID_CONT,
		   @SGD_DIR_DIRECCION,
		   @SGD_DIR_TELEFONO,
		   @SGD_DIR_MAIL,
		   @SGD_DIR_NOMBRE,
		   @SGD_DOC_FUN,
		   @SGD_DIR_NOMREMDES
		   )

	SELECT SGD_DIR_CODIGO
			,SGD_DIR_TIPO
			,SGD_OEM_CODIGO
			,SGD_CIU_CODIGO
			,RADI_NUME_RADI
			,SGD_ESP_CODI
			,MUNI_CODI
			,DPTO_CODI
			,ID_PAIS
			,ID_CONT
			,SGD_DIR_DIRECCION
			,SGD_DIR_TELEFONO
			,SGD_DIR_MAIL
			,SGD_SEC_CODIGO
			,SGD_TEMPORAL_NOMBRE
			,ANEX_CODIGO
			,SGD_ANEX_CODIGO
			,SGD_DIR_NOMBRE
			,SGD_DOC_FUN
			,SGD_DIR_NOMREMDES
			,SGD_TRD_CODIGO
			,SGD_DIR_TDOC
			,SGD_DIR_DOC
			,@mrec_codi as mrec_codi
   			,null as estado_envio_fax
			,null as numero_fax
			,null as id_fax
			,null as vobo_fax
			,@SGD_DIR_CODPOSTAL as SGD_DIR_CODPOSTAL
	FROM [dbo].[SGD_DIR_DRECCIONES]
	WHERE SGD_DIR_CODIGO  = @SGD_DIR_CODIGO
	
END
GO
/****** Object:  StoredProcedure [dbo].[SGD_EXP_EXPEDIENTEConsultaConFiltroSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SGD_EXP_EXPEDIENTEConsultaConFiltroSIT]
	
	@SGD_EXP_NUMERO varchar(25),
	@RADI_NUME_RADI numeric(15,0)

AS
/********************************************************************************* 
 Nombre:		SGD_EXP_EXPEDIENTEConsultaConFiltroSIT
 Propósito:		Obtiene datos de EXPEDIENTES asociado a un radicado
 Tablas:		SGD_EXP_EXPEDIENTE
 Autor:			Oscar Malagón 
 Fecha:			04/06/2013
 Modifica:      Fabian Losada
 Fecha Modifica:06/08/2019
 Resultado(s):	Datos de EXPEDIENTES asociado a un radicado
 **********************************************************************************/
 
DECLARE @SQL VARCHAR(MAX);
DECLARE @CON INT;
DECLARE @Resultado TABLE(
	SGD_EXP_NUMERO varchar(25)
				  ,RADI_NUME_RADI numeric(15)
				  ,SGD_EXP_FECH datetime
				  ,DEPE_CODI numeric(4)
				  ,USUA_CODI numeric(3)
				  ,USUA_DOC varchar(15)
				  ,SGD_EXP_ESTADO numeric(1)
)

BEGIN
	SET NOCOUNT ON;
	SET @CON=0;
	SET @SQL = 'SELECT SGD_EXP_NUMERO
				  ,RADI_NUME_RADI
				  ,SGD_EXP_FECH
				  ,DEPE_CODI
				  ,USUA_CODI
				  ,USUA_DOC
				  ,SGD_EXP_ESTADO
	FROM [dbo].[SGD_EXP_EXPEDIENTE]
				where 	1=1';
	
	if( LEN(@SGD_EXP_NUMERO) > 0 )
	begin
		set @SQL=@SQL+' AND SGD_EXP_NUMERO='''+CAST(@SGD_EXP_NUMERO as NVARCHAR)+'''';
		set @CON=1;
	end
	if( LEN(@RADI_NUME_RADI) > 0 )
	begin
		set @SQL=@SQL+' AND RADI_NUME_RADI='+CAST(@RADI_NUME_RADI as NVARCHAR);
		set @CON=1;
	end
	if( @CON = 0 )
	begin
		set @SQL=@SQL+' AND 1<>1';
	end

	INSERT INTO @Resultado
	exec(@SQL)

	SELECT SGD_EXP_NUMERO
				  ,RADI_NUME_RADI
				  ,SGD_EXP_FECH
				  ,DEPE_CODI
				  ,USUA_CODI
				  ,USUA_DOC
				  ,SGD_EXP_ESTADO
	FROM @Resultado
END;
GO
/****** Object:  StoredProcedure [dbo].[SGD_HFLD_HISTFLUJODOCInsertarSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SGD_HFLD_HISTFLUJODOCInsertarSIT]
(
	@SGD_HFLD_CODIGO int,
	@SGD_FEXP_CODIGO smallint,
	@SGD_EXP_FECHFLUJOANT datetime,
	@SGD_HFLD_FECH datetime,
	@SGD_EXP_NUMERO varchar(19),
	@RADI_NUME_RADI decimal(18, 0),
	@USUA_DOC varchar(14),
	@USUA_CODI decimal(18, 0),
	@DEPE_CODI int,
	@SGD_TTR_CODIGO decimal(18, 0),
	@SGD_FEXP_OBSERVA varchar(500),
	@SGD_HFLD_OBSERVA varchar(500),
	@SGD_FARS_CODIGO tinyint,
	@SGD_HFLD_AUTOMATICO tinyint
)
AS
BEGIN

/********************************************************************************* 
 Nombre	: SGD_HFLD_HISTFLUJODOCInsertar
																					
 Proposito: inserta los datos en la tabla de historial del EXPEDIENTES dbo.SGD_HFLD_HISTFLUJODOC  
																					
 Tablas	:	dbo.SGD_HFLD_HISTFLUJODOC
																					
 Autor	 :  Javier Manrique			Fecha: 22/06/2017
 Modifica:  Fabian Lsoada	06/08/2019

 NOTAS:	
 
 Parametros:	
  	@SGD_HFLD_CODIGO int,
	@SGD_FEXP_CODIGO smallint,
	@SGD_EXP_FECHFLUJOANT datetime,
	@SGD_HFLD_FECH datetime,
	@SGD_EXP_NUMERO varchar(19),
	@RADI_NUME_RADI decimal(18, 0),
	@USUA_DOC varchar(14),
	@USUA_CODI decimal(18, 0),
	@DEPE_CODI int,
	@SGD_TTR_CODIGO decimal(18, 0),
	@SGD_FEXP_OBSERVA varchar(500),
	@SGD_HFLD_OBSERVA varchar(500),
	@SGD_FARS_CODIGO tinyint,
	@SGD_HFLD_AUTOMATICO tinyint
	
 Resultado(s):	 Datos de Expediente Insertado
**********************************************************************************/

	INSERT INTO [dbo].[SGD_HFLD_HISTFLUJODOC]
           (SGD_HFLD_CODIGO ,
			SGD_FEXP_CODIGO,
			SGD_EXP_FECHFLUJOANT,
			SGD_HFLD_FECH,
			SGD_EXP_NUMERO ,
			RADI_NUME_RADI,
			USUA_DOC ,
			USUA_CODI,
			DEPE_CODI,
			SGD_TTR_CODIGO,
			SGD_FEXP_OBSERVA,
			SGD_HFLD_OBSERVA,
			SGD_FARS_CODIGO,
			SGD_HFLD_AUTOMATICO
		   )
		 VALUES
		(	
			@SGD_HFLD_CODIGO ,
			@SGD_FEXP_CODIGO,
			@SGD_EXP_FECHFLUJOANT,
			@SGD_HFLD_FECH,
			@SGD_EXP_NUMERO ,
			@RADI_NUME_RADI,
			@USUA_DOC ,
			@USUA_CODI,
			@DEPE_CODI,
			@SGD_TTR_CODIGO,
			@SGD_FEXP_OBSERVA,
			@SGD_HFLD_OBSERVA,
			@SGD_FARS_CODIGO,
			@SGD_HFLD_AUTOMATICO
		)

	SELECT  SGD_HFLD_CODIGO ,
			SGD_FEXP_CODIGO,
			SGD_EXP_FECHFLUJOANT,
			SGD_HFLD_FECH,
			SGD_EXP_NUMERO ,
			RADI_NUME_RADI,
			USUA_DOC ,
			USUA_CODI,
			DEPE_CODI,
			SGD_TTR_CODIGO,
			SGD_FEXP_OBSERVA,
			SGD_HFLD_OBSERVA,
			SGD_FARS_CODIGO,
			SGD_HFLD_AUTOMATICO
	FROM [dbo].[SGD_HFLD_HISTFLUJODOC]
	WHERE SGD_EXP_NUMERO  = @SGD_EXP_NUMERO and SGD_HFLD_FECH=@SGD_HFLD_FECH and SGD_TTR_CODIGO=@SGD_TTR_CODIGO and SGD_HFLD_CODIGO=@SGD_HFLD_CODIGO and SGD_FEXP_CODIGO=@SGD_FEXP_CODIGO 
			and SGD_EXP_FECHFLUJOANT=@SGD_EXP_FECHFLUJOANT and RADI_NUME_RADI=@RADI_NUME_RADI and USUA_DOC=@USUA_DOC and USUA_CODI=@USUA_CODI and DEPE_CODI=@DEPE_CODI and SGD_TTR_CODIGO=@SGD_TTR_CODIGO and SGD_FEXP_OBSERVA=@SGD_FEXP_OBSERVA and SGD_HFLD_OBSERVA=@SGD_HFLD_OBSERVA and SGD_FARS_CODIGO=@SGD_FARS_CODIGO and SGD_HFLD_AUTOMATICO=@SGD_HFLD_AUTOMATICO
	
END
GO
/****** Object:  StoredProcedure [dbo].[SGD_HIST_IMG_ANEX_EXPInsertarSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SGD_HIST_IMG_ANEX_EXPInsertarSIT]
(
	@ANEXOS_EXP_ID int,
	@RUTA varchar(150),
	@USUA_DOC varchar(14),
	@USUA_LOGIN varchar(24),
	@FECHA datetime,
	@ID_TTR_HIAN float
)
AS
BEGIN

/********************************************************************************* 
 Nombre	: SGD_HIST_IMG_ANEX_EXPInsertar
																					
 Proposito: inserta los datos en la tabla de historico dbo.SGD_HIST_IMG_ANEX_EXP  
																					
 Tablas	:	dbo.SGD_HIST_IMG_ANEX_EXP
																					
 Autor	 :  Oscar Malagón		Fecha: 2013/mayo/09

 Modifica:  Fabian Losada		2019/08/06

 NOTAS:	
 
 Parametros:	
  	
	
	@ANEXOS_EXP_ID int,
	@RUTA varchar(150),
	@USUA_DOC varchar(14),
	@USUA_LOGIN varchar(24),
	@FECHA datetime,
	@ID_TTR_HIAN float
	
 Resultado(s):	 Datos de SGD_HIST_IMG_ANEX_EXP Insertado
**********************************************************************************/

	INSERT INTO [dbo].[SGD_HIST_IMG_ANEX_EXP]
           (
			ANEXOS_EXP_ID,
			RUTA,
			USUA_DOC,
			USUA_LOGIN,
			FECHA,
			ID_TTR_HIAN
		   )
		 VALUES
		( 
			@ANEXOS_EXP_ID,
			@RUTA,
			@USUA_DOC,
			@USUA_LOGIN,
			@FECHA,
			@ID_TTR_HIAN
		   )

	SELECT ID_HIAE
		,ANEXOS_EXP_ID
		,RUTA
		,USUA_DOC
		,USUA_LOGIN
		,FECHA
		,ID_TTR_HIAN
		,ESTADO
	FROM [dbo].[SGD_HIST_IMG_ANEX_EXP]
	WHERE ID_HIAE =SCOPE_IDENTITY()
	
END
GO
/****** Object:  StoredProcedure [dbo].[SGD_HIST_IMG_RADInsertarSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SGD_HIST_IMG_RADInsertarSIT]
(
	@RADI_NUME_RADI numeric(15, 0),
	@RUTA varchar(150),
	@USUA_DOC varchar(14),
	@USUA_LOGIN varchar(24),
	@FECHA datetime,
	@ID_TTR_HIAN float
)
AS
BEGIN

/********************************************************************************* 
 Nombre	: SGD_HIST_IMG_RADInsertar
																					
 Proposito: inserta los datos en la tabla de historico dbo.SGD_HIST_IMG_RAD  
																					
 Tablas	:	dbo.SGD_HIST_IMG_RAD
																					
 Autor	 :  Oscar Malagón		Fecha: 2013/mayo/10

 Modifica:  Fabian Losada	06/08/2019

 NOTAS:	
 
 Parametros:	
  	
	@RADI_NUME_RADI numeric(15, 0),
	@RUTA varchar(150),
	@USUA_DOC varchar(14),
	@USUA_LOGIN varchar(24),
	@FECHA datetime,
	@ID_TTR_HIAN float
	
 Resultado(s):	 Datos de SGD_HIST_IMG_RAD Insertado
**********************************************************************************/
DECLARE 
	@DEPE_CODI numeric(6),
	@ROL_ID numeric(4),
	@USUA_CODI numeric(8)
	SELECT @DEPE_CODI=r.depe_codi,@ROL_ID=rol_codi,@USUA_CODI=r.usua_codi from sgd_urd_usuaroldep r, usuario u where u.usua_codi=r.usua_codi
		and u.usua_doc=@USUA_DOC;
	INSERT INTO [dbo].[hist_eventos]
           ([depe_codi]
           ,[hist_fech]
           ,[usua_codi]
           ,[radi_nume_radi]
           ,[hist_obse]
           ,[usua_codi_dest]
           ,[usua_doc]
           ,[usua_doc_old]
           ,[sgd_ttr_codigo]
           ,[hist_doc_dest]
           ,[depe_codi_dest]
           ,[id_rol]
           ,[id_rol_dest])
     VALUES
		(@DEPE_CODI
           ,@FECHA
           ,@USUA_CODI
           ,@RADI_NUME_RADI
           ,CONCAT('Ingreso imagen de radicado ruta: ', @RUTA)
           ,@USUA_CODI
           ,@USUA_DOC
           ,@USUA_DOC
           ,@ID_TTR_HIAN
           ,@USUA_DOC
           ,@DEPE_CODI
           ,@ROL_ID
           ,@ROL_ID
		   )
SELECT id,
			radi_nume_radi,
			@RUTA,
			USUA_DOC,
			@USUA_LOGIN,
			HIST_FECH,
			SGD_TTR_CODIGO
	FROM [dbo].[HIST_EVENTOS]  WITH (NOLOCK)
	WHERE id =SCOPE_IDENTITY()
	
END
GO
/****** Object:  StoredProcedure [dbo].[SGD_HMTD_HISMATDOCInsertarSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SGD_HMTD_HISMATDOCInsertarSIT]
(
	@SGD_HMTD_FECHA datetime,
	@RADI_NUME_RADI numeric (15, 0),
	@USUA_CODI numeric (10, 0),
	@SGD_HMTD_OBSE varchar (600),
	@USUA_DOC numeric (13, 0),
	@DEPE_CODI int,
	@SGD_MRD_CODIGO smallint
)
AS
BEGIN

/********************************************************************************* 
 Nombre	: SGD_HMTD_HISMATDOCInsertar
																					
 Proposito: inserta los datos en la tabla historico de la clasificacion TRD de un radicado dbo.SGD_HMTD_HISMATDOC  
																					
 Tablas	:	dbo.SGD_HMTD_HISMATDOC
																					
 Autor	 :  Oscar Malagón			Fecha: 2013/mayo/23
 Modifica:  Fabian Losada		2019/08/06

 NOTAS:	
 
 Parametros:	
  	@SGD_HMTD_FECHA ,
	@RADI_NUME_RADI,
	@USUA_CODI,
	@SGD_HMTD_OBSE,
	@USUA_DOC,
	@DEPE_CODI,
	@SGD_MRD_CODIGO
	
 Resultado(s):	 Datos de Radicado Insertado
**********************************************************************************/
DECLARE 
	@ROL_ID numeric(4),
	@tdoc numeric(8)
	SELECT @ROL_ID=rol_codi from sgd_urd_usuaroldep  where usua_codi=@USUA_CODI and depe_codi=@DEPE_CODI;
	select @tdoc=sgd_tpr_codigo from sgd_mrd_matrird where sgd_mrd_codigo=@SGD_MRD_CODIGO;
	INSERT INTO [dbo].[hist_eventos]
           ([depe_codi]
           ,[hist_fech]
           ,[usua_codi]
           ,[radi_nume_radi]
           ,[hist_obse]
           ,[usua_codi_dest]
           ,[usua_doc]
           ,[usua_doc_old]
           ,[sgd_ttr_codigo]
           ,[hist_doc_dest]
           ,[depe_codi_dest]
           ,[id_rol]
           ,[id_rol_dest])
     VALUES
           (@DEPE_CODI
           ,@SGD_HMTD_FECHA
           ,@USUA_CODI
           ,@RADI_NUME_RADI
           ,CONCAT (@SGD_HMTD_OBSE,' - Tipificacion radicado con TRD ', @SGD_MRD_CODIGO)
           ,@USUA_CODI
           ,@USUA_DOC
           ,@USUA_DOC
           ,331
           ,@USUA_DOC
           ,@DEPE_CODI
           ,@ROL_ID
           ,@ROL_ID
		   )

	SELECT  ID,
			hist_fech as SGD_HMTD_FECHA ,
			RADI_NUME_RADI,
			USUA_CODI,
			hist_obse as SGD_HMTD_OBSE,
			USUA_DOC,
			DEPE_CODI,
			SGD_TTR_CODIGO,
			@SGD_MRD_CODIGO as SGD_MRD_CODIGO,
			@tdoc as SGD_TPR_CODIGO
	FROM [dbo].hist_eventos
	WHERE ID  = SCOPE_IDENTITY();
	
END
GO
/****** Object:  StoredProcedure [dbo].[SGD_MRD_MATRIRDConsultaConFiltroSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SGD_MRD_MATRIRDConsultaConFiltroSIT]
	
	    @DEPE_CODI int,
		@SGD_SRD_CODIGO smallint,
		@SGD_SBRD_CODIGO smallint,
		@SGD_TPR_CODIGO smallint 

AS
/********************************************************************************* 
 Nombre:		SGD_MRD_MATRIRDConsultaConFiltroSIT
 Propósito:		Obtiene datos de la Matriz Documental
 Tablas:		SGD_MRD_MATRIRD
 Autor:			Javier Manrique
 Fecha:			16/07/2017
 Modifica:		Fabian Losada 09/08/2019
 Resultado(s):	Datos de Matriz Documental
 **********************************************************************************/
 
DECLARE @SQL VARCHAR(MAX);
DECLARE @CON1 INT,@CON2 INT,@CON3 INT,@CON4 INT;

BEGIN
	SET NOCOUNT ON;
	SET @CON1=0;
	SET @CON2=0;
	SET @CON3=0;
	SET @CON4=0;
	SET @SQL = 'SELECT  
					SGD_MRD_CODIGO,
					DEPE_CODI,
					SGD_SRD_CODIGO,
					SGD_SBRD_CODIGO,
					SGD_TPR_CODIGO,
					SOPORTE,
					SGD_MRD_FECHINI,
					SGD_MRD_FECHFIN,
					SGD_MRD_ESTA
			FROM SGD_MRD_MATRIRD
				where 	1=1 and SGD_MRD_ESTA=1' ;
	
	if( @DEPE_CODI > 0 )
	begin
		set @SQL=@SQL+' AND DEPE_CODI='+CAST(@DEPE_CODI as NVARCHAR);
		set @CON1=1;
	end
	if( @SGD_SRD_CODIGO > 0 )
	begin
		set @SQL=@SQL+' AND SGD_SRD_CODIGO='+CAST(@SGD_SRD_CODIGO as NVARCHAR);
		set @CON2=1;
	end
	if( @SGD_SBRD_CODIGO > 0 )
	begin
		set @SQL=@SQL+' AND SGD_SBRD_CODIGO='+CAST(@SGD_SBRD_CODIGO as NVARCHAR);
		set @CON3=1;
	end
	if( @SGD_TPR_CODIGO > 0 )
	begin
		set @SQL=@SQL+' AND SGD_TPR_CODIGO='+CAST(@SGD_TPR_CODIGO as NVARCHAR);
		set @CON4=1;
	end
	
	
	if( @CON1 = 0 or @CON1 = 0 or @CON1 = 0 or @CON1 = 0 )
	begin
		set @SQL=@SQL+' AND 1<>1';
	end


	DECLARE @Resultado TABLE 
	(
		SGD_MRD_CODIGO	smallint,
		DEPE_CODI	int,
		SGD_SRD_CODIGO	smallint,
		SGD_SBRD_CODIGO	smallint,
		SGD_TPR_CODIGO	smallint,
		SOPORTE	varchar(10),
		SGD_MRD_FECHINI	datetime,
		SGD_MRD_FECHFIN	datetime,
		SGD_MRD_ESTA	varchar(10)
	)

	INSERT INTO @Resultado
	exec(@SQL)

	SELECT SGD_MRD_CODIGO,
		DEPE_CODI,
		SGD_SRD_CODIGO,
		SGD_SBRD_CODIGO,
		SGD_TPR_CODIGO,
		SOPORTE,
		SGD_MRD_FECHINI,
		SGD_MRD_FECHFIN,
		SGD_MRD_ESTA
	FROM @Resultado

END;
GO
/****** Object:  StoredProcedure [dbo].[SGD_OEM_OEMPRESASConsultaConFiltroSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SGD_OEM_OEMPRESASConsultaConFiltroSIT]
	
	
	@SGD_OEM_CODIGO numeric(8,0),
	@SGD_OEM_OEMPRESA varchar(150),
	@SGD_OEM_NIT varchar(30)

AS
/********************************************************************************* 
 Nombre:		SGD_OEM_OEMPRESASConsultaConFiltro
 Propósito:		Obtiene datos de contacto Empresas
 Tablas:		SGD_OEM_OEMPRESAS
 Autor:			Oscar Malagón y Carlos Campos	
 Fecha:			11/04/2013
 Modifica:		Fabian Losada 08/08/2019
 Resultado(s):	Datos de contacto de tipo Empresa
 **********************************************************************************/
 
DECLARE @SQL VARCHAR(MAX);
DECLARE @CON INT;

BEGIN
	SET NOCOUNT ON;
	SET @CON=0;
	SET @SQL='Select SGD_OEM_CODIGO,
                        TDID_CODI,
                        SGD_OEM_OEMPRESA,
                        SGD_OEM_REP_LEGAL,
                        SGD_OEM_NIT,
                        SGD_OEM_SIGLA,
                        MUNI_CODI,
                        DPTO_CODI,
                        ID_PAIS,
                        ID_CONT,
                        SGD_OEM_DIRECCION,
                        SGD_OEM_TELEFONO,
                        EMAIL
	from dbo.SGD_OEM_OEMPRESAS 
	where 1=1';
	
	if( @SGD_OEM_CODIGO > 0 )
	begin
		set @SQL=@SQL+' AND SGD_OEM_CODIGO='''+CAST(@SGD_OEM_CODIGO as NVARCHAR)+'''';
		set @CON=1;
	end
	if( LEN(@SGD_OEM_OEMPRESA) > 0 )
	begin
		set @SQL=@SQL+' AND SGD_OEM_OEMPRESA like ''%'+CAST(@SGD_OEM_OEMPRESA as NVARCHAR)+'%''';
		set @CON=1;
	end
	if( LEN(@SGD_OEM_NIT) > 0 )
	begin
		set @SQL=@SQL+' AND SGD_OEM_NIT like ''%'+CAST(@SGD_OEM_NIT as NVARCHAR)+'%''';
		set @CON=1;
	end
	if( @CON = 0 )
	begin
		set @SQL=@SQL+' AND 1<>1';
	end

	DECLARE @Resultado TABLE
	(
		SGD_OEM_CODIGO		numeric(8, 0),
		TDID_CODI			numeric(2, 0),
		SGD_OEM_OEMPRESA	varchar(150),
		SGD_OEM_REP_LEGAL	varchar(150),
		SGD_OEM_NIT			varchar(30),
		SGD_OEM_SIGLA		varchar(50),
		MUNI_CODI			int,
		DPTO_CODI			int,
		ID_PAIS				smallint,
		ID_CONT				tinyint,
		SGD_OEM_DIRECCION	varchar(100),
		SGD_OEM_TELEFONO	varchar(50),
		EMAIL				varchar(100)
	)

	INSERT INTO @Resultado 
	exec(@SQL)

	select
		SGD_OEM_CODIGO,	
		TDID_CODI,		
		SGD_OEM_OEMPRESA,
		SGD_OEM_REP_LEGAL,
		SGD_OEM_NIT,	
		SGD_OEM_SIGLA,	
		MUNI_CODI,		
		DPTO_CODI,		
		ID_PAIS,			
		ID_CONT,		
		SGD_OEM_DIRECCION,
		SGD_OEM_TELEFONO,
		EMAIL,			
		null as SGD_OEM_CODPOSTAL
	FROM @Resultado

END;
GO
/****** Object:  StoredProcedure [dbo].[SGD_RDF_RETDOCFConsultaConFiltroSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SGD_RDF_RETDOCFConsultaConFiltroSIT]
		@RADI_NUME_RADI numeric(18, 0),
		@DEPE_CODI int 

AS
/********************************************************************************* 
 Nombre:		SGD_RDF_RETDOCFConsultaConFiltroSIT
 Propósito:		Obtiene datos de la tipificacion de un radicado especifico
 Tablas:		SGD_RDF_RETDOCF
 Autor:			Andrés Ochoa
 Fecha:			27/07/2017
 Modifica:		Fabian Losada 08/08/2019
 Resultado(s):	Datos de tipificacion de un radicado especifico
 **********************************************************************************/
 
DECLARE @SQL VARCHAR(MAX);

BEGIN
	SET NOCOUNT ON;
	SET @SQL = 'SELECT  
					SGD_MRD_CODIGO,
					RADI_NUME_RADI,
					DEPE_CODI,
					USUA_CODI,
					USUA_DOC,
					SGD_RDF_FECH
			FROM SGD_RDF_RETDOCF
				where 	1=1 ' ;

	if( @RADI_NUME_RADI  > 0 )
	begin
		set @SQL=@SQL+' AND RADI_NUME_RADI='+CAST(@RADI_NUME_RADI as NVARCHAR);
	end
	if( @DEPE_CODI  > 0 )
	begin
		set @SQL=@SQL+' AND DEPE_CODI='+CAST(@DEPE_CODI as NVARCHAR);
	end
	DECLARE @Resultado TABLE (
		SGD_MRD_CODIGO SMALLINT,
		RADI_NUME_RADI NUMERIC(15,0),
		DEPE_CODI INT,
		USUA_CODI NUMERIC(10,0),
		USUA_DOC VARCHAR(14),
		SGD_RDF_FECH DATETIME
	);


	INSERT INTO @Resultado
	exec(@SQL);

	SELECT  SGD_MRD_CODIGO,
			RADI_NUME_RADI,
			DEPE_CODI,
			USUA_CODI,
			USUA_DOC,
			SGD_RDF_FECH,
			null as xx 
	FROM @Resultado

END;
GO
/****** Object:  StoredProcedure [dbo].[SGD_SBRD_SUBSERIERDConsultaConFiltroSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SGD_SBRD_SUBSERIERDConsultaConFiltroSIT]
	
	@SGD_SRD_CODIGO smallint,
	@SGD_SBRD_CODIGO smallint

AS
/********************************************************************************* 
 Nombre:		SGD_SBRD_SUBSERIERDConsultaConFiltro
 Propósito:		Obtiene datos de SubSeries
 Tablas:		SGD_SBRD_SUBSERIERD
 Autor:			Andrés Ochoa
 Fecha:			27/07/2017
 Modifica:		Fabian Losada 08/08/2019
 Resultado(s):	Datos de Subseries
 **********************************************************************************/
 
DECLARE @SQL VARCHAR(MAX);

BEGIN
	SET NOCOUNT ON;
	SET @SQL = 'SELECT  
					SGD_SRD_CODIGO,
					SGD_SBRD_CODIGO,
					SGD_SBRD_DESCRIP,
					SGD_SBRD_FECHINI,
					SGD_SBRD_FECHFIN,
					SGD_SBRD_TIEMAG,
					SGD_SBRD_TIEMAC,
					SGD_SBRD_DISPFIN,
					SGD_SBRD_SOPORTE,
					SGD_SBRD_PROCEDI
			FROM SGD_SBRD_SUBSERIERD
				where 	1=1';
	
	if( @SGD_SRD_CODIGO > 0 )
	begin
		set @SQL=@SQL+' AND SGD_SRD_CODIGO='+CAST(@SGD_SRD_CODIGO as NVARCHAR);
	end
	if( @SGD_SBRD_CODIGO > 0 )
	begin
		set @SQL=@SQL+' AND SGD_SBRD_CODIGO ='+CAST(@SGD_SBRD_CODIGO as NVARCHAR);
	end

	DECLARE @Resultado TABLE 
	(
		SGD_SRD_CODIGO smallint,
		SGD_SBRD_CODIGO smallint,
		SGD_SBRD_DESCRIP varchar(200),
		SGD_SBRD_FECHINI datetime,
		SGD_SBRD_FECHFIN datetime,
		SGD_SBRD_TIEMAG smallint,
		SGD_SBRD_TIEMAC smallint,
		SGD_SBRD_DISPFIN varchar(50),
		SGD_SBRD_SOPORTE varchar(50),
		SGD_SBRD_PROCEDI varchar(400)
	)

	INSERT INTO @Resultado
	exec(@SQL);

	select  SGD_SRD_CODIGO,
			SGD_SBRD_CODIGO,
			SGD_SBRD_DESCRIP,
			SGD_SBRD_FECHINI,
			SGD_SBRD_FECHFIN,
			SGD_SBRD_TIEMAG,
			SGD_SBRD_TIEMAC,
			SGD_SBRD_DISPFIN,
			SGD_SBRD_SOPORTE,
			SGD_SBRD_PROCEDI
	from  @Resultado

END;
GO
/****** Object:  StoredProcedure [dbo].[SGD_SEXP_SECEXPEDIENTESConsultaConFiltroSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SGD_SEXP_SECEXPEDIENTESConsultaConFiltroSIT]
	
	
	@sgd_exp_numero varchar(25)

AS
/********************************************************************************* 
 Nombre:		SGD_SEXP_SECEXPEDIENTESConsultaConFiltroSIT
 Propósito:		Obtiene datos de EXPEDIENTES
 Tablas:		SGD_SEXP_SECEXPEDIENTES
 Autor:			Oscar Malagón 
 Fecha Modifica:20/06/2017
 Modifica:      Alexander Hernandez Maturana
 Resultado(s):	Datos de EXPEDIENTES
 **********************************************************************************/
 
DECLARE @SQL VARCHAR(MAX);
DECLARE @CON INT;
DECLARE @Resultado TABLE 
	( sgd_exp_numero varchar(25),
			sgd_srd_codigo smallint,
			sgd_sbrd_codigo smallint,
			sgd_sexp_secuencia int,
			depe_codi int,
			usua_doc varchar(15),
			sgd_sexp_fech datetime,
			sgd_sexp_ano smallint,
			usua_doc_responsable varchar(18),
			SGD_SEXP_PAREXP1 varchar(600),
			SGD_SEXP_PAREXP2 varchar(160),
			SGD_SEXP_PRIVADO int,
			SGD_SEXP_ESTADO bit,
			EXP_SECT_ID int,
			SGD_APLI_CODIGO int
)

BEGIN
	SET NOCOUNT ON;
	SET @CON=0;
	SET @SQL = 'SELECT sgd_exp_numero,
			sgd_srd_codigo,
			sgd_sbrd_codigo,
			sgd_sexp_secuencia,
			depe_codi,
			usua_doc,
			sgd_sexp_fech,
			sgd_sexp_ano,
			usua_doc_responsable,
			SGD_SEXP_PAREXP1,
			SGD_SEXP_PAREXP2,
			SGD_EXP_PRIVADO as SGD_SEXP_PRIVADO,
			SGD_SEXP_ESTADO,
			sgd_sexp_id as EXP_SECT_ID,
			null as SGD_APLI_CODIGO
	FROM [dbo].[SGD_SEXP_SECEXPEDIENTES]
				where 	1=1';
	
	if( LEN(@sgd_exp_numero) > 0 )
	begin
		set @SQL=@SQL+' AND sgd_exp_numero='''+CAST(@sgd_exp_numero as NVARCHAR)+'''';
		set @CON=1;
	end
	if( @CON = 0 )
	begin
		set @SQL=@SQL+' AND 1<>1';
	end
	--exec(@SQL);


	INSERT INTO @Resultado
	exec(@SQL)

	SELECT sgd_exp_numero,
			sgd_srd_codigo,
			sgd_sbrd_codigo,
			sgd_sexp_secuencia,
			depe_codi,
			usua_doc,
			sgd_sexp_fech,
			sgd_sexp_ano,
			usua_doc_responsable,
			SGD_SEXP_PAREXP1,
			SGD_SEXP_PAREXP2,
			SGD_SEXP_PRIVADO,
			SGD_SEXP_ESTADO,
			EXP_SECT_ID,
			SGD_APLI_CODIGO
	FROM @Resultado

END;
GO
/****** Object:  StoredProcedure [dbo].[SGD_SEXP_SECEXPEDIENTESInsertarSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SGD_SEXP_SECEXPEDIENTESInsertarSIT]
(
	@sgd_exp_numero varchar(25),
	@sgd_srd_codigo smallint,
	@sgd_sbrd_codigo smallint,
	@sgd_sexp_secuencia int,
	@depe_codi int,
	@usua_doc varchar(15),
	@sgd_sexp_fech datetime,
	@sgd_sexp_ano smallint,
	@usua_doc_responsable varchar(18),
	@SGD_SEXP_PAREXP1 varchar(600),
	@SGD_SEXP_PAREXP2 varchar(160),
	@SGD_SEXP_PRIVADO int,
	@SGD_SEXP_ESTADO bit,
	@EXP_SECT_ID int,
	@SGD_APLI_CODIGO int
)
AS
BEGIN

/********************************************************************************* 
 Nombre	: SGD_SEXP_SECEXPEDIENTESInsertarSIT
																					
 Proposito: inserta los datos en la tabla de Ciudadano dbo.SGD_CIU_CIUDADANO  
																					
 Tablas	:	dbo.SGD_SEXP_SECEXPEDIENTES
																					
 Autor	 :  Fabian Losada	Fecha: 2019/08/08

 Modifica:  
 */


	INSERT INTO [dbo].[SGD_SEXP_SECEXPEDIENTES]
           (sgd_exp_numero,
			sgd_srd_codigo,
			sgd_sbrd_codigo,
			sgd_sexp_secuencia,
			depe_codi,
			usua_doc,
			sgd_sexp_fech,
			sgd_sexp_ano,
			usua_doc_responsable,
			SGD_SEXP_PAREXP1,
			SGD_SEXP_PAREXP2,
			sgd_exp_privado,
			SGD_SEXP_ESTADO
		   )
		 VALUES
		(	
			@sgd_exp_numero,
			@sgd_srd_codigo,
			@sgd_sbrd_codigo,
			@sgd_sexp_secuencia,
			@depe_codi,
			@usua_doc,
			@sgd_sexp_fech,
			@sgd_sexp_ano,
			@usua_doc_responsable,
			@SGD_SEXP_PAREXP1,
			@SGD_SEXP_PAREXP2,
			@SGD_SEXP_PRIVADO,
			@SGD_SEXP_ESTADO
		)

	SELECT sgd_exp_numero,
			sgd_srd_codigo,
			sgd_sbrd_codigo,
			sgd_sexp_secuencia,
			depe_codi,
			usua_doc,
			sgd_sexp_fech,
			sgd_sexp_ano,
			usua_doc_responsable,
			SGD_SEXP_PAREXP1,
			SGD_SEXP_PAREXP2,
			sgd_exp_privado as SGD_SEXP_PRIVADO,
			SGD_SEXP_ESTADO,
			sgd_sexp_id as EXP_SECT_ID,
			null as SGD_APLI_CODIGO,
			SGD_FEXP_CODIGO,
			SGD_PEXP_CODIGO,
			SGD_SEXP_PAREXP3,
			SGD_SEXP_PAREXP4,
			SGD_SEXP_PAREXP5,
			null as SGD_SEXP_NOMBRE,
			null as SGD_EPRY_CODIGO,
			null as SEXP_BPIN,
			null as SGD_EMP_ID,
			null as SGD_SEXP_TIPO_ORDEN
	FROM [dbo].[SGD_SEXP_SECEXPEDIENTES]
	WHERE sgd_exp_numero  = @sgd_exp_numero
END
GO
/****** Object:  StoredProcedure [dbo].[SGD_SRD_SERIESRDConsultaConFiltroSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SGD_SRD_SERIESRDConsultaConFiltroSIT]
	
	  @SGD_SRD_CODIGO smallint

AS
/********************************************************************************* 
 Nombre:		SGD_SRD_SERIESRDConsultaConFiltroSIT
 Propósito:		Obtiene datos de Series documentales
 Tablas:		SGD_SRD_SERIESRD
 Autor:			Andres Ochoa
 Fecha:			27/07/2017
 Modifica:		Fabian Losada 08/08/2019
 Resultado(s):	Datos de Series Documentales
 **********************************************************************************/
 
DECLARE @SQL VARCHAR(MAX);

BEGIN
	SET NOCOUNT ON;
	SET @SQL = 'SELECT  
					SGD_SRD_CODIGO,
					SGD_SRD_DESCRIP,
					SGD_SRD_FECHINI,
					SGD_SRD_FECHFIN
			FROM SGD_SRD_SERIESRD
				where 	1=1';
	
	if( @SGD_SRD_CODIGO > 0 )
	begin
		set @SQL=@SQL+' AND SGD_SRD_CODIGO='+CAST(@SGD_SRD_CODIGO as NVARCHAR);
	end
	
	declare @Resultado TABLE (
		SGD_SRD_CODIGO smallint,
		SGD_SRD_DESCRIP nvarchar(120),
		SGD_SRD_FECHINI datetime,
		SGD_SRD_FECHFIN datetime
	);

	INSERT INTO @Resultado
	exec(@SQL);

	SELECT  SGD_SRD_CODIGO,
			SGD_SRD_DESCRIP,
			SGD_SRD_FECHINI,
			SGD_SRD_FECHFIN
	FROM @Resultado

END;
GO
/****** Object:  StoredProcedure [dbo].[SGD_TPR_TPDCUMENTOConsultaConFiltroSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SGD_TPR_TPDCUMENTOConsultaConFiltroSIT]
	
	@SGD_TPR_CODIGO smallint,
	@TIPORAD smallint

AS
/********************************************************************************* 
 Nombre:		SGD_TPR_TPDCUMENTOConsultaConFiltroSIT
 Propósito:		Obtiene datos de Tipos Documentales 
 Tablas:		SGD_TPR_TPDCUMENTO
 Autor:			Andrés Ochoa
 Fecha:			27/07/2017
 Modifica:		Fabian Losada 08/08/2019
 Resultado(s):	Datos de Tipos Documentales 
 **********************************************************************************/
 
DECLARE @SQL VARCHAR(MAX);

BEGIN
	SET NOCOUNT ON;
	SET @SQL = 'SELECT  
					SGD_TPR_CODIGO,
					SGD_TPR_DESCRIP,
					SGD_TPR_TERMINO,
					SGD_TPR_NUMERA,
					SGD_TPR_RADICA,
					SGD_TPR_TP1,
					SGD_TPR_TP2,
					SGD_TPR_TP3,
					SGD_TPR_TP5,
					SGD_TPR_TP9,
					SGD_TPR_ESTADO,
					SGD_TERMINO_REAL,
					SGD_TPR_TP4,
					SGD_TPR_TP6,
					SGD_TPR_TP7,
					SGD_TPR_TP8,
					SGD_TPR_ALERTA,
					SGD_PQR_DESCRIP,
					SGD_PQR_LABEL
					,cast(0 as tinyint) as REQUIERE_RESP 
			FROM SGD_TPR_TPDCUMENTO
				where 	1=1';
	
	if( @SGD_TPR_CODIGO > 0 )
	begin
		set @SQL=@SQL+' AND SGD_TPR_CODIGO ='+CAST(@SGD_TPR_CODIGO as NVARCHAR);
	end
	
	if(@TIPORAD >0 AND @TIPORAD <10)
	begin
		set @SQL=@SQL+' AND SGD_TPR_TP'+CAST(@TIPORAD as NVARCHAR)+' = 1';
	end
	
	DECLARE @Resultado TABLE (
		SGD_TPR_CODIGO SMALLINT,
		SGD_TPR_DESCRIP VARCHAR(150),
		SGD_TPR_TERMINO NUMERIC(4,0),
		SGD_TPR_NUMERA CHAR(1),
		SGD_TPR_RADICA CHAR(1),
		SGD_TPR_TP1 NUMERIC(18,0),
		SGD_TPR_TP2 NUMERIC(18,0),
		SGD_TPR_TP3 NUMERIC(1,0),
		SGD_TPR_TP5 NUMERIC(1,0),
		SGD_TPR_TP9 NUMERIC(1,0),
		SGD_TPR_ESTADO INT,
		SGD_TERMINO_REAL SMALLINT,
		SGD_TPR_TP4 TINYINT,
		SGD_TPR_TP6 TINYINT,
		SGD_TPR_TP7 TINYINT,
		SGD_TPR_TP8 TINYINT,
		SGD_TPR_ALERTA NUMERIC(18,0),
		SGD_PQR_DESCRIP VARCHAR(500)
		,REQUIERE_RESP tinyint
	);

	INSERT INTO @Resultado
	exec(@SQL);

	SELECT  SGD_TPR_CODIGO,
			SGD_TPR_DESCRIP,
			SGD_TPR_TERMINO,
			SGD_TPR_NUMERA,
			SGD_TPR_RADICA,
			SGD_TPR_TP1,
			SGD_TPR_TP2,
			SGD_TPR_TP3,
			SGD_TPR_TP5,
			SGD_TPR_TP9,
			SGD_TPR_ESTADO,
			SGD_TERMINO_REAL,
			SGD_TPR_TP4,
			SGD_TPR_TP6,
			SGD_TPR_TP7,
			SGD_TPR_TP8,
			SGD_TPR_ALERTA,
			0 as SGD_TPR_NOTIFICA,
			null as SGD_TPR_REPORT1,
			SGD_TPR_CODIGO as ID,
			null as SGD_TPR_EMAIL,
			SGD_PQR_DESCRIP,
			null as SGD_PQR_LABEL,
			null as CATEGORIZAR
			,REQUIERE_RESP as SGD_TPR_REQRESP
			,REQUIERE_RESP
	FROM @Resultado

END;



--exec [SGD_TPR_TPDCUMENTOConsultaConFiltroSIT] @SGD_TPR_CODIGO=884, @TIPORAD=2
GO
/****** Object:  StoredProcedure [dbo].[TRDConsultaConFiltroSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[TRDConsultaConFiltroSIT]
	
	@descripcion varchar(MAX),
	@codDepENDencia int,
	@codSerie smallint,
	@codSubSerie smallint,
	@codTipoDocumental smallint
	
AS
/********************************************************************************* 
 Nombre:		TRDConsultaConFiltroSIT
 Propósito:		Obtiene datos de TRD
 Tablas:		
 Autor:			Andrés Ochoa
 Fecha:			28/07/2017
 ModIFica:		Fabian Losada 08/08/2019
 Resultado(s):	Datos de división politica
 **********************************************************************************/
 
DECLARE @SQLS VARCHAR(MAX), @w VARCHAR(MAX) 

BEGIN

	SET NOCOUNT ON;

	---Filtro para continentes
	SET @w='';

	IF(  @codDepENDencia > 0 )
	BEGIN
		SET @w=' AND mat.DEPE_CODI='+CAST(@codDepENDencia as NVARCHAR);
	END
	IF(LEN(@descripcion)>0)
	BEGIN
		SET @w=@w+' AND (ser.SGD_SRD_DESCRIP LIKE ''%'+@descripcion+'%'' OR sbser.SGD_SBRD_DESCRIP LIKE ''%'+@descripcion+'%'' OR tpdoc.SGD_TPR_DESCRIP LIKE ''%'+@descripcion+'%'') ';
	END
	IF(  @codSerie > 0 )
	BEGIN
		SET @w=@w+' AND ser.SGD_SRD_CODIGO='+CAST(@codSerie as NVARCHAR);
	END
	IF(  @codSubSerie > 0 )
	BEGIN
		SET @w=@w+' AND sbser.SGD_SBRD_CODIGO='+CAST(@codSubSerie as NVARCHAR);
	END
	IF(  @codTipoDocumental > 0 )
	BEGIN
		SET @w=@w+' AND tpdoc.SGD_TPR_CODIGO='+CAST(@codTipoDocumental as NVARCHAR);
	END

	SET @SQLS = ' SELECT DISTINCT MAT.DEPE_CODI AS CODDEPENDENCIA, 
						SER.SGD_SRD_CODIGO AS CODSERIE, 
						SER.SGD_SRD_DESCRIP AS NOMBRESERIE,
						SBSER.SGD_SBRD_CODIGO AS CODSUBSERIE,
						SBSER.SGD_SBRD_DESCRIP AS NOMBRESUBSERIE,
						TPDOC.SGD_TPR_CODIGO AS CODTIPODOCUMENTAL, 
						TPDOC.SGD_TPR_DESCRIP AS NOMBRETIPODOCUMENTAL
					FROM SGD_MRD_MATRIRD MAT
					JOIN SGD_SRD_SERIESRD SER ON SER.SGD_SRD_CODIGO=MAT.SGD_SRD_CODIGO
					JOIN SGD_SBRD_SUBSERIERD SBSER ON SBSER.SGD_SBRD_CODIGO= MAT.SGD_SBRD_CODIGO AND SBSER.SGD_SRD_CODIGO= SER.SGD_SRD_CODIGO
					JOIN SGD_TPR_TPDCUMENTO TPDOC ON TPDOC.SGD_TPR_CODIGO=MAT.SGD_TPR_CODIGO
					WHERE 
					MAT.SGD_MRD_ESTA = 1 '+@w;
				
	DECLARE @Resultado TABLE (
		CODDEPENDENCIA INT,
		CODSERIE INT,
		NOMBRESERIE VARCHAR(255),
		CODSUBSERIE INT,
		NOMBRESUBSERIE VARCHAR(255),
		CODTIPODOCUMENTAL INT,
		NOMBRETIPODOCUMENTAL VARCHAR(255)
	);
	INSERT INTO @Resultado
	EXEC(@SQLS);

	SELECT  CODDEPENDENCIA,
			CODSERIE,
			NOMBRESERIE,
			CODSUBSERIE,
			NOMBRESUBSERIE,
			CODTIPODOCUMENTAL,
			NOMBRETIPODOCUMENTAL
	FROM @Resultado
END;
GO
/****** Object:  StoredProcedure [dbo].[USUARIOContactoConsultarConFiltroSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[USUARIOContactoConsultarConFiltroSIT]
	
	
	@CORREO varchar(50),
	@NOMBRE varchar(45),
	@DEPENDENCIA_NOMBRE varchar(70),
	@DOCUMENTO varchar(14),
	@LOGIN varchar(24)

AS
/********************************************************************************* 
 Nombre:		USUARIOContactoConsultarConFiltro
 Propósito:		Obtiene datos de usuario
 Tablas:		Usuario
 Autor:			Oscar Malagón y Carlos Campos		
 Fecha:			10/04/2013
 Modifica:		Fabian Losada 08/08/2019
 Resultado(s):	Datos basicos de Usuario como Contacto
 **********************************************************************************/
 
DECLARE @SQL VARCHAR(MAX);
DECLARE @CON INT;
BEGIN
	SET NOCOUNT ON;
	SET @CON=0;
	SET @SQL='Select U.USUA_CODI,
           R.DEPE_CODI,
           USUA_LOGIN,
           USUA_ESTA,
           USUA_NOMB,
           USUA_DOC,
           USUA_EMAIL,
		   D.DEPE_NOMB,
		   D.ID_CONT,
		   D.ID_PAIS,
		   D.DPTO_CODI,
		   D.MUNI_CODI,
		   NULL as DEP_CODPOSTAL
	from dbo.USUARIO U, SGD_URD_USUAROLDEP R
	join dbo.DEPENDENCIA  D ON D.DEPE_CODI=R.DEPE_CODI
	where R.USUA_CODI=U.USUA_CODI';
	
	if( LEN(@LOGIN) > 0 )
	begin
		set @SQL=@SQL+' AND USUA_LOGIN = '''+CAST(@LOGIN as NVARCHAR)+'''';
		set @CON=1;
	end
	if( LEN(@DOCUMENTO) > 0 )
	begin
		set @SQL=@SQL+' AND USUA_DOC like ''%'+CAST(@DOCUMENTO as NVARCHAR)+'%''';
		set @CON=1;
	end
	if( LEN(@CORREO) > 0 )
	begin
		set @SQL=@SQL+' AND USUA_EMAIL like ''%'+CAST(@CORREO as NVARCHAR)+'%''';
		set @CON=1;
	end
	if( LEN(@NOMBRE) > 0 )
	begin
		set @SQL=@SQL+' AND USUA_NOMB like ''%'+CAST(@NOMBRE as NVARCHAR)+'%''';
		set @CON=1;
	end
	if( LEN(@DEPENDENCIA_NOMBRE) > 0 )
	begin
		set @SQL=@SQL+' AND D.DEPE_NOMB like ''%'+CAST(@DEPENDENCIA_NOMBRE as NVARCHAR)+'%''';
		set @CON=1;
	end
	if( @CON = 0 )
	begin
		set @SQL=@SQL+' AND 1<>1';
	end

	DECLARE @Resultado TABLE 
	(
		USUA_CODI   INT,
		DEPE_CODI   INT,
		USUA_LOGIN  VARCHAR(24),
		USUA_ESTA   VARCHAR(10),
		USUA_NOMB   VARCHAR(45),		
		USUA_DOC    VARCHAR(14),
		USUA_EMAIL  VARCHAR(100),
		DEPE_NOMB   varchar(70),
		ID_CONT		tinyint,
		ID_PAIS     smallint,
		DPTO_CODI	INT,
		MUNI_CODI	INT,
		DEP_CODPOSTAL varchar(8)
	)
	INSERT INTO @Resultado
	exec(@SQL)

	SELECT USUA_CODI,   
		   DEPE_CODI,   
		   USUA_LOGIN,  
		   USUA_ESTA,  
		   USUA_NOMB,   		   
		   USUA_DOC,    
		   USUA_EMAIL,  
		   DEPE_NOMB,   
		   ID_CONT,		
		   ID_PAIS,     
		   DPTO_CODI,	
		   MUNI_CODI,	
		   DEP_CODPOSTAL
		   FROM @Resultado
END;
GO
/****** Object:  StoredProcedure [dbo].[USUARIODestinoConsultarConFiltroSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[USUARIODestinoConsultarConFiltroSIT]
	
	
	@USUA_LOGIN varchar(24),
	@USUA_DOC varchar(14)

AS
/********************************************************************************* 
 Nombre:		USUARIODestinoConsultarConFiltro
 Propósito:		Obtiene datos de usuario
 Tablas:		Usuario
 Autor:			Oscar Malagón y Carlos Campos		
 Fecha:			10/04/2013
 Modifica:		Fabian Losada 08/08/2019
 Resultado(s):	Datos basicos de Usuario destino de radicacion
 **********************************************************************************/
 
DECLARE @SQL VARCHAR(MAX);
DECLARE @CON INT;
BEGIN
	SET NOCOUNT ON;
	SET @CON=0;
	SET @SQL='Select u.USUA_CODI,
           r.DEPE_CODI,
		   (select m.sgd_drm_valor from sgd_drm_dep_mod_rol m where m.sgd_drm_depecodi=r.depe_codi and m.sgd_drm_rolcodi=r.rol_codi and m.sgd_drm_modecodi=52) as PERM_RADI,
           u.USUA_LOGIN,
           u.USUA_ESTA,
           u.USUA_NOMB,
           u.USUA_DOC,
           u.USUA_EMAIL,
		   (select m.sgd_drm_valor from sgd_drm_dep_mod_rol m where m.sgd_drm_depecodi=r.depe_codi and m.sgd_drm_rolcodi=r.rol_codi and m.sgd_drm_modecodi=40)  as CODI_NIVEL
	from dbo.USUARIO u, SGD_URD_USUAROLDEP R
	where R.USUA_CODI=U.USUA_CODI';
	
	if( LEN(@USUA_LOGIN) > 0 )
	begin
		set @SQL=@SQL+' AND USUA_LOGIN='''+CAST(@USUA_LOGIN as NVARCHAR)+'''';
		set @CON=1;
	end
	if( LEN(@USUA_DOC) > 0 )
	begin
		set @SQL=@SQL+' AND USUA_DOC='''+CAST(@USUA_DOC as NVARCHAR)+'''';
		set @CON=1;
	end
	if( @CON = 0 )
	begin
		set @SQL=@SQL+' AND 1<>1';
	end
	DECLARE @Resultado TABLE 
	(
		USUA_CODI  INT,
		DEPE_CODI  INT,
		PERM_RADI  VARCHAR(24),
		USUA_LOGIN VARCHAR(24),
		USUA_ESTA  VARCHAR(10),
		USUA_NOMB  VARCHAR(45),
		USUA_DOC   VARCHAR(14),
		USUA_EMAIL VARCHAR(100),
		CODI_NIVEL TINYINT
	)
	INSERT INTO @Resultado
	exec(@SQL)

	SELECT USUA_CODI, 
		   DEPE_CODI, 
		   USUA_LOGIN,
		   PERM_RADI,
		   USUA_ESTA, 
		   USUA_NOMB, 
		   USUA_DOC,  
		   USUA_EMAIL,
		   CODI_NIVEL
		   FROM @Resultado	

END;
GO
/****** Object:  StoredProcedure [dbo].[usuarioM]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usuarioM]
AS
BEGIN
	-- Declare the return variable here
	DECLARE @USUA_CODI	int,	 --Código del usuario en la dependencia nueva
			@USUA_CODI_OLD	int,	 --Código del usuario en la dependencia nueva
			@USUA_FECH_CREA	datetime, --Fecha del evento
			@DEPE_CODI	int,	-- Código de la Dependencia actual
			@USUA_LOGIN	varchar(50),	
			@USUA_DOC		varchar(14),	-- Número del documento del usuario
			@USUA_PASW		varchar(50),	-- password
			@USUA_ESTA		varchar(1),			-- Dependencia del usuario que realiza transacción.
			@USUA_NOMB		varchar(100),			-- Consecutivo del usuario que realiza transacción.
			@USUA_NUEVO			varchar(1),
			@USUA_SESION	varchar(100),
			@USUA_EMAIL		varchar(100),
			@USUA_AUTH_LDAP	int,
			@ID_GRUPO	int,
			@JEFE_GRUPO	int,
			@USUA_POLITICA int
	--SET @USUA_CODI = NEXT VALUE FOR [dbo].[sec_usua]; 
    -- Insert statements for procedure here
	DECLARE usua CURSOR FOR SELECT [USUA_CODI]
      ,[DEPE_CODI]
      ,[USUA_LOGIN]
      ,[USUA_FECH_CREA]
      ,[USUA_PASW]
      ,[USUA_ESTA]
      ,[USUA_NOMB]
      ,[USUA_NUEVO]
      ,[USUA_DOC]
      ,[USUA_SESION]
      ,[USUA_EMAIL]
      ,[USUA_AUTH_LDAP]
      ,[ID_GRUPO]
      ,[JEFE_GRUPO]
      ,[ID_POLITICA_FIRMA]
  FROM [bdorfeo].[dbo].[USUARIO];
  OPEN usua;
  WHILE (1=1)
  begin;
  FETCH NEXT
			   FROM usua
			   INTO @USUA_CODI_OLD, @DEPE_CODI, @USUA_LOGIN, @USUA_FECH_CREA,@USUA_PASW,@USUA_ESTA,@USUA_NOMB,@USUA_NUEVO,@USUA_DOC
      ,@USUA_SESION,@USUA_EMAIL,@USUA_AUTH_LDAP,@ID_GRUPO,@JEFE_GRUPO,@USUA_POLITICA;
	  IF @@FETCH_STATUS < 0 BREAK;
	  SET @USUA_CODI = NEXT VALUE FOR [dbo].[sec_usua]; 
	insert into usua_migracion VALUES(@USUA_CODI,@USUA_CODI_OLD,@DEPE_CODI,@USUA_FECH_CREA,@USUA_PASW,@USUA_ESTA,@USUA_NOMB COLLATE SQL_Latin1_General_CP1_CI_AI
	,@USUA_NUEVO,@USUA_DOC,@USUA_SESION,@USUA_EMAIL,@USUA_AUTH_LDAP,@ID_GRUPO,@JEFE_GRUPO,@USUA_POLITICA,@USUA_LOGIN) ;
 
	insert into PRU_GdOrfeo.dbo.usuario values(@USUA_CODI,@USUA_LOGIN,@USUA_FECH_CREA,@USUA_PASW,@USUA_ESTA,@USUA_NOMB COLLATE SQL_Latin1_General_CP1_CI_AI
	,@USUA_NUEVO,@USUA_DOC,@USUA_SESION,NULL,NULL,NULL,@USUA_EMAIL,@USUA_AUTH_LDAP,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,@USUA_POLITICA
	,NULL,NULL,NULL,@ID_GRUPO);
  end;
  close usua;
END
GO
/****** Object:  StoredProcedure [dbo].[USUARIORadicaConsultarConFiltroSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[USUARIORadicaConsultarConFiltroSIT]
	
	
	@USUA_LOGIN varchar(24),
	@USUA_DOC varchar(14)

AS
/********************************************************************************* 
 Nombre:		USUARIORadicaConsultarConFiltro
 Propósito:		Obtiene datos de usuario
 Tablas:		Usuario
 Autor:			Oscar Malagón y Carlos Campos			
 Fecha:			10/04/2013
 Modifica:		Fabian Losada 08/08/2019
 Resultado(s):	Datos basicos de Usuario que genera la radicacion
 **********************************************************************************/
 
DECLARE @SQL VARCHAR(MAX);
DECLARE @CON INT;
BEGIN
	SET NOCOUNT ON;
	SET @CON=0;
	SET @SQL='Select u.USUA_CODI,
					 r.DEPE_CODI,
                     u.USUA_LOGIN,
                     u.USUA_ESTA,
                     u.USUA_NOMB,
                     (select m.sgd_drm_valor from sgd_drm_dep_mod_rol m where m.sgd_drm_depecodi=r.depe_codi and m.sgd_drm_rolcodi=r.rol_codi and m.sgd_drm_modecodi=52) as PERM_RADI,
                     u.USUA_DOC,
                     u.USUA_EMAIL                     
	from dbo.USUARIO u, SGD_URD_USUAROLDEP R
	where R.USUA_CODI=U.USUA_CODI';
	
	if( LEN(@USUA_LOGIN) > 0 )
	begin
		set @SQL=@SQL+' AND USUA_LOGIN='''+CAST(@USUA_LOGIN as NVARCHAR)+'''';
		set @CON=1;
	end
	/*if( LEN(@USUA_DOC) > 0 )
	begin
		set @SQL=@SQL+' AND USUA_DOC='''+CAST(@USUA_DOC as NVARCHAR)+'''';
		
	end*/
	if( @CON = 0 )
	begin
		set @SQL=@SQL+' AND 1<>1';
	end
	DECLARE @Resultado TABLE 
	(
		USUA_CODI  INT,
		DEPE_CODI  INT,
		USUA_LOGIN VARCHAR(24),
		USUA_ESTA  VARCHAR(10),
		USUA_NOMB  VARCHAR(45),
		PERM_RADI  CHAR(1),
		USUA_DOC   VARCHAR(14),
		USUA_EMAIL VARCHAR(100)
	)
	INSERT INTO @Resultado
	exec(@SQL)

	SELECT USUA_CODI, 
		   DEPE_CODI, 
		   USUA_LOGIN,
		   USUA_ESTA, 
		   USUA_NOMB, 
		   PERM_RADI, 
		   USUA_DOC,  
		   USUA_EMAIL
		   FROM @Resultado

END;
GO
/****** Object:  StoredProcedure [dbo].[USUARIOTXConsultarConFiltroSIT]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[USUARIOTXConsultarConFiltroSIT]
	
	
	@USUA_LOGIN varchar(24),
	@USUA_DOC varchar(14)

AS
/********************************************************************************* 
 Nombre:		USUARIOTXConsultarConFiltro
 Propósito:		Obtiene datos de usuario
 Tablas:		Usuario
 Autor:			Oscar Malagón y Carlos Campos		
 Fecha:			22/04/2013
 Modifica:		Fabian Losada 08/08/2019
 Resultado(s):	Datos basicos de Usuario destino de radicacion
 **********************************************************************************/
 
DECLARE @SQL VARCHAR(MAX);
DECLARE @CON INT;
BEGIN
	SET NOCOUNT ON;
	SET @CON=0;
	SET @SQL='Select 
           convert(int,r.DEPE_CODI) as codigo_dependencia,
           u.USUA_LOGIN as login,
           u.USUA_NOMB as nombre,
           u.USUA_DOC as documento,
           u.USUA_EMAIL as correo
	from dbo.USUARIO  u, SGD_URD_USUAROLDEP R
	where R.USUA_CODI=U.USUA_CODI';
	
	if( LEN(@USUA_LOGIN) > 0 )
	begin
		set @SQL=@SQL+' AND u.USUA_LOGIN='''+CAST(@USUA_LOGIN as NVARCHAR)+'''';
		set @CON=1;
	end
	if( LEN(@USUA_DOC) > 0 )
	begin
		set @SQL=@SQL+' AND u.USUA_DOC='''+CAST(@USUA_DOC as NVARCHAR)+'''';
		set @CON=1;
	end
	if( @CON = 0 )
	begin
		set @SQL=@SQL+' AND 1<>1';
	end
	DECLARE @Resultado TABLE 
	(
		codigo_dependencia INT,
		login			   VARCHAR(24),
		nombre             VARCHAR(45),
		documento          VARCHAR(14),
		correo             VARCHAR(100)

	)
	INSERT INTO @Resultado
	exec(@SQL)

	SELECT codigo_dependencia, 
		   login,			   
		   nombre,             
		   documento,          
		   correo
		   FROM @Resultado
		                
END;
GO
/****** Object:  StoredProcedure [dbo].[versionamientoM]    Script Date: 5/09/2019 2:12:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[versionamientoM]
AS
BEGIN
	-- Declare the return variable here
	DECLARE @srd	int,	 --Código del usuario 
			@sbrd	int,	-- Código de la Dependencia actual
			@ver	int,	--secuencia informados
			@verd	int
    -- Insert statements for procedure here
	--ingreso data series 
	insert into sgd_vtrd_verstrd (sgd_srd_codigo,sgd_vtrd_crea,sgd_vtrd_modifica)
select sgd_srd_codigo, current_timestamp,current_timestamp from sgd_srd_seriesrd;
--actualizacion version de la serie
	DECLARE ver_srd CURSOR FOR select sgd_vtrd_codigo,sgd_srd_codigo,sgd_sbrd_codigo
 from sgd_vtrd_verstrd;
  OPEN ver_srd;
  WHILE (1=1)
  begin;
  FETCH NEXT
			   FROM ver_srd
			   INTO @ver, @srd, @sbrd;
	  IF @@FETCH_STATUS < 0 BREAK;
	update sgd_srd_seriesrd set sgd_srd_version=@ver where sgd_srd_codigo=@srd;
	update sgd_sbrd_subserierd set sgd_srd_version=@ver where sgd_srd_codigo=@srd;
	update sgd_sexp_secexpedientes set sgd_srd_version=@ver where sgd_srd_codigo=@srd;
	update sgd_mrd_matrird set sgd_srd_version=@ver where sgd_srd_codigo=@srd;
  end;
  close ver_srd;
  --ingreso data subserie
  insert into sgd_vtrd_verstrd (sgd_srd_codigo,sgd_sbrd_codigo,sgd_vtrd_crea,sgd_vtrd_modifica)
select sgd_srd_codigo, sgd_sbrd_codigo,current_timestamp,current_timestamp from sgd_sbrd_subserierd;
--actualizacion version de la subserie
	DECLARE ver_sbrd CURSOR FOR select sgd_vtrd_codigo,sgd_srd_codigo,sgd_sbrd_codigo
 from sgd_vtrd_verstrd where sgd_sbrd_codigo is not null;
  OPEN ver_sbrd;
  WHILE (1=1)
  begin;
  FETCH NEXT
			   FROM ver_sbrd
			   INTO @ver, @srd, @sbrd;
	  IF @@FETCH_STATUS < 0 BREAK;
	update sgd_sbrd_subserierd set sgd_sbrd_version=@ver where sgd_srd_codigo=@srd and sgd_sbrd_codigo=@sbrd;
	update sgd_sexp_secexpedientes set sgd_sbrd_version=@ver where sgd_sbrd_codigo=@sbrd;
	update sgd_mrd_matrird set sgd_sbrd_version=@ver where sgd_srd_codigo=@srd and sgd_sbrd_codigo=@sbrd;
  end;
  close ver_sbrd;
  
END
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de radicado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'anex_radi_nume'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de identificacion del anexo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'anex_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tipo de anexo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'anex_tipo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tamano del archivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'anex_tamano'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'si el anexo es solo lectura' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'anex_solo_lect'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'usuario creador' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'anex_creador'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion del anexo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'anex_desc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo del anexo basado en el radicado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'anex_numero'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre del archivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'anex_nomb_archivo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'bandera que indica si el anexo esta o no borrado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'anex_borrado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'si se va a convertir en una respuesta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'anex_salida'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de radicado respuesta  asignado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'radi_nume_salida'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de radicacion del anexo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'anex_radi_fech'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'estado del anexo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'anex_estado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'documento del usuario creador' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'usua_doc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'si toma datos del padre' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'sgd_rem_destino'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de envio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'anex_fech_envio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tipo de remitente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'sgd_dir_tipo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia del usuario creador' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'anex_depe_creador'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'identificacion del remitente o destinatario radicado padre' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'sgd_doc_padre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tipo documental' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'sgd_tpr_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo causal devolucion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'sgd_deve_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de devolucion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'sgd_deve_fech'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de marcado como impreso' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'sgd_fech_impres'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de creacion del anexo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'anex_fech_anex'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'dependencia del radicado origen' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'anex_depe_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de radicado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'sgd_dnufe_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'documento usuario creador' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'anex_usudoc_creador'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tipo de radicado asignado al anexo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'sgd_trad_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'municipio del destinatario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'muni_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'departamento del destinatario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'dpto_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de expediente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'sgd_exp_numero'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'cantidad de folios digitalizados' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'anex_folios_dig'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'bandera de recepcion del documento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos', @level2type=N'COLUMN',@level2name=N'anex_recib'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'identificador' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos_tipo', @level2type=N'COLUMN',@level2name=N'anex_tipo_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'extension' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos_tipo', @level2type=N'COLUMN',@level2name=N'anex_tipo_ext'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion tipo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'anexos_tipo', @level2type=N'COLUMN',@level2name=N'anex_tipo_desc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo Edificio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_edi_edificio', @level2type=N'COLUMN',@level2name=N'arch_edi_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre de edificio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_edi_edificio', @level2type=N'COLUMN',@level2name=N'arch_edi_nombre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sigla edificio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_edi_edificio', @level2type=N'COLUMN',@level2name=N'arch_edi_sigla'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de continente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_edi_edificio', @level2type=N'COLUMN',@level2name=N'codi_cont'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo pais' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_edi_edificio', @level2type=N'COLUMN',@level2name=N'codi_pais'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo departamento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_edi_edificio', @level2type=N'COLUMN',@level2name=N'codi_dpto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo Municipio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_edi_edificio', @level2type=N'COLUMN',@level2name=N'codi_muni'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_eit_items', @level2type=N'COLUMN',@level2name=N'arch_eit_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo del padre' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_eit_items', @level2type=N'COLUMN',@level2name=N'arch_eit_cod_padre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_eit_items', @level2type=N'COLUMN',@level2name=N'arch_eit_nombre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sigla' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_eit_items', @level2type=N'COLUMN',@level2name=N'arch_eit_sigla'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo edificio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_eit_items', @level2type=N'COLUMN',@level2name=N'arch_eit_codiedi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_evd_edivsdep', @level2type=N'COLUMN',@level2name=N'arch_evd_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo edificio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_evd_edivsdep', @level2type=N'COLUMN',@level2name=N'arch_evd_edificio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_evd_edivsdep', @level2type=N'COLUMN',@level2name=N'arch_evd_depe'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_evd_edivsdep', @level2type=N'COLUMN',@level2name=N'arch_evd_item'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo area' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_evd_edivsdep', @level2type=N'COLUMN',@level2name=N'arch_evd_area'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo modulo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_evd_edivsdep', @level2type=N'COLUMN',@level2name=N'arch_evd_modulo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo piso' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_evd_edivsdep', @level2type=N'COLUMN',@level2name=N'arch_evd_piso'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_mda_motdesarch', @level2type=N'COLUMN',@level2name=N'arch_mda_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_mda_motdesarch', @level2type=N'COLUMN',@level2name=N'arch_mda_nombre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'id del item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_tpa_tpalmacenaje', @level2type=N'COLUMN',@level2name=N'arch_tpa_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre del item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_tpa_tpalmacenaje', @level2type=N'COLUMN',@level2name=N'arch_tpa_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripcion del item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_tpa_tpalmacenaje', @level2type=N'COLUMN',@level2name=N'arch_tpa_desc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Guarda el tamaño de la caja que ocupa en el estante' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_tpa_tpalmacenaje', @level2type=N'COLUMN',@level2name=N'arch_tpa_tamano'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tipo de almacenaje' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_tpa_tpalmacenaje', @level2type=N'COLUMN',@level2name=N'arch_tpa_tipo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_uexp_ubicacionexp', @level2type=N'COLUMN',@level2name=N'arch_uexp_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'expediente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_uexp_ubicacionexp', @level2type=N'COLUMN',@level2name=N'sgd_exp_numero'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ubicacion fisica' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_uexp_ubicacionexp', @level2type=N'COLUMN',@level2name=N'arch_uexp_ubica'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'caja' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_uexp_ubicacionexp', @level2type=N'COLUMN',@level2name=N'arch_uexp_tpcaja'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'carpeta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_uexp_ubicacionexp', @level2type=N'COLUMN',@level2name=N'arch_uexp_tpcarpeta'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'edificio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_uexp_ubicacionexp', @level2type=N'COLUMN',@level2name=N'arch_uexp_edificio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'piso' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_uexp_ubicacionexp', @level2type=N'COLUMN',@level2name=N'arch_uexp_psio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'area' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_uexp_ubicacionexp', @level2type=N'COLUMN',@level2name=N'arch_uexp_area'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'modulo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_uexp_ubicacionexp', @level2type=N'COLUMN',@level2name=N'arch_uexp_modulo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'estante' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_uexp_ubicacionexp', @level2type=N'COLUMN',@level2name=N'arch_uexp_estante'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'entrepaño' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_uexp_ubicacionexp', @level2type=N'COLUMN',@level2name=N'arch_uexp_entrepano'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'	codificacion caja' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_uexp_ubicacionexp', @level2type=N'COLUMN',@level2name=N'arch_uexp_codcaja'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de referencia externo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'arch_uexp_ubicacionexp', @level2type=N'COLUMN',@level2name=N'arch_uexp_nref'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bodega_empresas', @level2type=N'COLUMN',@level2name=N'nombre_de_la_empresa'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero unico de identificacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bodega_empresas', @level2type=N'COLUMN',@level2name=N'nuir'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bodega_empresas', @level2type=N'COLUMN',@level2name=N'nit_de_la_empresa'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'sigla' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bodega_empresas', @level2type=N'COLUMN',@level2name=N'sigla_de_la_empresa'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'direccion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bodega_empresas', @level2type=N'COLUMN',@level2name=N'direccion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo departamento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bodega_empresas', @level2type=N'COLUMN',@level2name=N'codigo_del_departamento'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo municipio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bodega_empresas', @level2type=N'COLUMN',@level2name=N'codigo_del_municipio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'telefono 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bodega_empresas', @level2type=N'COLUMN',@level2name=N'telefono_1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'telefono 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bodega_empresas', @level2type=N'COLUMN',@level2name=N'telefono_2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'email' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bodega_empresas', @level2type=N'COLUMN',@level2name=N'email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'representante legal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bodega_empresas', @level2type=N'COLUMN',@level2name=N'nombre_rep_legal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'cargo representante legal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bodega_empresas', @level2type=N'COLUMN',@level2name=N'cargo_rep_legal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo empresa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bodega_empresas', @level2type=N'COLUMN',@level2name=N'identificador_empresa'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo esp' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bodega_empresas', @level2type=N'COLUMN',@level2name=N'are_esp_secue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'continente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bodega_empresas', @level2type=N'COLUMN',@level2name=N'id_cont'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'pais' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bodega_empresas', @level2type=N'COLUMN',@level2name=N'id_pais'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'activo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bodega_empresas', @level2type=N'COLUMN',@level2name=N'activa'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'localidad' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bodega_empresas', @level2type=N'COLUMN',@level2name=N'sgd_dir_localidad'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'carpeta', @level2type=N'COLUMN',@level2name=N'carp_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion carpeta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'carpeta', @level2type=N'COLUMN',@level2name=N'carp_desc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'estado de la carpeta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'carpeta', @level2type=N'COLUMN',@level2name=N'carp_estado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'carpeta_per', @level2type=N'COLUMN',@level2name=N'usua_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'dependencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'carpeta_per', @level2type=N'COLUMN',@level2name=N'depe_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre de la carpeta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'carpeta_per', @level2type=N'COLUMN',@level2name=N'nomb_carp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion carpeta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'carpeta_per', @level2type=N'COLUMN',@level2name=N'desc_carp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo carpeta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'carpeta_per', @level2type=N'COLUMN',@level2name=N'codi_carp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'rol' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'carpeta_per', @level2type=N'COLUMN',@level2name=N'id_rol'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'secuencia carpetas' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'carpeta_per', @level2type=N'COLUMN',@level2name=N'id_carp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'centro_poblado', @level2type=N'COLUMN',@level2name=N'cpob_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo municipio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'centro_poblado', @level2type=N'COLUMN',@level2name=N'muni_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo departamento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'centro_poblado', @level2type=N'COLUMN',@level2name=N'dpto_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre centro poblado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'centro_poblado', @level2type=N'COLUMN',@level2name=N'cpob_nomb'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre anterior' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'centro_poblado', @level2type=N'COLUMN',@level2name=N'cpob_nomb_anterior'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'departamento', @level2type=N'COLUMN',@level2name=N'dpto_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre departamento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'departamento', @level2type=N'COLUMN',@level2name=N'dpto_nomb'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo continente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'departamento', @level2type=N'COLUMN',@level2name=N'id_cont'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo pais' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'departamento', @level2type=N'COLUMN',@level2name=N'id_pais'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia', @level2type=N'COLUMN',@level2name=N'depe_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre dependencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia', @level2type=N'COLUMN',@level2name=N'depe_nomb'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo departamento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia', @level2type=N'COLUMN',@level2name=N'dpto_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia padre' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia', @level2type=N'COLUMN',@level2name=N'depe_codi_padre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo municipio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia', @level2type=N'COLUMN',@level2name=N'muni_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia territorial' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia', @level2type=N'COLUMN',@level2name=N'depe_codi_territorial'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'sigla dependencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia', @level2type=N'COLUMN',@level2name=N'dep_sigla'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'bandera que indica si se trata de una dependencia  del nivel entral' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia', @level2type=N'COLUMN',@level2name=N'dep_central'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'direccion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia', @level2type=N'COLUMN',@level2name=N'dep_direccion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numeracion interna' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia', @level2type=N'COLUMN',@level2name=N'depe_num_interna'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numeracion de resoluciones' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia', @level2type=N'COLUMN',@level2name=N'depe_num_resolucion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia radicadora de salida' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia', @level2type=N'COLUMN',@level2name=N'depe_rad_tp1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia radicadora de entrada' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia', @level2type=N'COLUMN',@level2name=N'depe_rad_tp2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia radicadora internos' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia', @level2type=N'COLUMN',@level2name=N'depe_rad_tp3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo continente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia', @level2type=N'COLUMN',@level2name=N'id_cont'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo pais' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia', @level2type=N'COLUMN',@level2name=N'id_pais'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'bandera que indica el estado de la dependencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia', @level2type=N'COLUMN',@level2name=N'depe_estado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'habilitado para radicacion tipo 8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia', @level2type=N'COLUMN',@level2name=N'depe_rad_tp8'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia radicacion tipo 4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia', @level2type=N'COLUMN',@level2name=N'depe_rad_tp4'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia radicacion tipo 5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia', @level2type=N'COLUMN',@level2name=N'depe_rad_tp5'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia radicacion tipo 6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia', @level2type=N'COLUMN',@level2name=N'depe_rad_tp6'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia radicacion tipo 7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia', @level2type=N'COLUMN',@level2name=N'depe_rad_tp7'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia radicacion tipo 9' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia', @level2type=N'COLUMN',@level2name=N'depe_rad_tp9'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia grupo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia', @level2type=N'COLUMN',@level2name=N'depe_grupo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia_visibilidad', @level2type=N'COLUMN',@level2name=N'codigo_visibilidad'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'dependencia visible' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia_visibilidad', @level2type=N'COLUMN',@level2name=N'dependencia_visible'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'dependencia que observa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dependencia_visibilidad', @level2type=N'COLUMN',@level2name=N'dependencia_observa'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'estado', @level2type=N'COLUMN',@level2name=N'esta_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'estado', @level2type=N'COLUMN',@level2name=N'esta_desc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'INDICA EL ESTADO DE FIRMA DEL RADICADO PARA EL USUARIO: 1-SOLICITUD; 2-FIRMADO; 3-RECHAZADO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'firma_radicados', @level2type=N'COLUMN',@level2name=N'estado_firma'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia origen' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hist_eventos', @level2type=N'COLUMN',@level2name=N'depe_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha transaccion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hist_eventos', @level2type=N'COLUMN',@level2name=N'hist_fech'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo usuario origen' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hist_eventos', @level2type=N'COLUMN',@level2name=N'usua_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de radicado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hist_eventos', @level2type=N'COLUMN',@level2name=N'radi_nume_radi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'observacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hist_eventos', @level2type=N'COLUMN',@level2name=N'hist_obse'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo usuario destino' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hist_eventos', @level2type=N'COLUMN',@level2name=N'usua_codi_dest'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'documento usuario origen' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hist_eventos', @level2type=N'COLUMN',@level2name=N'usua_doc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'documento usuario origen antiguo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hist_eventos', @level2type=N'COLUMN',@level2name=N'usua_doc_old'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de transaccion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hist_eventos', @level2type=N'COLUMN',@level2name=N'sgd_ttr_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'usuario creador' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hist_eventos', @level2type=N'COLUMN',@level2name=N'hist_usua_autor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'documento usuario destino' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hist_eventos', @level2type=N'COLUMN',@level2name=N'hist_doc_dest'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia destino' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hist_eventos', @level2type=N'COLUMN',@level2name=N'depe_codi_dest'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo rol' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hist_eventos', @level2type=N'COLUMN',@level2name=N'id_rol'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo rol destino' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hist_eventos', @level2type=N'COLUMN',@level2name=N'id_rol_dest'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de radicado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'informados', @level2type=N'COLUMN',@level2name=N'radi_nume_radi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'informados', @level2type=N'COLUMN',@level2name=N'usua_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'informados', @level2type=N'COLUMN',@level2name=N'depe_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'informados', @level2type=N'COLUMN',@level2name=N'info_desc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha informado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'informados', @level2type=N'COLUMN',@level2name=N'info_fech'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fue leido' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'informados', @level2type=N'COLUMN',@level2name=N'info_leido'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo usuario informado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'informados', @level2type=N'COLUMN',@level2name=N'usua_codi_info'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'documento usuario informado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'informados', @level2type=N'COLUMN',@level2name=N'info_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'documento usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'informados', @level2type=N'COLUMN',@level2name=N'usua_doc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo rol' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'informados', @level2type=N'COLUMN',@level2name=N'id_rol'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'id de tabla' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mas_csv_base', @level2type=N'COLUMN',@level2name=N'mas_csv_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre de la lista' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mas_csv_base', @level2type=N'COLUMN',@level2name=N'mas_csv_nombre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'estado del listado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mas_csv_base', @level2type=N'COLUMN',@level2name=N'mas_csv_estado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'dependencia creadora' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mas_csv_base', @level2type=N'COLUMN',@level2name=N'mas_csv_depe'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'campos extras para la combinacion del documento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mas_csv_base', @level2type=N'COLUMN',@level2name=N'mas_csv_extra'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'id de tabl' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mas_dat_csvdata', @level2type=N'COLUMN',@level2name=N'mas_dat_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'id de mas_csv_base' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mas_dat_csvdata', @level2type=N'COLUMN',@level2name=N'mas_dat_idcsv'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tipo de remitente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mas_dat_csvdata', @level2type=N'COLUMN',@level2name=N'mas_dat_tprem'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo del remitente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mas_dat_csvdata', @level2type=N'COLUMN',@level2name=N'mas_dat_codrem'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'campos extras de documento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mas_dat_csvdata', @level2type=N'COLUMN',@level2name=N'mas_dat_extras'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de expediente no obligatorio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mas_dat_csvdata', @level2type=N'COLUMN',@level2name=N'mas_dat_expediente'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'medio_recepcion', @level2type=N'COLUMN',@level2name=N'mrec_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'medio_recepcion', @level2type=N'COLUMN',@level2name=N'mrec_desc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo municipio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'municipio', @level2type=N'COLUMN',@level2name=N'muni_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo departamento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'municipio', @level2type=N'COLUMN',@level2name=N'dpto_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'municipio', @level2type=N'COLUMN',@level2name=N'muni_nomb'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo continente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'municipio', @level2type=N'COLUMN',@level2name=N'id_cont'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo pais' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'municipio', @level2type=N'COLUMN',@level2name=N'id_pais'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre municipio homologa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'municipio', @level2type=N'COLUMN',@level2name=N'homologa_muni'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo municipio homologa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'municipio', @level2type=N'COLUMN',@level2name=N'homologa_idmuni'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'bandera estado ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'municipio', @level2type=N'COLUMN',@level2name=N'activa'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ruta del archivo correspondiente al Header ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pdf_plantilla', @level2type=N'COLUMN',@level2name=N'pdf_head'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Rutan del archivo correspondiente al footer' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pdf_plantilla', @level2type=N'COLUMN',@level2name=N'pdf_footer'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Informacion Basica del destinatario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pdf_plantilla', @level2type=N'COLUMN',@level2name=N'pdf_remitente'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Datos basicos del destinatario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pdf_plantilla', @level2type=N'COLUMN',@level2name=N'pdf_destinatario'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Registrar el texto principal del documento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pdf_plantilla', @level2type=N'COLUMN',@level2name=N'pdf_texto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'estado de la plantilla 0 inactiva 1 activa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pdf_plantilla', @level2type=N'COLUMN',@level2name=N'pdf_estado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codiggo de la regional que creo la plantilla' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pdf_plantilla', @level2type=N'COLUMN',@level2name=N'pdf_regional'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'contiene el texto de ayuda descripcion de la plantilla' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pdf_plantilla', @level2type=N'COLUMN',@level2name=N'pdf_desc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo del Tipo de Radicacion al cual aplica la plantilla' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pdf_plantilla', @level2type=N'COLUMN',@level2name=N'pdf_trad_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'permite limitar la aplicacion de la plantilla a una sola dependencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pdf_plantilla', @level2type=N'COLUMN',@level2name=N'pdf_depe_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ruta del archivo que representativo de la plantilla' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pdf_plantilla', @level2type=N'COLUMN',@level2name=N'pdf_logo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera que indica si la plantilla s eaplicara 0 Institucionalmente 1 Regionalmente 2 Dependencia creadora' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pdf_plantilla', @level2type=N'COLUMN',@level2name=N'pdf_alcance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_tipo_plt', @level2type=N'COLUMN',@level2name=N'plt_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_tipo_plt', @level2type=N'COLUMN',@level2name=N'plt_desc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pla_entrega', @level2type=N'COLUMN',@level2name=N'pla_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numeor radicado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pla_entrega', @level2type=N'COLUMN',@level2name=N'radi_nume_radi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia generadora' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pla_entrega', @level2type=N'COLUMN',@level2name=N'depe_codi_genera'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha planilla entrega' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pla_entrega', @level2type=N'COLUMN',@level2name=N'pla_entrega_fech_crea'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo usuario creador' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pla_entrega', @level2type=N'COLUMN',@level2name=N'usua_codi_crea'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo rol' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pla_entrega', @level2type=N'COLUMN',@level2name=N'id_rol'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'estado entrega' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pla_entrega', @level2type=N'COLUMN',@level2name=N'pla_entrega_estado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de entrega' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pla_entrega', @level2type=N'COLUMN',@level2name=N'pla_entrega_desc_fecha'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'año de entrega' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pla_entrega', @level2type=N'COLUMN',@level2name=N'pla_entrega_ano'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'secuencia planilla' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pla_entrega', @level2type=N'COLUMN',@level2name=N'pla_codigo_secuencia'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pla_entrega_radi', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo planilla' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pla_entrega_radi', @level2type=N'COLUMN',@level2name=N'pla_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de radicado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pla_entrega_radi', @level2type=N'COLUMN',@level2name=N'radi_nume_radi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia entrega' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pla_entrega_radi', @level2type=N'COLUMN',@level2name=N'depe_codi_entrega'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'estado de la planilla' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pla_entrega_radi', @level2type=N'COLUMN',@level2name=N'pla_radi_estado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo usuario referencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pla_entrega_radi', @level2type=N'COLUMN',@level2name=N'usua_codi_ref'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de solicitud' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pla_entrega_radi', @level2type=N'COLUMN',@level2name=N'pla_fech_sol'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'bandera qeu indica si fue solicitada' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pla_entrega_radi', @level2type=N'COLUMN',@level2name=N'pla_solicitado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'copia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pla_entrega_radi', @level2type=N'COLUMN',@level2name=N'sgd_dir_tipo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'planillas', @level2type=N'COLUMN',@level2name=N'pla_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'usuario creador' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'planillas', @level2type=N'COLUMN',@level2name=N'usu_cre_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia creadora' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'planillas', @level2type=N'COLUMN',@level2name=N'dep_cre_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de creacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'planillas', @level2type=N'COLUMN',@level2name=N'pla_fec_creacion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'estado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'planillas', @level2type=N'COLUMN',@level2name=N'est_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'plantilla_pl', @level2type=N'COLUMN',@level2name=N'pl_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'plantilla_pl', @level2type=N'COLUMN',@level2name=N'depe_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre plantilla' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'plantilla_pl', @level2type=N'COLUMN',@level2name=N'pl_nomb'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre archivo planilla' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'plantilla_pl', @level2type=N'COLUMN',@level2name=N'pl_archivo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ddescripcion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'plantilla_pl', @level2type=N'COLUMN',@level2name=N'pl_desc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de cargue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'plantilla_pl', @level2type=N'COLUMN',@level2name=N'pl_fech'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'plantilla_pl', @level2type=N'COLUMN',@level2name=N'usua_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'estado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'plantilla_pl', @level2type=N'COLUMN',@level2name=N'pl_uso'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'prestamo', @level2type=N'COLUMN',@level2name=N'pres_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero radicado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'prestamo', @level2type=N'COLUMN',@level2name=N'radi_nume_radi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'usuario actual' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'prestamo', @level2type=N'COLUMN',@level2name=N'usua_login_actu'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'prestamo', @level2type=N'COLUMN',@level2name=N'depe_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'usuario prestamo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'prestamo', @level2type=N'COLUMN',@level2name=N'usua_login_pres'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion prestamo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'prestamo', @level2type=N'COLUMN',@level2name=N'pres_desc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha prestamo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'prestamo', @level2type=N'COLUMN',@level2name=N'pres_fech_pres'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha devolucion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'prestamo', @level2type=N'COLUMN',@level2name=N'pres_fech_devo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha solicitud' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'prestamo', @level2type=N'COLUMN',@level2name=N'pres_fech_pedi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'estado prestamo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'prestamo', @level2type=N'COLUMN',@level2name=N'pres_estado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'bandera indica si es un requerimiento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'prestamo', @level2type=N'COLUMN',@level2name=N'pres_requerimiento'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia archiva' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'prestamo', @level2type=N'COLUMN',@level2name=N'pres_depe_arch'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha vencimiento prestamo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'prestamo', @level2type=N'COLUMN',@level2name=N'pres_fech_venc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha vencimiento prestamo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'prestamo', @level2type=N'COLUMN',@level2name=N'dev_desc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion devolucion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'prestamo', @level2type=N'COLUMN',@level2name=N'pres_fech_canc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'usuario cancelacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'prestamo', @level2type=N'COLUMN',@level2name=N'usua_login_canc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'usuario recibe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'prestamo', @level2type=N'COLUMN',@level2name=N'usua_login_rx'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'usuario solicitud' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'prestamo', @level2type=N'COLUMN',@level2name=N'usua_login_auth'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ruta acta prestamo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'prestamo', @level2type=N'COLUMN',@level2name=N'pres_path_acta'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de acta de prestamo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'prestamo', @level2type=N'COLUMN',@level2name=N'pres_acta'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'remplaza acta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'prestamo', @level2type=N'COLUMN',@level2name=N'pres_anyo_acta'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo metadato' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radi_cam_metacampos', @level2type=N'COLUMN',@level2name=N'radi_data_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre metadato' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radi_cam_metacampos', @level2type=N'COLUMN',@level2name=N'radi_cam_nombre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tipo de metadato' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radi_cam_metacampos', @level2type=N'COLUMN',@level2name=N'radi_cam_tipo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'campo obligatorio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radi_cam_metacampos', @level2type=N'COLUMN',@level2name=N'radi_cam_oblig'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'habilitado en la busqueda' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radi_cam_metacampos', @level2type=N'COLUMN',@level2name=N'radi_cam_busq'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion del metadato' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radi_cam_metacampos', @level2type=N'COLUMN',@level2name=N'radi_cam_desc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'estado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radi_cam_metacampos', @level2type=N'COLUMN',@level2name=N'radi_cam_estado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radi_data_metainfo', @level2type=N'COLUMN',@level2name=N'radi_data_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre metadato' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radi_data_metainfo', @level2type=N'COLUMN',@level2name=N'radi_data_nomb'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'habilitado para busqueda' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radi_data_metainfo', @level2type=N'COLUMN',@level2name=N'radi_data_busq'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'estado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radi_data_metainfo', @level2type=N'COLUMN',@level2name=N'radi_data_estado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tipo de radicacion a la cual aplica' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radi_data_metainfo', @level2type=N'COLUMN',@level2name=N'sgd_trad_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radi_data_metainfo', @level2type=N'COLUMN',@level2name=N'depe_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de radicado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_nume_radi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de radicacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_fech_radi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo tipo documental' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'tdoc_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo tipo de remitente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'trte_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo medio de recepcion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'mrec_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo entidad oficial' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'eesp_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo otra entidad' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'eotra_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tipo empresa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_tipo_empr'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha oficio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_fech_ofic'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo tipo de identificacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'tdid_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'radicado de identificacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_nume_iden'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_nomb'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'primer apellido' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_prim_apel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'segundo apellido' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_segu_apel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'pais' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_pais'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo municipio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'muni_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo centro poblado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'cpob_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo carpeta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'carp_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo estado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'esta_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo departamento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'dpto_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo municipio central' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'cen_muni_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo departamento central' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'cen_dpto_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'direccion de correspondencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_dire_corr'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'telefono de contacto' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_tele_cont'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de hojas' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_nume_hoja'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion de anexos' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_desc_anex'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de radicado derivado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_nume_deri'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ruta del documento deifnitivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_path'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo usuario actual' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_usua_actu'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia actual' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_depe_actu'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de asignacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_fech_asig'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'asunto radicado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'ra_asun'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'usuario anterior' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_usu_ante'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia radicadora' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_depe_radi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'remitente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_rem'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo usuario radicador' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_usua_radi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nivel de seguridad' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'codi_nivel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'bandera de nivel' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'flag_nivel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo carpeta personal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'carp_per'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'radicado leido' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_leido'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'cuenta interna' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_cuentai'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tipo de asociacion del radicado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_tipo_deri'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'si o no' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'listo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo tema' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'sgd_tma_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo matriz' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'sgd_mtd_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo empresa de servicios publicos' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'par_serv_secue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de flujo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'sgd_fld_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'si esta agendado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_agend'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de agendado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_fech_agend'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha del documento radicado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_fech_doc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'secuencia del documento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'sgd_doc_secuencia'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo del proceso' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'sgd_pnufe_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'si esta anulado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'sgd_eanu_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de notificacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'sgd_not_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de notificacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_fech_notif'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo tipo descripcion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'sgd_tdec_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de aplicativo integrado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'sgd_apli_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de transaccion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'sgd_ttr_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'documento usuario anterior' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'usua_doc_ante'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha transaccion anterior' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_fech_antetx'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo tipo de radicado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'sgd_trad_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de vencimiento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'fech_vcmto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tipo documento de vencimiento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'tdoc_vcmto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'termino real' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'sgd_termino_real'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo continente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'id_cont'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 publico 1 privado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'sgd_spub_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo pais' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'id_pais'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'bandera  que indica que no requiere respuesta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_nrr'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'radicado documento anterior' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_usua_doc_actu'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo rol' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'id_rol'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia anterior' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'usua_depe_ante'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'rol usuario anterior' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'usua_rol_ante'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'rol del radicado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_rol_radi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'depe_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numero interno entidad' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_num_numinterno'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de folios' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_nume_folios'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de guia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_nume_guia'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de anexos' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_nume_anexo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de verificacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_codi_verifica'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'campo1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_campo1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'campo2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_campo2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'campo3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_campo3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'campo4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_campo4'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'campo5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_campo5'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'campo6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_campo6'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'campo7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_campo7'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de la planilla' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'cod_planilla'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo metadato relacionado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'radicado', @level2type=N'COLUMN',@level2name=N'radi_mdata_metadatos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de agendado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_agen_agendados', @level2type=N'COLUMN',@level2name=N'sgd_agen_fech'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'observacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_agen_agendados', @level2type=N'COLUMN',@level2name=N'sgd_agen_observacion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero radicado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_agen_agendados', @level2type=N'COLUMN',@level2name=N'radi_nume_radi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'documento usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_agen_agendados', @level2type=N'COLUMN',@level2name=N'usua_doc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_agen_agendados', @level2type=N'COLUMN',@level2name=N'depe_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_agen_agendados', @level2type=N'COLUMN',@level2name=N'sgd_agen_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de vencimiento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_agen_agendados', @level2type=N'COLUMN',@level2name=N'sgd_agen_fechplazo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'agendado activo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_agen_agendados', @level2type=N'COLUMN',@level2name=N'sgd_agen_activo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_anu_anulados', @level2type=N'COLUMN',@level2name=N'sgd_anu_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_anu_anulados', @level2type=N'COLUMN',@level2name=N'sgd_anu_desc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero radicado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_anu_anulados', @level2type=N'COLUMN',@level2name=N'radi_nume_radi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de anulacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_anu_anulados', @level2type=N'COLUMN',@level2name=N'sgd_eanu_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de solicitud de anulacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_anu_anulados', @level2type=N'COLUMN',@level2name=N'sgd_anu_sol_fech'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de anulacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_anu_anulados', @level2type=N'COLUMN',@level2name=N'sgd_anu_fech'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_anu_anulados', @level2type=N'COLUMN',@level2name=N'depe_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'documento usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_anu_anulados', @level2type=N'COLUMN',@level2name=N'usua_doc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_anu_anulados', @level2type=N'COLUMN',@level2name=N'usua_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de dependencia anuladora' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_anu_anulados', @level2type=N'COLUMN',@level2name=N'depe_codi_anu'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'documento usuario anulador' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_anu_anulados', @level2type=N'COLUMN',@level2name=N'usua_doc_anu'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo usuario anulador' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_anu_anulados', @level2type=N'COLUMN',@level2name=N'usua_codi_anu'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo usuario acta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_anu_anulados', @level2type=N'COLUMN',@level2name=N'usua_anu_acta'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ruta acta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_anu_anulados', @level2type=N'COLUMN',@level2name=N'sgd_anu_path_acta'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo tipo de radicado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_anu_anulados', @level2type=N'COLUMN',@level2name=N'sgd_trad_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_camexp_campoexpediente', @level2type=N'COLUMN',@level2name=N'sgd_camexp_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre campo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_camexp_campoexpediente', @level2type=N'COLUMN',@level2name=N'sgd_camexp_campo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo relacion expediente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_camexp_campoexpediente', @level2type=N'COLUMN',@level2name=N'sgd_parexp_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'llave foranea relacionada' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_camexp_campoexpediente', @level2type=N'COLUMN',@level2name=N'sgd_camexp_fk'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tabla relacionada' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_camexp_campoexpediente', @level2type=N'COLUMN',@level2name=N'sgd_camexp_tablafk'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'campo de la tabla' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_camexp_campoexpediente', @level2type=N'COLUMN',@level2name=N'sgd_camexp_campofk'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'valor del campo seleccionado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_camexp_campoexpediente', @level2type=N'COLUMN',@level2name=N'sgd_camexp_campovalor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tipo de orden' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_camexp_campoexpediente', @level2type=N'COLUMN',@level2name=N'sgd_campexp_orden'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de metadato' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_cexp_campdataexp', @level2type=N'COLUMN',@level2name=N'sgd_mexp_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo tags' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_cexp_campdataexp', @level2type=N'COLUMN',@level2name=N'sgd_cexp_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tipo de tag' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_cexp_campdataexp', @level2type=N'COLUMN',@level2name=N'sgd_cexp_tipo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre del tag' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_cexp_campdataexp', @level2type=N'COLUMN',@level2name=N'sgd_cexp_nombre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_cexp_campdataexp', @level2type=N'COLUMN',@level2name=N'sgd_cexp_desc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'estado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_cexp_campdataexp', @level2type=N'COLUMN',@level2name=N'sgd_cexp_estado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tag de busqueda' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_cexp_campdataexp', @level2type=N'COLUMN',@level2name=N'sgd_cexp_busq'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tag obligatorio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_cexp_campdataexp', @level2type=N'COLUMN',@level2name=N'sgd_cexp_oblig'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo tipo de identificacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_ciu_ciudadano', @level2type=N'COLUMN',@level2name=N'tdid_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_ciu_ciudadano', @level2type=N'COLUMN',@level2name=N'sgd_ciu_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombres' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_ciu_ciudadano', @level2type=N'COLUMN',@level2name=N'sgd_ciu_nombre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'direccion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_ciu_ciudadano', @level2type=N'COLUMN',@level2name=N'sgd_ciu_direccion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'primer apellido' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_ciu_ciudadano', @level2type=N'COLUMN',@level2name=N'sgd_ciu_apell1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'segundo apellido' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_ciu_ciudadano', @level2type=N'COLUMN',@level2name=N'sgd_ciu_apell2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'telefono' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_ciu_ciudadano', @level2type=N'COLUMN',@level2name=N'sgd_ciu_telefono'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'email' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_ciu_ciudadano', @level2type=N'COLUMN',@level2name=N'sgd_ciu_email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo municipio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_ciu_ciudadano', @level2type=N'COLUMN',@level2name=N'muni_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo depeartamento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_ciu_ciudadano', @level2type=N'COLUMN',@level2name=N'dpto_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numeor documento de identidad' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_ciu_ciudadano', @level2type=N'COLUMN',@level2name=N'sgd_ciu_cedula'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'id continente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_ciu_ciudadano', @level2type=N'COLUMN',@level2name=N'id_cont'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'id pais' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_ciu_ciudadano', @level2type=N'COLUMN',@level2name=N'id_pais'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'bandera que indica el estado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_ciu_ciudadano', @level2type=N'COLUMN',@level2name=N'sgd_ciu_estado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'id centro poblado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_ciu_ciudadano', @level2type=N'COLUMN',@level2name=N'id_centropoblado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'id persona' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_ciu_ciudadano', @level2type=N'COLUMN',@level2name=N'id_persona'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'id direccion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_ciu_ciudadano', @level2type=N'COLUMN',@level2name=N'id_direccion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo medio de envio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_clta_clstarif', @level2type=N'COLUMN',@level2name=N'sgd_fenv_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_clta_clstarif', @level2type=N'COLUMN',@level2name=N'sgd_clta_codser'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo tarifas de envio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_clta_clstarif', @level2type=N'COLUMN',@level2name=N'sgd_tar_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_clta_clstarif', @level2type=N'COLUMN',@level2name=N'sgd_clta_descrip'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'peso desde ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_clta_clstarif', @level2type=N'COLUMN',@level2name=N'sgd_clta_pesdes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'peso hasta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_clta_clstarif', @level2type=N'COLUMN',@level2name=N'sgd_clta_peshast'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_cob_campobliga', @level2type=N'COLUMN',@level2name=N'sgd_cob_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_cob_campobliga', @level2type=N'COLUMN',@level2name=N'sgd_cob_desc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'identificador tabla' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_cob_campobliga', @level2type=N'COLUMN',@level2name=N'sgd_cob_label'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo tipo identificador' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_cob_campobliga', @level2type=N'COLUMN',@level2name=N'sgd_tidm_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_def_continentes', @level2type=N'COLUMN',@level2name=N'id_cont'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_def_continentes', @level2type=N'COLUMN',@level2name=N'nombre_cont'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo pais' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_def_paises', @level2type=N'COLUMN',@level2name=N'id_pais'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo continente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_def_paises', @level2type=N'COLUMN',@level2name=N'id_cont'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre pais' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_def_paises', @level2type=N'COLUMN',@level2name=N'nombre_pais'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_deve_dev_envio', @level2type=N'COLUMN',@level2name=N'sgd_deve_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_dir_drecciones', @level2type=N'COLUMN',@level2name=N'sgd_dir_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tipo remitente 0 principal 7 otro' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_dir_drecciones', @level2type=N'COLUMN',@level2name=N'sgd_dir_tipo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo otras empresas' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_dir_drecciones', @level2type=N'COLUMN',@level2name=N'sgd_oem_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo ciudadanos' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_dir_drecciones', @level2type=N'COLUMN',@level2name=N'sgd_ciu_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de radicado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_dir_drecciones', @level2type=N'COLUMN',@level2name=N'radi_nume_radi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo empresas oficiales' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_dir_drecciones', @level2type=N'COLUMN',@level2name=N'sgd_esp_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo municipio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_dir_drecciones', @level2type=N'COLUMN',@level2name=N'muni_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo departamento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_dir_drecciones', @level2type=N'COLUMN',@level2name=N'dpto_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'direccion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_dir_drecciones', @level2type=N'COLUMN',@level2name=N'sgd_dir_direccion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'telefono' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_dir_drecciones', @level2type=N'COLUMN',@level2name=N'sgd_dir_telefono'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'email' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_dir_drecciones', @level2type=N'COLUMN',@level2name=N'sgd_dir_mail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo secuencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_dir_drecciones', @level2type=N'COLUMN',@level2name=N'sgd_sec_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de anexo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_dir_drecciones', @level2type=N'COLUMN',@level2name=N'sgd_anex_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'identificacion de funcionario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_dir_drecciones', @level2type=N'COLUMN',@level2name=N'sgd_doc_fun'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre del remitente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_dir_drecciones', @level2type=N'COLUMN',@level2name=N'sgd_dir_nomremdes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo TRD' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_dir_drecciones', @level2type=N'COLUMN',@level2name=N'sgd_trd_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo del tipo documental' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_dir_drecciones', @level2type=N'COLUMN',@level2name=N'sgd_dir_tdoc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de identificacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_dir_drecciones', @level2type=N'COLUMN',@level2name=N'sgd_dir_doc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo pais' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_dir_drecciones', @level2type=N'COLUMN',@level2name=N'id_pais'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo centro poblado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_dir_drecciones', @level2type=N'COLUMN',@level2name=N'sgd_dir_cpcodi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de copia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_dir_drecciones', @level2type=N'COLUMN',@level2name=N'radi_nume_copia'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia visualizadora' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_dpr_dep_per_rol', @level2type=N'COLUMN',@level2name=N'depe_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo rol' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_dpr_dep_per_rol', @level2type=N'COLUMN',@level2name=N'rol_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia visible' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_dpr_dep_per_rol', @level2type=N'COLUMN',@level2name=N'depe_codi_per'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_drm_dep_mod_rol', @level2type=N'COLUMN',@level2name=N'sgd_drm_depecodi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo rol' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_drm_dep_mod_rol', @level2type=N'COLUMN',@level2name=N'sgd_drm_rolcodi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo modulo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_drm_dep_mod_rol', @level2type=N'COLUMN',@level2name=N'sgd_drm_modecodi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'valor del permiso' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_drm_dep_mod_rol', @level2type=N'COLUMN',@level2name=N'sgd_drm_valor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_eanu_estanulacion', @level2type=N'COLUMN',@level2name=N'sgd_eanu_desc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_eanu_estanulacion', @level2type=N'COLUMN',@level2name=N'sgd_eanu_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_einv_inventario', @level2type=N'COLUMN',@level2name=N'sgd_einv_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'dependencia nombre' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_einv_inventario', @level2type=N'COLUMN',@level2name=N'sgd_depe_nomb'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_einv_inventario', @level2type=N'COLUMN',@level2name=N'sgd_depe_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de expediente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_einv_inventario', @level2type=N'COLUMN',@level2name=N'sgd_einv_expnum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'titulo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_einv_inventario', @level2type=N'COLUMN',@level2name=N'sgd_einv_titulo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'unidad documental' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_einv_inventario', @level2type=N'COLUMN',@level2name=N'sgd_einv_unidad'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha creacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_einv_inventario', @level2type=N'COLUMN',@level2name=N'sgd_einv_fech'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha fin' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_einv_inventario', @level2type=N'COLUMN',@level2name=N'sgd_einv_fechfin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero radicado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_einv_inventario', @level2type=N'COLUMN',@level2name=N'sgd_einv_radicados'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de folios' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_einv_inventario', @level2type=N'COLUMN',@level2name=N'sgd_einv_folios'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de documento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_einv_inventario', @level2type=N'COLUMN',@level2name=N'sgd_einv_nundocu'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de documento en bodega' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_einv_inventario', @level2type=N'COLUMN',@level2name=N'sgd_einv_nundocubodega'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de caja' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_einv_inventario', @level2type=N'COLUMN',@level2name=N'sgd_einv_caja'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de caja en bodega' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_einv_inventario', @level2type=N'COLUMN',@level2name=N'sgd_einv_cajabodega'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de serie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_einv_inventario', @level2type=N'COLUMN',@level2name=N'sgd_einv_srd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre de la serie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_einv_inventario', @level2type=N'COLUMN',@level2name=N'sgd_einv_nomsrd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de subserie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_einv_inventario', @level2type=N'COLUMN',@level2name=N'sgd_einv_sbrd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre de ls subserie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_einv_inventario', @level2type=N'COLUMN',@level2name=N'sgd_einv_nomsbrd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tiempo de retencion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_einv_inventario', @level2type=N'COLUMN',@level2name=N'sgd_einv_retencion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'destino final' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_einv_inventario', @level2type=N'COLUMN',@level2name=N'sgd_einv_disfinal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ubicacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_einv_inventario', @level2type=N'COLUMN',@level2name=N'sgd_einv_ubicacion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'observacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_einv_inventario', @level2type=N'COLUMN',@level2name=N'sgd_einv_observacion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_eit_items', @level2type=N'COLUMN',@level2name=N'sgd_eit_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo padre' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_eit_items', @level2type=N'COLUMN',@level2name=N'sgd_eit_cod_padre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre campo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_eit_items', @level2type=N'COLUMN',@level2name=N'sgd_eit_nombre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'sigla campo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_eit_items', @level2type=N'COLUMN',@level2name=N'sgd_eit_sigla'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo departamento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_eit_items', @level2type=N'COLUMN',@level2name=N'codi_dpto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo municipio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_eit_items', @level2type=N'COLUMN',@level2name=N'codi_muni'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo empresa de envios' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_enve_envioespecial', @level2type=N'COLUMN',@level2name=N'sgd_fenv_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'valor local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_enve_envioespecial', @level2type=N'COLUMN',@level2name=N'sgd_enve_valorl'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'valor nacional' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_enve_envioespecial', @level2type=N'COLUMN',@level2name=N'sgd_enve_valorn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion envio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_enve_envioespecial', @level2type=N'COLUMN',@level2name=N'sgd_enve_desc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero identificador del expediente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_numero'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero del radicado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'radi_nume_radi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de inclusion expediente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_fech'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de modificacion del expediente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_fech_mod'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'depe_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'usua_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'documento identificacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'usua_doc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'estado del expediente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_estado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'titulo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_titulo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'asunto' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_asunto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'carpeta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_carpeta'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'unidad fisica' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_ufisica'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'isla' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_isla'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'estante' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_estante'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'caja' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_caja'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de cierre' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_fech_arch'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo serie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_srd_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo subserie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_sbrd_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de flujo del expediente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_fexp_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'bandera que indica si fue o no archivado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_archivo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'bandera que indica si hace parte de la unidad documental' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_unicon'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha final' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_fechfin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de folios' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_folios'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'forman parte de la retencion documental' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_rete'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de entrepaño' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_entrepa'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'usuario que archiva' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'radi_usua_arch'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'identificador del edificio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_edificio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de caja en bodega' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_caja_bodega'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de carro' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_carro'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de carpeta en bodega' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_carpeta_bodega'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'bandera que indica si expediente es privado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_privado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'bandera que indica si posee CDs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_cd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de referencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_exp_expediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_nref'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_fenv_frmenvio', @level2type=N'COLUMN',@level2name=N'sgd_fenv_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_fenv_frmenvio', @level2type=N'COLUMN',@level2name=N'sgd_fenv_descrip'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'estado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_fenv_frmenvio', @level2type=N'COLUMN',@level2name=N'sgd_fenv_estado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'bandera que indica si genera planilla' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_fenv_frmenvio', @level2type=N'COLUMN',@level2name=N'sgd_fenv_planilla'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_fexp_flujoexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_fexp_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de proceso' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_fexp_flujoexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_pexp_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'orden' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_fexp_flujoexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_fexp_orden'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tiempo de vigencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_fexp_flujoexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_fexp_terminos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'imagen' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_fexp_flujoexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_fexp_imagen'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_fexp_flujoexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_fexp_descrip'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_fmenv_eventos', @level2type=N'COLUMN',@level2name=N'sgd_fmenv_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo menu de envios' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_fmenv_eventos', @level2type=N'COLUMN',@level2name=N'sgd_menv_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo empresa de envio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_fmenv_eventos', @level2type=N'COLUMN',@level2name=N'sgd_fenv_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'alias' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_fmenv_eventos', @level2type=N'COLUMN',@level2name=N'alias'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'estado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_fmenv_eventos', @level2type=N'COLUMN',@level2name=N'estado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'valor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_fmenv_eventos', @level2type=N'COLUMN',@level2name=N'valor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_iexp_metainfoexpediente', @level2type=N'COLUMN',@level2name=N'sgd_iexp_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de expediente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_iexp_metainfoexpediente', @level2type=N'COLUMN',@level2name=N'sgd_exp_numero'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo del metadato del expdiente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_iexp_metainfoexpediente', @level2type=N'COLUMN',@level2name=N'sgd_cexp_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'valor del metadato del expediente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_iexp_metainfoexpediente', @level2type=N'COLUMN',@level2name=N'sgd_iexp_valor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_masiva_excel', @level2type=N'COLUMN',@level2name=N'sgd_masiva_dependencia'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_masiva_excel', @level2type=N'COLUMN',@level2name=N'sgd_masiva_usuario'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo tipo radicado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_masiva_excel', @level2type=N'COLUMN',@level2name=N'sgd_masiva_tiporadicacion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo masiva' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_masiva_excel', @level2type=N'COLUMN',@level2name=N'sgd_masiva_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'bandera que indica si fue radicada' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_masiva_excel', @level2type=N'COLUMN',@level2name=N'sgd_masiva_radicada'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numeracion intervalo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_masiva_excel', @level2type=N'COLUMN',@level2name=N'sgd_masiva_intervalo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero inicial' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_masiva_excel', @level2type=N'COLUMN',@level2name=N'sgd_masiva_rangoini'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero final' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_masiva_excel', @level2type=N'COLUMN',@level2name=N'sgd_masiva_rangofin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_men_menu', @level2type=N'COLUMN',@level2name=N'sgd_men_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre del menu' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_men_menu', @level2type=N'COLUMN',@level2name=N'sgd_men_nombre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numeor del permiso' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_men_menu', @level2type=N'COLUMN',@level2name=N'sgd_men_perm'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'valor del permiso' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_men_menu', @level2type=N'COLUMN',@level2name=N'sgd_men_permvalor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'categoria donde aparece' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_men_menu', @level2type=N'COLUMN',@level2name=N'sgd_men_categoria'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'url de acceso al modulo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_men_menu', @level2type=N'COLUMN',@level2name=N'sgd_men_url'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_menv_metodos', @level2type=N'COLUMN',@level2name=N'sgd_menv_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'metodo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_menv_metodos', @level2type=N'COLUMN',@level2name=N'tipo_metodo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'campo de la table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_menv_metodos', @level2type=N'COLUMN',@level2name=N'query_campo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mexp_metadataexp', @level2type=N'COLUMN',@level2name=N'sgd_mexp_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de la serie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mexp_metadataexp', @level2type=N'COLUMN',@level2name=N'sgd_srd_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de la subserie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mexp_metadataexp', @level2type=N'COLUMN',@level2name=N'sgd_sbrd_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codig de la dependencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mexp_metadataexp', @level2type=N'COLUMN',@level2name=N'depe_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mexp_metadataexp', @level2type=N'COLUMN',@level2name=N'sgd_mexp_nombre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'estado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mexp_metadataexp', @level2type=N'COLUMN',@level2name=N'sgd_mexp_estado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'habilitado para busqueda' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mexp_metadataexp', @level2type=N'COLUMN',@level2name=N'sgd_mexp_busq'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mexp_metadataexp', @level2type=N'COLUMN',@level2name=N'sgd_mexp_desc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mod_modules', @level2type=N'COLUMN',@level2name=N'sgd_mod_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'permiso con que se identifica al modulo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mod_modules', @level2type=N'COLUMN',@level2name=N'sgd_mod_modulo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'maximo valor que alcanza' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mod_modules', @level2type=N'COLUMN',@level2name=N'sgd_mod_valmax'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'de orden de menor a mayor los titulos para los combobox separados por ; ejemplo   puede radicar anexos;puede radicar' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mod_modules', @level2type=N'COLUMN',@level2name=N'sgd_mod_detalles'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'EL path se requiere si el  script aparece en el menu' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mod_modules', @level2type=N'COLUMN',@level2name=N'sgd_mod_path'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Titulo para el  menu' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mod_modules', @level2type=N'COLUMN',@level2name=N'sgd_mod_titulo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre visible en el menu' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mod_modules', @level2type=N'COLUMN',@level2name=N'sgd_mod_menu'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'estado 1 activo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mod_modules', @level2type=N'COLUMN',@level2name=N'sgd_mod_estado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mod_values', @level2type=N'COLUMN',@level2name=N'id_mod_value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo del modulo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mod_values', @level2type=N'COLUMN',@level2name=N'sgd_mod_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'valor que asume' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mod_values', @level2type=N'COLUMN',@level2name=N'sgd_mod_value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion del valor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mod_values', @level2type=N'COLUMN',@level2name=N'sgd_mod_descrip'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mot_motivos', @level2type=N'COLUMN',@level2name=N'sgd_mot_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion del motivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mot_motivos', @level2type=N'COLUMN',@level2name=N'sgd_mot_descrp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Este campo define a que aplicacion pertence el motivo.0. des archivo.1. desanulacion. 2.Paz y Salvo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mot_motivos', @level2type=N'COLUMN',@level2name=N'sgd_mot_tpaplica'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo peso' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mpes_mddpeso', @level2type=N'COLUMN',@level2name=N'sgd_mpes_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mpes_mddpeso', @level2type=N'COLUMN',@level2name=N'sgd_mpes_descrip'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo matriz' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mrd_matrird', @level2type=N'COLUMN',@level2name=N'sgd_mrd_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de la dependencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mrd_matrird', @level2type=N'COLUMN',@level2name=N'depe_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de la serie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mrd_matrird', @level2type=N'COLUMN',@level2name=N'sgd_srd_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de la subserie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mrd_matrird', @level2type=N'COLUMN',@level2name=N'sgd_sbrd_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo del tipo documental' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mrd_matrird', @level2type=N'COLUMN',@level2name=N'sgd_tpr_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tipo de soporte' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mrd_matrird', @level2type=N'COLUMN',@level2name=N'soporte'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha inicial' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mrd_matrird', @level2type=N'COLUMN',@level2name=N'sgd_mrd_fechini'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha final ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mrd_matrird', @level2type=N'COLUMN',@level2name=N'sgd_mrd_fechfin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'estado de la relacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mrd_matrird', @level2type=N'COLUMN',@level2name=N'sgd_mrd_esta'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'indica si aplica a expedientes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mrd_matrird', @level2type=N'COLUMN',@level2name=N'sgd_mrd_esta_exp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'columna de presentar en radicacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mrd_matrird', @level2type=N'COLUMN',@level2name=N'sgd_mrd_esta_radi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'version serie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mrd_matrird', @level2type=N'COLUMN',@level2name=N'sgd_srd_version'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'version sub serie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mrd_matrird', @level2type=N'COLUMN',@level2name=N'sgd_sbrd_version'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo versionamiento trd' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_mrd_matrird', @level2type=N'COLUMN',@level2name=N'sgd_vmrd_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha festivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_noh_nohabiles', @level2type=N'COLUMN',@level2name=N'noh_fecha'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_oem_oempresas', @level2type=N'COLUMN',@level2name=N'sgd_oem_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo tipo de identificacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_oem_oempresas', @level2type=N'COLUMN',@level2name=N'tdid_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre empresa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_oem_oempresas', @level2type=N'COLUMN',@level2name=N'sgd_oem_oempresa'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre representante legal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_oem_oempresas', @level2type=N'COLUMN',@level2name=N'sgd_oem_rep_legal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_oem_oempresas', @level2type=N'COLUMN',@level2name=N'sgd_oem_nit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'sigla empresa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_oem_oempresas', @level2type=N'COLUMN',@level2name=N'sgd_oem_sigla'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo municipio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_oem_oempresas', @level2type=N'COLUMN',@level2name=N'muni_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo departamento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_oem_oempresas', @level2type=N'COLUMN',@level2name=N'dpto_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'direccion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_oem_oempresas', @level2type=N'COLUMN',@level2name=N'sgd_oem_direccion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'telefono' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_oem_oempresas', @level2type=N'COLUMN',@level2name=N'sgd_oem_telefono'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'id continente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_oem_oempresas', @level2type=N'COLUMN',@level2name=N'id_cont'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'id pais' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_oem_oempresas', @level2type=N'COLUMN',@level2name=N'id_pais'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'email' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_oem_oempresas', @level2type=N'COLUMN',@level2name=N'sgd_oem_email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'estado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_oem_oempresas', @level2type=N'COLUMN',@level2name=N'sgd_oem_estado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tipo campo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_oem_oempresas', @level2type=N'COLUMN',@level2name=N'sgd_tipo_camp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'id centro poblado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_oem_oempresas', @level2type=N'COLUMN',@level2name=N'id_centropoblado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_panu_peranulados', @level2type=N'COLUMN',@level2name=N'sgd_panu_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion permiso' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_panu_peranulados', @level2type=N'COLUMN',@level2name=N'sgd_panu_desc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion del parametro' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_parametro', @level2type=N'COLUMN',@level2name=N'param_nomb'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo parametro' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_parametro', @level2type=N'COLUMN',@level2name=N'param_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'valor parametro' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_parametro', @level2type=N'COLUMN',@level2name=N'param_valor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_parexp_paramexpediente', @level2type=N'COLUMN',@level2name=N'sgd_parexp_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_parexp_paramexpediente', @level2type=N'COLUMN',@level2name=N'depe_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tabla donde buscar parametro' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_parexp_paramexpediente', @level2type=N'COLUMN',@level2name=N'sgd_parexp_tabla'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'etiqueta del parametro' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_parexp_paramexpediente', @level2type=N'COLUMN',@level2name=N'sgd_parexp_etiqueta'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'orden' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_parexp_paramexpediente', @level2type=N'COLUMN',@level2name=N'sgd_parexp_orden'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_pexp_procexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_pexp_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion del proceso' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_pexp_procexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_pexp_descrip'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'si aplica terminos toma los de la subserie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_pexp_procexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_pexp_terminos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de la serie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_pexp_procexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_srd_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de la subserie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_pexp_procexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_sbrd_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'avance automatico' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_pexp_procexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_pexp_automatico'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'bandera que indica si pertenece a un flujo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_pexp_procexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_pexp_tieneflujo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de matriz de relacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_rdf_retdocf', @level2type=N'COLUMN',@level2name=N'sgd_mrd_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero del radicado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_rdf_retdocf', @level2type=N'COLUMN',@level2name=N'radi_nume_radi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de la dependencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_rdf_retdocf', @level2type=N'COLUMN',@level2name=N'depe_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo del usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_rdf_retdocf', @level2type=N'COLUMN',@level2name=N'usua_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numeor de documento del usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_rdf_retdocf', @level2type=N'COLUMN',@level2name=N'usua_doc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de asignacion de relacion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_rdf_retdocf', @level2type=N'COLUMN',@level2name=N'sgd_rdf_fech'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de agrupacion de envio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo empresa de envio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_fenv_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de creacion del envio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_fech'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de radicado de salida' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'radi_nume_sal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre de destinatario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_destino'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'telefono' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_telefono'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'email' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_mail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'peso del envio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_peso'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'valor asignado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_valor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'bandera que indica si se va por correo certificado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_certificado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'estado del envio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_estado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'documento del usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'usua_doc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre del destinatario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_nombre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'bandera que indica si es el mismo destinatario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_rem_destino'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de la tabla sgd_dir_drecciones' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_dir_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de planilla asignado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_planilla'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de envio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_fech_sal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de la dependencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'depe_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tipo de destinatario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_dir_tipo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero inicial del radicado que agrupa el envio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'radi_nume_grupo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'direccion del envio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_dir'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo del departamento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_depto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo del municipio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_mpio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'telefono 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_tel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'cantidad de hojas' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_cantidad'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tipo envio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_tipo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'observaciones del envio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_observa'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de devolucion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_deve_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha devolucion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_deve_fech'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'valor total envio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_valortotal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'valor de alistamiento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_valistamiento'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'valor del descuento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_vdescuento'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'valor adicional cobrado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_vadicional'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia que genera' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_depe_genera'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo pais' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_pais'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de planilla de sipost' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_sipostplan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de guia asignado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'sgd_renv_guia'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_renv_regenvio', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'consecutivo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_rol_roles', @level2type=N'COLUMN',@level2name=N'sgd_rol_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nombre del rol' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_rol_roles', @level2type=N'COLUMN',@level2name=N'sgd_rol_nombre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'estado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_rol_roles', @level2type=N'COLUMN',@level2name=N'sgd_rol_estado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo serie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sbrd_subserierd', @level2type=N'COLUMN',@level2name=N'sgd_srd_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de la subserie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sbrd_subserierd', @level2type=N'COLUMN',@level2name=N'sgd_sbrd_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'descripcion de la subserie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sbrd_subserierd', @level2type=N'COLUMN',@level2name=N'sgd_sbrd_descrip'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha inicio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sbrd_subserierd', @level2type=N'COLUMN',@level2name=N'sgd_sbrd_fechini'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha final' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sbrd_subserierd', @level2type=N'COLUMN',@level2name=N'sgd_sbrd_fechfin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tiempo de retencion archivo general' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sbrd_subserierd', @level2type=N'COLUMN',@level2name=N'sgd_sbrd_tiemag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tiempo de retencion archivo central' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sbrd_subserierd', @level2type=N'COLUMN',@level2name=N'sgd_sbrd_tiemac'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'disposicion final' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sbrd_subserierd', @level2type=N'COLUMN',@level2name=N'sgd_sbrd_dispfin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'soportes que maneja' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sbrd_subserierd', @level2type=N'COLUMN',@level2name=N'sgd_sbrd_soporte'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'procedimiento habilitado para cuando termina los tiempos' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sbrd_subserierd', @level2type=N'COLUMN',@level2name=N'sgd_sbrd_procedi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'version de la serie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sbrd_subserierd', @level2type=N'COLUMN',@level2name=N'sgd_srd_version'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'version de la subserie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sbrd_subserierd', @level2type=N'COLUMN',@level2name=N'sgd_sbrd_version'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de expediente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sexp_secexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_exp_numero'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de serie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sexp_secexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_srd_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de subserie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sexp_secexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_sbrd_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero de secuencia del expediente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sexp_secexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_sexp_secuencia'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo dependencia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sexp_secexpedientes', @level2type=N'COLUMN',@level2name=N'depe_codi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'documento usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sexp_secexpedientes', @level2type=N'COLUMN',@level2name=N'usua_doc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha creacion del expediente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sexp_secexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_sexp_fech'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo del flujo del expediente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sexp_secexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_fexp_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'vigencia del expediente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sexp_secexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_sexp_ano'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'documento del usuario responsable' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sexp_secexpedientes', @level2type=N'COLUMN',@level2name=N'usua_doc_responsable'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'etiqueta 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sexp_secexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_sexp_parexp1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'etiqueta 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sexp_secexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_sexp_parexp2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'etiqueta 3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sexp_secexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_sexp_parexp3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'etiqueta 4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sexp_secexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_sexp_parexp4'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'etiqueta 5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sexp_secexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_sexp_parexp5'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de parametro que aplica' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sexp_secexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_pexp_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de archivo del expediente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sexp_secexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_exp_fech_arch'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo del flujo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sexp_secexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_fld_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha de cambio del flujo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sexp_secexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_exp_fechflujoant'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo de matriz de relacion del radicado inicial' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sexp_secexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_mrd_codigo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'subexpediente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sexp_secexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_exp_subexpediente'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'bandera que indica si es privado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sexp_secexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_exp_privado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'fecha final del expediente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sexp_secexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_sexp_fechafin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'banderamqeu indica si tiene metadatos asociados' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sgd_sexp_secexpedientes', @level2type=N'COLUMN',@level2name=N'sgd_sexp_metadato'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'codigo usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'usuario', @level2type=N'COLUMN',@level2name=N'usua_codi'
GO
USE [master]
GO
ALTER DATABASE [PRU_GdOrfeo] SET  READ_WRITE 
GO
